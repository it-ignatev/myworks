object db_settings: Tdb_settings
  Left = 578
  Top = 233
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #1059#1095#1077#1090' '#1087#1083#1072#1090#1077#1078#1077#1081' - '#1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1073#1072#1079#1099
  ClientHeight = 208
  ClientWidth = 353
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Segoe UI Semibold'
  Font.Style = [fsBold]
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 17
  object exit1: TsButton
    Left = 8
    Top = 160
    Width = 161
    Height = 41
    Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    TabOrder = 0
    TabStop = False
    OnClick = exit1Click
    SkinData.SkinSection = 'BUTTON'
  end
  object grp1: TsGroupBox
    Left = 8
    Top = 8
    Width = 337
    Height = 145
    Caption = #1059#1087#1088#1072#1074#1083#1077#1085#1080#1077' '#1101#1083#1077#1082#1090#1088#1080#1095#1077#1089#1090#1074#1086#1084
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Segoe UI Semibold'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    SkinData.SkinSection = 'GROUPBOX'
    object lbl1: TsLabel
      Left = 8
      Top = 32
      Width = 143
      Height = 20
      Caption = #1058#1072#1088#1080#1092' '#1089#1074#1077#1090#1072' ('#1044#1077#1085#1100'): '
      ParentFont = False
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Segoe UI Semibold'
      Font.Style = [fsBold]
    end
    object lbl2: TsLabel
      Left = 8
      Top = 64
      Width = 144
      Height = 20
      Caption = #1058#1072#1088#1080#1092' '#1089#1074#1077#1090#1072' ('#1053#1086#1095#1100'): '
      ParentFont = False
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Segoe UI Semibold'
      Font.Style = [fsBold]
    end
    object lbl3: TsLabel
      Left = 8
      Top = 96
      Width = 134
      Height = 20
      Caption = #1050#1086#1083'-'#1074#1086' '#1089#1095#1077#1090#1095#1080#1082#1086#1074': '
      ParentFont = False
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Segoe UI Semibold'
      Font.Style = [fsBold]
    end
    object t_power_d: TsDecimalSpinEdit
      Left = 152
      Top = 32
      Width = 177
      Height = 28
      TabStop = False
      Color = 15195590
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Segoe UI Semibold'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
    end
    object t_power_n: TsDecimalSpinEdit
      Left = 152
      Top = 64
      Width = 177
      Height = 28
      TabStop = False
      Color = 15195590
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Segoe UI Semibold'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
    end
    object counters: TsSpinEdit
      Left = 152
      Top = 96
      Width = 177
      Height = 28
      Color = 15195590
      Enabled = False
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Segoe UI Semibold'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      MaxValue = 0
      MinValue = 0
      Value = 0
    end
  end
  object cancel: TsButton
    Left = 184
    Top = 159
    Width = 161
    Height = 41
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 2
    TabStop = False
    OnClick = cancelClick
    SkinData.SkinSection = 'BUTTON'
  end
end
