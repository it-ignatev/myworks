object pay_power: Tpay_power
  Left = 1030
  Top = 139
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #1059#1095#1077#1090' '#1087#1083#1072#1090#1077#1078#1077#1081' - '#1057#1074#1077#1090
  ClientHeight = 496
  ClientWidth = 369
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Segoe UI Semibold'
  Font.Style = [fsBold]
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 17
  object add_but: TsButton
    Left = 160
    Top = 352
    Width = 201
    Height = 41
    Caption = #1057#1076#1077#1083#1072#1090#1100' '#1091#1095#1077#1090
    TabOrder = 0
    TabStop = False
    OnClick = add_butClick
    SkinData.SkinSection = 'BUTTON'
  end
  object lst1: TsListBox
    Left = 8
    Top = 8
    Width = 145
    Height = 337
    TabStop = False
    Color = 15195590
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Segoe UI Semibold'
    Font.Style = [fsBold]
    ItemHeight = 16
    Items.Strings = (
      #1055#1091#1089#1090#1086)
    ParentFont = False
    TabOrder = 1
    OnClick = lst1Click
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'Tahoma'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
    SkinData.SkinSection = 'EDIT'
  end
  object interv: TsButton
    Left = 8
    Top = 352
    Width = 145
    Height = 41
    Caption = #1055#1086#1089#1084#1086#1090#1088#1077#1090#1100' '#1086#1090#1095#1077#1090#1099
    TabOrder = 2
    TabStop = False
    OnClick = intervClick
    SkinData.SkinSection = 'BUTTON'
  end
  object mmo1: TsMemo
    Left = 160
    Top = 8
    Width = 201
    Height = 337
    Color = 15195590
    Enabled = False
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Segoe UI Semibold'
    Font.Style = [fsBold]
    Lines.Strings = (
      #1042#1099#1073#1077#1088#1080#1090#1077' '#1076#1072#1090#1091' !')
    ParentFont = False
    TabOrder = 3
    Text = #1042#1099#1073#1077#1088#1080#1090#1077' '#1076#1072#1090#1091' !'
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'Tahoma'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
    SkinData.SkinSection = 'EDIT'
  end
  object but_edit: TsButton
    Left = 8
    Top = 400
    Width = 145
    Height = 41
    Caption = #1048#1079#1084#1077#1085#1080#1090#1100
    TabOrder = 4
    TabStop = False
    OnClick = but_editClick
    SkinData.SkinSection = 'BUTTON'
  end
  object but_del: TsButton
    Left = 160
    Top = 400
    Width = 201
    Height = 41
    Caption = #1059#1076#1072#1083#1080#1090#1100
    TabOrder = 5
    TabStop = False
    OnClick = but_delClick
    SkinData.SkinSection = 'BUTTON'
  end
  object exit: TsButton
    Left = 8
    Top = 448
    Width = 353
    Height = 41
    Caption = #1054#1073#1088#1072#1090#1085#1086
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Segoe UI Semibold'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 6
    TabStop = False
    OnClick = exitClick
    SkinData.SkinSection = 'BUTTON'
  end
end
