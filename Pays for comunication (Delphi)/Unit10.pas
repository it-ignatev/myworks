unit Unit10;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sButton, Mask, sMaskEdit, sCustomComboEdit, sTooledit,
  sEdit, sSpinEdit, sLabel;

type
  Tpay_water_add = class(TForm)
    lbl2: TsLabel;
    lbl4: TsLabel;
    cub_1: TsDecimalSpinEdit;
    cub_2: TsDecimalSpinEdit;
    lbl3: TsLabel;
    date: TsDateEdit;
    but_water_add_but: TsButton;
    but_water_back: TsButton;
    cub_1_m: TsEdit;
    cub_2_m: TsEdit;
    cub_result: TsEdit;
    kanal_1: TsEdit;
    kanal_2: TsEdit;
    kanal_result: TsEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  pay_water_add: Tpay_water_add;

implementation

{$R *.dfm}

end.
