unit Unit6;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sEdit, sSpinEdit, sLabel, sButton, sComboBox,
  sGroupBox;

type
  Tdb_settings = class(TForm)
    lbl1: TsLabel;
    lbl2: TsLabel;
    exit1: TsButton;
    lbl3: TsLabel;
    counters: TsSpinEdit;
    t_power_d: TsDecimalSpinEdit;
    t_power_n: TsDecimalSpinEdit;
    grp1: TsGroupBox;
    cancel: TsButton;
    procedure FormCreate(Sender: TObject);
    procedure exit1Click(Sender: TObject);
    procedure cancelClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  db_settings: Tdb_settings;

implementation

uses Unit1;

{$R *.dfm}

type
  Tdb = record
    Name : string[200];
    n1 : string[200];
    n2 : string[200];
    n3 : string[200];
    n4 : string[200];
    n5 : string[200];
    n6 : string[200];
    n7 : string[200];
    n8 : string[200];
    n9 : string[200];
    n10 : string[200];
    n11 : string[200];
end;

procedure Tdb_settings.FormCreate(Sender: TObject);
var
db: Tdb;
F : file of Tdb;
begin
    AssignFile(F,StringReplace(Login.room.Items.Strings[Login.room.ItemIndex] + '.dbp',' ','',[RFReplaceall]));
    Reset(F);
    try
      Seek(F, 1);
      Read (F, db);
    finally
      CloseFile(F);
    end;

    t_power_d.Text := db.n1;
    t_power_n.Text := db.n2;
    counters.Text := db.n3;
end;

procedure Tdb_settings.exit1Click(Sender: TObject);
var
db: Tdb;
F : file of Tdb;
begin
  if (t_power_n.Value <> 0) and (t_power_d.Value <> 0) then
  begin
    AssignFile(F,StringReplace(Login.room.Items.Strings[Login.room.ItemIndex] + '.dbp',' ','',[RFReplaceall]));
    Reset(F);
    try
      Seek(F, 1);
      Read (F, db);
      db.n1 := FloatToStr(t_power_d.Value);
      db.n2 := FloatToStr(t_power_n.Value);
      db.n3 := IntToStr(counters.Value);
      Seek(F, 1);
      Write(F, db);
    finally
      CloseFile(F);
    end;
    Self.Close;
  end
  else ShowMessage('�������� ����� � ���-�� ��������� !');
end;

procedure Tdb_settings.cancelClick(Sender: TObject);
begin
  Self.Close;
end;

end.
