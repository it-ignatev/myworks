unit Unit4;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sListBox, sButton, sMemo, sGroupBox, ComCtrls,
  sRichEdit;

type
  Tpay_power = class(TForm)
    add_but: TsButton;
    lst1: TsListBox;
    interv: TsButton;
    mmo1: TsMemo;
    but_edit: TsButton;
    but_del: TsButton;
    exit: TsButton;
    procedure add_butClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure lst1Click(Sender: TObject);
    procedure intervClick(Sender: TObject);
    procedure exitClick(Sender: TObject);
    procedure but_delClick(Sender: TObject);
    procedure but_editClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    last: Integer;
    all: array[0..200] of string;
    procedure pays();
  end;

var
  pay_power: Tpay_power;
  
implementation

uses Unit5, Unit1, Unit7, Unit8;

{$R *.dfm}

type
  Tdb = record
    Name : string[200];
    n1 : string[200];
    n2 : string[200];
    n3 : string[200];
    n4 : string[200];
    n5 : string[200];
    n6 : string[200];
    n7 : string[200];
    n8 : string[200];
    n9 : string[200];
    n10 : string[200];
    n11 : string[200];
end;

procedure Tpay_power.add_butClick(Sender: TObject);
begin
  Application.CreateForm(Tpay_power_add, pay_power_add);
  pay_power_add.ShowModal;
end;

procedure Tpay_power.pays();
var
db: Tdb;
F : file of Tdb;
i: integer;
begin
  AssignFile(F,StringReplace(Login.room.Items.Strings[Login.room.ItemIndex] + '.dbp',' ','',[RFReplaceall]));
  Reset(F);
  try
    i := 0;
    lst1.Items.Clear;
    while not Eof(F) do
    begin
    Read (F, db);
      if db.Name = 'Power' then
      begin
        i := i + 1;
        lst1.Items.Add(db.n3);
        all[i] := inttostr(FilePos(F));
      end;
    end;
  finally
    CloseFile(F);
  end;
  last := i;
end;

procedure Tpay_power.FormCreate(Sender: TObject);
begin
  pays();
end;

procedure Tpay_power.lst1Click(Sender: TObject);
var
db: Tdb;
F : file of Tdb;
i : Integer;
rubl: string;
begin
 if lst1.ItemIndex <> -1 then
 begin
    AssignFile(F,StringReplace(Login.room.Items.Strings[Login.room.ItemIndex] + '.dbp',' ','',[RFReplaceall]));
    Reset(F);
    try
      i := 0;
      i := lst1.ItemIndex + 1;
      Seek(F, StrToInt(all[i]) - 1);
      Read (F, db);
    finally
      CloseFile(F);
    end;

    mmo1.Lines.Clear;
    mmo1.Lines.Add('����: ' + db.n3);
    mmo1.Lines.Add('�������: -');
    mmo1.Lines.Add(' ');
    mmo1.Lines.Add('������: ');
    mmo1.Lines.Add('����: ' + db.n4 + ' ��\�');
    mmo1.Lines.Add('����: ' + db.n5 + ' ��\�');
    mmo1.Lines.Add(' ');
    mmo1.Lines.Add('������� ����: ');
    mmo1.Lines.Add('�����: ' + db.n1 + ' ��\�');
    mmo1.Lines.Add('�����: ' + db.n9 + ' ��\�');
    mmo1.Lines.Add(' ');
    mmo1.Lines.Add('������� ����: ');
    mmo1.Lines.Add('�����: ' + db.n2 + ' ��\�');
    mmo1.Lines.Add('�����: ' + db.n10 + ' ��\�');
    mmo1.Lines.Add(' ');
    mmo1.Lines.Add('������: ');
    if (StrToCurr(db.n6) <= StrToCurr('4')) and (StrToCurr(db.n6) >= StrToCurr('2'))then rubl := ' �����' else rubl := ' ������';
    mmo1.Lines.Add('����: ' + db.n6 + rubl);
    if (StrToCurr(db.n7) <= StrToCurr('4')) and (StrToCurr(db.n7) >= StrToCurr('2'))then rubl := ' �����' else rubl := ' ������';
    mmo1.Lines.Add('����: ' + db.n7 + rubl);
    if (StrToCurr(db.n8) <= StrToCurr('4')) and (StrToCurr(db.n8) >= StrToCurr('2'))then rubl := ' �����' else rubl := ' ������';
    mmo1.Lines.Add('����: ' + db.n8 + rubl);
    end;
end;

procedure Tpay_power.intervClick(Sender: TObject);
begin
  Application.CreateForm(Tpay_power_interval, pay_power_interval);
  pay_power_interval.showmodal;
end;

procedure Tpay_power.exitClick(Sender: TObject);
begin
  self.Close;
end;

procedure Tpay_power.but_delClick(Sender: TObject);
var
db: Tdb;
F : file of Tdb;
begin
  if lst1.ItemIndex <> -1 then
  begin
    if messageBox(Handle,PChar('�� ������������� ������ ������� ' + lst1.Items.Strings[lst1.itemindex] + ' ?'),'��������',mb_YesNo)= mrYes then
    begin
      db.Name := 'Empty';
      AssignFile(F,StringReplace(Login.room.Items.Strings[Login.room.ItemIndex] + '.dbp',' ','',[RFReplaceall]));
      Reset(F);
      try
       Seek(F,StrToInt(all[lst1.itemindex + 1]) - 1);
       Write(F,db);
      finally
      CloseFile(F);
      end;
      pays();
    end;
  end
  else ShowMessage('�������� ���� !');
end;
procedure Tpay_power.but_editClick(Sender: TObject);
begin
  if lst1.ItemIndex <> -1 then
  begin
    Application.CreateForm(Tpay_power_edit, pay_power_edit);
    pay_power_edit.ShowModal;
  end
  else ShowMessage('�������� ���� !');
end;

end.
