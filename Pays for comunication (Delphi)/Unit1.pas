unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, sScrollBox, sFrameBar, Grids, sSkinManager, StdCtrls, sComboBox,
  sButton;

type
  TLogin = class(TForm)
    enter: TsButton;
    room: TsComboBox;
    create: TsButton;
    sknmngr1: TsSkinManager;
    exit: TsButton;
    procedure createClick(Sender: TObject);
    procedure enterClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure exitClick(Sender: TObject);
  private
    { Private declarations }
  public
    procedure readdbp;
    { Public declarations }
   
  end;

var
  Login: TLogin;

implementation

uses Unit2, Unit3;

{$R *.dfm}

procedure Tlogin.readdbp();
var
i: Integer;
SR:TSearchRec;
TS : TStringList;                            
begin
  Login.room.Items.Clear;
  If FindFirst('*.dbp',faAnyFile,SR)=0 Then
  Repeat
    TS := TStringList.Create;
    TS.Delimiter := '.';
    TS.DelimitedText := StringReplace(SR.Name,' ','',[RFReplaceall]);
    Login.room.Items.Add(TS.Strings[0]);
  Until FindNext(SR)<>0;
  FindClose(SR);
end;

procedure TLogin.createClick(Sender: TObject);
begin
Application.CreateForm(TCreation, Creation);
Self.Hide;
Creation.show;
end;

procedure TLogin.enterClick(Sender: TObject);
begin
  if room.ItemIndex <> -1 then
  begin
    Application.CreateForm(Tchose_tab, chose_tab);
    chose_tab.Show;
    Self.Hide;
  end
  else ShowMessage('�������� ���� ��� �������� �� !');
end;

procedure TLogin.FormCreate(Sender: TObject);
begin
  readdbp();
end;

procedure TLogin.exitClick(Sender: TObject);
begin
  Application.Terminate;
end;

end.
