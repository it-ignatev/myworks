unit Unit5;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sButton, sEdit, sSpinEdit, sComboBox, Mask, sMaskEdit,
  sCustomComboEdit, sTooledit, sLabel;

type
  Tpay_power_add = class(TForm)
    counter: TsComboBox;
    pay_add_but: TsButton;
    date: TsDateEdit;
    back: TsButton;
    lbl1: TsLabel;
    lbl2: TsLabel;
    lbl3: TsLabel;
    lbl4: TsLabel;
    kv_m: TsEdit;
    kv2_m: TsEdit;
    kv: TsDecimalSpinEdit;
    kv2: TsDecimalSpinEdit;
    result: TsEdit;
    procedure backClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure pay_add_butClick(Sender: TObject);
    procedure kvChange(Sender: TObject);
    procedure kv2Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  pay_power_add: Tpay_power_add;
  tariv_d: String;
  tariv_n: String;
  last_d: string;
  last_n: string;
  cur2: string;
  cur1: string;

implementation

uses Unit1, Unit4;

{$R *.dfm}

type
  Tdb = record
    Name : string[200];
    n1 : string[200];
    n2 : string[200];
    n3 : string[200];
    n4 : string[200];
    n5 : string[200];
    n6 : string[200];
    n7 : string[200];
    n8 : string[200];
    n9 : string[200];
    n10 : string[200];
    n11 : string[200];
end;

procedure Tpay_power_add.kvChange(Sender: TObject);
var rubl: string;
begin
  cur1 := CurrToStr((FloatToCurr(kv.Value) - StrToCurr(last_d)) * StrToCurr(tariv_d));

  if (StrToCurr(cur1) <= StrToCurr('4')) and (StrToCurr(cur1) >= StrToCurr('2'))then rubl := ' �����' else rubl := ' ������';

  kv_m.Text := cur1 + rubl;

  result.Text := '����: ' + CurrToStr(StrToCurr(cur2) + StrToCurr(cur1)) + rubl;
end;

procedure Tpay_power_add.backClick(Sender: TObject);
begin
  Self.Close;
end;

procedure Tpay_power_add.FormCreate(Sender: TObject);
var
db: Tdb;
F : file of Tdb;
temp: Integer;
begin
    cur1 := '0';
    cur2 := '0';
    AssignFile(F,StringReplace(Login.room.Items.Strings[Login.room.ItemIndex] + '.dbp',' ','',[RFReplaceall]));
    Reset(F);
    try
      Seek(F, 1);
      Read (F, db);
      tariv_d := db.n1;
      tariv_n := db.n2;
      if pay_power.last = 0 then
        begin
        last_d := '0';
        last_n := '0';
        end
      else
        begin
        temp := StrToInt(pay_power.all[pay_power.last]);
        Seek(F, temp - 1);
        Read (F, db);
        last_d := db.n1;
        last_n := db.n2;            
        end;
    finally
      CloseFile(F);
    end;
end;

procedure Tpay_power_add.pay_add_butClick(Sender: TObject);
var
db : array[1..50] of Tdb;
f : file of Tdb;
len: Integer;
begin
  if (kv.Value <> 0 ) and (kv2.Value <> 0)then
  begin
    if (kv_m.Text = '0') or (kv2_m.Text = '0') then
    begin
    ShowMessage('��������� ���� ������� �� ����� !');
    Exit;
    end;
    
    len := 0;
    AssignFile(F,StringReplace(Login.room.Items.Strings[Login.room.ItemIndex] + '.dbp',' ','',[RFReplaceall]));
    Reset(F);
    try
      len := FileSize(F) + 1;
    finally
      CloseFile(F);
    end;
    
      db[len].Name := 'Power';
      db[len].n1 := FloatToStr(kv.Value);
      db[len].n2 := FloatToStr(kv2.Value);
      db[len].n3 := date.Text;
      db[len].n4 := tariv_d;
      db[len].n5 := tariv_n;
      db[len].n6 := cur1;
      db[len].n7 := cur2;
      db[len].n8 := CurrToStr(StrToCurr(cur2) + StrToCurr(cur1));
      db[len].n9 := CurrToStr(FloatToCurr(kv.Value) - StrToCurr(last_d));
      db[len].n10 := CurrToStr(FloatToCurr(kv2.Value) - StrToCurr(last_n));

      AssignFile(F,StringReplace(Login.room.Items.Strings[Login.room.ItemIndex] + '.dbp',' ','',[RFReplaceall]));
      Reset(f);
      try
          Seek(F, len - 1);
          Write (F, db[len]);
      finally
        CloseFile(F);
      end;
      pay_power.pays();
      self.Close;
  end
  else ShowMessage('������� ��\� � ���� !');
  end;
procedure Tpay_power_add.kv2Change(Sender: TObject);
var rubl: string;
begin
  cur2 := CurrToStr((FloatToCurr(kv2.Value) - StrToCurr(last_n)) * StrToCurr(tariv_n));

  if (StrToCurr(cur2) <= StrToCurr('4')) and (StrToCurr(cur2) >= StrToCurr('2'))then rubl := ' �����' else rubl := ' ������';

  kv2_m.Text := cur2 + rubl;

  result.Text := '����: ' + CurrToStr(StrToCurr(cur2) + StrToCurr(cur1)) + rubl;
end;

end.
