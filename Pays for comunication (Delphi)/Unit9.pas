unit Unit9;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sButton, sMemo, sListBox;

type
  Tpay_water = class(TForm)
    lst1: TsListBox;
    mmo1: TsMemo;
    but__water_interv: TsButton;
    but_water_but: TsButton;
    but_edit: TsButton;
    but_del: TsButton;
    but_water_exit: TsButton;
    procedure but_water_exitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    last: Integer;
    all: array[0..200] of string;
    procedure pays();
  end;

var
  pay_water: Tpay_water;

implementation

uses Unit1;

{$R *.dfm}

type
  Tdb = record
    Name : string[200];
    n1 : string[200];
    n2 : string[200];
    n3 : string[200];
    n4 : string[200];
    n5 : string[200];
    n6 : string[200];
    n7 : string[200];
    n8 : string[200];
    n9 : string[200];
    n10 : string[200];
    n11 : string[200];
end;

procedure Tpay_water.pays();
var
db: Tdb;
F : file of Tdb;
i: integer;
begin
  AssignFile(F,StringReplace(Login.room.Items.Strings[Login.room.ItemIndex] + '.dbp',' ','',[RFReplaceall]));
  Reset(F);
  try
    i := 0;
    lst1.Items.Clear;
    while not Eof(F) do
    begin
    Read (F, db);
      if db.Name = 'Water' then
      begin
        i := i + 1;
        lst1.Items.Add(db.n3);
        all[i] := inttostr(FilePos(F));
      end;
    end;
  finally
    CloseFile(F);
  end;
  last := i;
end;

procedure Tpay_water.but_water_exitClick(Sender: TObject);
begin
  Self.Close;
end;

procedure Tpay_water.FormCreate(Sender: TObject);
begin
  pays();
end;

end.
