unit Unit3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sButton, sMemo;

type
  Tchose_tab = class(TForm)
    power_but: TsButton;
    water_but: TsButton;
    hot_but: TsButton;
    kanaliz_but: TsButton;
    settting: TsButton;
    exit: TsButton;
    mmo1: TsMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure power_butClick(Sender: TObject);
    procedure exitClick(Sender: TObject);
    procedure setttingClick(Sender: TObject);
    procedure water_butClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  chose_tab: Tchose_tab;

implementation

uses Unit1, Unit4, Unit6, Unit9;

{$R *.dfm}

type
  Tdb = record
    Name : string[200];
    n1 : string[200];
    n2 : string[200];
    n3 : string[200];
    n4 : string[200];
    n5 : string[200];
    n6 : string[200];
    n7 : string[200];
    n8 : string[200];
    n9 : string[200];
    n10 : string[200];
    n11 : string[200];
end;

procedure Tchose_tab.FormCreate(Sender: TObject);
var
db: Tdb;
F : file of Tdb;
begin
  if FileExists(ExtractFileDir(Application.ExeName) + '\' + Login.room.Items.Strings[Login.room.ItemIndex] + '.dbp') then
  begin
    AssignFile(F,StringReplace(Login.room.Items.Strings[Login.room.ItemIndex] + '.dbp',' ','',[RFReplaceall]));
    Reset(F);
    try
      Seek(F, 0);
      Read (F, db);
    finally
      CloseFile(F);
    end;
    
    if db.n1 = 'True' then power_but.Enabled := True else power_but.Enabled := False;
    if db.n2 = 'True' then water_but.Enabled := True else water_but.Enabled := False;
    if db.n3 = 'True' then hot_but.Enabled := True else hot_but.Enabled := False;
    if db.n4 = 'True' then kanaliz_but.Enabled := True else kanaliz_but.Enabled := False;
    mmo1.Lines.Clear;
    mmo1.Lines.Add('�������� ����: ' + StringReplace(Login.room.Items.Strings[Login.room.ItemIndex],' ','',[RFReplaceall]));
  end
  else
  begin
    ShowMessage('���� ���� ������� ��� ���������� !');
    Login.Show;
    Self.Close;
  end;
end;

procedure Tchose_tab.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  self.hide;
  Login.Show;
end;

procedure Tchose_tab.power_butClick(Sender: TObject);
begin
  Application.CreateForm(Tpay_power, pay_power);
  pay_power.ShowModal;
end;

procedure Tchose_tab.exitClick(Sender: TObject);
begin
  Self.Close;
  Login.Show;
  Login.readdbp;
end;

procedure Tchose_tab.setttingClick(Sender: TObject);
begin
  Application.CreateForm(Tdb_settings, db_settings);
  db_settings.ShowModal;
end;

procedure Tchose_tab.water_butClick(Sender: TObject);
begin
  Application.CreateForm(Tpay_water, pay_water);
  pay_water.ShowModal;
end;

end.
