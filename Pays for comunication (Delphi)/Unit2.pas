unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sButton, sCheckBox, sEdit, sLabel, sSpinEdit,
  sGroupBox;

type
  Tcreation = class(TForm)
    back: TsButton;
    name: TsEdit;
    power: TsCheckBox;
    water: TsCheckBox;
    hot: TsCheckBox;
    kanaliz: TsCheckBox;
    create_but: TsButton;
    lbl1: TsLabel;
    lbl2: TsLabel;
    power_t: TsDecimalSpinEdit;
    t_power_n: TsDecimalSpinEdit;
    grp1: TsGroupBox;
    sGroupBox1: TsGroupBox;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    t_XBC: TsDecimalSpinEdit;
    t_rBC: TsDecimalSpinEdit;
    procedure backClick(Sender: TObject);
    procedure nameClick(Sender: TObject);
    procedure create_butClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  creation: Tcreation;

implementation

uses Unit1;

{$R *.dfm}

type
  Tdb = record
    Name : string[200];
    n1 : string[200];
    n2 : string[200];
    n3 : string[200];
    n4 : string[200];
    n5 : string[200];
    n6 : string[200];
    n7 : string[200];
    n8 : string[200];
    n9 : string[200];
    n10 : string[200];
    n11 : string[200];
end;

procedure Tcreation.backClick(Sender: TObject);
begin
Self.Close;
Login.show;
end;

procedure Tcreation.nameClick(Sender: TObject);
begin
  name.Text := '';
end;

procedure Tcreation.create_butClick(Sender: TObject);
var
powercity : string;
watercity : string;
hotcity : string;
kanalizcity : string;
db : array[1..50] of Tdb;
f : file of Tdb;
begin
  if name.Text <> '' then
  begin
    if power.State = cbChecked then powercity := 'True' else powercity := 'False';
    if water.State = cbChecked then watercity := 'True' else watercity := 'False';
    if hot.State = cbChecked then hotcity := 'True' else hotcity := 'False';
    if kanaliz.State = cbChecked then kanalizcity := 'True' else kanalizcity := 'False';
    if (power_t.Value = 0) or (t_power_n.Value = 0) then
    begin
      ShowMessage('�������� ����� !');
      Exit;
    end;

    if (powercity = 'False') and (watercity = 'False') and (hotcity = 'False') and (kanalizcity = 'False') then
    begin
      ShowMessage('�������� ���� 1 ���� !');
    end
    else
    begin

      db[1].Name := 'Info';
      db[1].n1 := powercity;
      db[1].n2 := watercity;
      db[1].n3 := hotcity;
      db[1].n4 := kanalizcity;

      db[2].Name := 'Pay_Power';
      db[2].n1 := FloatToStr(power_t.value);
      db[2].n2 := FloatToStr(t_power_n.value);
      db[2].n3 := '1';

      db[3].Name := 'Pay_Water';
      db[3].n1 := FloatToStr(t_XBC.value);
      db[3].n2 := FloatToStr(t_rBC.value);
      db[3].n3 := '1';

      AssignFile(F,StringReplace(name.Text + '.dbp',' ','',[RFReplaceall]));
      Rewrite(F);
      try
          Write (F, db[1]);
          Write (F, db[2]);
          Write (F, db[3]);
      finally
        CloseFile(F);
      end;

      ShowMessage('���� ������� !');
      Self.Close;
      Login.Show;
      Login.readdbp;
    end;
    
  end
  else ShowMessage('������� ��� !');
end;

procedure Tcreation.FormCreate(Sender: TObject);
begin
  power_t.Value := 0.00;
  t_power_n.Value := 0.00;
  t_XBC.Value := 0.00;
  t_rBC.Value := 0.00;
end;

end.
