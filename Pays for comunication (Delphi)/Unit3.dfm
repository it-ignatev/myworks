object chose_tab: Tchose_tab
  Left = 1437
  Top = 115
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #1059#1095#1077#1090' '#1087#1083#1072#1090#1077#1078#1077#1081' - '#1042#1099#1073#1086#1088
  ClientHeight = 240
  ClientWidth = 376
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Segoe UI Semibold'
  Font.Style = [fsBold]
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 17
  object power_but: TsButton
    Left = 8
    Top = 48
    Width = 177
    Height = 57
    Caption = #1059#1095#1077#1090' '#1089#1074#1077#1090#1072
    Enabled = False
    TabOrder = 0
    TabStop = False
    OnClick = power_butClick
    SkinData.SkinSection = 'BUTTON'
  end
  object water_but: TsButton
    Left = 192
    Top = 48
    Width = 177
    Height = 57
    Caption = #1059#1095#1077#1090' '#1074#1086#1076#1099
    Enabled = False
    TabOrder = 1
    TabStop = False
    OnClick = water_butClick
    SkinData.SkinSection = 'BUTTON'
  end
  object hot_but: TsButton
    Left = 8
    Top = 112
    Width = 177
    Height = 57
    Caption = #1059#1095#1077#1090' '#1086#1090#1086#1087#1083#1077#1085#1080#1103
    Enabled = False
    TabOrder = 2
    TabStop = False
    SkinData.SkinSection = 'BUTTON'
  end
  object kanaliz_but: TsButton
    Left = 192
    Top = 112
    Width = 177
    Height = 57
    Caption = #1059#1095#1077#1090' '#1082#1072#1085#1072#1083#1080#1079#1072#1094#1080#1080
    Enabled = False
    TabOrder = 3
    TabStop = False
    SkinData.SkinSection = 'BUTTON'
  end
  object settting: TsButton
    Left = 191
    Top = 176
    Width = 177
    Height = 57
    Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080' '#1073#1072#1079#1099
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Segoe UI Semibold'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    TabStop = False
    OnClick = setttingClick
    SkinData.SkinSection = 'BUTTON'
  end
  object exit: TsButton
    Left = 8
    Top = 176
    Width = 177
    Height = 57
    Caption = #1042#1099#1093#1086#1076' '#1080#1079' '#1073#1072#1079#1099
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Segoe UI Semibold'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
    TabStop = False
    OnClick = exitClick
    SkinData.SkinSection = 'BUTTON'
  end
  object mmo1: TsMemo
    Left = 8
    Top = 8
    Width = 361
    Height = 33
    Color = 15195590
    Enabled = False
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'Segoe UI Semibold'
    Font.Style = [fsBold]
    Lines.Strings = (
      #1047#1072#1075#1088#1091#1079#1082#1072'........')
    ParentFont = False
    ReadOnly = True
    TabOrder = 6
    Text = #1047#1072#1075#1088#1091#1079#1082#1072'........'
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'Tahoma'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
    SkinData.SkinSection = 'EDIT'
  end
end
