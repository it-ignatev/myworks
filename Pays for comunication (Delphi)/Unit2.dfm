object creation: Tcreation
  Left = 1511
  Top = 529
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = #1059#1095#1077#1090' '#1087#1083#1072#1090#1077#1078#1077#1081' - '#1057#1086#1079#1076#1072#1085#1080#1077
  ClientHeight = 432
  ClientWidth = 313
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Segoe UI Semibold'
  Font.Style = [fsBold]
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 17
  object back: TsButton
    Left = 160
    Top = 384
    Width = 137
    Height = 41
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 0
    TabStop = False
    OnClick = backClick
    SkinData.SkinSection = 'BUTTON'
  end
  object name: TsEdit
    Left = 16
    Top = 8
    Width = 281
    Height = 21
    TabStop = False
    Color = 15195590
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Segoe UI Semibold'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Text = #1053#1072#1079#1074#1072#1085#1080#1077' '#1080#1083#1080' '#1040#1076#1088#1077#1089
    OnClick = nameClick
    SkinData.SkinSection = 'EDIT'
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'Tahoma'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
  end
  object power: TsCheckBox
    Left = 16
    Top = 40
    Width = 161
    Height = 23
    TabStop = False
    Caption = #1059#1095#1077#1090' '#1101#1083#1077#1082#1090#1088#1080#1095#1077#1089#1090#1074#1072' ?'
    TabOrder = 2
    SkinData.SkinSection = 'CHECKBOX'
    ImgChecked = 0
    ImgUnchecked = 0
  end
  object water: TsCheckBox
    Left = 16
    Top = 64
    Width = 104
    Height = 23
    TabStop = False
    Caption = #1059#1095#1077#1090' '#1074#1086#1076#1099' ?'
    TabOrder = 3
    SkinData.SkinSection = 'CHECKBOX'
    ImgChecked = 0
    ImgUnchecked = 0
  end
  object hot: TsCheckBox
    Left = 16
    Top = 88
    Width = 138
    Height = 23
    TabStop = False
    Caption = #1059#1095#1077#1090' '#1086#1090#1086#1087#1083#1077#1085#1080#1103' ?'
    TabOrder = 4
    SkinData.SkinSection = 'CHECKBOX'
    ImgChecked = 0
    ImgUnchecked = 0
  end
  object kanaliz: TsCheckBox
    Left = 16
    Top = 112
    Width = 152
    Height = 23
    TabStop = False
    Caption = #1059#1095#1077#1090' '#1082#1072#1085#1072#1083#1080#1079#1072#1094#1080#1080' ?'
    TabOrder = 5
    SkinData.SkinSection = 'CHECKBOX'
    ImgChecked = 0
    ImgUnchecked = 0
  end
  object create_but: TsButton
    Left = 16
    Top = 384
    Width = 137
    Height = 41
    Caption = #1057#1086#1079#1076#1072#1090#1100
    TabOrder = 6
    TabStop = False
    OnClick = create_butClick
    SkinData.SkinSection = 'BUTTON'
  end
  object grp1: TsGroupBox
    Left = 16
    Top = 136
    Width = 281
    Height = 113
    Caption = #1058#1072#1088#1080#1092#1099' '#1101#1083#1077#1082#1090#1088#1080#1095#1077#1089#1090#1074#1072
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Segoe UI Semibold'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 7
    SkinData.SkinSection = 'GROUPBOX'
    object lbl1: TsLabel
      Left = 8
      Top = 29
      Width = 143
      Height = 20
      Caption = #1058#1072#1088#1080#1092' '#1089#1074#1077#1090#1072' ('#1044#1077#1085#1100'): '
      ParentFont = False
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Segoe UI Semibold'
      Font.Style = [fsBold]
    end
    object lbl2: TsLabel
      Left = 8
      Top = 69
      Width = 144
      Height = 20
      Caption = #1058#1072#1088#1080#1092' '#1089#1074#1077#1090#1072' ('#1053#1086#1095#1100'): '
      ParentFont = False
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Segoe UI Semibold'
      Font.Style = [fsBold]
    end
    object power_t: TsDecimalSpinEdit
      Left = 152
      Top = 32
      Width = 121
      Height = 29
      TabStop = False
      Color = 15195590
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Segoe UI Semibold'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
    end
    object t_power_n: TsDecimalSpinEdit
      Left = 152
      Top = 72
      Width = 121
      Height = 29
      TabStop = False
      Color = 15195590
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Segoe UI Semibold'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
    end
  end
  object sGroupBox1: TsGroupBox
    Left = 16
    Top = 256
    Width = 281
    Height = 113
    Caption = #1058#1072#1088#1080#1092#1099' '#1074#1086#1076#1099'('#1082#1072#1085#1072#1083#1080#1079#1072#1094#1080#1080')'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Segoe UI Semibold'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 8
    SkinData.SkinSection = 'GROUPBOX'
    object sLabel1: TsLabel
      Left = 8
      Top = 29
      Width = 134
      Height = 20
      Caption = #1058#1072#1088#1080#1092' '#1074#1086#1076#1099' ('#1061#1042#1057'): '
      ParentFont = False
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Segoe UI Semibold'
      Font.Style = [fsBold]
    end
    object sLabel2: TsLabel
      Left = 8
      Top = 69
      Width = 132
      Height = 20
      Caption = #1058#1072#1088#1080#1092' '#1074#1086#1076#1099' ('#1043#1042#1057'): '
      ParentFont = False
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Segoe UI Semibold'
      Font.Style = [fsBold]
    end
    object t_XBC: TsDecimalSpinEdit
      Left = 152
      Top = 32
      Width = 121
      Height = 29
      TabStop = False
      Color = 15195590
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Segoe UI Semibold'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
    end
    object t_rBC: TsDecimalSpinEdit
      Left = 152
      Top = 72
      Width = 121
      Height = 29
      TabStop = False
      Color = 15195590
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Segoe UI Semibold'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
    end
  end
end
