object pay_power_interval: Tpay_power_interval
  Left = 539
  Top = 196
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #1059#1095#1077#1090' '#1087#1083#1072#1090#1077#1078#1077#1081' - '#1054#1090#1095#1077#1090
  ClientHeight = 369
  ClientWidth = 664
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Segoe UI Semibold'
  Font.Style = [fsBold]
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 17
  object lbl1: TsLabel
    Left = 80
    Top = 136
    Width = 12
    Height = 25
    Caption = #1057
    ParentFont = False
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Segoe UI Semibold'
    Font.Style = [fsBold]
  end
  object lbl2: TsLabel
    Left = 328
    Top = 136
    Width = 22
    Height = 25
    Caption = #1087#1086
    ParentFont = False
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Segoe UI Semibold'
    Font.Style = [fsBold]
  end
  object strckbr2: TsTrackBar
    Left = 0
    Top = 96
    Width = 665
    Height = 33
    LineSize = 2
    Position = 10
    TabOrder = 0
    TabStop = False
    TickMarks = tmTopLeft
    OnChange = strckbr2Change
    SkinData.SkinSection = 'TRACKBAR'
    BarOffsetV = 0
    BarOffsetH = 0
  end
  object mmo1: TsMemo
    Left = 8
    Top = 40
    Width = 649
    Height = 49
    Cursor = crArrow
    TabStop = False
    Color = 15195590
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Segoe UI Semibold'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssHorizontal
    TabOrder = 1
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'Tahoma'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
    SkinData.SkinSection = 'EDIT'
    DisabledKind = []
  end
  object date1: TsEdit
    Left = 112
    Top = 136
    Width = 177
    Height = 25
    Color = 15195590
    Enabled = False
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Segoe UI Semibold'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    SkinData.SkinSection = 'EDIT'
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'Tahoma'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
  end
  object date2: TsEdit
    Left = 384
    Top = 136
    Width = 177
    Height = 25
    Color = 15195590
    Enabled = False
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Segoe UI Semibold'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    SkinData.SkinSection = 'EDIT'
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'Tahoma'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
  end
  object strckbr1: TsTrackBar
    Left = -1
    Top = 0
    Width = 666
    Height = 33
    LineSize = 2
    Position = 10
    TabOrder = 4
    TabStop = False
    OnChange = strckbr1Change
    SkinData.SkinSection = 'TRACKBAR'
    BarOffsetV = 0
    BarOffsetH = 0
  end
  object mmo2: TStringGrid
    Left = 8
    Top = 168
    Width = 649
    Height = 193
    TabStop = False
    ColCount = 3
    DefaultColWidth = 65
    DefaultRowHeight = 17
    FixedCols = 0
    RowCount = 9
    FixedRows = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine]
    ScrollBars = ssHorizontal
    TabOrder = 5
  end
end
