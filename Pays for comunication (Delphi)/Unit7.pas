unit Unit7;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, sTrackBar, StdCtrls, sListBox, ExtCtrls, sSplitter,
  sMemo, sEdit, sLabel, StrUtils, sRichEdit, Grids;

type
  Tpay_power_interval = class(TForm)
    strckbr2: TsTrackBar;
    mmo1: TsMemo;
    date1: TsEdit;
    date2: TsEdit;
    strckbr1: TsTrackBar;
    lbl1: TsLabel;
    lbl2: TsLabel;
    mmo2: TStringGrid;
    procedure FormCreate(Sender: TObject);
    procedure strckbr1Change(Sender: TObject);
    procedure strckbr2Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  pay_power_interval: Tpay_power_interval;
  dates : array[1..200] of string;
  counters1: Currency;
  counters2: Currency;
  moneys1: Currency;
  moneys2: Currency;
  kv: array[1..200] of string;
  kv2: array[1..200] of string;
  mon1: array[1..200] of string;
  mon2: array[1..200] of string;
implementation

uses Unit1;

{$R *.dfm}

type
  Tdb = record
    Name : string[200];
    n1 : string[200];
    n2 : string[200];
    n3 : string[200];
    n4 : string[200];
    n5 : string[200];
    n6 : string[200];
    n7 : string[200];
    n8 : string[200];
    n9 : string[200];
    n10 : string[200];
    n11 : string[200];
end;

function show_inv():Boolean;
var
db: Tdb;
F : file of Tdb;
i: integer;
coun_dates: Integer;
dates_d: String;
hGridRect: TGridRect;
begin
  AssignFile(F,StringReplace(Login.room.Items.Strings[Login.room.ItemIndex] + '.dbp',' ','',[RFReplaceall]));
  Reset(F);
  try
    i := 0;
    coun_dates := 0;
    while not Eof(F) do
    begin
    Read (F, db);
      if db.Name = 'Power' then
      begin
        coun_dates := coun_dates + 1;
        if (coun_dates >= (pay_power_interval.strckbr1.Position)) and (coun_dates <= (pay_power_interval.strckbr2.Position)) then
        begin
        i := i + 1;
        dates[i] := db.n3;
        counters1 := counters1 + StrToCurr(db.n9);
        counters2 := counters2 + StrToCurr(db.n10);
        moneys1 := moneys1 + StrToCurr(db.n6);
        moneys2 := moneys2 + StrToCurr(db.n7);
        kv[i] := db.n9;
        kv2[i] := db.n10;
        mon1[i] := db.n6;
        mon2[i] := db.n7;
        end;
      end;
    end;
  finally
    CloseFile(F);
  end;

  coun_dates := 0;
  
  pay_power_interval.date1.Text := dates[1];
  pay_power_interval.date2.Text := dates[i];

  pay_power_interval.mmo2.ColCount := i + 2;

  pay_power_interval.mmo2.Cells[i + 1,0] := '����';

  for coun_dates := 1 to i do
  begin
    pay_power_interval.mmo2.Cells[coun_dates,0] := dates[coun_dates];
    pay_power_interval.mmo2.Cells[coun_dates,1] := kv[coun_dates] + ' ��\�';
    pay_power_interval.mmo2.Cells[coun_dates,4] := kv2[coun_dates] + ' ��\�';
    pay_power_interval.mmo2.Cells[coun_dates,2] := mon1[coun_dates] + ' ���.';
    pay_power_interval.mmo2.Cells[coun_dates,5] := mon2[coun_dates] + ' ���.';
    pay_power_interval.mmo2.Cells[coun_dates,7] := CurrToStr(StrToCurr(kv[coun_dates]) + StrToCurr(kv2[coun_dates])) + ' ��\�';
    pay_power_interval.mmo2.Cells[coun_dates,8] := CurrToStr(StrToCurr(mon1[coun_dates]) + StrToCurr(mon2[coun_dates])) + ' ���.';
  end;

  coun_dates := 0;
  pay_power_interval.mmo2.Cells[i + 1,7] := '0';
  pay_power_interval.mmo2.Cells[i + 1,8] := '0';

  for coun_dates := 1 to i do
  begin
    pay_power_interval.mmo2.Cells[i + 1,7] := CurrToStr(StrToCurr(pay_power_interval.mmo2.Cells[i + 1,7]) + (StrToCurr(kv[coun_dates]) + StrToCurr(kv2[coun_dates])));
    pay_power_interval.mmo2.Cells[i + 1,8] := CurrToStr(StrToCurr(pay_power_interval.mmo2.Cells[i + 1,8]) + (StrToCurr(mon1[coun_dates]) + StrToCurr(mon2[coun_dates])));
  end;

  pay_power_interval.mmo2.Cells[i + 1,7] := pay_power_interval.mmo2.Cells[i + 1,7] + ' ��\�';
  pay_power_interval.mmo2.Cells[i + 1,8] := pay_power_interval.mmo2.Cells[i + 1,8] + ' ���.';

  pay_power_interval.mmo2.Repaint;
  end;

procedure Tpay_power_interval.FormCreate(Sender: TObject);
var
db: Tdb;
F : file of Tdb;
i: integer;
coun_dates: Integer;
dates_d: String;
hGridRect: TGridRect;
begin
  AssignFile(F,StringReplace(Login.room.Items.Strings[Login.room.ItemIndex] + '.dbp',' ','',[RFReplaceall]));
  Reset(F);
  try
    i := 0;
    while not Eof(F) do
    begin
    Read (F, db);
      if db.Name = 'Power' then
      begin
        i := i + 1;
        dates[i] := db.n3;
        counters1 := counters1 + StrToCurr(db.n9);
        counters2 := counters2 + StrToCurr(db.n10);
        moneys1 := moneys1 + StrToCurr(db.n6);
        moneys2 := moneys2 + StrToCurr(db.n7);
        kv[i] := db.n9;
        kv2[i] := db.n10;
        mon1[i] := db.n6;
        mon2[i] := db.n7;
        mmo1.Lines.Strings[0] := mmo1.Lines.Strings[0] + db.n3 + ' - ';
      end;
    end;
  finally
    CloseFile(F);
  end;
  strckbr1.Max := i;
  strckbr1.Min := 1;
  strckbr2.Max := i;
  strckbr2.Min := 1;
  strckbr1.Position := 1;
  strckbr2.Position := i;
  date1.Text := dates[1];
  date2.Text := dates[i];

  hGridRect.Top := -1;
  hGridRect.Left := -1;
  hGridRect.Right := -1;
  hGridRect.Bottom := -1;

  mmo2.ColCount := i + 2;

  mmo2.Cells[i + 1,0] := '����';
  mmo2.Cells[i + 1,1] := CurrToStr(counters1) + ' ��\�';
  mmo2.Cells[i + 1,4] := CurrToStr(counters2) + ' ��\�';
  mmo2.Cells[i + 1,2] := CurrToStr(moneys1) + ' ���.';
  mmo2.Cells[i + 1,5] := CurrToStr(moneys2) + ' ���.';

  mmo2.Selection :=  hGridRect;

  mmo2.ColWidths[0] := 120;
  for coun_dates := 1 to i do
  begin
    mmo2.Cells[coun_dates,0] := dates[coun_dates];
    mmo2.Cells[coun_dates,1] := kv[coun_dates] + ' ��\�';
    mmo2.Cells[coun_dates,4] := kv2[coun_dates] + ' ��\�';
    mmo2.Cells[coun_dates,2] := mon1[coun_dates] + ' ���.';
    mmo2.Cells[coun_dates,5] := mon2[coun_dates] + ' ���.';
    mmo2.Cells[coun_dates,7] := CurrToStr(StrToCurr(kv[coun_dates]) + StrToCurr(kv2[coun_dates])) + ' ��\�';
    mmo2.Cells[coun_dates,8] := CurrToStr(StrToCurr(mon1[coun_dates]) + StrToCurr(mon2[coun_dates])) + ' ���.';
  end;

  coun_dates := 0;
  mmo2.Cells[i + 1,7] := '0';
  mmo2.Cells[i + 1,8] := '0';

  for coun_dates := 1 to i do
  begin
    mmo2.Cells[i + 1,7] := CurrToStr(StrToCurr(mmo2.Cells[i + 1,7]) + (StrToCurr(kv[coun_dates]) + StrToCurr(kv2[coun_dates])));
    mmo2.Cells[i + 1,8] := CurrToStr(StrToCurr(mmo2.Cells[i + 1,8]) + (StrToCurr(mon1[coun_dates]) + StrToCurr(mon2[coun_dates])));
  end;

  mmo2.Cells[i + 1,7] := mmo2.Cells[i + 1,7] + ' ��\�';
  mmo2.Cells[i + 1,8] := mmo2.Cells[i + 1,8] + ' ���.';
  mmo2.Cells[0,0] := '����';
  mmo2.Cells[0,1] := '������� ����';
  mmo2.Cells[0,2] := '������ ����';
  mmo2.Cells[0,4] := '������� ����';
  mmo2.Cells[0,5] := '������ ����';
  mmo2.Cells[0,7] := '�������� (�����)';
  mmo2.Cells[0,8] := '������ (�����)';

  mmo2.Repaint;
  mmo2.Refresh;
  end;
  
procedure Tpay_power_interval.strckbr1Change(Sender: TObject);
begin
  if strckbr1.Position <= strckbr2.Position  then
  begin
     show_inv();
  end
  else
  begin
    strckbr1.Position := strckbr2.Position - 1;
  end;
end;

procedure Tpay_power_interval.strckbr2Change(Sender: TObject);
begin
  if strckbr2.Position >= strckbr1.Position  then
  begin
    show_inv();
  end
  else
  begin
    strckbr2.Position := strckbr1.Position + 1;
  end;
end;

end.
