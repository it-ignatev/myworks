unit Unit8;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sMemo, sGroupBox, sButton, sLabel, sEdit, sSpinEdit;

type
  Tpay_power_edit = class(TForm)
    exit: TsButton;
    change: TsButton;
    mmo1: TsMemo;
    grp2: TsGroupBox;
    lbl3: TsLabel;
    lbl4: TsLabel;
    day_c: TsDecimalSpinEdit;
    night_c: TsDecimalSpinEdit;
    money_d: TsEdit;
    money_n: TsEdit;
    money_al: TsEdit;
    lbl1: TsLabel;
    procedure FormCreate(Sender: TObject);
    procedure day_cChange(Sender: TObject);
    procedure night_cChange(Sender: TObject);
    procedure exitClick(Sender: TObject);
    procedure changeClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  pay_power_edit: Tpay_power_edit;
  last_d: string;
  last_n: string;
  tariv_d: string;
  tariv_n: string;
  cur1: Currency;
  cur2: Currency;
  date: string;

implementation

uses Unit1, Unit4;

{$R *.dfm}

type
  Tdb = record
    Name : string[200];
    n1 : string[200];
    n2 : string[200];
    n3 : string[200];
    n4 : string[200];
    n5 : string[200];
    n6 : string[200];
    n7 : string[200];
    n8 : string[200];
    n9 : string[200];
    n10 : string[200];
    n11 : string[200];
end;

procedure Tpay_power_edit.FormCreate(Sender: TObject);
var
db: Tdb;
F : file of Tdb;
begin
  cur1 := StrToCurr('0');
  cur2 := StrToCurr('0');
  last_d := '0';
  last_n := '0';
  tariv_n := '0';
  tariv_d := '0';
  
  AssignFile(F,StringReplace(Login.room.Items.Strings[Login.room.ItemIndex] + '.dbp',' ','',[RFReplaceall]));
  Reset(F);
  try
    Seek(F, StrToInt(pay_power.all[pay_power.lst1.ItemIndex + 1]) - 1);
    Read (F, db);
    mmo1.Lines.Clear;
    mmo1.Lines.Add('������� ����� �� ' + db.n3);
    date := db.n3;
    day_c.Value := StrToFloat(db.n1);
    night_c.Value := StrToCurr(db.n2);
    money_d.Text := db.n6 + ' ���.';
    money_n.Text := db.n7 + ' ���.';
    money_al.Text := '����: ' + db.n8 + ' ���.';
    if StrToInt(pay_power.all[pay_power.lst1.ItemIndex + 1]) > 3 then
    begin
      Seek(F, StrToInt(pay_power.all[pay_power.lst1.ItemIndex + 1]) - 1);
      Read (F, db);
      last_d := db.n1;
      last_n := db.n2;
    end
    else
    begin
    last_d := '0';
    last_n:= '0';
    end;
    Seek(F, 1);
    Read (F, db);
    tariv_d := db.n1;
    tariv_n := db.n2;
  finally
    CloseFile(F);
  end;
end;

procedure Tpay_power_edit.day_cChange(Sender: TObject);
begin
    cur1 := (FloatToCurr(day_c.Value) - StrToCurr(last_d)) * StrToCurr(tariv_d);

    money_d.Text := CurrToStr(cur1) + ' ���.';

    money_al.Text := '����: ' + CurrToStr(cur1+cur2) + ' ���.';
end;

procedure Tpay_power_edit.night_cChange(Sender: TObject);
begin
    cur2 := (FloatToCurr(night_c.Value) - StrToCurr(last_n)) * StrToCurr(tariv_n);

    money_n.Text := CurrToStr(cur2) + ' ���.';

    money_al.Text := '����: ' + CurrToStr(cur1+cur2) + ' ���.';
end;

procedure Tpay_power_edit.exitClick(Sender: TObject);
begin
  Self.Close;
end;

procedure Tpay_power_edit.changeClick(Sender: TObject);
var
db: Tdb;
F : file of Tdb;
begin
  db.Name := 'Power';
  db.n1 := CurrToStr(day_c.Value);
  db.n2 := CurrToStr(night_c.Value);
  db.n3 := date;
  db.n4 := tariv_d;
  db.n5 := tariv_n;
  db.n9 := CurrToStr(FloatToCurr(day_c.Value) - StrToCurr(last_d));
  db.n10 := CurrToStr(FloatToCurr(night_c.Value) - StrToCurr(last_n));
  db.n6 := CurrToStr(cur1);
  db.n7 := CurrToStr(cur2);
  db.n8 := CurrToStr(cur1 + cur2);

    AssignFile(F,StringReplace(Login.room.Items.Strings[Login.room.ItemIndex] + '.dbp',' ','',[RFReplaceall]));
      Reset(F);
      try
       Seek(F,StrToInt(pay_power.all[pay_power.lst1.ItemIndex + 1]) - 1);
       Write(F,db);
      finally
      CloseFile(F);
      end;

    pay_power.pays();
    self.Close;
end;

end.
