program Project1;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils, Math;

var Matrix: array of array of real;
    n, k, i, g: integer;
    d: real;

begin
  Write('n='); Readln(n);
  Write('k='); Readln(k);

  SetLength(Matrix, n, n);
  n:=n-1;
  k:=k-1;

  Write(#13#10'�������� ������� (���������)'#13#10#13#10);

  for i := 0 to n do
  begin
    for g := 0 to n do
    begin
      Randomize;
      Matrix[i][g] := RandomRange(2, 10);
      //Writeln('Matrix[', i, '][', g, '] = ', Matrix[i][g]:2:3);
      Write(Matrix[i][g]:2:2, '  ');
    end;
    Write(#13#10);
  end;

  Write(#13#10#13#10);

  d:=Matrix[k][k];

  Write('������� ������'#13#10#13#10);

  for i := 0 to n do
  begin
    //Writeln('Matrix[', k, '][', i, '] = ', Matrix[k][i]:2:3, '/', d:2:3);
    Write(Matrix[k][i]:2:2, '/', d:2:2, '  ');
    Matrix[k][i] := Matrix[k][i]/d;
  end;

  Write(#13#10#13#10#13#10);

  Write('���������'#13#10#13#10);

  for i := 0 to n do
  begin
    for g := 0 to n do
      Write(Matrix[i][g]:2:2, '  ');
    Write(#13#10);
  end;
      //Writeln('Matrix[', i, '][', g, '] = ', Matrix[i][g]:2:3);

  Readln;
end.
