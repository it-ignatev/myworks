program Project1;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils, Math;

type
  TA = array of real;

var a,b,c: TA;
    n, i, k: integer;
    M: real;

function maxOfArray(a: TA): real;
begin
  M := a[2];
  k := Length(a);

  for i := 1 to k do
    if (2*i)<=k then
      M := Max(a[2*i], M);

  //Write(#13#10'MAX k=', k, #13#10, Round(M), #13#10#13#10);

  result:=M;
end;

function minOfArray(a: TA): real;
begin
  M := a[1];
  k := Length(a);

  for i := 1 to k do
    if (2*i+1)<=k then
      M := Min(a[2*i+1], M);

  //Write(#13#10'MIN k=', k, #13#10, Round(M), #13#10#13#10);

  result:=M;
end;

function putArray(f: TA) : TA;
begin
  for i := 1 to Length(f) do
  begin
    Randomize;
    f[i] := RandomRange(-100, 100);
    Write('a[', i, '] = ', Round(f[i]), '  ');
  end;

  Writeln(#13#10);
  result := f;
end;

begin
  Write('n1='); Readln(n);
  SetLength(a, n);
  Write('n2='); Readln(n);
  SetLength(b, n);
  Write('n3='); Readln(n);
  SetLength(c, n);

  a:= putArray(a);
  b:= putArray(b);
  c:= putArray(c);

  Writeln('Result1=', (maxOfArray(a) + minOfArray(a)):2:2, #13#10);
  Writeln('Result2=',(maxOfArray(b) + minOfArray(b)):2:2, #13#10);
  Writeln('Result3=',(maxOfArray(c) + minOfArray(c)):2:2, #13#10);

  Readln;
end.
