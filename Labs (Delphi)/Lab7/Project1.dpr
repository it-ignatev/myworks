program Project1;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils, Math;

var a: array of real; n, i, positive, nonpositive, zero: integer;

begin
    Write('n='); Readln(n);
    n:=n-1;
    SetLength(a, n);

    positive:=0;
    zero:=0;
    nonpositive:=0;

    for i:=0 to n do
    begin
      Randomize;
      a[i]:=RandomRange(-1000, 1000);
    end;

    for i := 0 to n do
    begin
      if a[i]>0 then positive:=positive+1;
      if a[i]=0 then zero:=zero+1;
      if a[i]<0 then nonpositive:=nonpositive+1;
    end;

    Writeln('�������������: ', positive, #10#13'�������������: ', nonpositive, #10#13'�������: ', zero);

    Readln;
    Readln;
end.
