﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Collections;
using System.Threading;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;


namespace SeaBattle
{
    class LobbyServer
    {
        TcpListener server = null;
        public static NetworkStream stream;

        public LobbyServer()
        {
            try
            {
                // Определим нужное максимальное количество потоков
                // Пусть будет по 4 на каждый процессор
                int MaxThreadsCount = Environment.ProcessorCount * 4;
                Console.WriteLine(MaxThreadsCount.ToString());
                // Установим максимальное количество рабочих потоков
                ThreadPool.SetMaxThreads(MaxThreadsCount, MaxThreadsCount);
                // Установим минимальное количество рабочих потоков
                ThreadPool.SetMinThreads(2, 2);


                // Устанавливаем порт для TcpListener = 9595.
                Int32 port = 9595;
                IPAddress localAddr = IPAddress.Parse("127.0.0.1");
                int counter = 0;
                server = new TcpListener(localAddr, port);

                // Запускаем TcpListener и начинаем слушать клиентов.
                server.Start();

                Console.WriteLine("Сервер запущен !");

                // Принимаем клиентов в бесконечном цикле.
                while (true)
                {
                    Console.WriteLine("Новый клиент !");
                    // При появлении клиента добавляем в очередь потоков его обработку.
                    ThreadPool.QueueUserWorkItem(ObrabotkaZaprosa, server.AcceptTcpClient());
                    // Выводим информацию о подключении.
                    counter++;
                }
            }
            catch (SocketException e)
            {
                //В случае ошибки, выводим что это за ошибка.
                Console.WriteLine("SocketException: {0}", e);
            }
            finally
            {
                // Останавливаем TcpListener.
                server.Stop();
            }
        }

        static void ObrabotkaZaprosa(object client_obj)
        {
            // Буфер для принимаемых данных.
            Byte[] bytes = new Byte[256];
            String data = null;

            //Можно раскомментировать Thread.Sleep(1000); 
            //Запустить несколько клиентов
            //и наглядно увидеть как они обрабатываются в очереди. 
            //Thread.Sleep(1000);

            TcpClient client = client_obj as TcpClient;

            data = null;

            // Получаем информацию от клиента
            stream = client.GetStream();

            int i;

            // Принимаем данные от клиента в цикле пока не дойдём до конца.
            try
            {
                while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                {
                    // Преобразуем данные в ASCII string.
                    data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);

                    // Преобразуем строку к верхнему регистру.
                    string[] me = data.Split('_');

                    if (data == "IS_ALLIVE")
                    {
                        data = "ALLIVE";
                    }
                    else
                    {
                        if (me[0] == "CONNECT")
                        {
                            data = "CONNECT_127.0.0.1";
                            Lobby.SelfRef.Invoke();

                        }

                        if (data == "ALLIVE")
                        {
                            data = "OK";
                        }
                    }

                    Send(data);
                }
            }
            catch (IOException)
            {
                client.Close();
            }

            // Закрываем соединение.
            client.Close();
        }

        public static void Send(string meessage)
        {
            // Преобразуем полученную строку в массив Байт.
            byte[] msg = System.Text.Encoding.ASCII.GetBytes(meessage);

            // Отправляем данные обратно клиенту (ответ).
            stream.Write(msg, 0, msg.Length);
        }
    }
}
