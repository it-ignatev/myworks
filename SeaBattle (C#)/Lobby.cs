﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.Collections;

namespace SeaBattle
{
    public partial class Lobby : Form
    {
        delegate void labelTextDelegate(string text);
        public delegate void ControlForm();

        MultiplayerMain multm = new MultiplayerMain();

        public Lobby()
        {
            InitializeComponent();
            SelfRef = this;
        }

        public void LabelText(string text)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new labelTextDelegate(LabelText), new object[] { text });
                return;
            }
            else
            {
                label1.Text += "\r" + text;
            }
        }

        public static Lobby SelfRef{get;set;}            
            
        private void button1_Click(object sender, EventArgs e)
        {           
            button1.Enabled = false;
            button2.Enabled = false;

            multm.StartServer();

            LabelText("Ожидание игроков...");
        }

        private void Lobby_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (MultiplayerMain.host)
            {
                multm.StopServer();
            }

            Enterence frm_ent = new Enterence();
            frm_ent.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            multm.ClientConnect("127.0.0.1", "IS_ALLIVE", "CHECK");
        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != "")
            {
                multm.ClientConnect("127.0.0.1", "CONNECT_127.0.0.1", "CONNECT");
            }
        }

        public void GameStart()
        {
            Lobby.SelfRef.Hide();
            GameMultiplayer frm_mult = new GameMultiplayer();
            frm_mult.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (MultiplayerMain.host)
            {
                multm.SendServer("ALLIVE");
            }
            else
            {
                multm.SendClient("ALLIVE");
            }
        }
    }
}
