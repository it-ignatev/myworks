﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Text.RegularExpressions;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace SeaBattle
{
    class Client
    {
        public static Client Self;
        public static NetworkStream stream;
        public static Thread r;

        public Client(string ip, string message, string command)
        {
            Self = this;

            if (command == "CHECK")
            {
                Connect(ip, message, true);
            }

            if (command == "CONNECT")
            {
                Connect(ip, message, false);
            }
        }

        static void Connect(String server, String message, bool close)
        {
            try
            {
                // Создаём TcpClient.
                // Для созданного в предыдущем проекте TcpListener 
                // Настраиваем его на IP нашего сервера и тот же порт.

                Int32 port = 9595;
                TcpClient client = new TcpClient(server, port);

                // Переводим наше сообщение в ASCII, а затем в массив Byte.
                Byte[] data = System.Text.Encoding.ASCII.GetBytes(message);

                // Получаем поток для чтения и записи данных.
                stream = client.GetStream();

                // Отправляем сообщение нашему серверу. 
                Send(data);

                lock (Lobby.SelfRef.label1.Text)
                {
                    Lobby.SelfRef.label1.Text += "Sent: " + message;
                }

                // Получаем ответ от сервера.

                // Буфер для хранения принятого массива bytes.
                data = new Byte[256];

                // Строка для хранения полученных ASCII данных.
                String responseData = String.Empty;

                // Читаем первый пакет ответа сервера. 
                // Можно читать всё сообщение.
                // Для этого надо организовать чтение в цикле как на сервере.
                Int32 bytes = stream.Read(data, 0, data.Length);
                responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);

                MultiplayerMain.AnswerFS = responseData;
                Console.WriteLine("RESPONSE: " + responseData);

                if (close)
                {
                    // Закрываем всё.
                    stream.Close();
                    client.Close();
                }
                else
                {
                    r = new Thread(Recieve);
                    r.IsBackground = true;
                    r.Start();
                }
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine("ArgumentNullException: {0}", e);
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }
        }

        public static void Send(Byte[] data)
        {
            stream.Write(data, 0, data.Length);
        }

        static void Recieve()
        {
            Byte[] bytes = new Byte[256];
            String data = null;
            int i;

            try
            {
                while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                {
                    // Преобразуем данные в ASCII string.
                    data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);

                    // Преобразуем строку к верхнему регистру.

                    Lobby.SelfRef.LabelText(data);
                }
            }
            catch (IOException)
            {
                stream.Close();
                r.Abort();
            }
        }
    }
}
