﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace SeaBattle
{
    class Draw
    {
        public Draw()
        {

        }

        public static void DrawLines(Graphics grid, Boolean vertical, Brush background, float size)
        {
            int x, y;
            Pen main = new Pen(Color.Black, 1);

            if (vertical)
            {
                x = 0;
                y = 400;

                grid.FillRectangle(background, 0, 0, size, size);
                grid.DrawRectangle(main, 0, 0, size, size);

                for (int i = 0; i < 9; i++)
                {
                    x += 40;

                    Point p1 = new Point(x, 0), p2 = new Point(x, y);

                    grid.DrawLine(main, p1, p2);
                }
            }
            else
            {
                x = 400;
                y = 0;

                for (int i = 0; i < 9; i++)
                {
                    y += 40;

                    Point p1 = new Point(0, y), p2 = new Point(x, y);

                    grid.DrawLine(main, p1, p2);
                }
            }
        }

        public static void DrawShips(Graphics grid, Pen pen, bool user)
        {
            if (!Main.AccessPaint) { return; }

            for (int i = 0; i < 10; i++)
            {
                grid.DrawEllipse(pen, Main.UserShips[i, 0] * 40 + 5, Main.UserShips[i, 1] * 40 + 5, 30, 30);
            }

            /*
                * Отрисовка кораблей противника
                             
            Brush br = Brushes.Bisque;
            Font font = new Font("Arial", 16);

            for (int i = 0; i < 10; i++)
            {
                //grid.DrawEllipse(pen, ComputerShips[i, 0] * 40 + 5, ComputerShips[i, 1] * 40 + 5, 30, 30);
                //grid.DrawString(i.ToString(), font, br, ComputerShips[i, 0] * 40 + 5, ComputerShips[i, 1] * 40 + 5);
            }
                */
        }

        public static void DrawBombs(Graphics grid, Pen pen, bool user)
        {
            int x, y;

            if (!Main.AccessPaint) return;

            if (user)
            {
                for (int i = 0; i < 100; i++)
                {
                    x = Main.CompBombs[i, 0];
                    y = Main.CompBombs[i, 1];

                    if (!CheckShip(x, y, true))
                    {
                        Point p1, p2;
                        p1 = new Point(x * 40, y * 40);
                        p2 = new Point(x * 40 + 40, y * 40 + 40);
                        grid.DrawLine(pen, p1, p2);

                        p1 = new Point(x * 40 + 40, y * 40);
                        p2 = new Point(x * 40, y * 40 + 40);
                        grid.DrawLine(pen, p1, p2);
                    }
                    else
                    {
                        //Рисование уничтоженного корабля игрока
                        Brush b = Brushes.Black;

                        grid.FillRectangle(b, x * 40 + 5, y * 40 + 5, 30, 30);
                    }
                }
            }
            else
            {
                for (int i = 0; i < 100; i++)
                {
                    x = Main.UserBombs[i, 0];
                    y = Main.UserBombs[i, 1];

                    if (!CheckShip(x, y, false))
                    {
                        Point p1, p2;
                        p1 = new Point(x * 40, y * 40);
                        p2 = new Point(x * 40 + 40, y * 40 + 40);
                        grid.DrawLine(pen, p1, p2);

                        p1 = new Point(x * 40 + 40, y * 40);
                        p2 = new Point(x * 40, y * 40 + 40);
                        grid.DrawLine(pen, p1, p2);
                    }
                    else
                    {
                        //Рисование уничтоженного корабля компьютера
                        Brush b = Brushes.Black;

                        grid.FillRectangle(b, x * 40 + 5, y * 40 + 5, 30, 30);
                    }
                }
            }
        }

        public static bool CheckShip(int x, int y, bool user)
        {
            if (user)
            {
                for (int i = 0; i < 10; i++)
                {
                    if (x == Main.ComputerShips[i, 0] && y == Main.ComputerShips[i, 1])
                    {
                        return true;
                    }
                }
            }
            else
            {
                for (int i = 0; i < 10; i++)
                {
                    if (x == Main.UserShips[i, 0] && y == Main.UserShips[i, 1])
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
