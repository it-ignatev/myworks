﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows;

namespace SeaBattle
{
    public partial class Game : Form
    {
        public static Boolean Battle = false;
        Random RND = new Random();
        Main main = new Main();

        public Game()
        {
            InitializeComponent();
            SelfRef = this;
        }

        public static Game SelfRef
        {
            get;
            set;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Main.AccessPaint = true;
            button1.Enabled = false;
            pictureBox2.Enabled = true;

            label1.Text = "Расставте корабли !";

            Main.GenCompShips();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Brush background = Brushes.DarkGreen;
            Pen pen = new Pen(Color.Aqua, 2);
            Pen bomb = new Pen(Color.LightGoldenrodYellow, 2);

            Draw.DrawLines(e.Graphics, true, background, 400);
            Draw.DrawLines(e.Graphics, false, background, 400);
            //Draw.DrawShips(e.Graphics, pen, false);
            Draw.DrawBombs(e.Graphics, bomb, true);
        }

        private void pictureBox2_Paint(object sender, PaintEventArgs e)
        {
            Brush background = Brushes.DarkRed;
            Pen pen = new Pen(Color.White, 2);
            Pen bomb = new Pen(Color.LightCyan, 2);

            Draw.DrawLines(e.Graphics, true, background, 400);
            Draw.DrawLines(e.Graphics, false, background, 400);
            Draw.DrawShips(e.Graphics, pen, true);
            Draw.DrawBombs(e.Graphics, bomb, false);
        }

        private void pictureBox2_MouseUp(object sender, MouseEventArgs e)
        {
            int x, y;

            x = Convert.ToInt32(Math.Round(Convert.ToDecimal(e.X / 40), 0));
            y = Convert.ToInt32(Math.Round(Convert.ToDecimal(e.Y / 40), 0));

            Main.setUserShip(x, y); 
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Game_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            button2.Visible = false;
            Battle = true;
            Main.StepsInBattle = 0;
            Main.time = 0;
            timer1.Enabled = true;
            Main.DestroyedShipC = 0;
            Main.DestroyedShipU = 0;
            pictureBox1.Enabled = true;
            label1.Text = "Идет игра ! Уничтожте корабли противника !";
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (Battle)
            {
                int x, y;

                x = Convert.ToInt32(Math.Round(Convert.ToDecimal(e.X / 40), 0));
                y = Convert.ToInt32(Math.Round(Convert.ToDecimal(e.Y / 40), 0));

                Main.setBomb(x, y);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Main.time++;
            label2.Text = "Время игры: " + Main.time.ToString() + " сек.";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            button3.Visible = false;
            Main.ResetGame();
            pictureBox1.Invalidate();
            pictureBox2.Invalidate();
            Main.Steps = 0;
            pictureBox2.Enabled = true;

            label1.Text = "Расставте корабли !";
            label2.Text = "";
            label3.Text = "";

            Main.GenCompShips();
        }
    }
}
