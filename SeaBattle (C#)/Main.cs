﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SeaBattle
{
    class Main
    {
        public static int Steps = 0, StepsInBattle = 0, time = 0, DestroyedShipU = 0, DestroyedShipC = 0;

        public static int[,] UserShips = new int[10, 2];
        public static int[,] ComputerShips = new int[10, 2];

        public static int[,] UserBombs = new int[100, 2];
        public static int[,] CompBombs = new int[100, 2];

        public static int[,] ExplodedComputerShips = new int[10, 2];
        public static int[,] ExplodedUserShips = new int[10, 2];

        public static bool AccessPaint;

        static Random RND = new Random();

        public static string Name { get; set; }
        public static bool Multiplayer { get; set; }
        public static string connection { get; set; }
        public static string AnswerFS { get; set; }

        public Main()
        {
            ResetGame();
        }

        public static void ResetGame()
        {
            for (int i = 0; i < 10; i++)
            {
                UserShips[i, 0] = -2;
                UserShips[i, 1] = -2;

                ComputerShips[i, 0] = -2;
                ComputerShips[i, 1] = -2;

                ExplodedComputerShips[i, 0] = -2;
                ExplodedComputerShips[i, 1] = -2;

                ExplodedUserShips[i, 0] = -2;
                ExplodedUserShips[i, 1] = -2;
            }

            for (int i = 0; i < 100; i++)
            {
                CompBombs[i, 0] = -2;
                CompBombs[i, 1] = -2;

                UserBombs[i, 0] = -2;
                UserBombs[i, 1] = -2;
            }

            //Game.SelfRef.pictureBox1.Invalidate();
            //Game.SelfRef.pictureBox2.Invalidate();
        }

        public static void GenCompShips()
        {
            int x, y, i = 0;

            x = RND.Next(10);
            y = RND.Next(10);

            while (i < 10)
            {
                if (ComputerShips[i, 0] == x && ComputerShips[i, 1] == y)
                {
                    x = RND.Next(10);
                    y = RND.Next(10);

                    i = 0;
                }
                else
                {
                    ComputerShips[i, 0] = x;
                    ComputerShips[i, 1] = y;

                    x = RND.Next(10);
                    y = RND.Next(10);
                }

                i++;
            }
        }

        public static void setUserShip(int x, int y)
        {
            if (Steps >= 10) return;
            
            for (int i = 0; i < 10; i++)
            {
                if (UserShips[i, 0] == x && UserShips[i, 1] == y)
                {
                    return;
                }
            }

            //Graphics grid;
            //Pen l = new Pen(Color.Pink, 2);
            //Brush s = Brushes.WhiteSmoke;

            //grid = Graphics.FromHwnd(pictureBox2.Handle);

            //grid.DrawEllipse(l, x * 40 + 5, y * 40 + 5, 30, 30);

            UserShips[Steps, 0] = x;
            UserShips[Steps, 1] = y;

            Steps += 1;

            Game.SelfRef.pictureBox2.Invalidate();

            if (Main.Steps == 10)
            {
                Game.SelfRef.label1.Text = "Вы расставили корабли ! Можете начинать бой !";
                Game.SelfRef.button2.Visible = true;
                Game.SelfRef.pictureBox2.Enabled = false;
            }  

            //label1.Text = e.Location.ToString() + "\r\n Клетка: " + x.ToString() + "," + y.ToString() + " Ходов: " + Steps.ToString();
        }

        public static void setBomb(int x, int y)
        {

            for (int i = 0; i < Main.CompBombs.GetLength(0); i++)
            {
                if (Main.CompBombs[i, 0] == x && Main.CompBombs[i, 1] == y)
                {
                    return;
                }
            }

            Main.CompBombs[Main.StepsInBattle, 0] = x;
            Main.CompBombs[Main.StepsInBattle, 1] = y;

            Main.StepsInBattle += 1;

            ComputerStep(Main.StepsInBattle);

            if (Draw.CheckShip(x, y, true))
            {
                Main.DestroyedShipC += 1;

                if (Main.DestroyedShipC >= 6)
                {
                    Game.SelfRef.label1.Text = "Игра закончена ! Вы выиграли !";
                    Game.SelfRef.pictureBox1.Enabled = false;
                    Game.SelfRef.pictureBox2.Enabled = false;
                    Game.SelfRef.timer1.Enabled = false;
                    Game.SelfRef.button3.Visible = true;
                    Game.SelfRef.pictureBox1.Enabled = false;
                    Game.Battle = false;
                }
            }

            Game.SelfRef.pictureBox1.Invalidate();
            Game.SelfRef.pictureBox2.Invalidate();

            Game.SelfRef.label3.Text = "Кол-во ходов:" + Main.StepsInBattle.ToString() + "\r\n Уничтожено кораблей:" + Main.DestroyedShipC.ToString() + " Осталось:" + (6 - Main.DestroyedShipC).ToString() + "\r\n Уничтож.ваших корабли:" + Main.DestroyedShipU.ToString() + " Осталось:" + (6 - Main.DestroyedShipU).ToString(); 
        }

        private static void ComputerStep(int st)
        {
            int x, y, i = 0;

            x = RND.Next(10);
            y = RND.Next(10);

            while (i < 100)
            {
                if (Main.UserBombs[i, 0] == x && Main.UserBombs[i, 1] == y)
                {
                    x = RND.Next(10);
                    y = RND.Next(10);

                    i = 0;
                }

                i++;
            }

            Main.UserBombs[st, 0] = x;
            Main.UserBombs[st, 1] = y;

            if (Draw.CheckShip(x, y, false))
            {
                Main.DestroyedShipU += 1;

                if (Main.DestroyedShipU >= 6)
                {
                    Game.SelfRef.label1.Text = "Игра закончена ! Вы проиграли !";
                    Game.SelfRef.pictureBox1.Enabled = false;
                    Game.SelfRef.pictureBox2.Enabled = false;
                    Game.SelfRef.timer1.Enabled = false;
                    Game.SelfRef.button3.Visible = true;
                    Game.SelfRef.pictureBox1.Enabled = false;
                    Game.Battle = false;
                }
            }
        }
    }
}
