﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace SeaBattle
{
    public partial class Enterence : Form
    {
        public Enterence()
        {
            InitializeComponent();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            string c = e.KeyChar.ToString();

            if (!Regex.Match(c, @"[a-zA-Z]|[0-9]").Success)
            {
                e.Handled = true;
            }

            if (e.KeyChar == (char)Keys.Back)
            {
                e.Handled = false;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.TextLength >= 3)
            {
                button1.Enabled = true;
                button2.Enabled = true;
            }
            else
            {
                button1.Enabled = false;
                button2.Enabled = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Game frm_game = new Game();
            Main.Name = textBox1.Text;
            Main.Multiplayer = false;
            frm_game.Show();

            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Lobby frm_lobby = new Lobby();
            Main.Name = textBox1.Text;
            Main.Multiplayer = true;
            frm_lobby.Show();

            this.Hide();
        }

        private void Enterence_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.ExitThread();
            Application.Exit();
        }
    }
}
