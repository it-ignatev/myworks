﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace SeaBattle
{
    class MultiplayerMain
    {
        public static MultiplayerMain Self;
        public static string AnswerFS;
        public static bool host = false;

        private static Thread srv;

        public MultiplayerMain()
        {
            Self = this;
        }

        public void StartServer()
        {
            host = true;

            srv = new Thread(Server);
            srv.IsBackground = true;
            srv.Start();
        }

        private void Server()
        {
            new LobbyServer();
        }

        public void StopServer()
        {
            host = false;
            srv.Abort();
        }

        public void ClientConnect(string ip, string message, string command)
        {
            Client cl = new Client(ip, message, command);

            if (AnswerFS == "ALLIVE")
            {
                Lobby.SelfRef.listBox1.Items.Clear();
                Lobby.SelfRef.listBox1.Items.Add(ip);
            }

            if (AnswerFS == "CONNECT_" + ip)
            {
                Lobby.SelfRef.LabelText("Соединение установлено ! Запуск игры...");
                Lobby.SelfRef.Invoke(new Lobby.ControlForm(GameStart ));
            }
        }

        public void SendClient(string message)
        {
            Byte[] data = System.Text.Encoding.ASCII.GetBytes(message);

            Client.Send(data);
        }

        public void SendServer(string message)
        {
            LobbyServer.Send(message);
        }
    }
}
