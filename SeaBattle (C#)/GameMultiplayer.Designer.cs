﻿namespace SeaBattle
{
    partial class GameMultiplayer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.but_close = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // but_close
            // 
            this.but_close.Location = new System.Drawing.Point(484, 368);
            this.but_close.Name = "but_close";
            this.but_close.Size = new System.Drawing.Size(177, 38);
            this.but_close.TabIndex = 0;
            this.but_close.Text = "Отключиться";
            this.but_close.UseVisualStyleBackColor = true;
            this.but_close.Click += new System.EventHandler(this.but_close_Click);
            // 
            // GameMultiplayer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(673, 418);
            this.Controls.Add(this.but_close);
            this.Name = "GameMultiplayer";
            this.Text = "GameMultiplayer";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.GameMultiplayer_FormClosed);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button but_close;
    }
}