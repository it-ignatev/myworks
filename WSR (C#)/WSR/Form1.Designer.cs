﻿namespace WSR
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.новыйToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.режимРаботыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.нормальныйРежимToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выведенаВРемонтВЛ220КВАгломерат1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выведенаВРемонтВЛ220КВАгломерат2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.пользовательToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.результатыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.режимРаботыToolStripMenuItem,
            this.пользовательToolStripMenuItem,
            this.результатыToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(459, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.новыйToolStripMenuItem,
            this.выходToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // новыйToolStripMenuItem
            // 
            this.новыйToolStripMenuItem.Name = "новыйToolStripMenuItem";
            this.новыйToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.новыйToolStripMenuItem.Text = "Новый";
            this.новыйToolStripMenuItem.Click += new System.EventHandler(this.новыйToolStripMenuItem_Click);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.выходToolStripMenuItem_Click);
            // 
            // режимРаботыToolStripMenuItem
            // 
            this.режимРаботыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.нормальныйРежимToolStripMenuItem,
            this.выведенаВРемонтВЛ220КВАгломерат1ToolStripMenuItem,
            this.выведенаВРемонтВЛ220КВАгломерат2ToolStripMenuItem});
            this.режимРаботыToolStripMenuItem.Name = "режимРаботыToolStripMenuItem";
            this.режимРаботыToolStripMenuItem.Size = new System.Drawing.Size(101, 20);
            this.режимРаботыToolStripMenuItem.Text = "Режим работы";
            // 
            // нормальныйРежимToolStripMenuItem
            // 
            this.нормальныйРежимToolStripMenuItem.Name = "нормальныйРежимToolStripMenuItem";
            this.нормальныйРежимToolStripMenuItem.Size = new System.Drawing.Size(319, 22);
            this.нормальныйРежимToolStripMenuItem.Text = "Нормальный режим";
            this.нормальныйРежимToolStripMenuItem.Click += new System.EventHandler(this.нормальныйРежимToolStripMenuItem_Click);
            // 
            // выведенаВРемонтВЛ220КВАгломерат1ToolStripMenuItem
            // 
            this.выведенаВРемонтВЛ220КВАгломерат1ToolStripMenuItem.Enabled = false;
            this.выведенаВРемонтВЛ220КВАгломерат1ToolStripMenuItem.Name = "выведенаВРемонтВЛ220КВАгломерат1ToolStripMenuItem";
            this.выведенаВРемонтВЛ220КВАгломерат1ToolStripMenuItem.Size = new System.Drawing.Size(319, 22);
            this.выведенаВРемонтВЛ220КВАгломерат1ToolStripMenuItem.Text = "Выведена в ремонт ВЛ 220 кВ «Агломерат 1»";
            this.выведенаВРемонтВЛ220КВАгломерат1ToolStripMenuItem.Click += new System.EventHandler(this.выведенаВРемонтВЛ220КВАгломерат1ToolStripMenuItem_Click);
            // 
            // выведенаВРемонтВЛ220КВАгломерат2ToolStripMenuItem
            // 
            this.выведенаВРемонтВЛ220КВАгломерат2ToolStripMenuItem.Enabled = false;
            this.выведенаВРемонтВЛ220КВАгломерат2ToolStripMenuItem.Name = "выведенаВРемонтВЛ220КВАгломерат2ToolStripMenuItem";
            this.выведенаВРемонтВЛ220КВАгломерат2ToolStripMenuItem.Size = new System.Drawing.Size(319, 22);
            this.выведенаВРемонтВЛ220КВАгломерат2ToolStripMenuItem.Text = "Выведена в ремонт ВЛ 220 кВ «Агломерат 2»";
            this.выведенаВРемонтВЛ220КВАгломерат2ToolStripMenuItem.Click += new System.EventHandler(this.выведенаВРемонтВЛ220КВАгломерат2ToolStripMenuItem_Click);
            // 
            // пользовательToolStripMenuItem
            // 
            this.пользовательToolStripMenuItem.Name = "пользовательToolStripMenuItem";
            this.пользовательToolStripMenuItem.Size = new System.Drawing.Size(96, 20);
            this.пользовательToolStripMenuItem.Text = "Пользователь";
            // 
            // результатыToolStripMenuItem
            // 
            this.результатыToolStripMenuItem.Name = "результатыToolStripMenuItem";
            this.результатыToolStripMenuItem.Size = new System.Drawing.Size(81, 20);
            this.результатыToolStripMenuItem.Text = "Результаты";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Image = global::WSR.Properties.Resources.Схема;
            this.pictureBox1.Location = new System.Drawing.Point(27, 55);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(408, 406);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 250);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "label1";
            // 
            // richTextBox1
            // 
            this.richTextBox1.HideSelection = false;
            this.richTextBox1.Location = new System.Drawing.Point(27, 467);
            this.richTextBox1.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(408, 61);
            this.richTextBox1.TabIndex = 3;
            this.richTextBox1.Text = "";
            this.richTextBox1.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(459, 540);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem режимРаботыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem пользовательToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem результатыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem новыйToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem нормальныйРежимToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выведенаВРемонтВЛ220КВАгломерат1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выведенаВРемонтВЛ220КВАгломерат2ToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.RichTextBox richTextBox1;
    }
}

