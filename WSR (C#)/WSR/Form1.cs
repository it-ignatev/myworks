﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Drawing.Drawing2D;

namespace WSR
{
    public partial class Form1 : Form
    {
        private static Image img;
        public static PictureBox pic;
        static bool blink = true;
        bool work = false;
        int mode = 0;

        public Form1()
        {
            InitializeComponent();
            pic = pictureBox1;

            CoordinatesSetup();

            img = pictureBox1.Image;
            pictureBox1.Paint += pictureBox1_Paint;

            Vars.Blink = new Thread(() => Threads.Blinking(1000));
            Vars.Blink.IsBackground = true;
            Vars.Blink.Start();
        }

        private void нормальныйРежимToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (work)
            {
                richTextBox1.AppendText("Переход в нормальный режим...\r\n");
                нормальныйРежимToolStripMenuItem.Enabled = false;
                pic.Enabled = true;
                return;
            }

            нормальныйРежимToolStripMenuItem.Enabled = false;
            выведенаВРемонтВЛ220КВАгломерат1ToolStripMenuItem.Enabled = true;
            выведенаВРемонтВЛ220КВАгломерат2ToolStripMenuItem.Enabled = true;
            pic.Enabled = false;

            if (Vars.Blink != null) if(Vars.Blink.IsAlive)Vars.Blink.Abort();
            blink = false;
            richTextBox1.Clear();

            richTextBox1.AppendText("Переход в нормальный режим осуществлен !\r\n");

            foreach (ListOfCoords t in Vars.Coordinates)
            {
                t.color = "red";

                if (t.name == "СМВ1" || t.name == "СМВ2") t.color = "green";
            }

            pic.Refresh();
            pic.Invalidate();
        }

        //Координаты объектов
        private void CoordinatesSetup()
        {
            Vars.Coordinates = new List<ListOfCoords>();

            Vars.Coordinates.Add(new ListOfCoords(22, 62, 20, 20, "ОД2", true, null));
            Vars.Coordinates.Add(new ListOfCoords(182, 62, 20, 20, "ВВ220кВ", true, null));
            Vars.Coordinates.Add(new ListOfCoords(126, 62, 20, 20, "ОД1", true, null));
            Vars.Coordinates.Add(new ListOfCoords(270, 62, 20, 20, "ОД3", true, null));
            Vars.Coordinates.Add(new ListOfCoords(258, 94, 48, 44, "Т3", true, null));
            Vars.Coordinates.Add(new ListOfCoords(124, 94, 30, 44, "Т1", true, null));
            Vars.Coordinates.Add(new ListOfCoords(15, 94, 30, 44, "Т2", true, null));
            Vars.Coordinates.Add(new ListOfCoords(326, 46, 20, 20, "ВВ220кВ2", true, null));
            Vars.Coordinates.Add(new ListOfCoords(22, 150, 20, 20, "ТР2", true, null));
            Vars.Coordinates.Add(new ListOfCoords(254, 150, 20, 20, "ТР1", true, null));
            Vars.Coordinates.Add(new ListOfCoords(22, 190, 20, 20, "В2", true, null));
            Vars.Coordinates.Add(new ListOfCoords(78, 190, 20, 20, "СМВ1", true, null));
            Vars.Coordinates.Add(new ListOfCoords(254, 190, 20, 20, "В1", true, null));
            Vars.Coordinates.Add(new ListOfCoords(254, 222, 20, 20, "ШР1", true, null));
            Vars.Coordinates.Add(new ListOfCoords(22, 222, 20, 20, "ШР2", true, null));
            Vars.Coordinates.Add(new ListOfCoords(22, 278, 20, 20, "ТР4", true, null));
            Vars.Coordinates.Add(new ListOfCoords(238, 278, 20, 20, "ТР3", true, null));
            Vars.Coordinates.Add(new ListOfCoords(238, 310, 20, 20, "В3", true, null));
            Vars.Coordinates.Add(new ListOfCoords(182, 310, 20, 20, "СМВ2", true, null));
            Vars.Coordinates.Add(new ListOfCoords(22, 310, 20, 20, "В4", true, null));
            Vars.Coordinates.Add(new ListOfCoords(22, 350, 20, 20, "ШР4", true, null));
            Vars.Coordinates.Add(new ListOfCoords(238, 350, 20, 20, "ШР3", true, null));

        }

        //Отрисовка
        public static void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = Graphics.FromImage(img);
            g.Clear(Color.White);

            g.DrawImage(Properties.Resources.Схема, 0, 0);

            List<ListOfCoords> Coordinates = Vars.Coordinates;

            if (blink)
            {
                for (int i = 0; i < Coordinates.Count; i++)
                {
                    if (Coordinates[i].blink)
                    {
                        g.FillRectangle(new SolidBrush(Color.FromArgb(180, 255, 0, 0)), new Rectangle(Coordinates[i].x, Coordinates[i].y, Coordinates[i].width, Coordinates[i].height));
                        Coordinates[i].blink = false;
                    }
                    else
                    {
                        g.FillRectangle(new SolidBrush(Color.FromArgb(180, 0, 255, 0)), new Rectangle(Coordinates[i].x, Coordinates[i].y, Coordinates[i].width, Coordinates[i].height));
                        Coordinates[i].blink = true;
                    }
                }
            }
            else
            {
                for (int i = 0; i < Coordinates.Count; i++)
                {
                    Color color = (Coordinates[i].color == "green") ? Color.FromArgb(180, 0, 255, 0) : Color.FromArgb(180, 255, 0, 0);
                    g.FillRectangle(new SolidBrush(color), new Rectangle(Coordinates[i].x, Coordinates[i].y, Coordinates[i].width, Coordinates[i].height));
                }
            }
        }

        private void новыйToolStripMenuItem_Click(object sender, EventArgs e)
        {
            нормальныйРежимToolStripMenuItem.Enabled = true;
            work = false;

            Vars.Blink = new Thread(() => Threads.Blinking(1000));
            Vars.Blink.IsBackground = true;
            Vars.Blink.Start();
            blink = true;
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void CheckModes()
        {
            if (mode != 0)
            {
                if (mode == 11)
                {
                    int success = 11;

                    foreach (ListOfCoords t in Vars.Coordinates)
                    {
                        if (t.name == "СМВ1" && t.color == "red") success -= 1;
                        if (t.name == "В1" && t.color == "green") success -= 1;
                        if (t.name == "ТР1" && t.color == "green") success -= 1;
                        if (t.name == "ШР1" && t.color == "green") success -= 1;
                        if (t.name == "СМВ2" && t.color == "red") success -= 1;
                        if (t.name == "В3" && t.color == "green") success -= 1;
                        if (t.name == "ТР3" && t.color == "green") success -= 1;
                        if (t.name == "ШР3" && t.color == "green") success -= 1;
                        if (t.name == "ВВ220кВ2" && t.color == "green") success -= 1;
                        if (t.name == "Т3" && t.color == "green") success -= 1;
                        if (t.name == "ОД3" && t.color == "green") success -= 1;
                    }

                    if (success == 0)
                    {
                        richTextBox1.AppendText("Переход в нормальный режим осуществлен !\r\n");
                        pic.Enabled = false;
                        нормальныйРежимToolStripMenuItem.Enabled = false;
                        выведенаВРемонтВЛ220КВАгломерат1ToolStripMenuItem.Enabled = true;
                        выведенаВРемонтВЛ220КВАгломерат2ToolStripMenuItem.Enabled = true;
                        mode = 1;
                    }
                }

                if (mode == 12)
                {
                    int success = 11;

                    foreach (ListOfCoords t in Vars.Coordinates)
                    {
                        if (t.name == "СМВ1" && t.color == "red") success -= 1;
                        if (t.name == "В2" && t.color == "green") success -= 1;
                        if (t.name == "ТР4" && t.color == "green") success -= 1;
                        if (t.name == "ШР4" && t.color == "green") success -= 1;
                        if (t.name == "СМВ2" && t.color == "red") success -= 1;
                        if (t.name == "В4" && t.color == "green") success -= 1;
                        if (t.name == "ТР2" && t.color == "green") success -= 1;
                        if (t.name == "ШР2" && t.color == "green") success -= 1;
                        if (t.name == "ВВ220кВ" && t.color == "green") success -= 1;
                        if (t.name == "ОД2" && t.color == "green") success -= 1;
                        if (t.name == "ОД1" && t.color == "green") success -= 1;
                    }

                    if (success == 0)
                    {
                        richTextBox1.AppendText("Переход в нормальный режим осуществлен !\r\n");
                        pic.Enabled = false;
                        нормальныйРежимToolStripMenuItem.Enabled = false;
                        выведенаВРемонтВЛ220КВАгломерат1ToolStripMenuItem.Enabled = true;
                        выведенаВРемонтВЛ220КВАгломерат2ToolStripMenuItem.Enabled = true;
                        mode = 1;
                    }
                }

                if (mode == 2)
                {
                    int success = 11;

                    foreach (ListOfCoords t in Vars.Coordinates)
                    {
                        if (t.name == "СМВ1" && t.color == "green") success -= 1;
                        if (t.name == "В1" && t.color == "red") success -= 1;
                        if (t.name == "ТР1" && t.color == "red") success -= 1;
                        if (t.name == "ШР1" && t.color == "red") success -= 1;
                        if (t.name == "СМВ2" && t.color == "green") success -= 1;
                        if (t.name == "В3" && t.color == "red") success -= 1;
                        if (t.name == "ТР3" && t.color == "red") success -= 1;
                        if (t.name == "ШР3" && t.color == "red") success -= 1;
                        if (t.name == "ВВ220кВ2" && t.color == "red") success -= 1;
                        if (t.name == "Т3" && t.color == "red") success -= 1;
                        if (t.name == "ОД3" && t.color == "red") success -= 1;
                    }

                    if (success == 0)
                    {
                        richTextBox1.AppendText("Переход в режим \"Выведена в ремонт ВЛ 220 кВ «Агломерат 1»\" осуществлен !\r\n");
                        pic.Enabled = false;
                        нормальныйРежимToolStripMenuItem.Enabled = true;
                        mode = 11;
                    }
                }

                if (mode == 3)
                {
                    int success = 11;

                    foreach (ListOfCoords t in Vars.Coordinates)
                    {
                        if (t.name == "СМВ1" && t.color == "green") success -= 1;
                        if (t.name == "В2" && t.color == "red") success -= 1;
                        if (t.name == "ТР2" && t.color == "red") success -= 1;
                        if (t.name == "ШР2" && t.color == "red") success -= 1;
                        if (t.name == "СМВ2" && t.color == "green") success -= 1;
                        if (t.name == "В4" && t.color == "red") success -= 1;
                        if (t.name == "ТР4" && t.color == "red") success -= 1;
                        if (t.name == "ШР4" && t.color == "red") success -= 1;
                        if (t.name == "ВВ220кВ" && t.color == "red") success -= 1;
                        if (t.name == "ОД1" && t.color == "red") success -= 1;
                        if (t.name == "ОД2" && t.color == "red") success -= 1;
                    }

                    if (success == 0)
                    {
                        richTextBox1.AppendText("Переход в режим \"Выведена в ремонт ВЛ 220 кВ «Агломерат 2»\" осуществлен !\r\n");
                        pic.Enabled = false;
                        нормальныйРежимToolStripMenuItem.Enabled = true;
                        mode = 12;
                    }
                }

            }
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            if (blink) return;

            foreach (ListOfCoords t in Vars.Coordinates)
            {
                Rectangle rc = new Rectangle(t.x, t.y, t.width, t.height);
                if (rc.Contains(e.X, e.Y))
                {
                    t.color = (t.color == "green") ? "red" : "green";
                    richTextBox1.AppendText("Состояние " + t.name + " было изменено(на " + ((t.color == "green") ? "Вкл." : "Выкл.")+ ")\r\n");
                    pic.Refresh();
                    pic.Invalidate();
                    break;
                }
            }

            CheckModes();
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void выведенаВРемонтВЛ220КВАгломерат1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.AppendText("Переход в режим \"Выведена в ремонт ВЛ 220 кВ «Агломерат 1»\"...\r\n");
            выведенаВРемонтВЛ220КВАгломерат1ToolStripMenuItem.Enabled = false;
            выведенаВРемонтВЛ220КВАгломерат2ToolStripMenuItem.Enabled = false;
            нормальныйРежимToolStripMenuItem.Enabled = false;
            work = true;
            pic.Enabled = true;
            mode = 2;
            CheckModes();
        }

        private void выведенаВРемонтВЛ220КВАгломерат2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.AppendText("Переход в режим \"Выведена в ремонт ВЛ 220 кВ «Агломерат 2»\"...\r\n");
            выведенаВРемонтВЛ220КВАгломерат1ToolStripMenuItem.Enabled = false;
            выведенаВРемонтВЛ220КВАгломерат2ToolStripMenuItem.Enabled = false;
            нормальныйРежимToolStripMenuItem.Enabled = false;
            work = true;
            pic.Enabled = true;
            mode = 3;
            CheckModes();
        }
    }
}
