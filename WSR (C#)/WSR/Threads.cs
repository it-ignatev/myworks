﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;

namespace WSR
{
    class Threads
    {
        public static void Blinking (int i)
        {
            while (true)
            {
                Form1.pictureBox1_Paint(null, null);
                Form1.pic.Invoke((MethodInvoker)(() => Form1.pic.Refresh()));
                Form1.pic.Invoke((MethodInvoker)(() => Form1.pic.Invalidate()));
                new ManualResetEvent(false).WaitOne(i);
            }
        }
    }
}
