﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class ListOfCoords
{
    public ListOfCoords(int x, int y, int width, int height, string name, bool b, string color)
    {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.name = name;
        this.blink = b;
        this.color = color;
    }

    public int x { get; set; }
    public int y { get; set; }
    public int width { get; set; }
    public int height { get; set; }
    public string name { get; set; }
    public bool blink { get; set; }
    public string color { get; set; }
}

