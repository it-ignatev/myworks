﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using System.Diagnostics;
using System.Net;
using Microsoft.Win32;
using System.Security.Cryptography;
using System.Threading;
using System.Reflection;
using System.Resources;
using System.IO.Compression;
using System.Collections.Generic;
using System.Resources;

namespace Launcher___BADTEAM
{
    public partial class Main : Form
    {
        public string appData;
        private string Game;
        protected bool start;
        private int downloadedfiles;
        WebClient webclient = new WebClient();
        private string clientn;
        private string java;
        private string memory;
        public static string UniqCode;
        private string login;
        Thread srv;
        public static Main self;
       
        public Main()
        {
            InitializeComponent();
            self = this;
            appData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

            CheckRegistry();

            Select();

        }


        private void CheckRegistry()
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\Badteam", true);

            if (key != null)
            {
                if (key.GetValue("memory") == null)
                {
                    key.SetValue("memory", "900");
                }

                if (key.GetValue("java") == null)
                {
                    key.SetValue("java", "C:\\Program Files\\Java\\jre7\\");
                }

                login = Form1.self.textBox1.Text;
            }

            key.Close();

            Vote();
        }

        private void Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                SocketControl.work = false;
                SocketControl.self.STOPALL();

                if (srv.IsAlive)
                {
                    srv.Abort();
                }
            }
            catch { }

            Form1.self.Show();
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private bool CheckInstalled(string path)
        {
            if (Directory.Exists(path) || File.Exists(path))
            {
                return true;
            }

            return false;
        }

        private void Launch(string client, string session, string user, bool admin, bool console)
        {
            var dir = new DirectoryInfo(appData + "\\.badteam\\" + client + "\\libraries\\");
            var files = new List<String>();

            RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\Badteam", true);

            java = (string)key.GetValue("java");
            memory = (string)key.GetValue("memory");

            key.Close();

            if(!File.Exists(java + "javaw.exe"))
            {
                MessageBox.Show("Java не найдена !");

                return;
            }

            FileInfo[] finfo = dir.GetFiles("*.jar", SearchOption.AllDirectories);

            foreach (var x in finfo)
            {
                files.Add("libraries\\" + (x.FullName.Replace(dir.FullName, "")));
            }

            string lib = string.Join(";", files.ToArray());

            if (console)
            {
                java = java + "java.exe";
            }
            else
            {
                java = java + "javaw.exe";
            }

            Directory.SetCurrentDirectory(appData + @"\\.badteam\\" + client + @"\\");

            //Clipboard.SetText("\"" + java + "\" -Xincgc -noverify -Dfml.ignoreInvalidMinecraftCertificates=true -Dfml.ignorePatchDiscrepancies=true -Xms" + memory + "m -Xmx" + memory + "m -Djava.library.path=\"bin\\natives\" -cp \"" + lib + ";bin\\minecraft.jar;\" net.minecraft.launchwrapper.Launch --version Forge9.11.0.884  --username " + user + " --session " + session + " --assetsDir assets\\ --tweakClass cpw.mods.fml.common.launcher.FMLTweaker");
            
            //Process.Start(java + "javaw.exe", "-Xincgc -noverify -Dfml.ignoreInvalidMinecraftCertificates=true -Dfml.ignorePatchDiscrepancies=true -Xms" + memory + "m -Xmx" + memory + "m -Djava.library.path=bin\\natives -cp " + lib + ";bin\\minecraft.jar; net.minecraft.launchwrapper.Launch --version Forge9.11.0.884  --username " + user + " --accessToken " + user + " --uuid " + session + " --session " + session + " --assetsDir assets\\ --tweakClass cpw.mods.fml.common.launcher.FMLTweaker");
            try
            {
                ProcessStartInfo mcStartInfo = new ProcessStartInfo("\"" + java + "\"", @"-Xincgc -noverify -Dfml.ignoreInvalidMinecraftCertificates=true -Dfml.ignorePatchDiscrepancies=true -Xms" + memory + "m -Xmx" + memory + "m -Djava.library.path=\"bin\\natives\" -cp \"" + lib + ";bin\\minecraft.jar;\" net.minecraft.launchwrapper.Launch --version 1.7.10 --username " + user + " --session " + session + " --accessToken " + user + " --uuid " + session + " --assetIndex 1.7.10 --assetsDir assets\\ --tweakClass cpw.mods.fml.common.launcher.FMLTweaker --userProperties {} --userType mojang");
                mcStartInfo.UseShellExecute = false;
                Process.Start(mcStartInfo);
            }
            catch (Win32Exception e)
            {
                MessageBox.Show(e.ToString());
            }

            WorkSlience();
        }

        public void Mes(string s)
        {
            MessageBox.Show(s);
        }
        
        private void groupBox1_ParentChanged(object sender, EventArgs e)
        {
            
        }

        private string GenUniq(int count)
        {
            Random rnd = new Random();

            string[] letters = {"Q","W", "E", "R", "T", "Y", "U", "I", "O", "P", "A", "S", "D", "F", "G", "H", "J", "K", "L", "Z", "X", "C", "V", "B", "N", "M", "q", "w", "e","r","t","y","u","i","o","p","a","s","d","f","g","h","j","k","l","z","x", "c", "v", "b","n", "m", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
            string uniq="";

            for (int i = 0; i < count; i++)
            {
                uniq += letters[rnd.Next(letters.Length)];
            }

            return uniq;
        }

        private void UnZip(string client, string archive)
        {
            
            // Open an existing zip file for reading
            ZipStorer zip = ZipStorer.Open(appData + "/.badteam/" + client + "/bin/" + archive + ".zip", FileAccess.Read);

            // Read the central directory collection
            List<ZipStorer.ZipFileEntry> dir = zip.ReadCentralDir();

            string path;
            // Look for the desired file
            foreach (ZipStorer.ZipFileEntry entry in dir)
            {
                path = appData + "/.badteam/" + client + "/" + entry.FilenameInZip;
                zip.ExtractFile(entry, path);
            }

            zip.Close();

        }

        private void WorkSlience()
        {
            srv = new Thread(RunSock);
            srv.IsBackground = true;
            srv.Start();

            this.Hide();

            timer1.Enabled = true;
        }

        public void Mesa(string ses)
        {
            MessageBox.Show(ses);
        }

        private void RunSock()
        {
            new SocketControl();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            Start("hitech");
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            Start("tfc");
        }

        private void Start(string client)
        {
            button3.Enabled = false;
            radioButton2.Enabled = false;
            rb_ht2.Enabled = false;

            if (!CheckInstalled(appData + "/.badteam/"))
            {
                Directory.CreateDirectory(appData + "/.badteam/");
            }

            if (!CheckInstalled(appData + "/.badteam/" + client + "/") || !CheckInstalled(appData + "/.badteam/" + client + "/bin/"))
            {                    
                Directory.CreateDirectory(appData + "/.badteam/" + client + "/bin/");

                InstallGame(client);

                return;
            }

            if (!CheckInstalled(appData + "/.badteam/" + client + "/bin/minecraft.jar") || !CheckInstalled(appData + "/.badteam/" + client + "/bin/client.zip") || !CheckInstalled(appData + "/.badteam/" + client + "/bin/assets.zip"))
            {
                Clear(client, true, true, true, true);
                InstallGame(client);

                return;
            }
 
            if (GetMD5FilesLocal(client).ToUpper() != GetMD5FilesWeb(client).ToUpper())
            {
                Clear(client, true, true, true, true);
                InstallGame(client);

                return;
            }

            if (!CheckInstalled(appData + "/.badteam/" + client + "/assets/"))
            {
                UnZip(client, "assets");
            }

            if (!CheckInstalled(appData + "/.badteam/" + client + "/mods/"))
            {
                Clear(client, false, false, true, true);
                UnZip(client, "client");
            }

            Game = client;

            button3.Enabled = true;
            radioButton2.Enabled = true;
            rb_ht2.Enabled = true;
        }

        private void Clear(string client, bool bin, bool mods, bool config, bool libs)
        {
            if (bin)
            {
                try
                {
                    Directory.Delete(appData + "/.badteam/" + client + "/bin/", true);
                }
                catch (Exception)
                {
                    // throw;
                }
            }

            if (mods)
            {
                try
                {
                    Directory.Delete(appData + "/.badteam/" + client + "/mods/", true);
                }
                catch (Exception)
                {
                   // throw;
                }
            }

            if (config)
            {
                try
                {
                    Directory.Delete(appData + "/.badteam/" + client + "/config/", true);
                }
                catch (Exception)
                {
                  //  throw;
                }
            }

            if (libs)
            {
                try
                {
                    Directory.Delete(appData + "/.badteam/" + client + "/libraries/", true);
                }
                catch (Exception)
                {
                    //  throw;
                }
            }
        }

        private string GetMD5FilesWeb(string client)
        {
            return POST("http://badteam.ru/client/launcher/launcher.php", "version=6.0&client=" + client);
        }

        private string GetMD5FilesLocal(string client)
        {
            string hash;

            hash = ComputeMD5Checksum(appData + "/.badteam/" + client + "/bin/minecraft.jar");
            hash += ComputeMD5Checksum(appData + "/.badteam/" + client + "/bin/client.zip");
            hash += ComputeMD5Checksum(appData + "/.badteam/" + client + "/bin/assets.zip");

            return hash;
        }

        private void Vote()
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\Badteam", true);

            if (key != null)
            {
                if ((string)key.GetValue("vote") != "")
                {
                    DateTime votetime, now;

                    votetime = Convert.ToDateTime((string)key.GetValue("vote"));
                    now = DateTime.Now;

                    if (DateTime.Compare(now, votetime) >= 0)
                    {
                        DialogResult res = MessageBox.Show("Вы хотите проголосовать за сервер ? (Вы получите 10 монет в личный кабинет)", "Голосование", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (res == DialogResult.Yes)
                        {
                            Process.Start("http://topcraft.ru/servers/162");

                            key.SetValue("vote", DateTime.Now.AddDays(1).ToString());
                        }
                    }
                }
                else
                {
                    DialogResult res = MessageBox.Show("Вы хотите проголосовать за сервер ? (Вы получите 10 монет в личный кабинет)", "Голосование", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (res == DialogResult.Yes)
                    {
                        Process.Start("http://topcraft.ru/servers/162");

                        key.SetValue("vote", DateTime.Now.AddDays(1).ToString());
                    }
                }
            }

            key.Close();
        }

        private string ComputeMD5Checksum(string path)
        {
           
                using (FileStream fs = System.IO.File.OpenRead(path))
                {
                    try
                    {
                        MD5 md5 = new MD5CryptoServiceProvider();
                        byte[] fileData = new byte[fs.Length];
                        fs.Read(fileData, 0, (int)fs.Length);
                        byte[] checkSum = md5.ComputeHash(fileData);
                        string result = BitConverter.ToString(checkSum).Replace("-", String.Empty);
                        return result;
                    }
                    catch { }
                }

                return "0";
        }

        private string POST(string Url, string Data)
        {
            WebRequest req = WebRequest.Create(Url);
            req.Method = "POST";
            req.Timeout = 100000;
            req.ContentType = "application/x-www-form-urlencoded";
            byte[] sentData = Encoding.Default.GetBytes(Data);
            req.ContentLength = sentData.Length;

            Stream sendStream = req.GetRequestStream();
            sendStream.Write(sentData, 0, sentData.Length);
            sendStream.Close();

            WebResponse res = req.GetResponse();
            Stream ReceiveStream = res.GetResponseStream();
            StreamReader sr = new StreamReader(ReceiveStream, Encoding.UTF8);
            //Кодировка указывается в зависимости от кодировки ответа сервера
            Char[] read = new Char[256];
            int count = sr.Read(read, 0, 256);
            string Out = String.Empty;
            while (count > 0)
            {
                String str = new String(read, 0, count);
                Out += str;
                count = sr.Read(read, 0, 256);
            }
            return Out;
        }

        private void InstallGame(string client)
        {
            radioButton2.Enabled = false;
            rb_ht2.Enabled = false;
            start = false;

            webclient.DownloadProgressChanged += webclient_DownloadProgressChanged;
            webclient.DownloadFileCompleted += webclient_DownloadFileCompleted;

            downloadedfiles = 0;

            DownloadFile(0, client);
            clientn = client;
        }

        private void DownloadFile(int file, string client)
        {
            Random rnd = new Random();
            int server = rnd.Next(3);
            string url = null;

            if (server == 0)
            {
                url = "http://badteam.ru/client/";
            }

            if (server == 1)
            {
                url = "http://5.34.176.136/bad/";
            }

            if (server == 2)
            {
                url = "http://dl.dropboxusercontent.com/u/48129448/Badteam_Client/New/";
            }

            if (file == 0)
            {
                webclient.DownloadFileAsync(new Uri(url + client + "/minecraft.jar"), appData + "/.badteam/" + client + "/bin/minecraft.jar");
            }
            
            if (file == 1)
            {
                webclient.DownloadFileAsync(new Uri(url + client + "/client.zip"), appData + "/.badteam/" + client + "/bin/client.zip");
            }

            if (file == 2)
            {
                webclient.DownloadFileAsync(new Uri(url + client + "/assets.zip"), appData + "/.badteam/" + client + "/bin/assets.zip");
            }
        }

        private void webclient_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            progressBar1.Maximum = (int)e.TotalBytesToReceive;
            progressBar1.Value = (int)e.BytesReceived;
        }

        private void webclient_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                start = false;
                // handle error scenario
                throw e.Error;
            }
            if (e.Cancelled)
            {
                start = false;
                // handle cancelled scenario
            }

            downloadedfiles += 1;

            if (downloadedfiles == 1)
            {
                DownloadFile(1, clientn);
            }

            if (downloadedfiles == 2)
            {
                if (!File.Exists(appData + "/.badteam/" + clientn + "/bin/assets.zip"))
                {
                    DownloadFile(2, clientn);
                }
                else
                {
                    downloadedfiles += 1;
                }
            }

            if (downloadedfiles >= 3)
            {
                Clear(clientn, false, true, true, true);
                start = true;
                Start(clientn);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var runningProcs = from proc in Process.GetProcesses(".") orderby proc.Id select proc;
            if (runningProcs.Count(p => p.ProcessName.Contains("javaw")) > 0 && login != "bad")
            {
                MessageBox.Show("Игра уже запущена !");
                return;
            }

            if (Game != null)
            {
                radioButton2.Enabled = false;
                rb_ht2.Enabled = false;
                button3.Enabled = false;

                UniqCode = GenUniq(32);

                if (Form1.self.textBox1.Text == "bad")
                {
                    DialogResult res = MessageBox.Show("Запустить консоль ?", "Консоль", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (DialogResult.Yes == res)
                    {
                        Launch(Game, UniqCode, login, false, true);
                    }
                    else
                    {
                        Launch(Game, UniqCode, login, false, false);
                    }
                }
                else
                {
                    Launch(Game, UniqCode, login, false, false);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frm_settings frm_setting = new frm_settings();
            frm_setting.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Process.Start("http://badteam.ru/client/launcher/shop.php?u=" +  Form1.self.textBox1.Text + "&p=" + Form1.self.textBox2.Text + "&t=i&s=h1");
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            var runningProcs = from proc in Process.GetProcesses(".") orderby proc.Id select proc;
            if (runningProcs.Count(p => p.ProcessName.Contains("javaw")) == 0)
            {
                Application.Exit();
            }
        }

        private void rb_ht2_CheckedChanged(object sender, EventArgs e)
        {
            Start("hitech2");
        }

        private void radioButton1_CheckedChanged_1(object sender, EventArgs e)
        {
            //Launch("d", "123", "bad", true, true);
            Start("hitech_1_7_10");
        }

        private void avcraft_CheckedChanged(object sender, EventArgs e)
        {
            Start("average_craft");
        }

    }
}
