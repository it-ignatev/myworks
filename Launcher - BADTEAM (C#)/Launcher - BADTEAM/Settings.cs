﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using System.IO;

namespace Launcher___BADTEAM
{
    public partial class frm_settings : Form
    {
        public frm_settings()
        {
            InitializeComponent();

            Load();
        }

        private void Load()
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\Badteam", true);

            if (key != null)
            {
                nud_memory.Text = (string)key.GetValue("memory");

                string path = Environment.GetFolderPath(Environment.SpecialFolder.System);
                path = Path.GetPathRoot(path);

                cb_java.Items.Add(path + "Program Files\\Java\\jre7\\bin\\");
                cb_java.Items.Add(path + "Program Files\\Java\\jre8\\bin\\");
                cb_java.Items.Add(path + "Program Files (x86)\\Java\\jre7\\bin\\");               
                cb_java.Items.Add(path + "Program Files (x86)\\Java\\jre8\\bin\\");
                cb_java.Items.Add(path + "Windows\\SysWOW64\\");
                cb_java.Items.Add(path + "Windows\\System32\\");
                cb_java.SelectedItem = 1;
            }

            key.Close();
        }

        private void but_save_Click(object sender, EventArgs e)
        {
            if (cb_java.SelectedIndex != -1)
            {
                if (File.Exists(cb_java.SelectedItem + "javaw.exe"))
                {
                    RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\Badteam", true);

                    key.SetValue("memory", nud_memory.Value.ToString());
                    key.SetValue("java", cb_java.SelectedItem.ToString());

                    key.Close();

                    this.Dispose();
                }
                else
                {
                    MessageBox.Show("Java не найдена !");
                }
            }
        }
    }
}
