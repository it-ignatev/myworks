﻿namespace Launcher___BADTEAM
{
    partial class frm_settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_settings));
            this.but_save = new System.Windows.Forms.Button();
            this.lbl_memory = new System.Windows.Forms.Label();
            this.lbl_java = new System.Windows.Forms.Label();
            this.cb_java = new System.Windows.Forms.ComboBox();
            this.nud_memory = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.nud_memory)).BeginInit();
            this.SuspendLayout();
            // 
            // but_save
            // 
            this.but_save.Location = new System.Drawing.Point(185, 97);
            this.but_save.Name = "but_save";
            this.but_save.Size = new System.Drawing.Size(254, 37);
            this.but_save.TabIndex = 0;
            this.but_save.Text = "Сохранить";
            this.but_save.UseVisualStyleBackColor = true;
            this.but_save.Click += new System.EventHandler(this.but_save_Click);
            // 
            // lbl_memory
            // 
            this.lbl_memory.AutoSize = true;
            this.lbl_memory.Location = new System.Drawing.Point(12, 9);
            this.lbl_memory.Name = "lbl_memory";
            this.lbl_memory.Size = new System.Drawing.Size(167, 21);
            this.lbl_memory.TabIndex = 2;
            this.lbl_memory.Text = "Оперативная память: ";
            // 
            // lbl_java
            // 
            this.lbl_java.AutoSize = true;
            this.lbl_java.Location = new System.Drawing.Point(12, 54);
            this.lbl_java.Name = "lbl_java";
            this.lbl_java.Size = new System.Drawing.Size(162, 21);
            this.lbl_java.TabIndex = 3;
            this.lbl_java.Text = "Использование Java: ";
            // 
            // cb_java
            // 
            this.cb_java.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_java.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cb_java.FormattingEnabled = true;
            this.cb_java.Location = new System.Drawing.Point(185, 51);
            this.cb_java.Name = "cb_java";
            this.cb_java.Size = new System.Drawing.Size(254, 29);
            this.cb_java.TabIndex = 4;
            // 
            // nud_memory
            // 
            this.nud_memory.Location = new System.Drawing.Point(185, 7);
            this.nud_memory.Maximum = new decimal(new int[] {
            4096,
            0,
            0,
            0});
            this.nud_memory.Minimum = new decimal(new int[] {
            900,
            0,
            0,
            0});
            this.nud_memory.Name = "nud_memory";
            this.nud_memory.Size = new System.Drawing.Size(254, 29);
            this.nud_memory.TabIndex = 5;
            this.nud_memory.Value = new decimal(new int[] {
            900,
            0,
            0,
            0});
            // 
            // frm_settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(449, 142);
            this.Controls.Add(this.nud_memory);
            this.Controls.Add(this.cb_java);
            this.Controls.Add(this.lbl_java);
            this.Controls.Add(this.lbl_memory);
            this.Controls.Add(this.but_save);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_settings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Badteam - Launcher";
            ((System.ComponentModel.ISupportInitialize)(this.nud_memory)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button but_save;
        private System.Windows.Forms.Label lbl_memory;
        private System.Windows.Forms.Label lbl_java;
        private System.Windows.Forms.ComboBox cb_java;
        private System.Windows.Forms.NumericUpDown nud_memory;
    }
}