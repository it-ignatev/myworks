﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Diagnostics;

namespace Launcher___BADTEAM
{
    public partial class frm_launcherupdate : Form
    {
        bool update;

        public frm_launcherupdate()
        {
            InitializeComponent();
        }

        private void frm_launcherupdate_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (update)
            {
                Application.Exit();
            }
            else
            {
                MessageBox.Show("Обновить лаунчер ! Запуск игры не возможен.");
                Application.Exit();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex != -1)
            {
                button1.Enabled = false;
                comboBox1.Enabled = false;

                Download();
            }
            else
            {
                MessageBox.Show("Выберите сервер !");
            }
        }

        private void Download()
        {
            WebClient webclient = new WebClient();

            webclient.DownloadProgressChanged += webclient_DownloadProgressChanged;
            webclient.DownloadFileCompleted += webclient_DownloadFileCompleted;

            File.Move(Application.ExecutablePath, Application.StartupPath + "\\Badteam.bak");

            if (comboBox1.SelectedIndex == 1 || comboBox1.SelectedIndex == 0)
            {
                webclient.DownloadFileAsync(new Uri("http://dl.dropboxusercontent.com/u/48129448/Badteam%20launcher/Badteam.exe"), Application.StartupPath + "\\Badteam.exe");
            }

            if (comboBox1.SelectedIndex == 2)
            {
                webclient.DownloadFileAsync(new Uri("http://5.34.176.136/bad/Badteam.exe"), Application.StartupPath + "\\Badteam.exe");
            }
        }

        private void webclient_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            progressBar1.Maximum = (int)e.TotalBytesToReceive;
            progressBar1.Value = (int)e.BytesReceived;
        }

        private void webclient_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                throw e.Error;
            }
            if (e.Cancelled)
            {

            }

            update = true;

            Process.Start(Application.StartupPath + "\\Badteam.exe");

            Application.Exit();
        }
    }
}
