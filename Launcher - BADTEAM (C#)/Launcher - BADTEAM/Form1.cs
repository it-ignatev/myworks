﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using Microsoft.Win32;
using System.Net;
using System.IO;
using System.Security.Cryptography;
using System.Reflection;
using System.Resources;

namespace Launcher___BADTEAM
{
    public partial class Form1 : Form
    {
        public static string[] ans;
        public static Form1 self;

        public Form1()
        {
            InitializeComponent();
            self = this;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://badteam.ru/recovery");
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://badteam.ru/registration");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CheckFile();

            RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\Badteam", true);

            if (key == null)
            {
                key = Registry.CurrentUser.CreateSubKey("Software\\Badteam");
                key = Registry.CurrentUser.OpenSubKey("Software\\Badteam", true);
            }

            textBox1.Text = (string)key.GetValue("login");
            textBox2.Text = (string)key.GetValue("password");
            if (textBox1.Text != "") { checkBox1.Checked = true; }
            
            key.Close();

            label2.Text = "Версия: " + Application.ProductVersion.ToString();

            if (CheckUpdate())
            {
                frm_launcherupdate frm_lnupd = new frm_launcherupdate();

                frm_lnupd.ShowDialog();
            }
        }

        private bool CheckUpdate()
        {
            string ver = POST("http://badteam.ru/client/launcher/launcher.php", "new=true&launcher=240");

            if (ver == "OK")
            {
                return false;
            }

            return true;
        }

        private void CheckFile()
        {
            if (File.Exists(Application.StartupPath + "\\Badteam.bak"))
            {
                File.Delete(Application.StartupPath + "\\Badteam.bak");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "")
            {
                if (textBox2.Text.Length < 32)
                {
                    textBox2.Text = CalculateMD5Hash(textBox2.Text);
                }

                string s = POST("http://badteam.ru/user/loginserver.php", "user=" + textBox1.Text + "&password=" + textBox2.Text + "&version=33");
                ans = s.Split(':');

                if (ans.Length == 11 && ans[2] == textBox1.Text)
                {
                    RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\Badteam", true);

                    if (checkBox1.Checked)
                    {
                        key.SetValue("login", textBox1.Text);
                        key.SetValue("password", textBox2.Text);
                    }
                    else
                    {
                        key.SetValue("login", "");
                        key.SetValue("password", "");
                    }

                    key.Close();

                    Main frm_main = new Main();
                    frm_main.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Проверьте корректность логина или пароля ! Помните, что регистр букв важен !(Bad или bAd или BaD - разные ники)");
                }
            }
            else
            {
                MessageBox.Show("Введите логин и пароль !");
            }
        }

        private string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.Default.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

        private string POST(string Url, string Data)
        {
          WebRequest req = WebRequest.Create(Url);
          req.Method = "POST";
          req.Timeout = 100000;
          req.ContentType = "application/x-www-form-urlencoded";
          byte[] sentData = Encoding.Default.GetBytes(Data);
          req.ContentLength = sentData.Length;

          Stream sendStream = req.GetRequestStream();
          sendStream.Write(sentData, 0, sentData.Length);
          sendStream.Close();

          WebResponse res = req.GetResponse();
          Stream ReceiveStream = res.GetResponseStream();
          StreamReader sr = new StreamReader(ReceiveStream, Encoding.UTF8);
          //Кодировка указывается в зависимости от кодировки ответа сервера
          Char[] read = new Char[256];
          int count = sr.Read(read, 0, 256);
          string Out = String.Empty;
          while (count > 0)
          {
            String str = new String(read, 0, count);
            Out += str;
            count = sr.Read(read, 0, 256);
          }
          return Out;
        }
    }
}
