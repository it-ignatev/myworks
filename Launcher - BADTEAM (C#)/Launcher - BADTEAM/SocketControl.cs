﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Security.Cryptography;
using System.Windows.Forms;

namespace Launcher___BADTEAM
{
    class SocketControl
    {
        TcpListener server = null;
        public static NetworkStream stream;
        public static bool work = true;
        public static SocketControl self;

        public SocketControl()
        {
            Run("127.0.0.1", 6735);
            self = this;
        }

        private void Run(string ip, int port)
        {
            try
            {
                // Определим нужное максимальное количество потоков
                // Пусть будет по 4 на каждый процессор
                int MaxThreadsCount = Environment.ProcessorCount * 4;
                Console.WriteLine(MaxThreadsCount.ToString());
                // Установим максимальное количество рабочих потоков
                ThreadPool.SetMaxThreads(MaxThreadsCount, MaxThreadsCount);
                // Установим минимальное количество рабочих потоков
                ThreadPool.SetMinThreads(2, 2);


                // Устанавливаем порт для TcpListener = 9595.
                IPAddress localAddr = IPAddress.Parse(ip);
                int counter = 0;
                server = new TcpListener(localAddr, port);

                // Запускаем TcpListener и начинаем слушать клиентов.
                server.Start();

                Console.WriteLine("Сервер запущен !");

                // Принимаем клиентов в бесконечном цикле.
                while (work)
                {
                    Console.WriteLine("Новый клиент !");
                    // При появлении клиента добавляем в очередь потоков его обработку.
                    ThreadPool.QueueUserWorkItem(ObrabotkaZaprosa, server.AcceptTcpClient());
                    // Выводим информацию о подключении.
                    counter++;
                }
            }
            catch (SocketException e)
            {
                //В случае ошибки, выводим что это за ошибка.
                Console.WriteLine("SocketException: {0}", e);
            }
            finally
            {
                // Останавливаем TcpListener.
                server.Stop();
            }
        }

        public void STOPALL()
        {
            server.Stop();
        }

        static void ObrabotkaZaprosa(object client_obj)
        {
            // Буфер для принимаемых данных.
            Byte[] bytes = new Byte[256];
            String data = null;

            //Можно раскомментировать Thread.Sleep(1000); 
            //Запустить несколько клиентов
            //и наглядно увидеть как они обрабатываются в очереди. 
            //Thread.Sleep(1000);

            TcpClient client = client_obj as TcpClient;

            data = null;

            // Получаем информацию от клиента
            stream = client.GetStream();

            int i;

            // Принимаем данные от клиента в цикле пока не дойдём до конца.
            try
            {
                while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                {
                    // Преобразуем данные в ASCII string.
                    data = System.Text.Encoding.UTF8.GetString(bytes, 0, i);

                    // Преобразуем строку к верхнему регистру.
                    string[] me = data.Split('_');

                    if (me[0] == "tfc" || me[3] == CreateMD5Hash(Form1.ans[3] + "An5Y2KdVMyyF8N8cc@0h.vRo1K[G.dY7EA").ToLower())
                    {
                        if (me[1] == "12")
                        {
                            if (me[3] == Main.UniqCode)
                            {
                                //if (me[2] == "44eeb51adbae917556cb3ffe88282d55")
                                //{
                                    Send(Form1.ans[3].ToLower());
                                //}
                            }
                        }
                    }

                    if (me[0] == "hitech2")
                    {
                        if (me[1] == "35")
                        {
                            if (me[3] == Main.UniqCode || me[3] == CreateMD5Hash(Form1.ans[3] + "An5Y2KdVMyyF8N8cc@0h.vRo1K[G.dY7EA").ToLower())
                            {
                                //if (me[2].ToLower().ToString() == "8eeda4a370552c6a37d5647cd20dadcf" || me[2].ToLower().ToString() == "f8444c29617eeabc55a440c9fc8a6cbf" || me[2].ToLower().ToString() == "f8444c29617eeabc55a440c9fc8a6cbf")
                                //{
                                    
                                    Send(Form1.ans[3].ToLower());
                                //}
                            }
                        }
                    }

                    if (me[0] == "events")
                    {
                        if (me[1] == "35")
                        {
                            if (me[3] == Main.UniqCode || me[3] == CreateMD5Hash(Form1.ans[3] + "An5Y2KdVMyyF8N8cc@0h.vRo1K[G.dY7EA").ToLower())
                            {
                               // if (me[2].ToLower().ToString() == "8eeda4a370552c6a37d5647cd20dadcf" || me[2].ToLower().ToString() == "f8444c29617eeabc55a440c9fc8a6cbf" || me[2].ToLower().ToString() == "f8444c29617eeabc55a440c9fc8a6cbf")
                                //{

                                    Send(Form1.ans[3].ToLower());
                                //}
                            }
                        }
                    }

                    //Main.self.Mesa("GET: " + data.ToString());

                    Console.WriteLine("GET: " + data.ToString());
                    Console.WriteLine("MD5: " + CreateMD5Hash(Form1.ans[3] + "An5Y2KdVMyyF8N8cc@0h.vRo1K[G.dY7EA").ToLower());
                }
            }
            catch (IOException)
            {
                client.Close();
            }

            // Закрываем соединение.
            client.Close();
        }

        public static void Send(string meessage)
        {
            // Преобразуем полученную строку в массив Байт.
            byte[] msg = System.Text.Encoding.Default.GetBytes(meessage);

            // Отправляем данные обратно клиенту (ответ).
            stream.Write(msg, 0, msg.Length);
        }

        public static string CreateMD5Hash(string input)
        {
            // Use input string to calculate MD5 hash
            MD5 md5 = MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hashBytes = md5.ComputeHash(inputBytes);

            // Convert the byte array to hexadecimal string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(hashBytes[i].ToString("X2"));
                // To force the hex string to lower-case letters instead of
                // upper-case, use he following line instead:
                // sb.Append(hashBytes[i].ToString("x2")); 
            }
            return sb.ToString();
        }
    }
}
