<?php
interface IModuleActionSetup {
    function indexAction();
    function basicSetup();
    function endSetup();
}
