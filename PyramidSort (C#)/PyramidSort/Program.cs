﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PyramidSort
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("n: ");
            int n = Convert.ToInt32(Console.ReadLine());
            Print(Create(n));

            Console.ReadKey();
        }

        private static int[] Create(int n)
        {
            int[] a = new int[n];
            Random rnd = new Random();

            for (int i = 0; i < n; i++)
                a[i] = rnd.Next(1, 10);

            for(int i = a.Length / 2 - 1; i >= 0; i--)
            {

            }

            return a;
        }

        private static void Print(int[] a)
        {
            foreach (int elem in a)
                Console.Write("{0} ", elem);
        }
    }
}
