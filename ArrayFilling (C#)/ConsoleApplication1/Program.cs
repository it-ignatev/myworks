﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            int dir = 1, n = 0;
        

            n = Convert.ToInt32(Console.ReadLine());
            dir = Convert.ToInt32(Console.ReadLine());

            int[,] matrix = new int[n,n];

            int row = 0;
            int col = 0;

            int dx = (dir.Equals(1) ? 1 : 0);
            int dy = (dir.Equals(1) ? 0 : 1);
            dir = (dir.Equals(1) ? 1 : -1);

            int dirChanges = 0;
            int visits = n;

            for (int i = 0; i < n * n; i++)
            {
                matrix[row,col] = i + 1;
                if (--visits == 0)
                {
                    visits = n * (dirChanges % 2) + n * ((dirChanges + 1) % 2) - (dirChanges / 2 - 1) - 2;
                    int temp = dir * dx;
                    dx = dir * -dy;
                    dy = temp;
                    dirChanges++;
                }
                col += dx;
                row += dy;
            }

            for (int i = 0; i < n; i++)
            {
                for (int k = 0; k < n; k++)
                    Console.Write(matrix[i,k] + " ");
                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
