unit UpdateLauncher;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, acPNG, Vcl.ExtCtrls, Vcl.ComCtrls,
  acProgressBar, Vcl.StdCtrls, sLabel, ShellApi;

type
  Tfrm_updatelauncher = class(TForm)
    pg_upd: TsProgressBar;
    Image1: TImage;
    sLabel1: TsLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    function FrmClose(): boolean;
  end;

var
  frm_updatelauncher: Tfrm_updatelauncher;

implementation

{$R *.dfm}

uses Log, ThreadUpdLauncher;

function Tfrm_updatelauncher.FrmClose(): boolean;
begin
  ShellExecute(self.handle, 'open',PChar(ExtractFilePath(Application.ExeName) + 'Badteam.exe'),nil,nil,SW_SHOW);
  frm_log.Close();
  Application.Terminate;
  ExitProcess(0);
end;

procedure Tfrm_updatelauncher.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  ExitProcess(0);
end;

procedure Tfrm_updatelauncher.FormCreate(Sender: TObject);
begin
  sLabel1.Caption := 'Launcher ' + frm_log.Launcher;
  UpdLauncher.Create(false);
end;


end.
