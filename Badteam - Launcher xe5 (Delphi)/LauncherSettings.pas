unit LauncherSettings;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, sButton, Vcl.ComCtrls,
  acProgressBar;

type
  Tfrm_launchersettings = class(TForm)
    but_installjava: TsButton;
    pg_java: TsProgressBar;
    procedure FormCreate(Sender: TObject);
    procedure but_installjavaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_launchersettings: Tfrm_launchersettings;

implementation

{$R *.dfm}

uses Log, ThreadJavaInstall;

procedure Tfrm_launchersettings.but_installjavaClick(Sender: TObject);
begin
  JavaInstall.create(false).FreeOnTerminate := True;
end;

procedure Tfrm_launchersettings.FormCreate(Sender: TObject);
begin
  if DirectoryExists(frm_log.Appdata + 'Java\') then but_installjava.Enabled := False;
  pg_java.Visible := False;
end;

end.
