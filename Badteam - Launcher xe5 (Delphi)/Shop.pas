unit Shop;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, sComboBox, Vcl.OleCtrls,
  SHDocVw, sLabel;

type
  Tfrm_shop = class(TForm)
    WebBrowser1: TWebBrowser;
    sComboBox1: TsComboBox;
    sLabel1: TsLabel;
    procedure sComboBox1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_shop: Tfrm_shop;

implementation

{$R *.dfm}

uses Log;

procedure Tfrm_shop.sComboBox1Change(Sender: TObject);
begin
  if sComboBox1.ItemIndex = 0 then
  begin
    WebBrowser1.Navigate('http://badteam.ru/client/launcher/shop.php?s=h1&t=i&u=' + frm_log.te_login.Text + '&p=' + frm_log.te_password.Text);
  end;
end;

end.
