unit ThreadGameUpdate;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, sLabel, Vcl.ComCtrls,
  acProgressBar, Vcl.ExtCtrls, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdHTTP;

type
  GameUpdate = class(TThread)
  idHTTP1: TIdHTTP;
  procedure idHTTP1Work(ASender: TObject; AWorkMode: TWorkMode;AWorkCount: Int64);
  procedure idHTTP1WorkBegin(ASender: TObject; AWorkMode: TWorkMode;AWorkCountMax: Int64);
  private
    { Private declarations }
  protected
    procedure Execute; override;
  end;

implementation

{ GameUpdate }

uses Log, Load;


procedure GameUpdate.Execute;
var
LoadStream: TFileStream;
Stream: TMemoryStream;
downloadf: array[0..3] of string;
i,r: integer;
server: string;
begin
  if not DirectoryExists(frm_log.Appdata + frm_load.client_d +'\bin\') then ForceDirectories(frm_log.Appdata + frm_load.client_d + '\bin\');

  if DirectoryExists(frm_log.Appdata + frm_load.client_d + '\bin\') then
  begin
    DeleteFile(frm_log.Appdata + frm_load.client_d  +'\bin\minecraft.jar');
    DeleteFile(frm_log.Appdata + frm_load.client_d  +'\bin\client.zip');
    DeleteFile(frm_log.Appdata + frm_load.client_d  +'\bin\version');
  end;

  downloadf[1] := 'minecraft.jar';
  downloadf[2] := 'client.zip';
  downloadf[3] := 'version';
  r := Random(10);
  if r >= 2 then
  begin
    if r >= 7 then server := 'http://dl.dropboxusercontent.com/u/48129448/Badteam_Client/' else server := 'http://bad.cthulchu.ru/';
  end
  else
  begin
    server := 'http://188.40.54.76/client/';
  end;

  i:=0;
  frm_load.Enabled := False;
  for i := 1 to 3 do
  begin
    try
        //LoadStream := TFileStream.Create(frm_log.Appdata + frm_load.client_d + '\bin\' + downloadf[i],fmCreate);
        Stream:=TMemoryStream.Create;
        IdHTTP1 := TIdHTTP.Create;
        IdHTTP1.OnWork := IdHTTP1Work;
        IdHTTP1.OnWorkBegin := idHTTP1WorkBegin;
        idHTTP1.Get(server + frm_load.client_d + '/' + downloadf[i],Stream);
        Stream.SaveToFile(frm_log.Appdata + frm_load.client_d + '\bin\' + downloadf[i]);
        Stream.Free;
    Except
      frm_load.pic_hitech1.Enabled := True;
      frm_load.but_exit.Enabled := True;
      frm_load.Enabled := True;
      frm_load.pic_hitech1.Visible := True;
      frm_load.pic_sandbox.Visible := True;
      frm_load.pic_survivalgames.Visible := True;
      frm_load.pic_technology.Visible := True;
      frm_load.pic_load.Visible := False;
      ShowMessage('��������� ���������� !');
      Break;
    end;

    if i >= 3 then
    begin
      frm_load.pic_hitech1.Enabled := True;
      frm_load.but_exit.Enabled := True;
      frm_load.Enabled := True;
      frm_load.pic_hitech1.Visible := True;
      frm_load.pic_sandbox.Visible := True;
      frm_load.pic_survivalgames.Visible := True;
      frm_load.pic_technology.Visible := True;
      frm_load.pic_load.Visible := False;
      ShowMessage('��������� !');
      ShowMessage('��� ����� ������������� ������� !');
      Terminate;
    end;
  end;
end;

procedure GameUpdate.idHTTP1Work(ASender: TObject; AWorkMode: TWorkMode; AWorkCount: Int64);
var pos:integer;
begin
  pos := AWorkCount - frm_load.Progbar.Position;
  frm_load.Progbar.Position := frm_load.Progbar.Position + pos;
end;

procedure GameUpdate.idHTTP1WorkBegin(ASender: TObject; AWorkMode: TWorkMode; AWorkCountMax: Int64);
begin
  frm_load.Progbar.Visible := True;
  frm_load.Progbar.Position := 0;
  frm_load.Progbar.Max := AWorkCountMax;
end;

end.
