unit ThreadZipJava;

interface

uses
  System.Classes, ZipForge, System.SysUtils, System.Variants;

type
  UnZipJava = class(TThread)
  private
    { Private declarations }
  protected
    procedure Execute; override;
  end;

implementation

{ UnZipJava }

uses Log, LauncherSettings, ThreadJavaInstall;

procedure UnZipJava.Execute;
var
Archiver: TZipForge;
Install: JavaInstall;
begin
  archiver := TZipForge.Create(nil);
  try
  with archiver do
  begin
    // The name of the ZIP file to unzip
    FileName := frm_log.Appdata + 'Java.zip';
    // Open an existing archive
    OpenArchive(fmOpenRead);
    // Set base (default) directory for all archive operations
    BaseDir := frm_log.Appdata;
    // Extract all files from the archive to C:\Temp folder
    ExtractFiles('*.*');
    CloseArchive();
  end;
  except
  on E: Exception do
    begin
      Writeln('Exception: ', E.Message);
      // Wait for the key to be pressed
      Readln;
    end;
  end;

  frm_launchersettings.close();

  FreeOnTerminate := True;
  Free;

  Terminate;
  Exit;
end;

end.
