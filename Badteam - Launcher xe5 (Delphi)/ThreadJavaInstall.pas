unit ThreadJavaInstall;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, sLabel, Vcl.ComCtrls,
  acProgressBar, Vcl.ExtCtrls, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdHTTP;

type
  JavaInstall = class(TThread)
  IdHTTP: TIdHTTP;
  procedure idHTTPWork(ASender: TObject; AWorkMode: TWorkMode;AWorkCount: Int64);
  procedure idHTTPWorkBegin(ASender: TObject; AWorkMode: TWorkMode;AWorkCountMax: Int64);
  protected
    procedure Execute; override;
  end;

implementation

{ ThreadJavaInstall }

uses Log, LauncherSettings, ThreadZipJava;

procedure JavaInstall.Execute;
var
mem:TFileStream;
Zip: UnZipJava;
begin
  mem :=  TFileStream.Create(frm_log.Appdata + 'Java.zip', fmCreate);
  try
    frm_LauncherSettings.Enabled := False;
    IdHTTP := TIdHTTP.Create;
    IdHTTP.OnWork := IdHTTPWork;
    IdHTTP.OnWorkBegin := idHTTPWorkBegin;
    IdHTTP.Get('http://badteam.ru/client/launcher/Java.zip',mem);
  finally
    mem.Free;
  end;
  frm_LauncherSettings.Enabled := True;
  Zip.Create(False).FreeOnTerminate := True;

  FreeOnTerminate := True;
  Free;

  Terminate;
  Exit;
end;

procedure JavaInstall.IdHTTPWork(ASender: TObject; AWorkMode: TWorkMode; AWorkCount: Int64);
var pos:integer;
begin
  pos := AWorkCount - frm_LauncherSettings.pg_java.Position;
  frm_LauncherSettings.pg_java.Position := frm_LauncherSettings.pg_java.Position + pos;
end;

procedure JavaInstall.idHTTPWorkBegin(ASender: TObject; AWorkMode: TWorkMode; AWorkCountMax: Int64);
begin
  frm_LauncherSettings.but_installjava.Enabled := False;
  frm_LauncherSettings.pg_java.Visible := True;
  frm_LauncherSettings.pg_java.Position := 0;
  frm_LauncherSettings.pg_java.Max := AWorkCountMax;
end;

end.
