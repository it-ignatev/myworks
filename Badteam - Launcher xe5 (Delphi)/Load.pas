unit Load;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, sGroupBox, Vcl.ExtCtrls,
  Vcl.ComCtrls, acProgressBar, sButton, sLabel, acPNG, Registry, ShellApi, NB30,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP, tlhelp32,
  System.Win.ScktComp, System.DateUtils, IdHash,
   IdHashMessageDigest, acImage;

type
  Tfrm_load = class(TForm)
    pic_hitech1: TImage;
    sGroupBox1: TsGroupBox;
    Progbar: TsProgressBar;
    but_shop: TsButton;
    but_exit: TsButton;
    but_settings: TsButton;
    question: TImage;
    IdHTTP1: TIdHTTP;
    ServerSocket1: TServerSocket;
    ServerSocket2: TServerSocket;
    Timer1: TTimer;
    Timer2: TTimer;
    sLabel1: TsLabel;
    pic_load: TsImage;
    pic_sandbox: TImage;
    pic_survivalgames: TImage;
    pic_technology: TImage;
    procedure but_exitClick(Sender: TObject);
    procedure UpdateMac(mac:string);
    procedure pic_hitech1Click(Sender: TObject);
    procedure but_settingsClick(Sender: TObject);
    procedure questionClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure ServerSocket1Accept(Sender: TObject; Socket: TCustomWinSocket);
    procedure ServerSocket1ClientRead(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure ServerSocket2Accept(Sender: TObject; Socket: TCustomWinSocket);
    procedure but_shopClick(Sender: TObject);
    procedure pic_sandboxClick(Sender: TObject);
    procedure pic_survivalgamesClick(Sender: TObject);
    procedure pic_technologyClick(Sender: TObject);
  private
    { Private declarations }

    md5_jar: string;
    md5_folders: string;


  public
    { Public declarations }
    client_d: string;
    session: string;
    Uniq:String;
    pathj: string;
    first: boolean;
  end;

var
  frm_load: Tfrm_load;
  Registry: TRegistry;

implementation

{$R *.dfm}
{$R load.res}
{$R Hitech_start.res}
{$R Hitech.res}

uses Log, GameSettings, Help, ThreadGameUpdate, ThreadClear, ThreadUnZip, Shop;

function GetAdapterInfo(Lana: Char): string;
var
  Adapter: TAdapterStatus;
  NCB: TNCB;
begin
  FillChar(NCB, SizeOf(NCB), 0);
  NCB.ncb_command := Char(NCBRESET);
  NCB.ncb_lana_num := AnsiChar(Lana);
  if Netbios(@NCB) <> Char(NRC_GOODRET) then
  begin
    Result := 'mac not found';
    Exit;
  end;

  FillChar(NCB, SizeOf(NCB), 0);
  NCB.ncb_command := Char(NCBASTAT);
  NCB.ncb_lana_num := AnsiChar(Lana);
  NCB.ncb_callname := '*';

  FillChar(Adapter, SizeOf(Adapter), 0);
  NCB.ncb_buffer := @Adapter;
  NCB.ncb_length := SizeOf(Adapter);
  if Netbios(@NCB) <> Char(NRC_GOODRET) then
  begin
    Result := 'mac not found';
    Exit;
  end;
  Result :=
  IntToHex(Byte(Adapter.adapter_address[0]), 2) + '-' +
  IntToHex(Byte(Adapter.adapter_address[1]), 2) + '-' +
  IntToHex(Byte(Adapter.adapter_address[2]), 2) + '-' +
  IntToHex(Byte(Adapter.adapter_address[3]), 2) + '-' +
  IntToHex(Byte(Adapter.adapter_address[4]), 2) + '-' +
  IntToHex(Byte(Adapter.adapter_address[5]), 2);
end;

function GetMACAddress: string;
var
  AdapterList: TLanaEnum;
  NCB: TNCB;
begin
  FillChar(NCB, SizeOf(NCB), 0);
  NCB.ncb_command := Char(NCBENUM);
  NCB.ncb_buffer := @AdapterList;
  NCB.ncb_length := SizeOf(AdapterList);
  Netbios(@NCB);
  if Byte(AdapterList.length) > 0 then
    Result := GetAdapterInfo(Char(AdapterList.lana[0]))
  else
    Result := 'N\A';
end;

function GetDirFullSize(aPath: String): Int64;
var
  sr: TSearchRec;
  tPath: String;
  sum: Int64;
begin
  //������� ������ ��������� ������ ����
  sum := 0;
  //��������� ����������� ����, ���� �����
  tPath := IncludeTrailingBackSlash(aPath);
  //���������� ���������� ��������
  if FindFirst(tPath + '*.*', faAnyFile, sr) = 0 then
  begin
    try
      repeat
        //���� ��� ������� ��� ������������ ������� - �� �������
        if (sr.Name = '.') or (sr.Name = '..') then
          Continue;
        //���� ��� ������� - ���������� ���������� ��� ������ � �����
        if (sr.Attr and faDirectory) <> 0 then
        begin
          sum := sum + GetDirFullSize(tPath + sr.Name);
          Continue;
        end;
        //���� ���� - �� ��������� ��� ������ � �����
        sum := sum + (sr.FindData.nFileSizeHigh shl 32) + sr.FindData.nFileSizeLow;
      until FindNext(sr) <> 0;
    finally
      //���������� ��������� � ��������� �����
      Result := sum;
      FindClose(sr);
    end;
  end;
end;

function IsRunning(sName: string): boolean; // ?????????, ??????? ?? ??????? sName
var
  han: THandle;
  ProcStruct: PROCESSENTRY32;
  sID: string;
begin
  Result := false;

  han := CreateToolhelp32Snapshot(TH32CS_SNAPALL, 0);
  if han = 0 then
    exit;


  ProcStruct.dwSize := sizeof(PROCESSENTRY32);
  if Process32First(han, ProcStruct) then
  begin
    repeat
      sID := ExtractFileName(ProcStruct.szExeFile);

      if uppercase(copy(sId, 1, length(sName))) = uppercase(sName) then
      begin

        Result := true;
        Break;
      end;
    until not Process32Next(han, ProcStruct);
  end;

  CloseHandle(han);
end;

function KillTask(ExeFileName: string): Integer;
const
  PROCESS_TERMINATE = $0001;
var
  ContinueLoop: BOOL;
  FSnapshotHandle: THandle;
  FProcessEntry32: TProcessEntry32;
begin
  Result := 0;
  FSnapshotHandle := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  FProcessEntry32.dwSize := SizeOf(FProcessEntry32);
  ContinueLoop := Process32First(FSnapshotHandle, FProcessEntry32);

  while Integer(ContinueLoop) <> 0 do
  begin
    if ((UpperCase(ExtractFileName(FProcessEntry32.szExeFile)) =
      UpperCase(ExeFileName)) or (UpperCase(FProcessEntry32.szExeFile) =
      UpperCase(ExeFileName))) then
      Result := Integer(TerminateProcess(
                        OpenProcess(PROCESS_TERMINATE,
                                    BOOL(0),
                                    FProcessEntry32.th32ProcessID),
                                    0));
     ContinueLoop := Process32Next(FSnapshotHandle, FProcessEntry32);
  end;
  CloseHandle(FSnapshotHandle);
end;

function MD5(const fileName : string) : string;
var
   idmd5 : TIdHashMessageDigest5;
   fs : TFileStream;
   hash : T4x4LongWordRecord;
begin
   idmd5 := TIdHashMessageDigest5.Create;
   fs := TFileStream.Create(fileName, fmOpenRead OR fmShareDenyWrite) ;
   try
     result := LowerCase(idmd5.HashStreamAsHex(fs));
   finally
     fs.Free;
     idmd5.Free;
   end;
end;

function CheckClient(client: string): Boolean;
var
PostData:TStringList;
TS : TStringList;
files: array[0..2] of string;
files_all: Integer;
md: string;
i: integer;
begin
  PostData:=TStringList.Create;
  PostData.Clear;
  PostData.Add('client=' + client);
  PostData.Add('version=3.0');
  //�������� ������ � ������(���)
  try
    md := frm_Load.idhttp1.Post('http://badteam.ru/client/launcher/launcher.php',PostData);
  except
    ShowMessage('������ ����������� !');
    Exit;
  end;

  PostData.Free;
  md := Trim(md);

  TS := TStringList.Create;
  TS.Delimiter := '|';
  TS.DelimitedText := md;
  //�������� �� ��������
  if DirectoryExists(frm_log.Appdata + client) then
  begin
  if (not FileExists(frm_log.Appdata + client + '\bin\minecraft.jar')) or (not FileExists(frm_log.Appdata + client + '\bin\client.zip') or (not FileExists(frm_log.Appdata + client + '\bin\version'))) then
  begin
    Result := False;
    Exit;
  end
  else
  begin

    // ������� ����

    files[0] := Md5(frm_log.Appdata + client + '\bin\minecraft.jar');
    files[1] := Md5(frm_log.Appdata+ client + '\bin\client.zip');
    files[2] := Md5(frm_log.Appdata + client + '\bin\version');


    files_all := Length(files);
    i := 0;
    for i := 0 to (files_all - 1) do
    begin
      //showmessage(files[i] + '---' + ts.strings[i]);
      if files[i] <> TS.Strings[i] then
      begin
        Result := False;
        Break;
      end
      else
      begin
        if i >= (files_all - 1) then
        begin
          //������� ����� ���������� ��� minecraft.jar
          Result := True;
          frm_load.md5_jar := files[0];
          Break;
        end;
      end;
    end;

  FreeAndNil(TS);
  end;


  end else Result := False;
end;

function GetWin(Comand: string): string;
var
  buff: array [0 .. $FF] of char;
begin
  ExpandEnvironmentStrings(PChar(Comand), buff, SizeOf(buff));
  Result := buff;
end;

procedure Tfrm_load.but_settingsClick(Sender: TObject);
begin
  Application.CreateForm(Tfrm_gamesettings, frm_gamesettings);
  frm_gamesettings.ShowModal;
end;

procedure Tfrm_load.but_shopClick(Sender: TObject);
begin
  Application.CreateForm(Tfrm_shop, frm_shop);
  frm_shop.ShowModal;
end;

function JavaCheck(const java:string): string;
begin
//�������� Java ��� �������
  if java = 'x64' then
  begin
  frm_load.pathj:='C:\Program Files\Java\jre7\bin\javaw.exe ';
    if FileExists(frm_load.pathj) = False then
    begin
      frm_load.pic_hitech1.Enabled := True;
      frm_load.but_exit.Enabled := True;
      frm_load.pic_hitech1.Visible := True;
      frm_load.pic_sandbox.Visible := True;
      frm_load.pic_survivalgames.Visible := True;
      frm_load.pic_technology.Visible := True;
      frm_load.pic_load.Visible := False;
      ShowMessage('Java x64 �� ���������� ! �������� Java � ����������.');
      Exit;
    end;
  end
  else
  begin
    if java = 'x32' then
    begin
    frm_load.pathj:='C:\Program Files (x86)\Java\jre7\bin\javaw.exe ';
      if FileExists(frm_load.pathj) = False then
      begin
        frm_load.pic_hitech1.Enabled := True;
        frm_load.but_exit.Enabled := True;
        frm_load.pic_hitech1.Visible := True;
        frm_load.pic_sandbox.Visible := True;
        frm_load.pic_survivalgames.Visible := True;
        frm_load.pic_technology.Visible := True;
        frm_load.pic_load.Visible := False;
        ShowMessage('Java x32 �� ���������� ! �������� Java � ����������.');
        Exit;
      end;
    end
    else
      if (java = '����������') and (DirectoryExists(frm_log.Appdata + 'Java\') = True) then
      begin
        frm_load.pathj := frm_log.Appdata + 'Java\bin\javaw.exe ';
      end
      else
      begin
        if FileExists(GetWin('%systemroot%') + '\SysWOW64\javaw.exe') then
        begin
          frm_load.pathj := GetWin('%systemroot%') + '\SysWOW64\javaw.exe ';
        end
        else
        begin
          if FileExists(GetWin('%systemroot%') + '\System32\javaw.exe') then
          begin
            frm_load.pathj := GetWin('%systemroot%') + '\System32\javaw.exe ';
          end
          else
          begin
            frm_load.pic_hitech1.Enabled := True;
            frm_load.but_exit.Enabled := True;
            frm_load.pic_hitech1.Visible := True;
            frm_load.pic_sandbox.Visible := True;
            frm_load.pic_survivalgames.Visible := True;
            frm_load.pic_technology.Visible := True;
            frm_load.pic_load.Visible := False;
            ShowMessage('���������� JAVA !');
            Exit;
          end;
        end;
      end;
    end;
end;

procedure Delay(Value: Cardinal);
var
  F, N: Cardinal;
begin
  N := 0;
  while N <= (Value div 10) do
  begin
    SleepEx(1, True);
    Application.ProcessMessages;
    Inc(N);
  end;
  F := GetTickCount;
  repeat
    Application.ProcessMessages;
    N := GetTickCount;
  until (N - F >= (Value mod 10)) or (N < F);
end;

procedure Tfrm_load.pic_hitech1Click(Sender: TObject);
var client: string;
begin
  pic_hitech1.Visible := False;
  pic_sandbox.Visible := False;
  pic_survivalgames.Visible := False;
  pic_load.Visible := True;
  pic_technology.Visible := False;

  pic_hitech1.Enabled := False;
  but_exit.Enabled := False;
  client := 'Hitech_1';

  //--����� Java
  Registry := TRegistry.Create;
  Registry.RootKey := hkey_current_user;
  Registry.OpenKey('software\Badteam',false);
  JavaCheck(Registry.ReadString('java'));
  Registry.CloseKey;
  Registry.Free;

  //--�������� �� ����
  if (IsRunning('java.exe') = True) or (IsRunning('javaw.exe') = True) then
  begin
    if (frm_log.te_login.Text <> 'bad') and (frm_log.te_login.Text <> 'WahPaniker') and (frm_log.te_login.Text <> 'Teacher') and (frm_log.te_login.Text <> 'SensoR') then
    begin
      pic_hitech1.Enabled := True;
      pic_hitech1.Visible := True;
      pic_sandbox.Visible := True;
      pic_survivalgames.Visible := True;
      pic_technology.Visible := True;
      pic_load.Visible := False;
      but_exit.Enabled := True;
      MessageDlg('���� ��� �������� !', mtWarning, [mbOK], 0);
      Exit;
    end;
  end;

  client_d := client;

  //--�������� MD5
  if CheckClient(client) = False then
  begin
    GameUpdate.Create(false).FreeOnTerminate := True;
  end
  else
  begin
    Clear.Create(false).FreeOnTerminate := True;
    //���������� ���(������) ����� mods
    md5_folders := IntToStr(GetDirFullSize(frm_log.Appdata + client + '\mods\'));
  end;

end;

procedure Tfrm_load.pic_sandboxClick(Sender: TObject);
var client: string;
begin
  pic_hitech1.Visible := False;
  pic_sandbox.Visible := False;
  pic_survivalgames.Visible := False;
  pic_load.Visible := True;
  pic_technology.Visible := False;

  Delay(1500);
  pic_hitech1.Enabled := False;
  but_exit.Enabled := False;
  client := 'Classic';

  //--����� Java
  Registry := TRegistry.Create;
  Registry.RootKey := hkey_current_user;
  Registry.OpenKey('software\Badteam',false);
  JavaCheck(Registry.ReadString('java'));
  Registry.CloseKey;
  Registry.Free;

  //--�������� �� ����
  if (IsRunning('java.exe') = True) or (IsRunning('javaw.exe') = True) then
  begin
    if (frm_log.te_login.Text <> 'bad') and (frm_log.te_login.Text <> 'WahPaniker') and (frm_log.te_login.Text <> 'Teacher') and (frm_log.te_login.Text <> 'SensoR') then
    begin
      pic_hitech1.Enabled := True;
      pic_hitech1.Visible := True;
      pic_sandbox.Visible := True;
      pic_survivalgames.Visible := True;
      pic_technology.Visible := True;
      pic_load.Visible := False;
      but_exit.Enabled := True;
      MessageDlg('���� ��� �������� !', mtWarning, [mbOK], 0);
      Exit;
    end;
  end;

  client_d := client;

  //--�������� MD5
  if CheckClient(client) = False then
  begin
    GameUpdate.Create(false).FreeOnTerminate := True;
  end
  else
  begin
    Clear.Create(false).FreeOnTerminate := True;
    md5_folders := IntToStr(GetDirFullSize(frm_log.Appdata + client + '\mods\'));
  end;
end;

procedure Tfrm_load.pic_survivalgamesClick(Sender: TObject);
var client:string;
begin
  pic_hitech1.Visible := False;
  pic_sandbox.Visible := False;
  pic_survivalgames.Visible := False;
  pic_load.Visible := True;
  pic_technology.Visible := False;

  pic_hitech1.Enabled := False;
  but_exit.Enabled := False;
  client := 'SurvivalGames';

  //--����� Java
  Registry := TRegistry.Create;
  Registry.RootKey := hkey_current_user;
  Registry.OpenKey('software\Badteam',false);
  JavaCheck(Registry.ReadString('java'));
  Registry.CloseKey;
  Registry.Free;

  //--�������� �� ����
  if (IsRunning('java.exe') = True) or (IsRunning('javaw.exe') = True) then
  begin
    if (frm_log.te_login.Text <> 'bad') and (frm_log.te_login.Text <> 'WahPaniker') and (frm_log.te_login.Text <> 'Teacher') and (frm_log.te_login.Text <> 'SensoR') then
    begin
      pic_hitech1.Enabled := True;
      pic_hitech1.Visible := True;
      pic_sandbox.Visible := True;
      pic_survivalgames.Visible := True;
      pic_technology.Visible := True;
      pic_load.Visible := False;
      but_exit.Enabled := True;
      MessageDlg('���� ��� �������� !', mtWarning, [mbOK], 0);
      Exit;
    end;
  end;

  client_d := client;

  //--�������� MD5
  if CheckClient(client) = False then
  begin
    GameUpdate.Create(false).FreeOnTerminate := True;
  end
  else
  begin
    Clear.Create(false).FreeOnTerminate := True;
    //���������� ���(������) ����� mods
    md5_folders := IntToStr(GetDirFullSize(frm_log.Appdata + client + '\mods\'));
  end;

end;

procedure Tfrm_load.pic_technologyClick(Sender: TObject);
var client: string;
TS : TStringList;

begin

  TS := TStringList.Create;
  TS.Delimiter := ':';
  TS.DelimitedText := frm_log.ans;

  if (Trim(TS.Strings[5]) <> 'Admin') and (Trim(TS.Strings[5]) <> 'Technology') then
  begin
    ShowMessage('���� ������ �������. �� ����� �������� ������ ����� ������� ����������.(��������� ����� ������ � �������� �� �����)');
    Exit;
  end;

  TS.Free;

  pic_hitech1.Visible := False;
  pic_sandbox.Visible := False;
  pic_survivalgames.Visible := False;
  pic_technology.Visible := False;
  pic_load.Visible := True;

  Delay(1500);
  pic_hitech1.Enabled := False;
  but_exit.Enabled := False;
  client := 'Technology';

  //--����� Java
  Registry := TRegistry.Create;
  Registry.RootKey := hkey_current_user;
  Registry.OpenKey('software\Badteam',false);
  JavaCheck(Registry.ReadString('java'));
  Registry.CloseKey;
  Registry.Free;

  //--�������� �� ����
  if (IsRunning('java.exe') = True) or (IsRunning('javaw.exe') = True) then
  begin
    if (frm_log.te_login.Text <> 'bad') and (frm_log.te_login.Text <> 'WahPaniker') and (frm_log.te_login.Text <> 'Teacher') and (frm_log.te_login.Text <> 'SensoR') then
    begin
      pic_hitech1.Enabled := True;
      pic_hitech1.Visible := True;
      pic_sandbox.Visible := True;
      pic_technology.Visible := True;
      pic_survivalgames.Visible := True;
      pic_load.Visible := False;
      but_exit.Enabled := True;
      MessageDlg('���� ��� �������� !', mtWarning, [mbOK], 0);
      Exit;
    end;
  end;

  client_d := client;

  //--�������� MD5
  if CheckClient(client) = False then
  begin
    GameUpdate.Create(false).FreeOnTerminate := True;
  end
  else
  begin
    Clear.Create(false).FreeOnTerminate := True;
    md5_folders := IntToStr(GetDirFullSize(frm_log.Appdata + client + '\mods\'));
  end;
end;

procedure Tfrm_load.questionClick(Sender: TObject);
begin
  Application.CreateForm(Tfrm_help, frm_help);
  frm_help.ShowModal;
end;

procedure Tfrm_load.ServerSocket1Accept(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  Socket.SendStream(TStringStream.Create(Uniq + #10#13));
end;

procedure Tfrm_load.ServerSocket1ClientRead(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  Socket.SendStream(TStringStream.Create(session + #10#13));
  Socket.SendText(session + #10#13);
end;

procedure Tfrm_load.ServerSocket2Accept(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  Socket.SendStream(TStringStream.Create(session + #10#13));
end;

procedure Tfrm_load.Timer1Timer(Sender: TObject);
begin
  if first then self.Hide;
  first := False;

  if not IsRunning('javaw.exe') then
  begin
    ExitProcess(0);
  end;
end;

procedure Tfrm_load.Timer2Timer(Sender: TObject);
begin
  if md5_folders <> IntToStr(GetDirFullSize(frm_log.Appdata + client_d + '\mods\')) then
  begin
    KillTask('javaw.exe');
  end;
end;

procedure Tfrm_load.UpdateMac(mac:string);
var PostData:TStringList;
begin
  PostData:=TStringList.Create;
  PostData.Clear;
  PostData.Add('u=' + frm_log.te_login.Text);
  PostData.Add('m=' + mac);
  IdHTTP1.Post('http://188.40.54.76/user/mac.php', POSTDATA);
  postdata.free;
end;

function RandomPassword(PLen: Integer): string;
var
  str: string;
begin
  Randomize;
  //string with all possible chars
  str    := 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  Result := '';
  repeat
    Result := Result + str[Random(Length(str)) + 1];
  until (Length(Result) = PLen)
end;

procedure Tfrm_load.FormCreate(Sender: TObject);
var
Time: _SYSTEMTIME;
TS: TStringList;
vote: string;
mac: string;
timenow:  TDateTime;
votetime: TDateTime;
begin
  Uniq := RandomPassword(32);

  Registry := TRegistry.Create;
  Registry.RootKey := hkey_current_user;
  Registry.OpenKey('software\Badteam',false);

  vote := '';

  GetLocalTime(Time);

  //--- ����������� ��� � 24 ����
  vote := Registry.ReadString('vote');

  Delay(2500);

  if vote = '' then
  begin
    if MessageBox(self.Handle ,'�� ������ ������������ �� ������ ?' + #13 + '(�� �������� 3 ������ �� ��� �� ������� ���-������ ������ � ��������.)','�����������',MB_YESNO) = id_yes then
    begin
      ShellExecute (self.Handle, nil, 'http://mctop.su/rating/vote/bad', nil, nil, SW_RESTORE);
      ShellExecute (self.Handle, nil, 'http://topcraft.ru/servers/162', nil, nil, SW_RESTORE);

      Registry.WriteString('vote', DateTimeToStr(IncDay(Now, 1)));
    end;
  end
  else
  begin
    try
      votetime := StrToDateTime(vote);
    except
      votetime := EncodeDateTime(2000,1,1,1,0,0,0);
    end;

    timenow := Now;

    if CompareDateTime(timenow, votetime) >= 0 then
    begin
        if MessageBox(self.Handle ,'�� ������ ������������ �� ������ ?' + #13 + '(�� �������� 3 ������ �� ��� �� ������� ���-������ ������ � ��������.)','�����������',MB_YESNO) = id_yes then
        begin
          ShellExecute (self.Handle, nil, 'http://mctop.su/rating/vote/bad', nil, nil, SW_RESTORE);
          ShellExecute (self.Handle, nil, 'http://topcraft.ru/servers/162', nil, nil, SW_RESTORE);

          Registry.WriteString('vote', DateTimeToStr(IncDay(Now, 1)));
        end
        else
        begin
           if MessageBox(self.Handle ,'�� �� ��� 100% �������, ��� �� ������ ������������� �� ������ ?' + #13 + '(�� �������� 10 �����, �� ��� �� ������� ���-������ ������ � ��������.)','�����������',MB_YESNO) = ID_NO then
           begin
              ShellExecute (self.Handle, nil, 'http://mctop.su/rating/vote/bad', nil, nil, SW_RESTORE);
              ShellExecute (self.Handle, nil, 'http://topcraft.ru/servers/162', nil, nil, SW_RESTORE);

              Registry.WriteString('vote', DateTimeToStr(IncDay(Now, 1)));
           end
           else
           begin
            if MessageBox(self.Handle ,'��������� ���� ! �� �� ��� 100% ������� ?' + #13 + '(�� �������� 15 �����, �� ��� �� ������� ���-������ ������ � ��������.)','�����������',MB_YESNO) = ID_NO then
            begin
              ShellExecute (self.Handle, nil, 'http://mctop.su/rating/vote/bad', nil, nil, SW_RESTORE);
              ShellExecute (self.Handle, nil, 'http://topcraft.ru/servers/162', nil, nil, SW_RESTORE);

              Registry.WriteString('vote', DateTimeToStr(IncDay(Now, 1)));
            end
            else
            begin
              MessageBox(self.Handle ,'�� ��������� "��" ������ ��� ��� ����� ��-��������� ?' + #13 + '(�� �������� 25 �����, �� ��� �� ������� ���-������ ������ � ��������.)','�����������',MB_YESNO);
            end;
           end;
        end;
    end;
  end;

  Registry.CloseKey;
  Registry.Free;

  TS := TStringList.Create;
  TS.Delimiter := ':';
  TS.DelimitedText := frm_log.ans;

  mac := GetMACAddress();

  if TS.Strings[9] <> mac then UpdateMac(mac);
end;



procedure Tfrm_load.but_exitClick(Sender: TObject);
var p: pointer;
begin

  P := @Application.Mainform;
  Pointer(P^) := frm_log;

  ServerSocket1.Close;
  ServerSocket2.Close;

  frm_log.Show;
  frm_load.Close;
end;

end.
