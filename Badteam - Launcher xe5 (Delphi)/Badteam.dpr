program Badteam;

uses
  Vcl.Forms,
  Log in 'Log.pas' {frm_log},
  Load in 'Load.pas' {frm_load},
  UpdateLauncher in 'UpdateLauncher.pas' {frm_updatelauncher},
  ThreadUpdLauncher in 'ThreadUpdLauncher.pas',
  News in 'News.pas' {frm_news},
  LauncherSettings in 'LauncherSettings.pas' {frm_launchersettings},
  ThreadJavaInstall in 'ThreadJavaInstall.pas',
  ThreadZipJava in 'ThreadZipJava.pas',
  GameSettings in 'GameSettings.pas' {frm_gamesettings},
  Help in 'Help.pas' {frm_help},
  ThreadGameUpdate in 'ThreadGameUpdate.pas',
  ThreadClear in 'ThreadClear.pas',
  ThreadUnZip in 'ThreadUnZip.pas',
  ThreadLaunch in 'ThreadLaunch.pas',
  Shop in 'Shop.pas' {frm_shop};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(Tfrm_log, frm_log);
  Application.CreateForm(Tfrm_news, frm_news);
  Application.Run;
end.
