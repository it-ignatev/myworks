unit Log;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, sSkinManager, Vcl.StdCtrls, acPNG,
  Vcl.ExtCtrls, sCheckBox, sEdit, sButton, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdHTTP, sLabel, Registry, ShellApi, IdHash,
   IdHashMessageDigest;

type
  Tfrm_log = class(TForm)
    sSkinManager1: TsSkinManager;
    te_login: TsEdit;
    te_password: TsEdit;
    ch_remember: TsCheckBox;
    pic_logo: TImage;
    Label1: TLabel;
    but_enter: TsButton;
    but_settings: TsButton;
    IdHTTP1: TIdHTTP;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    ch_auto: TsCheckBox;
    but_news: TsButton;
    but_forget: TsButton;
    procedure FormCreate(Sender: TObject);
    procedure but_enterClick(Sender: TObject);
    procedure but_forgetClick(Sender: TObject);
    procedure but_newsClick(Sender: TObject);
    procedure but_settingsClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Path: String;
    Appdata: String;
    Launcher: string;
    ans: string;
  end;

var
  frm_log: Tfrm_log;
  Registry: TRegistry;

implementation

{$R *.dfm}

uses Load, UpdateLauncher, News, LauncherSettings;

procedure Delay(Value: Cardinal);
var
  F, N: Cardinal;
begin
  N := 0;
  while N <= (Value div 10) do
  begin
    SleepEx(1, True);
    Application.ProcessMessages;
    Inc(N);
  end;
  F := GetTickCount;
  repeat
    Application.ProcessMessages;
    N := GetTickCount;
  until (N - F >= (Value mod 10)) or (N < F);
end;

function CheckLauncher():Boolean;
var
PostData:TStringList;
res:string;
TS : TStringList;
begin
    PostData:=TStringList.Create;
    PostData.Clear;
    PostData.Add('launcher=105');
    try
    res := frm_log.IdHTTP1.Post('http://188.40.54.76/client/launcher/launcher.php',PostData);
    except
    ShowMessage('��� �������� ���������� !(�������� ��������� ���������.)');
    Result := True;
    Exit;
    end;
    PostData.Free;

    res := Trim(res);

    TS := TStringList.Create;
    TS.Delimiter := '|';
    TS.DelimitedText := res;

    Result:= StrToBool(TS.Strings[0]);
    frm_log.Launcher := Ts.Strings[1];
end;


function GetWin(Comand: string): string;
var
  buff: array [0 .. $FF] of char;
begin
  ExpandEnvironmentStrings(PChar(Comand), buff, SizeOf(buff));
  Result := buff;
end;

function Auth(): boolean;
var
PostData:TStringList;
begin


  Delay(2000);

  if (frm_log.te_login.Text <> '') and (frm_log.te_password.Text <> '') then
  begin

    PostData:=TStringList.Create;
    PostData.Clear;
    PostData.Add('user=' + frm_log.te_login.Text);
    PostData.Add('password=' + frm_log.te_password.Text);
    PostData.Add('version=33');
    PostData.Add('sec=jinuinmoiuun@|_asd.sa6556.123sdgfa_./|asd');
    frm_log.ans :=frm_log.IdHTTP1.Post('http://188.40.54.76/user/loginserver.php',PostData);
    PostData.Free;
    frm_log.ans := Trim(frm_log.ans);

    if frm_log.ans <> '' then
    begin
      if (frm_log.ans <> 'Old version') and (frm_log.ans <> 'Bad login')then
      begin

        if frm_log.ch_remember.State = cbChecked then
        begin
          Registry := TRegistry.Create;
          { ������������� �������� ����; ������� hkey_local_machine ��� hkey_current_user }
          Registry.RootKey := hkey_current_user;
          { ��������� � ������ ���� }
          Registry.OpenKey('software\Badteam',true);
          { ���������� �������� }
          Registry.WriteString('login',frm_log.te_login.Text);
          Registry.WriteString('password',frm_log.te_password.Text);
          { ��������� � ����������� ���� }
          Registry.CloseKey;
          Registry.Free;
        end
        else
        begin
          Registry := TRegistry.Create;
          { ������������� �������� ����; ������� hkey_local_machine ��� hkey_current_user }
          Registry.RootKey := hkey_current_user;
          { ��������� � ������ ���� }
          Registry.OpenKey('software\Badteam',true);
          { ���������� �������� }
          Registry.WriteString('login','');
          Registry.WriteString('password','');
          { ��������� � ����������� ���� }
          Registry.CloseKey;
          Registry.Free;
        end;

        if frm_log.ch_auto.State = cbChecked then
        begin
          Registry := TRegistry.Create;
          { ������������� �������� ����; ������� hkey_local_machine ��� hkey_current_user }
          Registry.RootKey := hkey_current_user;
          { ��������� � ������ ���� }
          Registry.OpenKey('software\Badteam',true);
          { ���������� �������� }
          Registry.WriteString('autologin','true');
          { ��������� � ����������� ���� }
          Registry.CloseKey;
          Registry.Free;
        end
        else
        begin
          Registry := TRegistry.Create;
          { ������������� �������� ����; ������� hkey_local_machine ��� hkey_current_user }
          Registry.RootKey := hkey_current_user;
          { ��������� � ������ ���� }
          Registry.OpenKey('software\Badteam',true);
          { ���������� �������� }
          Registry.WriteString('autologin','false');
          { ��������� � ����������� ���� }
          Registry.CloseKey;
          Registry.Free;
        end;

        Application.CreateForm(Tfrm_load, frm_load);

        frm_load.show;
        frm_log.Hide;

      end
      else ShowMessage('�������! �� ����� �� ������ ����� ��� ������ !');
    end
    else showmessage('�������! � ��� ���� ��������� !');
  end;
end;

function MD5():boolean;
var MD5 : TIdHashMessageDigest5;
begin
  if Length(frm_log.te_password.Text) < 32 then
  begin
    MD5 := TIdHashMessageDigest5.Create;
    frm_log.te_password.Text := MD5.HashStringAsHex(frm_log.te_password.Text);
    MD5.Free;
  end;
end;

procedure Tfrm_log.but_enterClick(Sender: TObject);
begin
  MD5();
  Auth();
end;

procedure Tfrm_log.but_forgetClick(Sender: TObject);
begin
  ShellExecute (self.Handle, nil, 'http://badteam.ru/?page=recovery', nil, nil, SW_RESTORE);
end;

procedure Tfrm_log.but_newsClick(Sender: TObject);
begin
  frm_news.ShowModal;
end;

procedure Tfrm_log.but_settingsClick(Sender: TObject);
begin
  Application.CreateForm(Tfrm_launchersettings, frm_launchersettings);
  frm_launchersettings.ShowModal;
end;

procedure Tfrm_log.FormCreate(Sender: TObject);
var LauncherUPD: boolean;
begin
  //--��������� ������
  Registry := TRegistry.Create;
  Registry.RootKey := hkey_current_user;
  Registry.OpenKey('software\Badteam',True);

  //--���� ������� �����, �� ����� ������ � ���.
  if Registry.ReadString('memory') = '' then Registry.WriteString('memory', '980');
  if Registry.ReadString('java') = '' then Registry.WriteString('java', '�������������');

  //--��������� ���� �� ������ � �������
  if Registry.ReadString('login') <> '' then
  begin
    ch_remember.State := cbChecked;
    te_login.Text := Registry.ReadString('login');
    te_password.Text := Registry.ReadString('password');
  end;

  LauncherUPD := False;

  //--������� ������ �������
  if FileExists(ExtractFilePath(Application.ExeName) + 'Badteam.exe.bak') then
  begin
    Sleep(2500);
    DeleteFile(ExtractFilePath(Application.ExeName) + 'Badteam.exe.bak');
  end;

  //--��������� ������� �� ����� ������
  if CheckLauncher() = False then
  begin
    if MessageBox(self.Handle ,'��� ������� ������. �� ������ �������� ������� ?','���������� ��������',MB_YESNO) = id_Yes then
    begin
      Application.CreateForm(Tfrm_updatelauncher, frm_updatelauncher);
      frm_updatelauncher.Show;
      self.Hide;
      LauncherUPD := True;
    end
    else
    begin
      Application.Terminate;
      Exit;
    end;
  end;

  //--������� AppData
  appdata := GetWin('%AppData%');
  appdata := appdata + '\.badteam\';

  //--���� ������ ����, �� ������� ����� .badteam
  if not DirectoryExists(appdata) then ForceDirectories(appdata);

  //--��������
  if (Registry.ReadString('autologin') = 'true') And (LauncherUPD = False)then
  begin
    ch_auto.State := cbChecked;
    Registry.CloseKey;
    Registry.Free;
    MD5();
    Auth();
    Exit;
  end;

  Registry.CloseKey;
  Registry.Free;
end;

end.
