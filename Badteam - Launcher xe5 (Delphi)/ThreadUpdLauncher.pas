unit ThreadUpdLauncher;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, sLabel, Vcl.ComCtrls,
  acProgressBar, Vcl.ExtCtrls, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdHTTP, Registry, ShellApi;

type
  UpdLauncher = class(TThread)
  idHTTP: TIdHTTP;
  procedure idHTTPWork(ASender: TObject; AWorkMode: TWorkMode;AWorkCount: Int64);
  procedure idHTTPWorkBegin(ASender: TObject; AWorkMode: TWorkMode;AWorkCountMax: Int64);
  protected
    procedure Execute; override;
  end;

implementation

{ UpdLauncher }

uses UpdateLauncher;

procedure UpdLauncher.Execute;
var mem:TFileStream;
r:Integer;
server: String;
begin
  RenameFile(ExtractFileName(Application.ExeName),'Badteam.exe.bak');
  mem :=  TFileStream.Create(ExtractFilePath(Application.ExeName) + 'Badteam.exe', fmCreate);

  r := Random(10);
  if r >= 5 then
  begin
    server := 'http://bad.cthulchu.ru/';
  end
  else
  begin
    server := 'http://dl.dropboxusercontent.com/u/48129448/Badteam_Client/'
  end;

  try
    IdHTTP := TIdHTTP.Create;
    IdHTTP.OnWork := IdHTTPWork;
    IdHTTP.OnWorkBegin := idHTTPWorkBegin;
    IdHTTP.Get(server + 'Badteam.exe',mem);
  finally
    mem.Free;
  end;

  frm_updatelauncher.FrmClose();
end;

procedure UpdLauncher.idHTTPWork(ASender: TObject; AWorkMode: TWorkMode; AWorkCount: Int64);
var pos:integer;
begin
  pos := AWorkCount - frm_UpdateLauncher.pg_upd.Position;
  frm_UpdateLauncher.pg_upd.Position := frm_UpdateLauncher.pg_upd.Position + pos;
end;

procedure UpdLauncher.idHTTPWorkBegin(ASender: TObject; AWorkMode: TWorkMode; AWorkCountMax: Int64);
begin
  frm_UpdateLauncher.pg_upd.Position := 0;
  frm_UpdateLauncher.pg_upd.Max := AWorkCountMax;
end;

end.
