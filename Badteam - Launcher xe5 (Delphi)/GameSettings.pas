unit GameSettings;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, sButton, sEdit, sSpinEdit,
  sComboBox, sLabel, Registry;

type
  Tfrm_gamesettings = class(TForm)
    sLabel1: TsLabel;
    val_java: TsComboBox;
    val_mem: TsSpinEdit;
    sLabel2: TsLabel;
    but_save: TsButton;
    procedure FormCreate(Sender: TObject);
    procedure but_saveClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_gamesettings: Tfrm_gamesettings;

implementation

{$R *.dfm}

uses Log;

procedure Tfrm_gamesettings.but_saveClick(Sender: TObject);
var Registry: TRegistry;
begin
  if val_mem.Value <> 0 then
  begin
    Registry := TRegistry.Create;
    Registry.RootKey := hkey_current_user;
    Registry.OpenKey('software\Badteam',True);

    if val_java.Items.Strings[val_java.ItemIndex] = '����������' then
    begin
      if not DirectoryExists(frm_log.Appdata + 'Java\') then
      begin
        ShowMessage('���������� Java �� ����������� !');
        Exit;
      end;
    end;

    Registry.WriteString('java', val_java.Items.Strings[val_java.ItemIndex]);
    Registry.WriteString('memory', val_mem.Value.ToString());

    Registry.CloseKey;
    Registry.Free;

    self.close;
  end
  else ShowMessage('������� ���-�� ����������� ������ !');
end;

procedure Tfrm_gamesettings.FormCreate(Sender: TObject);
var Registry: TRegistry;
begin
  Registry := TRegistry.Create;
  Registry.RootKey := hkey_current_user;
  Registry.OpenKey('software\Badteam',True);

  val_java.ItemIndex := val_java.IndexOf(Registry.ReadString('java'));
  val_mem.Value := Registry.ReadString('memory').ToInteger;

  Registry.CloseKey;
  Registry.Free;
end;

end.
