unit ThreadUnZip;

interface

uses
  System.Classes, ZipForge, System.SysUtils, System.Variants;

type
  UnZip = class(TThread)
  private
    { Private declarations }
  protected
    procedure Execute; override;
  end;

implementation

{ UnZip }

uses Log, Load, ThreadLaunch;

procedure UnZip.Execute;
var
Archiver: TZipForge;
begin
  archiver := TZipForge.Create(nil);
  try
  with archiver do
  begin
    // The name of the ZIP file to unzip
    FileName := frm_log.Appdata + frm_load.client_d + '\bin\client.zip';
    // Open an existing archive
    OpenArchive(fmOpenRead);
    // Set base (default) directory for all archive operations
    BaseDir := frm_log.Appdata + frm_load.client_d + '\';
    // Extract all files from the archive to C:\Temp folder
    ExtractFiles('*.*');
    CloseArchive();
  end;
  except
  on E: Exception do
    begin
      Writeln('Exception: ', E.Message);
      // Wait for the key to be pressed
      Readln;
    end;
  end;

  Launch.Create(false).FreeOnTerminate := True;

  FreeOnTerminate := True;
  Free;

  Terminate;

end;

end.
