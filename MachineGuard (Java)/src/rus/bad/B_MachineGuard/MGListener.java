package rus.bad.B_MachineGuard;

import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.ApplicableRegionSet;

public class MGListener implements Listener {

	public static MachineGuard plugin;
	public WorldGuardPlugin wg;

	public MGListener(MachineGuard instance) {
		plugin = instance;
	}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		if (!event.hasBlock())return;

		int id = event.getClickedBlock().getTypeId();

		RegionManager regionManager = wg.getRegionManager(event.getClickedBlock().getWorld());
		ApplicableRegionSet set = regionManager.getApplicableRegions(event.getClickedBlock().getLocation());

		if (plugin.isBlocked(id)) {
			if (wg.canBuild(event.getPlayer(), event.getClickedBlock().getLocation()) || set.allows(DefaultFlag.CHEST_ACCESS)) {
			//������
			} else {
				event.getPlayer().sendMessage(plugin.msg);
				event.setCancelled(true);
			}
		}

		if (id == 137 && event.getClickedBlock().getData() == (byte) 3) {
			event.getPlayer().sendMessage("Tymczasowo wylaczone");
			event.setCancelled(true);
		}
	}
}
