<?php
    include_once("/php/main.php");
    $page = $_GET['page'];
    include_once("/page/auth.php");

    if(isset($page) && !empty($page))
    {
        if(isset(Settings::$pages[$page]))
        {
            if(Settings::$pages[$page][2] == true && !empty(Settings::$USERID) || Settings::$pages[$page][2] == false)
            {
                echo "<link href=\"/css/$page.css\" rel=\"stylesheet\" media=\"screen\">";
                $SCaption = Settings::$pages[$page][0];
                echo "<title>$SCaption</title>";
                $file = dirname(__FILE__)."/page/".Settings::$pages[$page][1];

                if(file_exists($file))
                {
                    $SText = file_get_contents($file);

                    if(Settings::$pages[$page][3] == true)
                    {
                        $user = SiteBD::getUserInformation(Settings::$USERID);

                        $replace = array(
                            "<nick>" => $user['username'],
                            "<mail>" => $user['mail'],
                            "<dateREG>" => $user['dateREG'],
                            "<lang>" => $user['language'],
                            "<money>" => $user['money'],
                            "<lastLogin>" => $user['dateLOGIN'],
							"<shop>" => SiteBD::getShop()
                        );

                        $SText = strtr($SText, $replace);
                    }
                }
                else
                    $SText = $file;
            }
            else
            {
                echo "<title>Доступ запрещен</title>";
                $SCaption = "Доступ запрещен";
            }
        }
        else
        {
            echo "<title>Страница не найдена</title>";
            $SCaption = "Страница не найдена";
        }
    }
    else
    {
        $SCaption = "Новости";
        echo "<title>Главная</title>";

        if(isset($_GET['nid']))
        {
            $news = SiteBD::getNews($_GET['nid']);

            $img = (empty($news['img']) ? "http://pimcraft.com:81/img/news1.png" : $news['img']);
            $SText = "<div style='margin-bottom: 60px;padding-left:54px;padding-right:54px;'>
            $news[title]<br>
            <span style='font-size: 14pt;'>$news[text]</span>
            <img src=\"$img\" style='width: 100%; height: auto;'>
            <div style='font-size: 14px;'>$news[time]</div>
            <button class='main-button' style='float: right;' onClick=\"javascript: location.href='/'\">назад</button>
            </div><hr>
            <div id=\"vk_comments\"></div>
            ";
        }
        else
        {
            $news = SiteBD::getNews();

            foreach ($news as $val) {
                $img = (empty($val['img']) ? "http://pimcraft.com:81/img/news1.png" : $val['img']);
                $SText .= "<div style='margin-bottom: 60px;padding-left:54px;padding-right:54px;'>
                $val[title]<br>
                <span style='font-size: 14pt;'>$val[text]</span>
                <img src=\"$img\" style='width: 100%; height: auto;'>
                <div style='font-size: 14px;'>$val[time]</div>
                <button class='main-button' style='float: right;' onClick=\"javascript: location.href='/?nid=$val[id]'\">подробнее</button>
                </div>
                <hr>";
            }
        }
    }
?>
<html>
    <head>
        <meta charset="utf-8">
        <link href="/css/main.css" rel="stylesheet" media="screen">
        <link rel="shortcut icon" href="favicon.ico">
        <script type="text/javascript" src="http://code.jquery.com/jquery-2.1.3.js"></script>
        <script type="text/javascript" src="http://vk.com/js/api/openapi.js?121"></script>
        <script>
            VK.init({apiId: 4906073, onlyWidgets: true});
            VK.Widgets.Comments("vk_comments", {limit: 10, width: "570", attach: false});

            var messages = {
                "SOMETHING_IS_EMPTY": "Извините, вы оставили пустое поле.",
                "ERROR_NAME_IS_TOO_LONG": "Извините, ваш ник слишком длинный.",
                "USER_IS_ALREADY_REGISTERED": "Извините, данный ник уже зарегистрирован.",
                "ERROR_USERNAME_IS_INCORRECT": "Извините, ваш ник введен некорректно.",
                "ERROR_MAIL_IS_INCORRECT": "Извините, ваш email введен некорректно.",
                "ERROR_LANG_NOT_FOUND": "Извините, язык не найден!",
                "USER_IS_ALREADY_REGISTERED": "Извините, пользователь с такими данными уже зарегистрирован.",
                "SUCCESS": "Операция выполнена успешно!",
                "USER_HAS_BEEN_BANNED":"Извините, данный аккаунт заблокирован.",
                "PASSWORD_IS_NOT_MATCH":"Извините, пароль к аккаунту введен неверно.",
                "ERROR_UPDATING_HASH":"Проблема при обновлении кеша!",
                "USER_IS_NOT_AUTHORIZATED":"Извините, но вы не авторизованы!",
                "ERROR_SESSION_IS_INCORRECT": "Извините, но ваша сессия устарела!",
                "ERROR_USER_NOT_FOUND": "Извините, но данного пользователя не существует!"
            }

            var regexes = {
                "MAIL": /^\w+?@\w+?\..+$/,
                "PASSWORD": /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$/,
                "LOGIN": /^[a-zA-Z0-9_]{3,15}$/,
                "PHONE": ""
            }

            function AlertSuccess(str, t)
            {
                $("#alert").removeClass();
                $("#alert").addClass("alert-success");
                $("#alert").html(str);
                $("#alert").fadeIn(t).delay(1500).fadeOut(t+500);
            }
            function AlertError(str, t)
            {
                $("#alert").removeClass();
                $("#alert").addClass("alert-error");
                $("#alert").html(str);
                $("#alert").fadeIn(t).delay(1500).fadeOut(t+500);
            }
            function logIn()
            {
                var vals = $('#enter_form').serialize() + "&method=login";

                $.ajax({
                type: 'POST',
                url: '/php/worker.php',
                data: vals,
                async: false,
                success: function(data) {
                    if(data.trim() == "SUCCESS")
                    {
                        AlertSuccess("Вы успешно авторизовались !", 1000);
                        setTimeout("location.href='/'", 1000);
                    }
                    else
                    {
                        if(messages[data]) data = messages[data];
                        AlertError(data, 1000);
                    }
                },
                error:  function(te, ar){
                    AlertError(data, 1000);
                }
                });
            }
            function logout()
            {
                AlertSuccess("Вы успешно вышли !", 1000);
                document.cookie = "HASH=; expires=Thu, 01 Jan 1970 00:00:01 GMT";
                document.cookie = "LOGIN=; expires=Thu, 01 Jan 1970 00:00:01 GMT";
                setTimeout("location.reload()", 1200);
            }
            function restore(str)
            {
                if(str)
                {
                    str = str.trim();
                    if(!str.match(regexes["MAIL"])&&!str.match(regexes["LOGIN"]))
                    {
                        AlertError("Введенная комбинация не содержит EMAIL или Логина!");
                    }else{
                        if(str.match(regexes["MAIL"]))
                            t = "e"; else t = "u";

                        var vals = "&method=restore&type="+t+"&val="+str;

                        $.ajax({
                        type: 'POST',
                        url: '/php/worker.php',
                        data: vals,
                        async: false,
                        success: function(data) {
                            if(data.trim() == "SUCCESS")
                            {
                                AlertSuccess("Мы отправили вам письмо на почту, следуйте дальнейшим инструкциям в письме.", 1000);
                                setTimeout("location.href='/'", 1000);
                            }
                            else
                            {
                                if(messages[data]) data = messages[data];
                                AlertError(data, 1000);
                            }
                        },
                        error:  function(te, ar){
                            AlertError(data, 1000);
                        }
                        });
                    }
                }
                else
                {
                    AlertError("Введите E-Mail или ник!");
                }
            }
        </script>
    </head>
    <body>
        <div>
            <div class="header">
                <a href="/"><img src="/img/logo.png" style="position: absolute; left: 0; top:-90;right:0;bottom:0; margin: auto; width: auto;"></a>
            </div>

            <div class="menu">
                <ul>
                    <a href="/"><li>ГЛАВНАЯ</li></a>
                    <a href="?page=faq"><li>ФОРУМ</li></a>
                    <a href="?page=bans"><li>БАН-ЛИСТ</li></a>
                </ul>
            </div>

            <div class="inf-block">
                <span class="caption">Добро пожаловать на SAKH TEAM</span>
                <br>
                <span class="text">Следите за новостями игры, присоединяйтесь к сообществу и голосуйте за наш проект!</span>
                <br>
                <div id="alert" style="display: none;"></div>
            </div>

            <div class="content">
                <aside>
                    <div class="aside-block auth">
                        <div class="aside-subtitle"></div>
                        <div class="aside-subblock">
                            <?php
                                echo $authform;
                            ?>
                        </div>
                    </div>
                    <div class="aside-block monitoring">
                        <div class="aside-subtitle"></div>
                        <div class="aside-subblock">
                            <div class="aside-title">Мониторинг</div>
                            <?php
                                $json = json_decode(file_get_contents(dirname(__FILE__)."/php/mon.json"), true);

                                if($json != null)
                                {
                                    foreach($json['Servers'] as $i => $n)
                                    {
                                        $server = $i;
                                        $online = ($n['status'] == "online" ? "$n[players]/$n[maxplayers]" : "Сервер не доступен");
                                        $percent = $n['percent'];

                                        $version = ($n['version'] != null ? $n['version'] : "Не доступно");
                                        $ping = ($n['ping'] != null || $n['ping'] == "0" ? $n['ping'] : "Не доступно");
                                        $time = date("H:i:s d.m.y", $n['time']);

                                        echo "<div style='font: normal 15pt Panton-ExtraLight, serif;padding: 4px;padding-left: 8px;padding-right: 8px; background-color: #d9d9d9;width: 150px;margin: auto; margin-top: 10px; margin-bottom: 10px;'>
                                            <span>$server</span>
                                            <div style='display: inline-block;float: right;'>$online</div>
                                        </div>";
                                    }
                                }
                            ?>
                        </div>
                    </div>
                    <div class="aside-block voting">
                        <div class="aside-subtitle"></div>
                        <div class="aside-subblock">
                            <div class="aside-title">Голосование</div>
                        </div>
                    </div>
                    <div class="aside-block links">
                        <div class="aside-subtitle"></div>
                      <div class="aside-subblock">
                        <div class="aside-title">Ссылки</div>
                        sdhjdj
                      </div>
                    </div>
                </aside>
                <section>
                    <div class="caption">
                      <?php
                        echo $SCaption;
                      ?>
                    </div>
                    <div class="text">
                      <?php
                        echo $SText;
                      ?>
                    </div>
                </section>
            </div>
        </div>
    </body>
    <footer>
        Copyright © 2015 SakhTeam.com. Дизайн VADIK:L3X. Разработчик bad.
    </footer>
</html>
