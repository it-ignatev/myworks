<?php
$str = <<<'EOD'

program ConsoleTest;
{$APPTYPE CONSOLE}
uses SysUtils;

begin
  WriteLn('Program ConsoleTest is running.'); readln(a);
  WriteLn('Press the ENTER key to stop');
  ReadLn;
end.

EOD;



$image = Struct::IO("input", array("a"));
// вывод изображения
header('Content-type: image/png');
imagepng($image);
imagedestroy($image);

class Struct{
    static function IO($type = "input", $params = null)
    {
        $values = array(
                    50,  0,  // Point 1 (x, y)
                    0,  100, // Point 2 (x, y)
                    200,  100,  // Point 3 (x, y)
                    250, 0,  // Point 4 (x, y)
                    );

        $im = imagecreatetruecolor(251, 101);
        imagefilledrectangle($im, 0, 0, 250, 100, imagecolorallocate($im, 255, 255, 255));
        $black = imagecolorallocate($im, 0, 0, 0);
        imageline($im, 0, 100, 50, 0, $black);
        imageline($im, 50, 0, 250, 0, $black);
        imageline($im, 250, 0, 200, 100, $black);
        imageline($im, 200, 100, 0, 100, $black);

        imagestring($im, 5, 110, 20, ($type == "input" ? "INPUT" : "OUTPUT"), $black);

        if($params != null)
        {
            $params = array_reverse($params);

            for($i = 0; $i < count($params); $i++)
            {
                imagestring($im, 5, (120+count($params)*2.8)-($i*12-12), 50, $params[$i], $black);
            }
        }
        //imagefilledpolygon($im, $values, 4, imagecolorallocate($im, 100, 0, 0));
        return $im;
    }
    static function BE($type = 1)
    {
        
    }
}
?>
