<?php
session_start();

$method = $_POST['method'];

$methods = array("login", "reg", "checkAuth", "changeMail", "changePassword", "skin", "restore", "ResetPass");

if(in_array($method, $methods))
{
    require_once("main.php");

    if($method == "reg")
    {
        $uname = $_POST['username'];
        $upass = $_POST['password'];
        $umail = $_POST['mail'];
        $ulang = $_POST['lang'];

        if(empty($uname) || empty($upass) || empty($umail) || empty($ulang)) exit("SOMETHING_IS_EMPTY");
        if(Util::checkType("string", $uname) === null) exit("ERROR_USERNAME_IS_INCORRECT");
        if(strlen($uname) > 15) exit("ERROR_NAME_IS_TOO_LONG");
        if(preg_match("/(\w+?@\w+?\x2E.+)/", $umail) == 0) exit("ERROR_MAIL_IS_INCORRECT");
        if(!in_array($ulang, array('ru', 'en'))) exit("ERROR_LANG_NOT_FOUND");
        if(SiteBD::CheckREG($uname, $umail) != null) exit("USER_IS_ALREADY_REGISTERED");

        $upass = Util::makePassword($upass);
        if(SiteBD::AddUser($uname, $upass, $ulang, $umail)) exit("SUCCESS");
        exit("FAIL");
    }

    if($method == "login")
    {
        $uname = $_POST['login'];
        $upass = $_POST['password'];
        $rem = $_POST['remember'];

        if(empty($uname) || empty($upass)) exit("SOMETHING_IS_EMPTY");
        if(Util::checkType("string", $uname) === null) exit("ERROR_USERNAME_IS_INCORRECT");
        if(strlen($uname) > 15) exit("ERROR_NAME_IS_TOO_LONG");
        $uid = SiteBD::SearchUser($uname);
        if($uid == null || empty($uid)) exit("USER_IS_NOT_REGISTERED");

        if(SiteBD::isBanned($uid)) exit("USER_HAS_BEEN_BANNED");
        if(!SiteBD::CheckPassword($uid, $upass)) exit("PASSWORD_IS_NOT_MATCH");

        $hash = '$2a$10$'.substr(str_replace('+', '.', base64_encode(pack('N4', mt_rand(), mt_rand(), mt_rand(),mt_rand()))), 0, 22) . '$';
        if(!SiteBD::UpdateHash($uid, $hash)) exit("ERROR_UPDATING_HASH");

        if($rem == "on")
        {
            setcookie("HASH", $hash, time()*10, "/");
            setcookie("LOGIN", $uname, time()*10,"/");
        }
        else
        {
            setcookie("HASH", $hash, time()+3600, "/");
            setcookie("LOGIN", $uname, time()+3600, "/");
        }

        exit("SUCCESS");
    }

    if($method == "changeMail")
    {
        $upass = $_POST['password'];
        $umail = $_POST['nmail'];

        $uname = $_COOKIE['LOGIN'];
        $uhash = $_COOKIE['HASH'];

        if(preg_match("/(\w+?@\w+?\x2E.+)/", $umail) == 0) exit("ERROR_MAIL_IS_INCORRECT");

        if(Util::checkType("string", $uname) != null)
        {
            if(SiteBD::CheckAuth($uname, $uhash))
            {
                if(SiteBD::CheckPassword(Settings::$USERID, $upass))
                {
                    SiteBD::UpdateMail(Settings::$USERID, $umail);
                    exit("SUCCESS");
                }
                else exit("PASSWORD_IS_NOT_MATCH");
            }
            else exit("USER_IS_NOT_AUTHORIZATED");
        }
        else exit("ERROR_USERNAME_IS_INCORRECT");
    }

    if($method == "changePassword")
    {
        $upass = $_POST['password'];
        $unewpass = $_POST['npassword'];

        $uname = $_COOKIE['LOGIN'];
        $uhash = $_COOKIE['HASH'];

        if(Util::checkType("string", $uname) != null)
        {
            if(SiteBD::CheckAuth($uname, $uhash))
            {
                if(SiteBD::CheckPassword(Settings::$USERID, $upass))
                {
                    $unewpass = Util::makePassword($unewpass);
                    SiteBD::UpdatePassword(Settings::$USERID, $unewpass);
                    Util::ClearCookies();
                    exit("SUCCESS");
                }
                else exit("PASSWORD_IS_NOT_MATCH");
            }
            else exit("USER_IS_NOT_AUTHORIZATED");
        }
        else exit("ERROR_USERNAME_IS_INCORRECT");
    }

    if($method == "skin")
    {
        $uname = $_COOKIE['LOGIN'];
        $uhash = $_COOKIE['HASH'];

        if(Util::checkType("string", $uname) != null)
        {
            if(SiteBD::CheckAuth($uname, $uhash))
            {
                define ("MAX_SIZE","1000");

            	$filename = stripslashes($_FILES['skin']['name']);
            	$size= filesize($_FILES['skin']['tmp_name']);
            	$data = getimagesize($_FILES['skin']['tmp_name']);
            	$width = $data[0];
            	$height = $data[1];

            	// проверка расширения
            	if(strtolower(getExtension($filename)) == "png")
            	{
            		// проверка размера файла
            		if ($size < (MAX_SIZE*1024))
            		{
            			if($width == ("1024" || "64" || "512" || "128") && $height == ("512" || "32" || "256" || "64"))
            			{
            				$uploaddir = '../skins/';
            				$image_name=$uname.'.png';
            				$newname=$uploaddir.$image_name;
            				// перемещение файла в папку uploads
            				move_uploaded_file($_FILES['skin']['tmp_name'], $newname);

            				exit("SUCCESS");
            			}
            			else
            			{
            				exit('IMGSIZE');
            			}
            		}
            		else
            		{
            			exit('SIZE');
            		}
            	}
            	else
            	{
            		exit('EXTENSION');
            	}
            }
            else exit("USER_IS_NOT_AUTHORIZATED");
        }
        else exit("ERROR_USERNAME_IS_INCORRECT");
    }

    if($method == "restore")
    {
        $type = $_POST['type'];
        $val = $_POST['val'];

        if(!in_array($type, array("u", "e"))) exit("ERROR_RESTORE_METHOD_NOT_FOUND");
        if(Util::checkType("string", $val) == null) exit("ERROR_USERNAME_IS_INCORRECT");

        if($type === "u")
        {
            $uid = SiteBD::SearchUser($val);
            if($uid == null) exit("ERROR_USER_NOT_FOUND");
            $inf = SiteBD::getUserInformation($uid);
            $npass = Util::rnd(13);
            SiteBD::AddRestore($uid, $npass);
            $npass = "http://sakhteam.com/?page=forgot&session=".$npass;
            $text = str_replace(array("<nick>", "<date>", "<npass>", "<email>"), array($inf['username'], time(), $npass, $inf['mail']), Settings::$LETTER_RECOVERY);

            Util::SendMail("Восстановление Аккаунта", $text, $inf["mail"], Settings::$MAIL_RECOVERY);

            exit("SUCCESS");
        }

        if($type === "e")
        {
            $uid = SiteBD::SearchUser(null, $val);
            if($uid == null) exit("ERROR_USER_NOT_FOUND");
            $inf = SiteBD::getUserInformation($uid);
            $npass = Util::rnd(13);
            SiteBD::AddRestore($uid, $npass);
            $npass = "http://sakhteam.com/?page=forgot&session=".$npass;
            $text = str_replace(array("<nick>", "<date>", "<npass>", "<email>"), array($inf['username'], time(), $npass, $inf['mail']), Settings::$LETTER_RECOVERY);

            Util::SendMail("Восстановление Аккаунта", $text, $inf["mail"], Settings::$MAIL_RECOVERY);

            exit("SUCCESS");
        }
    }

    if($method == "ResetPass")
    {
        $session = $_POST['ses'];
        $password = $_POST['pass'];

        if(Util::checkType("string", $session) == null) exit("ERROR_SESSION_IS_INCORRECT");

        $uid = SiteBD::GetRestore($session);
        if($uid == null) exit("ERROR_SESSION_IS_NOT_FOUND");
        $npass = Util::makePassword($password);
        SiteBD::DeleteRestore($session);
        SiteBD::UpdatePassword($uid['uid'], $npass);

        exit("SUCCESS");
    }
}
else exit("METHOD_NOT_FOUND");


function getExtension($str)
{
	$i = strrpos($str,".");
	if (!$i) { return ""; }
	$l = strlen($str) - $i;
	$ext = substr($str,$i+1,$l);
	return $ext;
}
 ?>
