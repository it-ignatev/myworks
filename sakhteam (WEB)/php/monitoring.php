﻿<?php
echo "TIMESTAMP: ".time().MinecraftServer::WriteJSON();

class MinecraftServer {// Class written by xPaw & modded by book777
	const STATISTIC = 0x00;
	const HANDSHAKE = 0x09;
	private static $socket;

	static function WriteFile($json)
	{
		chdir(__DIR__);
		$file = fopen("recors.json", "w+");
		fwrite($file, json_encode($json));
		fclose($file);
	}

	static function WriteJSON()
	{
		$servers["HiTech"] = "188.242.163.78:25565";
		//$servers["PimFTB"] = "127.0.0.1:25566";

		while ($serverip = current($servers)) {
			$server = key($servers);

			$row = self::getq($serverip);
			$stat['time'] = time();
			$stat['status'] = $row['status'];
			$stat['players'] = $row['online'];
			$stat['maxplayers'] = $row['slots'];
			$stat['percent'] =  $row['percent'];
			$stat['ping'] =  $row['ping'];
			$stat['version'] =  $row['version'];
			$result[$server] = $stat;

			$rec = self::SearchOnlineRecord($server, "total");
			if($rec == null || $rec < $row['online'])
			{
				self::WriteRecords($server, "total", $row['online']);
			}
			$rec = self::SearchOnlineRecord($server, "day");
			$rtime = self::SearchOnlineRecord($server, "dayTime");
			if($rec == null || $rec < $row['online'] || $rtime == null || $rtime < time())
			{
				if($rtime == null || $rtime < time())
				{
					self::WriteRecords($server, "day", 0);
					self::WriteRecords($server, "dayTime", time() + 24*60*60);
				}
				else
				{
					self::WriteRecords($server, "day", $row['online']);
				}
			}

			next($servers);
		}

		$json = array("Servers" => $result);

		chdir(__DIR__);
		$file = fopen("mon.json", "w");
		fwrite($file, json_encode($json));
		fclose($file);
	}

	static function WriteRecords($server, $type, $val)
	{
		$json = json_decode(file_get_contents("recors.json"), true);

		if($json != null)
		{
			foreach ($json['Servers'] as $key => $value)
			{
				if($key == $server)
				{
					unset($value[$type]);
					$json['Servers'][$server][$type] = $val;
					self::WriteFile($json);

					return true;
				}
			}
		}
		else
		{
			$json['Servers'] = array($server => array($type => $val));
			self::WriteFile($json);

			return true;
		}
	}

	static function SearchOnlineRecord($server, $type = "total")
	{
		$json = json_decode(file_get_contents("recors.json"), true);

		if($json != null)
		{
			foreach ($json['Servers'] as $key => $value)
			{
				if($key == $server)
				{
					return $value[$type];
				}
			}
		}
		else
		{
			return null;
		}
	}

	static function getp($address, $timeout = 3) {
		$thetime = microtime(true);

		$address = explode(":", $address);
		$port = $address[1];
		$address = $address[0];

		if(!$in = @fsockopen($address, $port, $errno, $errstr, $timeout))
			return array(
				'address' => $address,
				'status' => 'Выключен'
			);
		if(round((microtime(true)-$thetime)*1000) > $timeout * 1000)
			return array(
				'address' => $address,
				'status' => 'Большой пинг'
			);
		@stream_set_timeout($in, $timeout);
		$ping = round((microtime(true)-$thetime)*1000);
		fwrite($in, "\xFE\x01");
		$data = fread($in, 4096);
		$Len = strlen($data);
		if($Len < 4 || $data[0] !== "\xFF")
			return array(
				'address' => $address,
				'status' => 'Неизвестное ядро'
			);
		$data = substr($data, 3);
		$data = iconv('UTF-16BE', 'UTF-8', $data);
		if($data [1] === "\xA7" && $data[2] === "\x31") {
			$data = explode("\x00", $data);
			return  array(
				'address' => $address,
				'status' => 'online',
				'online' => intval($data[4]),
				'motd' => self::motd($data[3]),
				'type' => $data[0],
				'slots' => intval($data[5]),
				'percent' => @floor((intval($data[4])/intval($data[5]))*100),
				'version' => $data[2],
				'ping' => $ping
			);
		}
		$data = explode("\xA7", $data);
		return array(
			'address' => $address,
			'status' => 'online',
			'online' => isset($data[1]) ? intval($data[1]) : 0,
			'slots' => isset($data[2]) ? intval($data[2]) : 0,
			'percent' => @floor((intval($data[1])/intval($data[2]))*100),
			'version' => '< 1.4',
			'ping' => $ping
		);
	}

	static function getq($address, $timeout = 3) {
		$thetime = microtime(true);

		$address = explode(":", $address);
		$port = $address[1];
		$address = $address[0];

		self::$socket = @fsockopen('udp://'.$address, $port, $ErrNo, $ErrStr, $timeout);
		if(self::$socket === false)
			return array(
				'status' => 'Выключен',
				'address' => $address
			);
		stream_set_timeout(self::$socket, $timeout);
		stream_set_blocking(self::$socket, true);
		$Challenge = self::GetChallenge();
		$info = array('ping' => round((microtime(true)-$thetime)*1000));
		$data = self::writedata(self :: STATISTIC, $Challenge.Pack('c*', 0x00, 0x00, 0x00, 0x00));
		if(!$data)
			return self::getp($address.":".$port, $timeout);// Пробуем получить данные обычным способом
		fclose(self::$socket);
		$Last = '';
		$data = substr($data, 11);
		$data = explode("\x00\x00\x01player_\x00\x00", $data);
		if(count($data) !== 2)
			return array(
				'status' => 'Неудачная дешифрация',
				'address' => $address
			);
		$info['names'] = explode("\x00", substr($data[1], 0, -2));
		$data = explode("\x00", $data[0]);
		$Keys = array(
			'numplayers' => 'online',
			'maxplayers' => 'slots',
			'hostname' => 'motd',
			'version' => 'version',
			'gametype' => 'type',
			'game_id' => 'game',
			'plugins' => 'plugins',
			'map' => 'map'
		);
		if($info['plugins']) {
			$data = explode(': ', $info['plugins'], 2);
			$info['core'] = $data[0];
			if(sizeof($data) == 2)
				$info['plugins'] = explode('; ', $data[1]);
		} else {
			$info['core'] = $info['plugins'];
			unset($info['plugins']);
		}
		foreach($data as $Key => $Value) {
			if(~$Key & 1) {
				if(!array_key_exists($Value, $Keys)) {
					$Last = false;
					continue;
				}
				$Last = $Keys[$Value];
				$info[$Last] = '';
			}
			else if($Last != false)
				$info[$Last] = $Value;
		}
		$info += array(
			'status' => 'online',
			'address' => $address,
			'online' => intval($info['online']),
			'slots' => intval($info['slots']),
			'percent' => @floor(($info['online']/$info['slots'])*100)
		);
		return $info;
	}
	private function writedata($command, $Append = '') {
		$command = Pack('c*', 0xFE, 0xFD, $command, 0x01, 0x02, 0x03, 0x04).$Append;
		$Length = strlen($command);
		if( $Length !== fwrite(self::$socket, $command, $Length))
			return array(
				'status' => 'Неудачный запрос'
			);
		$data = fread(self::$socket, 4096);
		if( $data === false )
			return array(
				'status' => 'Не удалось прочитать ответ'
			);
		if(strlen($data) < 5 || $data[0] != $command[2])
			return false;
		return substr($data, 5);
	}
	private function motd($text) {
		$mass = explode('§', $text);
		foreach ($mass as $val)
			$out .= substr($val, 1);
		return $out;
	}
	private function GetChallenge() {
		$data = self::writedata(self :: HANDSHAKE);
		if($data === false)
			return array(
				'status' => 'failed to receive challenge'
			);
		return Pack('N', $data);
	}
}

?>
