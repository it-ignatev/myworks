<?php
class Settings{
  //Настройки базы данных
  protected static $dbHost = "localhost";//ip-адрес
  protected static $dbPort = "3306";//Порт подключения
  protected static $dbUser = "Site";//Пользователь
  protected static $dbPass = "Site";//Пароль
  protected static $dbBase = "sakhteam";//Имя базы данных (для сайта)

  protected static $news = 4;//кол-во новостей

  public static $pages = array(
    "reg" => array(
      "Регистрация",
      "registration.html",
      false,
      false
    ),
    "settings" => array(
        "Настройки",
        "settings.html",
        true,
        true
    ),
    "cabinet" => array(
        "Личный Кабинет",
        "cabinet.html",
        true,
        true
    ),
    "shop" => array(
        "Магазин",
        "shop.html",
        true,
        true
    ),
    "bans" => array(
        "Бан-Лист",
        "banlist.html",
        false,
        false
    ),
    "faq" => array(
        "FAQ",
        "faq.html",
        false,
        false
    ),
    "forgot" => array(
        "Восстановление пароля",
        "forgot.html",
        false,
        false
    )
  );

  public static $USERID;
  public static $MAIL_RECOVERY = "restore@sakhteam.com";
  public static $LETTER_RECOVERY = "Здравствуйте, <nick>!\r
    Вы пытались восстановить пароль. Чтобы изменить пароль перейдите по ссылке: <npass>
  ";
}
class Util{
    static function makePassword($pass)
    {
        $salt = '$2a$10$'.substr(str_replace('+', '.', base64_encode(pack('N4', mt_rand(), mt_rand(), mt_rand(),mt_rand()))), 0, 22) . '$';
		return crypt($pass, $salt);
    }
    function uuid($uuid)
    {
      if(!isset($uuid) || empty($uuid)) return null;

      if(!preg_match("/^[a-zA-Z0-9_-]+$/", $uuid))
        return null;

      $uuid = substr($uuid, 0, 8)."-".substr($uuid, 8);
      $uuid = substr($uuid, 0, 13)."-".substr($uuid, 13);
      $uuid = substr($uuid, 0, 18)."-".substr($uuid, 18);
      $uuid = substr($uuid, 0, 23)."-".substr($uuid, 23);

      return $uuid;
    }
    static function checkType($type, $var)
    {
        if(empty($var)) return null;

        switch ($type) {
            case 'int':
              return intval($var);
            case 'string':
              return preg_match("/^[A-Za-z0-9\_\@\.]+$/", $var) > 0 ? $var : null;
            case 'float':
              return (float)$var;
            default:
              return null;
        }
      return null;
    }
    static function ClearCookies()
    {
        unset($_COOKIE['HASH']);
        unset($_COOKIE['LOGIN']);
        setcookie('HASH', null, -1, '/');
        setcookie('LOGIN', null, -1, '/');
    }
    static function SendMail($topic, $text, $mail, $from)
    {
        $headers['From']    = $from;
		$headers['To']      = $mail;
		$headers['Subject'] = $topic;
		$headers['Delivered-To'] = $from;
		$headers['Return-path'] = $from;
		$headers['Content-Type'] = "multipart/alternative; charset=UTF-8;";
		$headers['Reply-To'] = $mail;

        $header = "From:$from\r\nTo:$mail\r\nDelivered-To:$from\r\nReturn-path:$from\r\nContent-Type:text/plain; charset=UTF-8;\r\nReply-To:$mail\r\n";

        return mail($mail, $topic, $text, $header);
    }
    static function rnd($length)
    {
        $random_string = "";
        $valid_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        $num_valid_chars = strlen($valid_chars);

        for ($i = 0; $i < $length; $i++)
        {
            $random_pick = mt_rand(1, $num_valid_chars);
            $random_char = $valid_chars[$random_pick-1];
            $random_string .= $random_char;
        }
        return $random_string;
    }
}
class SiteBD extends Settings{
    static function connect()
    {
        try {
            $pdo = new PDO("mysql:host=".parent::$dbHost.";port=".parent::$dbPort.";dbname=".parent::$dbBase.";charset=UTF8;", parent::$dbUser, parent::$dbPass, array(
              PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
              PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true,
              PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
              PDO::ATTR_EMULATE_PREPARES => false
            ));
        } catch (PDOException $e) {
            exit($e->getMessage());
        }

      return $pdo;
    }
    static function AddRestore($uid, $session)
    {
        $pdo = self::connect();
        $sql = "INSERT INTO `Restore` (`uid`, `session`, `date`) VALUES (:uid, :ses, now())";
        $sql = $pdo->prepare($sql);
        $sql->bindValue('uid', $uid);
        $sql->bindValue('ses', $session);
        $sql->execute();

        if($sql->rowCount() > 0) return true;
        return false;
    }
    static function AddUser($uname, $upass, $ulang, $umail)
    {
        $pdo = self::connect();
        $sql = "INSERT INTO `Accounts` (`username`, `password`, `language`, `mail`) VALUES (:name, :pass, :lang, :mail)";
        $sql = $pdo->prepare($sql);
        $sql->bindValue('name', $uname);
        $sql->bindValue('pass', $upass);
        $sql->bindValue('lang', $ulang);
        $sql->bindValue('mail', $umail);
        $sql->execute();

        if($sql->rowCount() > 0) return true;
        return false;
    }
    static function CheckREG($name, $mail)
    {
        $pdo = self::connect();
        $sql = "SELECT `id` FROM `Accounts` WHERE `username`=:name OR `mail`=:mail";
        $sql = $pdo->prepare($sql);
        $sql->bindValue('name', $name);
        $sql->bindValue('mail', $mail);
        $sql->execute();

        if($sql->rowCount() > 0)
        {
            $result = $sql->fetch(PDO::FETCH_ASSOC);
            return $result['id'];
        }

        return null;
    }
    static function SearchUser($name, $mail = null, $ip = null)
    {
        $pdo = self::connect();
        $sql = "SELECT `id` FROM `Accounts` WHERE BINARY `username`=:name OR BINARY `mail`=:mail";
        $sql = $pdo->prepare($sql);
        $sql->bindValue('name', $name);
        $sql->bindValue('mail', $mail);
        $sql->execute();

        if($sql->rowCount() == 1)
        {
            $result = $sql->fetch(PDO::FETCH_ASSOC);
            return $result['id'];
        }

        return null;
    }
    static function isBanned($uid, $mac = null)
    {
        $pdo = self::connect();
        $sql = "SELECT `id` FROM `Accounts` WHERE `id`=:id AND `isBanned`=1";
        $sql = $pdo->prepare($sql);
        $sql->bindValue('id', $uid);
        $sql->execute();

        if($sql->rowCount() > 0) return true;
        return false;
    }
    static function CheckPassword($uid, $upass)
    {
        $pdo = self::connect();
        $sql = "SELECT `password` FROM `Accounts` WHERE `id`=:id";
        $sql = $pdo->prepare($sql);
        $sql->bindValue('id', $uid);
        $sql->execute();

        if($sql->rowCount() != 1) return false;
        $pass = $sql->fetch(PDO::FETCH_ASSOC);
        if(crypt($upass, $pass['password']) === $pass['password']) return true;
        return false;
    }
    static function UpdateHash($uid, $hash)
    {
        $pdo = self::connect();
        $sql = "UPDATE `Accounts` SET `auth_hash`=:hash, `dateLOGIN`=NOW() WHERE `id`=:id";
        $sql = $pdo->prepare($sql);
        $sql->bindValue('hash', $hash);
        $sql->bindValue('id', $uid);
        $sql->execute();

        if($sql->rowCount() == 1) return true;
        return false;
    }
    static function UpdateMail($uid, $nmail)
    {
        $pdo = self::connect();
        $sql = "UPDATE `Accounts` SET `mail`=:mail WHERE `id`=:id";
        $sql = $pdo->prepare($sql);
        $sql->bindValue('mail', $nmail);
        $sql->bindValue('id', $uid);
        $sql->execute();

        if($sql->rowCount() == 1) return true;
        return false;
    }
    static function UpdatePassword($uid, $npass)
    {
        $pdo = self::connect();
        $sql = "UPDATE `Accounts` SET `password`=:pass WHERE `id`=:id";
        $sql = $pdo->prepare($sql);
        $sql->bindValue('pass', $npass);
        $sql->bindValue('id', $uid);
        $sql->execute();

        if($sql->rowCount() == 1) return true;
        return false;
    }
    static function CheckAuth($uname, $uhash)
    {
        $pdo = self::connect();
        $sql = "SELECT `id` FROM `Accounts` WHERE BINARY `username`=:name AND BINARY `auth_hash`=:hash";
        $sql = $pdo->prepare($sql);
        $sql->bindValue('name', $uname);
        $sql->bindValue('hash', $uhash);
        $sql->execute();

        if($sql->rowCount() == 1)
        {
            $result = $sql->fetch(PDO::FETCH_ASSOC);
            parent::$USERID = $result['id'];
            return true;
        }
        parent::$USERID = null;
        return false;
    }
    static function getUserInformation($uid)
    {
        $pdo = self::connect();
        $sql = "SELECT `username`, `language`, `money`, `mail`, `dateREG`, `dateLOGIN` FROM `Accounts` WHERE `id`=:id";
        $sql = $pdo->prepare($sql);
        $sql->bindValue('id', $uid);
        $sql->execute();

        if($sql->rowCount() == 1) return $sql->fetch(PDO::FETCH_ASSOC);
        return null;
    }
    static function getNews($id = null)
    {
        $pdo = self::connect();
        $sql = isset($id) ?
            "SELECT `title`, `text`, `img`, `author`, `time` FROM `News` WHERE `id`=:id ORDER BY `id` DESC LIMIT :lim" :
            "SELECT `title`, `text`, `img`, `author`, `time`, `id` FROM `News` ORDER BY `id` DESC LIMIT :lim";
        $sql = $pdo->prepare($sql);
        $sql->bindValue('lim', Settings::$news);
        if(isset($id)) $sql->bindValue('id', $id);
        $sql->execute();

        if($sql->rowCount() > 0&&!isset($id)) return $sql->fetchAll(PDO::FETCH_ASSOC);
        if($sql->rowCount() > 0&&isset($id)) return $sql->fetch(PDO::FETCH_ASSOC);
        return null;
    }
    static function GetRestore($session)
    {
        $pdo = self::connect();
        $sql = "SELECT `uid` FROM `Restore` WHERE BINARY `session`=:ses";
        $sql = $pdo->prepare($sql);
        $sql->bindValue('ses', $session);
        $sql->execute();

        if($sql->rowCount() > 0) return $sql->fetch(PDO::FETCH_ASSOC);
        return null;
    }
    static function DeleteRestore($session)
    {
        $pdo = self::connect();
        $sql = "DELETE FROM `Restore` WHERE BINARY `session`=:ses";
        $sql = $pdo->prepare($sql);
        $sql->bindValue('ses', $session);
        $sql->execute();

        if($sql->rowCount() > 0) return true;
        return false;
    }
	static function getShop()
	{
		//$db = mysql_connect('mysql.hostinger.ru','u967720486_sakh','password') or die(mysql_error()); 
		//mysql_select_db("u967720486_sakh", $db) or die("Не могу соединиться с базой"); 
		//$sql = mysql_query("SELECT * FROM `shop`", $db); 
		//while($row = mysql_fetch_array($sql)) 
		//{ 
			return " 
			<ul2> 
			<li2 style=background-repeat: no-repeat;background-position: left bottom;background-size: 70px 70px;> 
			<form method=POST> 
			Название: ".$row['name']."<br> 
			Цена: ".$row['cost']."<br> 
			Описание: ".$row['description']."<br> 

			<input type=submit name=a_name value=Купить> 
			</form> 
			</li2> 
			</ul2>"; 
		//} 
		//mysql_close($db); 
		/* 
		if (isset($_POST['a_name'])) 
		{ 
		include_once('rcon.php'); 
		$rcon = new CServerRcon('91.147.3.211','27015','43864Commo'); 
		if($rcon->Auth()) 
		{ 
		$rcon->rconCommand('status'); 
		echo 'Команда выполнена'; 
		} 
		}*/ 
	}
}
?>
