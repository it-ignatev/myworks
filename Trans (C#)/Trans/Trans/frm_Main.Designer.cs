﻿namespace Trans
{
    partial class frm_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_open_vocab = new System.Windows.Forms.Button();
            this.btn_open_invoice = new System.Windows.Forms.Button();
            this.btn_open_information = new System.Windows.Forms.Button();
            this.btn_invoices = new System.Windows.Forms.Button();
            this.btn_ordering = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_open_vocab
            // 
            this.btn_open_vocab.Location = new System.Drawing.Point(12, 59);
            this.btn_open_vocab.Name = "btn_open_vocab";
            this.btn_open_vocab.Size = new System.Drawing.Size(229, 41);
            this.btn_open_vocab.TabIndex = 0;
            this.btn_open_vocab.Text = "Справочники";
            this.btn_open_vocab.UseVisualStyleBackColor = true;
            this.btn_open_vocab.Click += new System.EventHandler(this.btn_open_vocab_Click);
            // 
            // btn_open_invoice
            // 
            this.btn_open_invoice.Location = new System.Drawing.Point(12, 12);
            this.btn_open_invoice.Name = "btn_open_invoice";
            this.btn_open_invoice.Size = new System.Drawing.Size(229, 41);
            this.btn_open_invoice.TabIndex = 1;
            this.btn_open_invoice.Text = "Заказы";
            this.btn_open_invoice.UseVisualStyleBackColor = true;
            this.btn_open_invoice.Click += new System.EventHandler(this.btn_open_invoice_Click);
            // 
            // btn_open_information
            // 
            this.btn_open_information.Location = new System.Drawing.Point(12, 106);
            this.btn_open_information.Name = "btn_open_information";
            this.btn_open_information.Size = new System.Drawing.Size(229, 41);
            this.btn_open_information.TabIndex = 2;
            this.btn_open_information.Text = "Информация";
            this.btn_open_information.UseVisualStyleBackColor = true;
            this.btn_open_information.Click += new System.EventHandler(this.btn_open_information_Click);
            // 
            // btn_invoices
            // 
            this.btn_invoices.Location = new System.Drawing.Point(12, 153);
            this.btn_invoices.Name = "btn_invoices";
            this.btn_invoices.Size = new System.Drawing.Size(229, 41);
            this.btn_invoices.TabIndex = 3;
            this.btn_invoices.Text = "Счета";
            this.btn_invoices.UseVisualStyleBackColor = true;
            this.btn_invoices.Click += new System.EventHandler(this.btn_invoices_Click);
            // 
            // btn_ordering
            // 
            this.btn_ordering.Location = new System.Drawing.Point(12, 200);
            this.btn_ordering.Name = "btn_ordering";
            this.btn_ordering.Size = new System.Drawing.Size(229, 41);
            this.btn_ordering.TabIndex = 4;
            this.btn_ordering.Text = "Выписка";
            this.btn_ordering.UseVisualStyleBackColor = true;
            this.btn_ordering.Click += new System.EventHandler(this.btn_ordering_Click);
            // 
            // frm_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(253, 250);
            this.Controls.Add(this.btn_ordering);
            this.Controls.Add(this.btn_invoices);
            this.Controls.Add(this.btn_open_information);
            this.Controls.Add(this.btn_open_invoice);
            this.Controls.Add(this.btn_open_vocab);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frm_Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Главная";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_open_vocab;
        private System.Windows.Forms.Button btn_open_invoice;
        private System.Windows.Forms.Button btn_open_information;
        private System.Windows.Forms.Button btn_invoices;
        private System.Windows.Forms.Button btn_ordering;
    }
}

