﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Trans
{
    public partial class frm_Main : Form
    {
        public static Form self;

        public frm_Main()
        {
            InitializeComponent();

            self = this;

            new Logger();
            Sql sql = new Sql();
            Logger.info("SQL CLASS IS LOADED");
        }

        private void btn_open_vocab_Click(object sender, EventArgs e)
        {
            Form frm_vocab = new frm_vocab();
            frm_vocab.ShowDialog();
        }

        public static void Message(string message)
        {
            MessageBox.Show(message);
        }

        public static void Close()
        {
            Application.Exit();
            Environment.Exit(0);
        }

        private void btn_open_information_Click(object sender, EventArgs e)
        {
            Form frm_inf = new frm_information();
            frm_inf.ShowDialog();
        }

        private void btn_open_invoice_Click(object sender, EventArgs e)
        {
            Form frm_order = new Forms.frm_order();
            frm_order.ShowDialog();
        }

        private void btn_invoices_Click(object sender, EventArgs e)
        {
            Form frm_inv = new Forms.frm_invoices();
            frm_inv.ShowDialog();
        }

        private void btn_ordering_Click(object sender, EventArgs e)
        {
            Form frm_order = new Forms.frm_ordering();
            frm_order.ShowDialog();
        }
    }
}
