﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.IO;
using Microsoft.Win32;

namespace Trans
{
    class Logger
    {
        private static string p = AppDomain.CurrentDomain.BaseDirectory;

        [DllImport("kernel32.dll")]
        static extern bool AllocConsole();
        [DllImport("kernel32.dll")]
        static extern bool AttachConsole(int pw);
        private static bool _hasConsole, _keepConsoleOpen;
        static private void EnsureConsole()
        {
            _hasConsole = _hasConsole || AttachConsole(-1);
            _keepConsoleOpen = !_hasConsole || _keepConsoleOpen;
            _hasConsole = _hasConsole || AllocConsole();
        }

        [STAThread]
        private static void Main(string[] args)
        {
                EnsureConsole();
        }
        public Logger()
        {
            Main(null);

            log("LOGGER IS RUNNING");        
        }
        public static void log(string text)
        {
            Write("LOG", text);
        }
        public static void info(string text)
        {
            Write("INFO", text);
        }
        public static void error(string text)
        {
            Write("ERROR", text);
        }
        private static void Write(string inf, string text)
        {
            Console.WriteLine("[" + DateTime.Now.ToString("HH:mm:ss") + "] [" + inf + "] " + text);
            File.AppendAllText(p + "loger.log", "[" + DateTime.Now.ToString("HH:mm:ss") + "] [" + inf + "] " + text + Environment.NewLine);
        }
    }
}
