﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System.Windows.Forms;

namespace Trans
{
    class Sql
    {
        public static MySqlConnection mycon;
        public Sql()
        {
            Logger.info("MAKING CONNETION WITH SERVER...");

            try
            {
                string constr = "Server=146.120.67.132;Database=sfb7911d_trans;Uid=sfb7911d_trans;Pwd=123456;Allow User Variables=True";
                mycon = new MySqlConnection(constr);
                mycon.Open();

                Logger.info("SUCCESSFULLY !");
                frm_Main.Message("Соединение с сервером установлено !");

            }
            catch (MySqlException e)
            {
                Logger.error("CAN'T CONNECT TO SERVER, WITH EXCEPTION " + e.ToString());
                frm_Main.Message("Нет подключения к серверу ! \r\n" + e.Message);
                mycon.Close();
                frm_Main.Close();
            }
        }
        public static void Check()
        {            
            if(mycon.State == System.Data.ConnectionState.Open || mycon.State == System.Data.ConnectionState.Connecting || mycon.State == System.Data.ConnectionState.Broken) mycon.Close();
            
            try
            {
                string constr = "Server=146.120.67.132;Database=sfb7911d_trans;Uid=sfb7911d_trans;Pwd=123456;Allow User Variables=True";
                mycon = new MySqlConnection(constr);
                mycon.Open();
            }
            catch (MySqlException e)
            {             
                Console.WriteLine(e.ToString());
                return;
            }
        }
        public static string AddClient(string obj, string name, string inn, string kpp, string adr1, string adr2, string p_c, string bank, string bik, string pc, string ad)
        {
            Check();

            try
            {
                MySqlCommand cmd = new MySqlCommand();
                //OleDbCommand cmd = new OleDbCommand();

                cmd.Connection = mycon;
                cmd.CommandText = "INSERT INTO Clients(object, name, inn, kpp, address_1, address_2, p_c, bank, bik, pc, additional, `time`, `date`, `editTime`) VALUES(@object, @name, @inn, @kpp, @address_1, @address_2, @p_c, @bank, @bik, @pc, @additional, @time, @date, @et)";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@object", obj);
                cmd.Parameters.AddWithValue("@name", name);
                cmd.Parameters.AddWithValue("@inn", inn);
                cmd.Parameters.AddWithValue("@kpp", kpp);
                cmd.Parameters.AddWithValue("@address_1", adr1);
                cmd.Parameters.AddWithValue("@address_2", adr2);
                cmd.Parameters.AddWithValue("@p_c", p_c);
                cmd.Parameters.AddWithValue("@bank", bank);
                cmd.Parameters.AddWithValue("@bik", bik);
                cmd.Parameters.AddWithValue("@pc", pc);
                cmd.Parameters.AddWithValue("@additional", ad);
                cmd.Parameters.AddWithValue("@time",DateTime.Now.TimeOfDay);
                cmd.Parameters.AddWithValue("@date", DateTime.Now.Date);
                cmd.Parameters.AddWithValue("@et", DateTime.Now);
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return "Error #1";
            }
            
            return "Добавлено !";         
        }

        public static string AddCompany(string name, string adr, string iban, string bank, string swift)
        {
            Check();

            try
            {
                MySqlCommand cmd = new MySqlCommand();
                //OleDbCommand cmd = new OleDbCommand();

                cmd.Connection = mycon;
                cmd.CommandText = "INSERT INTO Companies(name, adress, iban, bank, swift, `time`, `date`, editTime) VALUES(@name, @adress, @iban, @bank, @swift, @time, @date, @et)";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@name", name);
                cmd.Parameters.AddWithValue("@adress", adr);
                cmd.Parameters.AddWithValue("@iban", iban);        
                cmd.Parameters.AddWithValue("@bank", bank);
                cmd.Parameters.AddWithValue("@swift", swift);
                cmd.Parameters.AddWithValue("@time", DateTime.Now.TimeOfDay);
                cmd.Parameters.AddWithValue("@date", DateTime.Now.Date);
                cmd.Parameters.AddWithValue("@et", DateTime.Now);
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return "Error #2";
            }

            return "Добавлено !";
        }

        public static string AddCar(string number, int client, int type)
        {
            Check();

            try
            {
                MySqlCommand cmd = new MySqlCommand();
                //OleDbCommand cmd = new OleDbCommand();

                cmd.Connection = mycon;
                cmd.CommandText = "INSERT INTO Cars(number, client, type) VALUES(@number, @client, @type)";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@number", number);
                cmd.Parameters.AddWithValue("@client", client);
                cmd.Parameters.AddWithValue("@type", type);
                cmd.ExecuteNonQuery();

                return "Добавлено !";
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return "Error #7";
            }
        }

        public static string CopyContract(string number)
        {
            Check();

            try
            {
                MySqlCommand cmd = new MySqlCommand();
                //OleDbCommand cmd = new OleDbCommand();
                MySqlTransaction trans;
                trans = mycon.BeginTransaction();
                cmd.Connection = mycon;
                cmd.Transaction = trans;
                cmd.CommandText = @"SELECT @tid:=Contracts.`timetable`, @ad:=`ad` FROM Contracts WHERE Contracts.`id`=" + number + @";
                INSERT INTO TimeTable(`period`) SELECT TimeTable.period FROM TimeTable WHERE TimeTable.id=@tid;
                SELECT @tid:=@@IDENTITY;
                INSERT INTO Additional(`name`) SELECT Additional.name FROM Additional WHERE Additional.id=@ad;
                SELECT @ad:=@@IDENTITY;
                INSERT INTO Contracts(`object`, `name`, `route`, `timetable`, `ad`, `time`, `date`, `editTime`, `client`, `company`) SELECT Contracts.object,  CONCAT(Contracts.`name`, ' (КОПИЯ)'), Contracts.route, @tid, @ad, Contracts.`time`, Contracts.`date`, Contracts.editTime, Contracts.`client`, Contracts.company FROM Contracts WHERE Contracts.id=" + number + @";";
                cmd.ExecuteNonQuery();
                trans.Commit();

                return "Скопировано !";
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return "Error #7";
            }
        }

        public static string AddTimeTable(DataGridView[] dgv)
        {
            Check();

            try
            {
                MySqlCommand cmd = new MySqlCommand();
                //OleDbCommand cmd = new OleDbCommand();

                cmd.Connection = mycon;
                cmd.CommandText = "INSERT INTO TimeTable (period) VALUES( @per)";
                cmd.Prepare();

                TimePeriods tp = new TimePeriods();                       
                Dictionary<string, Dictionary<string, TimePeriodsValue>> days = new Dictionary<string, Dictionary<string, TimePeriodsValue>>();

                for (int i = 0; i < 7; i++)
                {
                    Dictionary<string, TimePeriodsValue> dict = new Dictionary<string, TimePeriodsValue>();

                    for (int j = 0; j < dgv[i].RowCount; j++)
                    {
                        TimePeriodsValue tpva = new TimePeriodsValue();   

                        tpva.Time1 = (string)dgv[i].Rows[j].Cells[0].Value;
                        tpva.Time2 = (string)dgv[i].Rows[j].Cells[1].Value;
                        tpva.Price1 = (string)dgv[i].Rows[j].Cells[2].Value;
                        tpva.Price2 = (string)dgv[i].Rows[j].Cells[3].Value;
                        tpva.Lenght = (string)dgv[i].Rows[j].Cells[4].Value;

                        dict.Add("P" + j.ToString(), tpva);
                    }

                    days.Add("D" + i.ToString(), dict);            
                }

                tp.Days = days;



                string json = JsonConvert.SerializeObject(tp, Formatting.Indented);

                Console.WriteLine(json);

                cmd.Parameters.AddWithValue("@per", json);
                cmd.ExecuteNonQuery();

                string query2 = "Select @@Identity";
                cmd.CommandText = query2;
                string res = cmd.ExecuteScalar().ToString();

                return res;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return "Error #14";
            }
        }

        public static string AddCarType(string type)
        {
            Check();

            try
            {
                MySqlCommand cmd = new MySqlCommand();
                //OleDbCommand cmd = new OleDbCommand();

                cmd.Connection = mycon;
                cmd.CommandText = "INSERT INTO CarTypes(name) VALUES(@name)";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@name", type);
                cmd.ExecuteNonQuery();

                return "Добавлено !";
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return "Error #5";
            }
        }

        public static string AddContract(string face, string tid, string route, string ad, string client = null, string company = null)
        {
            Check();

            try
            {
                MySqlCommand cmd = new MySqlCommand();
                //OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = mycon;
                int cac = 0;
                if (ad == string.Empty) ad = "";

                if (client == null) { cac = 1; company = company.Trim(); } else { client = client.Trim(); }
                             
                string[] cc = {"Clients.`name`='" + client + "'", "Companies.`name`='" + company +"'"};
                string[] par = { "`client`", "`company`" };
                string[] par2 = { "Clients.`id`", "Companies.`id`" };

                cmd.CommandText = "INSERT INTO Contracts(`object`, `name`, `route`, `timetable`, `ad`, `time`, `date`, `editTime`, " + par[cac] + ") SELECT @obj, @name, Routes.`id`, @tid, @ad, @time, @date, @et, " + par2[cac] + " FROM `Routes`, `Clients`, `Companies` WHERE Routes.`name`= @route AND " + cc[cac] + " LIMIT 1";
                cmd.Prepare();

                if(face == "Клиент") face = "Клиент"; else face = "Компания";

                cmd.Parameters.AddWithValue("@obj", face);
                cmd.Parameters.AddWithValue("@name", tid + " (" + face + ")");
                cmd.Parameters.AddWithValue("@tid", tid);
                cmd.Parameters.AddWithValue("@ad", ad);
                cmd.Parameters.AddWithValue("@route", route);
                cmd.Parameters.AddWithValue("@time", DateTime.Now.TimeOfDay);
                cmd.Parameters.AddWithValue("@date", DateTime.Now.Date);
                cmd.Parameters.AddWithValue("@et", DateTime.Now);
                cmd.ExecuteNonQuery();

                return "Добавлено !";
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return "Error #16";
            }
        }

        public static string AddOrdering(string name, string dt, string credit, string deb, string client)
        {
            Check();

            try
            {
                MySqlCommand cmd = new MySqlCommand();
                //OleDbCommand cmd = new OleDbCommand();

                cmd.Connection = mycon;
                cmd.CommandText = "INSERT INTO Ordering(`name`, `date`, `client`, `credit`, `debit`) SELECT @name, @dt, Clients.`id`, @cr, @de FROM `Clients` WHERE Clients.`name`= @cl";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@name", name);
                cmd.Parameters.AddWithValue("@dt", dt);
                cmd.Parameters.AddWithValue("@cr", credit);
                cmd.Parameters.AddWithValue("@de", deb);
                cmd.Parameters.AddWithValue("@cl", client);
                cmd.ExecuteNonQuery();

                return "Добавлено !";
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return "Error #13";
            }
        }

        public static string AddRoute(string name)
        {
            Check();

            try
            {
                MySqlCommand cmd = new MySqlCommand();
                //OleDbCommand cmd = new OleDbCommand();

                cmd.Connection = mycon;
                cmd.CommandText = "INSERT INTO Routes(name) VALUES(@name)";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@name", name);
                cmd.ExecuteNonQuery();

                return "Добавлено !";
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return "Error #13";
            }
        }

        public static string AddAd(string name)
        {
            Check();

            try
            {
                MySqlCommand cmd = new MySqlCommand();
                //OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = mycon;
                cmd.CommandText = "INSERT INTO AdditionalOptions(name) VALUES(@name)";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@name", name);
                cmd.ExecuteNonQuery();

                return "Добавлено !";
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return "Error #19";
            }
        }

        public static string AddInvoice(string[] id, string inv, string price1, string price2, string client, int count)
        {
            Check();

            try
            {
                MySqlCommand cmd = new MySqlCommand();
                //OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = mycon;
                cmd.CommandText = "INSERT INTO Invoices(invoice, price1, price2, `dateTime`, `client`, `Count`) VALUES(@name, @pr1, @pr2, @dt, @cl, @c)";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@name", inv);
                cmd.Parameters.AddWithValue("@pr1", price1);
                cmd.Parameters.AddWithValue("@pr2", price2);
                cmd.Parameters.AddWithValue("@dt", DateTime.Now.ToString("yyyy-MM-dd HH:mm"));
                cmd.Parameters.AddWithValue("@cl", client);
                cmd.Parameters.AddWithValue("@c", count);
                cmd.ExecuteNonQuery();

                return "Добавлено !";
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return "Error #19";
            }
        }

        public static string AddInvoice(string[] id, string inv, string price1, string price2, string client, string prices, int count)
        {
            Check();

            try
            {
                MySqlCommand cmd = new MySqlCommand();
                //OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = mycon;
                cmd.CommandText = "INSERT INTO Invoices(invoice, price1, price2, `dateTime`, `client`, `prices`, `Count`) VALUES(@name, @pr1, @pr2, @dt, @cl, @prs, @c)";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@name", inv);
                cmd.Parameters.AddWithValue("@pr1", price1);
                cmd.Parameters.AddWithValue("@pr2", price2);
                cmd.Parameters.AddWithValue("@dt", DateTime.Now.ToString("yyyy-MM-dd HH:mm"));
                cmd.Parameters.AddWithValue("@cl", client);
                cmd.Parameters.AddWithValue("@prs", prices);
                cmd.Parameters.AddWithValue("@c", count);
                cmd.ExecuteNonQuery();

                return "Добавлено !";
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return "Error #19";
            }
        }

        public static List<Dictionary<string, string>> AddOrder(string company, string client, string loaded, string car1, 
            string car2, string len, string we, string per, string r1, string r2, string ad, string price, 
            string date, string ClientContract, string ClientPrice)
        {
            Logger.info("CALLED AddOrder");

            string sql = @"INSERT INTO Orders(company, client, loaded, car_1, car_2, length, weight, period, route_1, route_2, ad, price, dateTime, `ContractID`, `ClientPrice`) SELECT Companies.id, Clients.id,@load,@car1,@car2,@len,@we,@per,@r1,@r2,@ad,@price,@dt,@clientContract,@clientPrice FROM Clients, Companies WHERE Clients.name=@cl AND Companies.name=@co LIMIT 1";

            return MakeSQLWithStr(sql, new Dictionary<string, string>() 
            { 
                { "@co", company },
                { "@cl", client},
                { "@load", loaded},
                { "@car1", car1},
                { "@car2", car2},
                { "@len", len},
                { "@we", we},
                { "@per", per},
                { "@r1", r1},
                { "@r2", r2},
                { "@ad", ad},
                { "@price", price},
                { "@dt",  Convert.ToDateTime(date).ToString("yyyy-MM-dd HH:mm:ss")},
                { "@clientContract", ClientContract},
                { "@clientPrice", ClientPrice}
            });
        }

        public static string AddAdditional(DataGridView dgv)
        {
            Check();

            try
            {
                MySqlCommand cmd = new MySqlCommand();
                //OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = mycon;
                cmd.CommandText = "INSERT INTO Additional(name) VALUES(@name)";
                cmd.Prepare();

                Additional ad = new Additional();
                Dictionary<string, AdditionalValue> adop = new Dictionary<string, AdditionalValue>();
                
                for (int i = 0; i < dgv.RowCount; i++)
                {
                    AdditionalValue adv = new AdditionalValue();

                    adv.name = (string)dgv.Rows[i].Cells[0].Value;
                    adv.price1 = (string)dgv.Rows[i].Cells[1].Value;
                    adv.price2 = (string)dgv.Rows[i].Cells[2].Value;
                    adv.weight = (string)dgv.Rows[i].Cells[3].Value;

                    adop.Add("V" + i.ToString(), adv);
                }

                ad.AD = adop;

                string json = JsonConvert.SerializeObject(ad, Formatting.Indented);

                cmd.Parameters.AddWithValue("@name",json);
                cmd.ExecuteNonQuery();

                string query2 = "Select @@Identity";
                cmd.CommandText = query2;
                string res = cmd.ExecuteScalar().ToString();

                return res;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return "Error #21";
            }
        }

        public static List<Dictionary<string, string>> LoadClients()
        {
            string sql = "SELECT `id`, `object`, `name`, `inn`, `kpp`, `address_1`, `address_2`, `p_c`, `bank`, `bik`, `pc`, `additional`, `MAIN`, `foreign` FROM `Clients`";

            return MakeSQLWithStr(sql);
        }


        public static string LoadCompanies()
        {
            Check();

            try
            {
                //OleDbDataReader rdr = null;
                MySqlDataReader rdr = null;

                string stm = "SELECT * FROM Companies";
                MySqlCommand cmd = new MySqlCommand(stm, mycon);
                //OleDbCommand cmd = new OleDbCommand(stm, mycon);
                rdr = cmd.ExecuteReader();

                string res = null;

                while (rdr.Read())
                {
                    res += rdr.GetString(1) + ":";
                }

                rdr.Close();

                return res;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return "Error #8";
            }
        }
        public static List<Dictionary<string, string>> LoadCompaniesFromContracts()
        {
            string sql = "SELECT `Contracts`.`id`, Contracts.`name`, Routes.`name`, TimeTable.`id`, TimeTable.`period`, Additional.`id`, Additional.`name`, Companies.name, Companies.`id` FROM Companies, Contracts, Routes, TimeTable, Additional WHERE Companies.id=Contracts.company AND Contracts.company IS NOT NULL AND Routes.id=Contracts.route AND TimeTable.id=Contracts.timetable AND Additional.id=Contracts.ad";

            return MakeSQLWithStr(sql);
        }
        public static List<Dictionary<string,string>> getTimeTableAndAdFromClient(string id)
        {
            string sql = "SELECT TimeTable.`period`, Additional.`name` FROM `TimeTable`, `Additional`, `Contracts` WHERE `Contracts`.`id`=@id AND TimeTable.`id`=`Contracts`.`timetable` AND `Additional`.`id`=`Contracts`.`ad`";
            
            return MakeSQLWithStr(sql, new Dictionary<string,string>(){ {"@id", id} });
        }
        public static List<Dictionary<string, string>> LoadClientsFromContracts()
        {
            string sql = "SELECT `Contracts`.`name`, `Routes`.`name`, `Cars`.`number`, `Clients`.`name`, `Contracts`.`id`, `Clients`.`id` FROM `Clients`, `Contracts`, `Routes`, `Cars` WHERE `Clients`.`id` = `Contracts`.`client` AND `Contracts`.`client`=`Contracts`.`client` IS NOT NULL AND `Routes`.`id` = `Contracts`.`route` AND `Cars`.`client` = `Contracts`.`client`";

            return MakeSQLWithStr(sql);
        }
        public static string LoadCars()
        {
            Check();

            try
            {
                //OleDbDataReader rdr = null;
                MySqlDataReader rdr = null;

                string stm = "SELECT * FROM Cars";
                MySqlCommand cmd = new MySqlCommand(stm, mycon);
                //OleDbCommand cmd = new OleDbCommand(stm, mycon);
                rdr = cmd.ExecuteReader();

                string res = null;

                while (rdr.Read())
                {
                    res += rdr.GetString(1) + ":";
                }

                rdr.Close();

                return res;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return "Error #12";
            }
        }

        public static string LoadRoutes()
        {
            Check();

            try
            {
                //OleDbDataReader rdr = null;
                MySqlDataReader rdr = null;

                string stm = "SELECT * FROM Routes";
                MySqlCommand cmd = new MySqlCommand(stm, mycon);
                //OleDbCommand cmd = new OleDbCommand(stm, mycon);
                rdr = cmd.ExecuteReader();

                string res = null;

                while (rdr.Read())
                {
                    res += rdr.GetString(1) + ":";
                }

                rdr.Close();

                return res;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return "Error #15";
            }
        }

        public static string LoadAd()
        {
            Check();

            try
            {
                //OleDbDataReader rdr = null;
                MySqlDataReader rdr = null;

                string stm = "SELECT * FROM AdditionalOptions";
                MySqlCommand cmd = new MySqlCommand(stm, mycon);
                //OleDbCommand cmd = new OleDbCommand(stm, mycon);
                rdr = cmd.ExecuteReader();

                string res = null;

                while (rdr.Read())
                {
                    res += rdr.GetValue(1).ToString() + ":";
                }

                rdr.Close();

                return res;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return "Error #17";
            }
        }

        public static string LoadMAIN()
        {
            Check();

            try
            {
                //OleDbDataReader rdr = null;
                MySqlDataReader rdr = null;

                string stm = "SELECT * FROM Clients WHERE MAIN=1";
                MySqlCommand cmd = new MySqlCommand(stm, mycon);
                //OleDbCommand cmd = new OleDbCommand(stm, mycon);
                rdr = cmd.ExecuteReader();

                string res = null;
                rdr.Read();

                res = rdr.GetString(2) + ":" + rdr.GetString(3) + ":" + rdr.GetString(4) + ":" + rdr.GetString(5) + ":" + rdr.GetString(3) + ":" + rdr.GetString(7) + ":" + rdr.GetString(8) + ":" + rdr.GetString(9) + ":" + rdr.GetString(10);


                rdr.Close();

                return res;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return "Error #17";
            }
        }

        public static string[] LoadAdOrder(string id)
        {
            Check();

            try
            {
                //OleDbDataReader rdr = null;
                MySqlDataReader rdr = null;

                string stm = "SELECT ad, weight, loaded FROM Orders WHERE id=@id";
                MySqlCommand cmd = new MySqlCommand(stm, mycon);
                //OleDbCommand cmd = new OleDbCommand(stm, mycon);
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id", id);

                rdr = cmd.ExecuteReader();

                string[] res = new string[3];

                rdr.Read();
                res[0] = ToString(rdr.GetValue(0));
                res[1] = ToString(rdr.GetValue(1));
                res[2] = ToString(rdr.GetValue(2));
                rdr.Close();

                return res;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }


        public static string[] LoadOrdering(Dictionary<string, string> invoice)
        {
            Check();

            try
            {
                //OleDbDataReader rdr = null;
                MySqlDataReader rdr = null;
                string stm = null;
                string cmds = null;
                string cl = null;

                foreach (var pair in invoice)
                {
                    switch (pair.Key)
                    {
                        case "SELECT_CLIENT": cmds += "Ordering.client=Clients.`id` AND "; cl = pair.Value;
                            break;

                        case "SELECT_DTP_1": cmds += "Ordering.`date` >= '" + pair.Value + "' AND ";
                            break;

                        case "SELECT_DTP_2": cmds += "Ordering.`date` <= '" + pair.Value + "' AND ";
                            break;

                        default: cmds += "";
                            break;
                    }
                }


                if (cmds == null)
                {
                    stm = @"
                    SELECT Ordering.id FROM Ordering
                    LEFT JOIN Clients ON Clients.id=Orders.`client`";
                }
                else
                {
                    stm = @"
                    SELECT Ordering.id, Ordering.name, Ordering.credit, Ordering.debit, Ordering.`date`, Ordering.`saldo` FROM Ordering
                    LEFT JOIN Clients ON Clients.name='" + cl + @"'
                    WHERE " + (cmds != string.Empty ? cmds.Substring(0, cmds.Length - 5) : cmds) + " ORDER BY `date`";
                }

                MySqlCommand cmd = new MySqlCommand(stm, mycon);
                //OleDbCommand cmd = new OleDbCommand(stm, mycon);
                rdr = cmd.ExecuteReader();

                string[] res = new string[6];

                while (rdr.Read())
                {
                    res[0] += rdr.GetString(0) + ":";
                    res[1] += rdr.GetValue(1) + ":";
                    res[2] += rdr.GetValue(2) + ":";
                    res[3] += rdr.GetValue(3) + ":";
                    res[4] += rdr.GetString(4) + "!";
                    res[5] += rdr.GetString(5) + ":";
                }

                rdr.Close();

                return res;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }

        public static string[] LoadInvoices(Dictionary<string, string> invoice)
        {
            Check();

            try
            {
                //OleDbDataReader rdr = null;
                MySqlDataReader rdr = null;
                string stm = null;
                string cmds = null;

                foreach (var pair in invoice)
                {
                    switch (pair.Key)
                    {
                        case "SELECT_INVOICE": cmds += "Invoices.invoice='" + pair.Value + "' AND ";
                            break;

                        case "SELECT_DTP_1": cmds += "Invoices.`dateTime` >= '" + pair.Value + "' AND ";
                            break;

                        case "SELECT_DTP_2": cmds += "Invoices.`dateTime` <= '" + pair.Value + "' AND ";
                            break;

                        case "SELECT_NAME": cmds += "Invoices.`client` = '" + pair.Value + "' AND ";
                            break;

                        default: cmds += "";
                            break;
                    }
                }


                if (cmds == null)
                {
                    stm = @"
                    SELECT Invoices.id, Invoices.client, Invoices.invoice, Invoices.price1, Invoices.price2, Invoices.`dateTime` FROM Invoices";
                }
                else
                {
                    stm = @"
                    SELECT Invoices.id, Invoices.client, Invoices.invoice, Invoices.price1, Invoices.price2, Invoices.`dateTime` FROM Invoices
                    WHERE " + (cmds != string.Empty ? cmds.Substring(0, cmds.Length - 5) : cmds);
                }

                MySqlCommand cmd = new MySqlCommand(stm, mycon);
                //OleDbCommand cmd = new OleDbCommand(stm, mycon);
                rdr = cmd.ExecuteReader();

                string[] res = new string[11];

                while (rdr.Read())
                {
                    res[0] += rdr.GetString(0) + ":";
                    res[1] += rdr.GetValue(1) + ":";
                    res[2] += rdr.GetString(2) + ":";
                    res[3] += rdr.GetString(3) + ":";
                    res[4] += rdr.GetString(4) + ":";
                    res[5] += rdr.GetString(5) + "!";
                }

                rdr.Close();

                return res;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }

        public static List<Dictionary<string, string>> LoadOrders(Dictionary<string, string> invoice)
        {
            string stm = null;
            string cmds = null;

            foreach(var pair in invoice)
            {
                switch (pair.Key)
                {
                    case "SELECT_INVOICE": cmds += "Orders.invoice IS NULL AND ";
                    break;

                    case "SELECT_INVOICE_GOT": cmds += "Orders.invoice IS NOT NULL AND ";
                    break;

                    case "SELECT_INVOICE_CLIENT": cmds += "Orders.invoice_client IS NULL AND ";
                    break;

                    case "SELECT_INVOICE_CLIENT_SET": cmds += "Orders.invoice_client IS NOT NULL AND ";
                    break;

                    case "SELECT_ROUTE": cmds += "Orders.route_1='" + pair.Value + @"' AND Orders.route_2='" + pair.Value + @"' AND ";
                    break;

                    case "SELECT_CAR": cmds += "Orders.car_1='" + pair.Value + @"' OR Orders.car_2='" + pair.Value + @"' AND ";
                    break;

                    case "SELECT_DTP_1": cmds += "Orders.`dateTime` >= '" + pair.Value + "' AND ";
                    break;

                    case "SELECT_DTP_2": cmds += "Orders.`dateTime` <= '" + pair.Value + "' AND ";
                    break;

                    default: cmds += "";
                    break;
                }
            }

            string sql = null;

            if (cmds == null)
            {
                sql = @"
                SELECT Orders.id, Clients.name, Companies.name, Orders.loaded, Orders.car_1, Orders.car_2, Orders.route_1, Orders.price, Orders.`dateTime`, Orders.invoice, Orders.`invoice_client` FROM Orders 
                LEFT JOIN Clients ON Clients.id=Orders.`client`
                LEFT JOIN Companies ON Companies.id=Orders.company";
            }
            else
            {
                sql = @"
                SELECT Orders.id, Clients.name, Companies.name, Orders.loaded, Orders.car_1, Orders.car_2, Orders.route_1, Orders.price, Orders.`dateTime`, Orders.invoice, Orders.`invoice_client` FROM Orders 
                LEFT JOIN Clients ON Clients.id=Orders.`client`
                LEFT JOIN Companies ON Companies.id=Orders.company
                WHERE " + (cmds != string.Empty ? cmds.Substring(0, cmds.Length - 5) : cmds);
            }

            return MakeSQLWithStr(sql);    
        }

        public static string LoadContracts()
        {
            Check();

            try
            {
                //OleDbDataReader rdr = null;
                MySqlDataReader rdr = null;

                string stm = "SELECT * FROM Contracts";
                MySqlCommand cmd = new MySqlCommand(stm, mycon);
                //OleDbCommand cmd = new OleDbCommand(stm, mycon);
                rdr = cmd.ExecuteReader();

                string res = null;

                while (rdr.Read())
                {
                    if (rdr.GetString(2) == null || rdr.GetString(2) == string.Empty) res += rdr.GetString(0) + " (" + rdr.GetString(1) + "):"; else res += rdr.GetString(2) + ":";
                }

                rdr.Close();

                return res;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return "Error #16";
            }
        }

        public static string LoadCarTypes()
        {
            Check();

            try
            {
                //OleDbDataReader rdr = null;
                MySqlDataReader rdr = null;

                string stm = "SELECT * FROM CarTypes";
                MySqlCommand cmd = new MySqlCommand(stm, mycon);
                //OleDbCommand cmd = new OleDbCommand(stm, mycon);
                rdr = cmd.ExecuteReader();

                string res = null;

                while (rdr.Read())
                {
                    res += rdr.GetValue(1).ToString() + ":" + rdr.GetValue(0).ToString() + ":";
                }

                rdr.Close();

                return res;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return "Error #4";
            }
        }

        public static int getCarTypeId(string name)
        {
            Check();

            try
            {
                string stm = "SELECT id FROM CarTypes WHERE name=@name";
                MySqlCommand cmd = new MySqlCommand(stm, mycon);
                //OleDbCommand cmd = new OleDbCommand(stm, mycon);
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@name", name);

                int res = (int)cmd.ExecuteScalar();

                return res;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return -1;
            }
        }

        public static string[] getClient(string name)
        {
            Check();

            try
            {
                
                //OleDbDataReader rdr = null;
                MySqlDataReader rdr = null;

                string stm = "SELECT * FROM Clients WHERE name=@name";
                MySqlCommand cmd = new MySqlCommand(stm, mycon);
                //OleDbCommand cmd = new OleDbCommand(stm, mycon);
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@name", name);
                rdr = cmd.ExecuteReader();
                rdr.Read();
                string[] res = new string[14];
                res[0] = ToString(rdr.GetValue(1));
                res[1] = ToString(rdr.GetValue(3));
                res[2] = ToString(rdr.GetValue(4));
                res[3] = ToString(rdr.GetValue(5));
                res[4] = ToString(rdr.GetValue(6));
                res[5] = ToString(rdr.GetValue(7));
                res[6] = ToString(rdr.GetValue(8));
                res[7] = ToString(rdr.GetValue(9));
                res[8] = ToString(rdr.GetValue(10));
                res[9] = ToString(rdr.GetValue(11));
                res[10] = ToString(rdr.GetValue(12));
                res[11] = ToString(rdr.GetValue(13));
                res[12] = ToString(rdr.GetValue(14));
                res[13] = rdr.GetValue(16).ToString();
                
                rdr.Close();

                return res;
            }
            catch (MySqlException e)
            {
                string[] test = { "", "" };
                Console.WriteLine(e.ToString());
                return test;
            }
        }

        public static int getClientIDByName(string name)
        {
            Check();

            try
            {
                MySqlDataReader rdr = null;
                string stm = "SELECT id FROM Clients WHERE name=@name";
                MySqlCommand cmd = new MySqlCommand(stm, mycon);
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@name", name);
                rdr = cmd.ExecuteReader();
                rdr.Read();
                int res = (int)rdr.GetValue(0);

                rdr.Close();

                return res;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return 0;
            }
        }
        private static List<Dictionary<string, string>> MakeSQLWithStr(string sql, Dictionary<string, string> Params = null)
        {
            Logger.info("CHECKING CONNECTION WITH SERVER");
            Check();
            Logger.info("CONNECTION IS ESTABLISHED");
            Logger.info("TRYING TO MAKE REQUEST");

            try
            {
                MySqlDataReader rdr = null;
                List<Dictionary<string, string>> result = new List<Dictionary<string, string>>();

                MySqlCommand cmd = new MySqlCommand(sql, mycon);
                cmd.Prepare();     
                
                if(Params != null)
                    foreach(KeyValuePair<string, string> param in Params)
                    {
                        cmd.Parameters.AddWithValue(param.Key, param.Value);
                    }

                rdr = cmd.ExecuteReader();

                while(rdr.Read())
                {                                 
                    Dictionary<string, string> r = new Dictionary<string,string>();

                    for (int i = 0; i < rdr.FieldCount; i++)
                    {
                        if (r.ContainsKey(rdr.GetName(i)))
                            r.Add(rdr.GetName(i) + "_" + i.ToString(), rdr.GetValue(i).ToString());
                        else
                            r.Add(rdr.GetName(i), rdr.GetValue(i).ToString());
                    }

                    result.Add(r);
                }

                Logger.info("SUCCESSFUL REQUEST, SENDING ANSWER");

                if (result.Count == 0) return null;

                return result;

            }catch(Exception e)
            {
                Logger.log("FAILED MAKE REQUEST, BECAUSE EXCEPTION HAS OCCURED: ");
                Logger.error(e.ToString());
                return null;
            }
        }
        private static bool MakeSQLWithBool(string sql, Dictionary<string, string> Params = null)
        {
            Logger.info("CHECKING CONNECTION WITH SERVER");
            Check();
            Logger.info("CONNECTION IS ESTABLISHED");
            Logger.info("TRYING TO MAKE REQUEST");

            try
            {
                MySqlDataReader rdr = null;
                List<Dictionary<string, string>> result = new List<Dictionary<string, string>>();

                MySqlCommand cmd = new MySqlCommand(sql, mycon);
                cmd.Prepare();

                if (Params != null)
                    foreach (KeyValuePair<string, string> param in Params)
                    {
                        cmd.Parameters.AddWithValue(param.Key, param.Value);
                    }

                rdr = cmd.ExecuteReader();

                Logger.info("SUCCESSFUL REQUEST, SENDING ANSWER");

                if (rdr.RecordsAffected > 0) return true;

                return false;

            }
            catch (Exception e)
            {
                Logger.log("FAILED MAKE REQUEST, BECAUSE EXCEPTION HAS OCCURED: ");
                Logger.error(e.ToString());
                return false;
            }
        }
        public static List<Dictionary<string, string>> getInvoiceCount(string id)
        {
            Logger.info("CALLED getInvoiceCount");

            string sql = @"SELECT `Count` FROM `Invoices` WHERE `id`=@id";

            return MakeSQLWithStr(sql, new Dictionary<string, string>() { { "@id", id } });
        }
        public static List<Dictionary<string, string>> getOrderByID(string id)
        {
            Logger.info("CALLED getOrderByID");

            string sql = @"SELECT `invoice`, `invoice_client`, `company`, `client`, `loaded`, `car_1`, `car_2`, `length`, `weight`, `period`, `route_1`, `route_2`, `ad`, `price`, `dateTime`, `ContractID` FROM `Orders` WHERE `id`=@id";

            return MakeSQLWithStr(sql, new Dictionary<string,string>(){ { "@id", id } });
        }

        public static string getInvoicePrices(string id)
        {
            Check();

            try
            {
                MySqlDataReader rdr = null;

                string stm = "SELECT Invoices.prices FROM Invoices WHERE id=@id";
                MySqlCommand cmd = new MySqlCommand(stm, mycon);
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@id", id);
                rdr = cmd.ExecuteReader();
                rdr.Read();

                string res = rdr.GetString(0);

                rdr.Close();

                return res;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return "NULL";
            }
        }

        public static string[] getCompany(string name)
        {
            Check();

            try
            {

                //OleDbDataReader rdr = null;
                MySqlDataReader rdr = null;

                string stm = "SELECT * FROM Companies WHERE name=@name";
                MySqlCommand cmd = new MySqlCommand(stm, mycon);
                //OleDbCommand cmd = new OleDbCommand(stm, mycon);
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@name", name);
                rdr = cmd.ExecuteReader();
                rdr.Read();
                string[] res = new string[8];
                res[0] = ToString(rdr.GetValue(1));
                res[1] = ToString(rdr.GetValue(2));
                res[2] = ToString(rdr.GetValue(3));
                res[3] = ToString(rdr.GetValue(4));
                res[4] = ToString(rdr.GetValue(5));
                res[5] = ToString(rdr.GetValue(6));
                res[6] = ToString(rdr.GetValue(7));
                res[7] = ToString(rdr.GetValue(8));

                rdr.Close();

                return res;
            }
            catch (MySqlException e)
            {
                string[] test = { "", "" };
                Console.WriteLine(e.ToString());
                return test;
            }
        }

        public static string[] getContract(string name)
        {
            Check();

            try
            {

                //OleDbDataReader rdr = null;
                MySqlDataReader rdr = null;

                string stm = @"SELECT Contracts.*, Routes.name, TimeTable.`id`, TimeTable.`period`, Additional.`id`, Additional.`name`  FROM Contracts
                    LEFT JOIN Routes ON Routes.id=Contracts.route
                    LEFT JOIN TimeTable ON TimeTable.id=Contracts.timetable
                    LEFT JOIN Additional ON Additional.id=Contracts.ad
                    WHERE Contracts.name=@name";
                MySqlCommand cmd = new MySqlCommand(stm, mycon);
                //OleDbCommand cmd = new OleDbCommand(stm, mycon);
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@name", name.Trim());
                rdr = cmd.ExecuteReader();
                string[] res = new string[16];
                rdr.Read();
                
                res[0] = ToString(rdr.GetValue(0));
                res[1] = ToString(rdr.GetValue(1));
                res[2] = ToString(rdr.GetValue(2));
                res[3] = ToString(rdr.GetValue(3));
                res[4] = ToString(rdr.GetValue(4));
                res[5] = ToString(rdr.GetValue(5));
                res[6] = ToString(rdr.GetValue(6));
                res[7] = ToString(rdr.GetValue(7));
                res[8] = ToString(rdr.GetValue(8));
                res[9] = ToString(rdr.GetValue(9));
                res[10] = ToString(rdr.GetValue(10));

                res[11] = ToString(rdr.GetValue(11));

                res[12] = ToString(rdr.GetValue(12));
                res[13] = ToString(rdr.GetValue(13));

                res[14] = ToString(rdr.GetValue(14));
                res[15] = ToString(rdr.GetValue(15));
                rdr.Close();
                

                return res;
            }
            catch (MySqlException e)
            {
                string[] test = { "", "" };
                Console.WriteLine(e.ToString());
                return test;
            }
        }
        public static List<Dictionary<string, string>> getClientFromId(string id)
        {
            string sql = "SELECT `name` FROM `Clients` WHERE `id`=@id";

            return MakeSQLWithStr(sql, new Dictionary<string, string>() { { "@id", id } });
        }
        public static List<Dictionary<string, string>> getCompanyFromId(string id)
        {
            string sql = "SELECT `name` FROM `Companies` WHERE `id`=@id";

            return MakeSQLWithStr(sql, new Dictionary<string, string>() { { "@id", id } });
        }
        public static bool UpdateOrder(Dictionary<string, string> order)
        {
            string sql = "UPDATE `Orders` SET `company`=@company, `client`=@client, `loaded`=@load, `car_1`=@car1, `car_2`=@car2, `length`=@len, `weight`=@weight, `period`=@per, `route_1`=@r1, `route_2`=@r2, `ad`=@ad, `price`=@price, `dateTime`=@dt, `editTime`=@et, `ClientPrice`=@cpr WHERE `id`=@id";

            return MakeSQLWithBool(sql, new Dictionary<string, string>() { 
                { "@company", order["company"] },
                { "@client", order["client"] },
                { "@load", order["load"] },
                { "@car1", order["car1"] },
                { "@car2", order["car2"] },
                { "@len", order["len"] },
                { "@weight", order["weight"] },
                { "@per", order["period"] },
                { "@r1", order["route1"] },
                { "@r2", order["route2"] },
                { "@ad", order["ad"] },
                { "@price", order["price"] },
                { "@dt", order["date"] },
                { "@et", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") },
                { "@id", order["id"] },
                { "@cpr", order["clientprice"] }
            });
        }
        public static string[] getCar(string name)
        {
            Check();

            try
            {

                //OleDbDataReader rdr = null;
                MySqlDataReader rdr = null;

                string stm = "SELECT CarTypes.name, Clients.name FROM Cars, CarTypes, Clients WHERE Cars.number=@name AND CarTypes.id = Cars.`type` AND Clients.id = Cars.`client`";
                MySqlCommand cmd = new MySqlCommand(stm, mycon);
                //OleDbCommand cmd = new OleDbCommand(stm, mycon);
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@name", name);
                rdr = cmd.ExecuteReader();
                rdr.Read();
                string[] res = new string[2];
                res[0] = ToString(rdr.GetValue(0));
                res[1] = ToString(rdr.GetValue(1));

                rdr.Close();

                return res;
            }
            catch (MySqlException e)
            {
                string[] test = { "", "" };
                Console.WriteLine(e.ToString());
                return test;
            }
        }

        public static string UpdateClient(string oldname, string newname, string face, string inn, string kpp, string adr1, string adr2, string p_c, string bank, string bik, string pc, string ad, string foreign)
        {
            Check();

            try
            {
                MySqlCommand cmd = new MySqlCommand();
                //OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = mycon;
                cmd.CommandText = "UPDATE Clients SET object=@obj, name=@newname, inn=@inn, kpp=@kpp, address_1=@adr1, address_2=@adr2, p_c=@p_c, bank=@bank, bik=@bik, pc=@pc, additional=@ad, editTime=@et, `foreign`=@fr WHERE name=@oldname";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@oldname", oldname);
                cmd.Parameters.AddWithValue("@obj", face);
                cmd.Parameters.AddWithValue("@newname", newname);
                cmd.Parameters.AddWithValue("@inn", inn);
                cmd.Parameters.AddWithValue("@kpp", kpp);
                cmd.Parameters.AddWithValue("@adr1", adr1);
                cmd.Parameters.AddWithValue("@adr2", adr2);
                cmd.Parameters.AddWithValue("@p_c", p_c);
                cmd.Parameters.AddWithValue("@bank", bank);
                cmd.Parameters.AddWithValue("@bik", bik);
                cmd.Parameters.AddWithValue("@pc", pc);
                cmd.Parameters.AddWithValue("@ad", ad);
                cmd.Parameters.AddWithValue("@et", DateTime.Now);
                cmd.Parameters.AddWithValue("@fr", foreign);
                cmd.ExecuteNonQuery();

                return "Добавлено !";
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return "Error #11";
            }
        }

        public static string UpdateCompany(string oldname, string newname, string adr, string iban, string bank, string swift)
        {
            Check();

            try
            {
                MySqlCommand cmd = new MySqlCommand();
                //OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = mycon;
                cmd.CommandText = "UPDATE Companies SET name=@newname, adress=@adr, iban=@iban, bank=@bank, swift=@swift, editTime=@et WHERE name=@oldname";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@oldname", oldname);
                cmd.Parameters.AddWithValue("@newname", newname);
                cmd.Parameters.AddWithValue("@adr", adr);
                cmd.Parameters.AddWithValue("@iban", iban);
                cmd.Parameters.AddWithValue("@bank", bank);
                cmd.Parameters.AddWithValue("@swift", swift);
                cmd.Parameters.AddWithValue("@et", DateTime.Now);
                cmd.ExecuteNonQuery();

                return "Добавлено !";
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return "Error #12";
            }
        }

        public static string UpdateContract(string number, string newname, string route, string time, string face1, string face2, string ad)
        {
            Check();

            try
            {
                string sql = null;
                bool client = false;

                if (face1 == "Компания")
                    sql = "Contracts.`company`=Companies.`id`, Contracts.`client`=NULL";
                else               
                    sql = "Contracts.`client`=Clients.`id`, Contracts.`company`=NULL";

                if (face1 == "Клиент")
                {
                    face1 = "Clients.`name`='" + face2 + "'";
                    client = true;
                }
                else
                    face1 = "Companies.`name`='" + face2 + "'";

                MySqlCommand cmd = new MySqlCommand();
                //OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = mycon;
                cmd.CommandText = "UPDATE Contracts, Routes, TimeTable, Clients, Companies, Additional SET Contracts.`object`=@obj, Contracts.`name`='" + newname + "', Contracts.`route`=Routes.`id`, Contracts.`editTime`=@et, " + sql + ", TimeTable.period=@per, Additional.`name`=@adname WHERE Contracts.`id`=@number AND Routes.`name`=@route AND TimeTable.`id`=Contracts.timetable AND " + face1 + " AND Additional.`id`=Contracts.`ad`";
                cmd.Prepare();
           
                cmd.Parameters.AddWithValue("@et", DateTime.Now);
                cmd.Parameters.AddWithValue("@cl", face1);
                cmd.Parameters.AddWithValue("@co", face2);
                cmd.Parameters.AddWithValue("@number", number);
                cmd.Parameters.AddWithValue("@route", route);

                cmd.Parameters.AddWithValue("@per", time);
                cmd.Parameters.AddWithValue("@adname", ad);
                cmd.Parameters.AddWithValue("@obj", (client ? "Клиент" : "Компания"));

                cmd.ExecuteNonQuery();

                return "Сохранено";
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return "Error #12";
            }
        }

        public static string UpdateCar(string oldname, string newname, string client, string type)
        {
            Check();

            try
            {
                MySqlCommand cmd = new MySqlCommand();
                //OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = mycon;
                cmd.CommandText = "UPDATE Cars, CarTypes, Clients SET Cars.number=@newname, Cars.client=Clients.id, Cars.`type`=CarTypes.id WHERE Cars.number=@oldname AND Clients.name=@client AND CarTypes.name=@type";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@oldname", oldname);
                cmd.Parameters.AddWithValue("@newname", newname);
                cmd.Parameters.AddWithValue("@client", client);
                cmd.Parameters.AddWithValue("@type", type);
                cmd.ExecuteNonQuery();

                return "Сохранено";
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return "Error #13";
            }
        }

        public static string UpdateAD(string id, string name)
        {
            Check();

            try
            {
                MySqlCommand cmd = new MySqlCommand();
                //OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = mycon;
                cmd.CommandText = "UPDATE Additional SET name=@name WHERE id=@id";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@name", name);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();

                return "Сохранено";
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return "Error #13";
            }
        }

        public static string UpdateOrderInvoice(string id, string invoice, string weight, string price)
        {
            Check();

            try
            {
                MySqlCommand cmd = new MySqlCommand();
                //OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = mycon;
                cmd.CommandText = "UPDATE Orders SET invoice=@inv, weight=@w, `price`=@p WHERE id=@id AND invoice IS NULL";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@inv", invoice);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@w", weight);
                cmd.Parameters.AddWithValue("@p", price);
                cmd.ExecuteNonQuery();

                return "Сохранено";
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return "Error #13";
            }
        }


        public static string UpdateOrderClientInvoice(string[] id, string invoice)
        {
            Check();

            try
            {
                string quer=null;

                if (id.Length == 1)
                {
                    quer = "id='" + id[0] + "')";
                }
                else
                {
                    for (int i = 0; i < id.Length; i++)
                    {
                        quer += "id='" + id[i] + "' OR ";
                    }

                    quer = quer.Substring(0, quer.Length - 3) + ")";
                }

                
                MySqlCommand cmd = new MySqlCommand();
                //OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = mycon;
                cmd.CommandText = "UPDATE Orders SET invoice_client=@inv WHERE invoice_client IS NULL AND (" + quer;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@inv", invoice);
                cmd.ExecuteNonQuery();

                return "Сохранено";
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return "Error #13";
            }
        }

        private static string ToString(object value)
        {
            if (value == null)
            {
                return string.Empty;
            }
            else
            {
                return value.ToString().Trim();
            }
        }
    }
}
