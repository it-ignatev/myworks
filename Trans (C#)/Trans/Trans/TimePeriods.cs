﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Trans
{
    public class TimePeriods
    {
        public Dictionary<string, Dictionary<string, TimePeriodsValue>> Days { get; set; }
    }
    public class TimePeriodsValue
    {
        public string Time1 { get; set; }
        public string Time2 { get; set; }
        public string Price1 { get; set; }
        public string Price2 { get; set; }
        public string Lenght { get; set; }
    }

    public class Additional
    {
        public Dictionary<string, AdditionalValue> AD { get; set; }
    }

    public class Foreign
    {
        public Dictionary<string, string> foreign { get; set; }
    }

    public class AdditionalValue
    {
        public string name { get; set; }
        public string price1 { get; set; }
        public string price2 { get; set; }
        public string weight { get; set; }
    }
}
