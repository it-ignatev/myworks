﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Trans
{
    public partial class frm_newContract : Form
    {
        private string[] ads = null;

        public frm_newContract()
        {
            InitializeComponent();

            LoadCl();
            LoadCo();
            AddCars();
            LoadAdInf();
        }
        private void LoadCl()
        {
            List<Dictionary<string, string>> res = Sql.LoadClients();

            cb_clients.Items.Clear();

            foreach(Dictionary<string, string> r in res)
            {
                cb_clients.Items.Add(r["name"]);
            }        
        }

        private void LoadCo()
        {
            string res = Sql.LoadCompanies();
            if (res == null) return;
            string[] result = res.Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            
            cb_companies.Items.Clear();
            cb_companies.Items.AddRange(result);
        }

        private void AddCars()
        {
            string res = Sql.LoadRoutes();
            if (res == null) return;
            string[] result = res.Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            cb_route1.Items.Clear();
            cb_route1.Items.AddRange(result);


            cb_route2.Items.Clear();
            cb_route2.Items.AddRange(result);
        }

        private void LoadAdInf()
        {
            string res = Sql.LoadAd();
            if (res == null) return;
            string[] result = res.Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            ((DataGridViewComboBoxColumn)dgv_ad.Columns["nameOfAdDGV"]).Items.AddRange(result);
            ((DataGridViewComboBoxColumn)dgv_ad_companies.Columns["ad_c_name"]).Items.AddRange(result);
            ads = result;
        }
        private void dataGridView1_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                if (dgv_0.Columns[e.ColumnIndex].Name == "time1" || dgv_0.Columns[e.ColumnIndex].Name == "time2")
                    TimeSpan.Parse(e.FormattedValue.ToString().Trim());
            }
            catch
            {
                MessageBox.Show("Некорректный формат !");
                e.Cancel = true;
            }
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            if (cb_route1.Text != null && cb_route1.Text != string.Empty)
            {
                if (cb_route1.Items.IndexOf(cb_route1.Text) == -1)
                {
                    Sql.AddRoute(cb_route1.Text);
                }
                DataGridView[] dgv_time = new DataGridView[8];

                for(int i = 0;i<7;i++)
                {
                    dgv_time[i] = ((DataGridView)tab_periods.TabPages[i].Controls["dgv_" + i.ToString()]);
                }

                string res = Sql.AddTimeTable(dgv_time);

                for (int i = 0; i < dgv_ad.RowCount; i++)
                {
                    if((dgv_ad.Rows[i].Cells[0]).Value == null) continue;

                    if (Array.IndexOf(ads, (dgv_ad.Rows[i].Cells[0]).Value) == -1)
                    {
                        Sql.AddAd((string)(dgv_ad.Rows[i].Cells[0]).Value);
                    }
                }
            

                string ad = null;
                ad = Sql.AddAdditional(dgv_ad);

                res = Sql.AddContract("Клиент", res, cb_route1.Text, ad, cb_clients.Text);

                frm_Main.Message(res);
                this.Dispose();
            }
        }

        private void dgv_ad_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == nameOfAdDGV.DisplayIndex)
            {
                if (!this.nameOfAdDGV.Items.Contains(e.FormattedValue))
                {
                    this.nameOfAdDGV.Items.Add(e.FormattedValue);
                }
            }
        }

        private void dgv_ad_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (dgv_ad.CurrentCell.ColumnIndex == 0)
            {
                ComboBox combo = e.Control as ComboBox;

                if (combo == null)
                    return;

                combo.DropDownStyle = ComboBoxStyle.DropDown;
            }
        }

        private void btn_comp_save_Click(object sender, EventArgs e)
        {
            if (cb_route2.Text != null && cb_route2.Text != string.Empty)
            {
                if (cb_route2.Items.IndexOf(cb_route2.Text) == -1)
                {
                    if (cb_route2.Text != null && cb_route2.Text != string.Empty)
                        Sql.AddRoute(cb_route2.Text);
                }
                DataGridView[] dgv_time = new DataGridView[8];

                for (int i = 0; i < 7; i++)
                {
                    dgv_time[i] = ((DataGridView)tab_perdiods_companies.TabPages[i].Controls["dgv_comp_" + i.ToString()]);
                }

                string res = Sql.AddTimeTable(dgv_time);

                for (int i = 0; i < dgv_ad_companies.RowCount; i++)
                {
                    if ((dgv_ad_companies.Rows[i].Cells[0]).Value == null) continue;

                    if (Array.IndexOf(ads, (dgv_ad_companies.Rows[i].Cells[0]).Value) == -1)
                    {
                        Sql.AddAd((string)(dgv_ad_companies.Rows[i].Cells[0]).Value);
                    }
                }


                string ad = null;
                ad = Sql.AddAdditional(dgv_ad_companies);

                res = Sql.AddContract("Компания", res, cb_route2.Text, ad, null, cb_companies.Text);

                frm_Main.Message(res);
                this.Dispose();
            }
        }

        private DataGridView Copied;
        private void button1_Click(object sender, EventArgs e)
        {
            Copied  = ((DataGridView)tab_periods.SelectedTab.Controls["dgv_" + tab_periods.SelectedIndex.ToString()]);
        }

        private void CopyDataGridView(DataGridView dgv_org)
        {
            dgv_org.Rows.Clear();
            dgv_org.Columns.Clear();

            try
            {
                if (dgv_org.Columns.Count == 0)
                {
                    foreach (DataGridViewColumn dgvc in Copied.Columns)
                    {
                        dgv_org.Columns.Add(dgvc.Clone() as DataGridViewColumn);
                    }
                }

                DataGridViewRow row = new DataGridViewRow();

                for (int i = 0; i < Copied.Rows.Count - 1; i++)
                {
                    row = (DataGridViewRow)Copied.Rows[i].Clone();
                    int intColIndex = 0;
                    foreach (DataGridViewCell cell in Copied.Rows[i].Cells)
                    {
                        row.Cells[intColIndex].Value = cell.Value;
                        intColIndex++;
                    }
                    dgv_org.Rows.Add(row);
                }

                dgv_org.Refresh();

            }
            catch (Exception ex)
            {
                
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CopyDataGridView(((DataGridView)tab_periods.SelectedTab.Controls["dgv_" + tab_periods.SelectedIndex.ToString()]));
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Copied = ((DataGridView)tab_perdiods_companies.SelectedTab.Controls["dgv_comp_" + tab_perdiods_companies.SelectedIndex.ToString()]);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            CopyDataGridView(((DataGridView)tab_perdiods_companies.SelectedTab.Controls["dgv_comp_" + tab_perdiods_companies.SelectedIndex.ToString()]));
        }
    }
}
