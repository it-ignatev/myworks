﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Trans
{
    public partial class frm_newCar : Form
    {
        public frm_newCar()
        {
            InitializeComponent();

            SetCb();
            SetCarTypes();
        }

        private int[] clients;

        private void btn_SaveAndExit_Click(object sender, EventArgs e)
        {
            if (cb_client.SelectedIndex != -1)
            {
                if ((int)cb_type.Items.IndexOf(cb_type.Text) == -1)
                {
                    Sql.AddCarType(cb_type.Text);
                }

                string mes = Sql.AddCar(te_number.Text, clients[cb_client.SelectedIndex], Sql.getCarTypeId(cb_type.Text));

                frm_Main.Message(mes);

                this.Dispose();
            }
            else
            {
                frm_Main.Message("Выберите клиента !");
            }
        }

        private void SetCarTypes()
        {
            string res = Sql.LoadCarTypes();
            if (res == null) return;
            string[] result = res.Split(":".ToCharArray());

            cb_type.Items.Clear();

            for (int i = 0; i <= result.Length; i += 2)
            {
                res = result[i];
                if (res != string.Empty) cb_type.Items.Add(res);
            }   
        }

        private void SetCb()
        {
            List<Dictionary<string, string>> res = Sql.LoadClients();

            cb_client.Items.Clear();

            clients = new int[res.Count];

            int i = 0;

            foreach(Dictionary<string, string> r in res)
            {
                clients[i] = Convert.ToInt32(r["id"]); 
                cb_client.Items.Add(r["name"]);
                i++;
            }
        }
    }
}
