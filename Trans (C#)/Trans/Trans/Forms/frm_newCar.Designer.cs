﻿namespace Trans
{
    partial class frm_newCar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cb_client = new System.Windows.Forms.ComboBox();
            this.te_number = new System.Windows.Forms.TextBox();
            this.cb_type = new System.Windows.Forms.ComboBox();
            this.btn_SaveAndExit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Клиент:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Номер машины:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Тип:";
            // 
            // cb_client
            // 
            this.cb_client.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_client.FormattingEnabled = true;
            this.cb_client.Location = new System.Drawing.Point(123, 6);
            this.cb_client.Name = "cb_client";
            this.cb_client.Size = new System.Drawing.Size(223, 25);
            this.cb_client.TabIndex = 3;
            // 
            // te_number
            // 
            this.te_number.Location = new System.Drawing.Point(123, 37);
            this.te_number.Name = "te_number";
            this.te_number.Size = new System.Drawing.Size(223, 25);
            this.te_number.TabIndex = 4;
            // 
            // cb_type
            // 
            this.cb_type.FormattingEnabled = true;
            this.cb_type.Location = new System.Drawing.Point(123, 68);
            this.cb_type.Name = "cb_type";
            this.cb_type.Size = new System.Drawing.Size(223, 25);
            this.cb_type.TabIndex = 5;
            // 
            // btn_SaveAndExit
            // 
            this.btn_SaveAndExit.Location = new System.Drawing.Point(123, 99);
            this.btn_SaveAndExit.Name = "btn_SaveAndExit";
            this.btn_SaveAndExit.Size = new System.Drawing.Size(223, 35);
            this.btn_SaveAndExit.TabIndex = 6;
            this.btn_SaveAndExit.Text = "Добавить и Закрыть";
            this.btn_SaveAndExit.UseVisualStyleBackColor = true;
            this.btn_SaveAndExit.Click += new System.EventHandler(this.btn_SaveAndExit_Click);
            // 
            // frm_newCar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(358, 149);
            this.Controls.Add(this.btn_SaveAndExit);
            this.Controls.Add(this.cb_type);
            this.Controls.Add(this.te_number);
            this.Controls.Add(this.cb_client);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frm_newCar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Новая машина";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox te_number;
        private System.Windows.Forms.ComboBox cb_type;
        private System.Windows.Forms.Button btn_SaveAndExit;
        public System.Windows.Forms.ComboBox cb_client;
    }
}