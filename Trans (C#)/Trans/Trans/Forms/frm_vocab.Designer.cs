﻿namespace Trans
{
    partial class frm_vocab
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_newClient = new System.Windows.Forms.Button();
            this.btn_newContract = new System.Windows.Forms.Button();
            this.btn_newCompany = new System.Windows.Forms.Button();
            this.btn_newCar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_newClient
            // 
            this.btn_newClient.Location = new System.Drawing.Point(12, 68);
            this.btn_newClient.Name = "btn_newClient";
            this.btn_newClient.Size = new System.Drawing.Size(213, 50);
            this.btn_newClient.TabIndex = 0;
            this.btn_newClient.Text = "Новый клиент";
            this.btn_newClient.UseVisualStyleBackColor = true;
            this.btn_newClient.Click += new System.EventHandler(this.btn_newClient_Click);
            // 
            // btn_newContract
            // 
            this.btn_newContract.Location = new System.Drawing.Point(12, 12);
            this.btn_newContract.Name = "btn_newContract";
            this.btn_newContract.Size = new System.Drawing.Size(213, 50);
            this.btn_newContract.TabIndex = 1;
            this.btn_newContract.Text = "Новый договор";
            this.btn_newContract.UseVisualStyleBackColor = true;
            this.btn_newContract.Click += new System.EventHandler(this.btn_newContract_Click);
            // 
            // btn_newCompany
            // 
            this.btn_newCompany.Location = new System.Drawing.Point(12, 124);
            this.btn_newCompany.Name = "btn_newCompany";
            this.btn_newCompany.Size = new System.Drawing.Size(213, 50);
            this.btn_newCompany.TabIndex = 2;
            this.btn_newCompany.Text = "Новая Паромная Компания";
            this.btn_newCompany.UseVisualStyleBackColor = true;
            this.btn_newCompany.Click += new System.EventHandler(this.btn_newCompany_Click);
            // 
            // btn_newCar
            // 
            this.btn_newCar.Location = new System.Drawing.Point(12, 180);
            this.btn_newCar.Name = "btn_newCar";
            this.btn_newCar.Size = new System.Drawing.Size(213, 50);
            this.btn_newCar.TabIndex = 3;
            this.btn_newCar.Text = "Новая машина";
            this.btn_newCar.UseVisualStyleBackColor = true;
            this.btn_newCar.Click += new System.EventHandler(this.btn_newCar_Click);
            // 
            // frm_vocab
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(237, 242);
            this.Controls.Add(this.btn_newCar);
            this.Controls.Add(this.btn_newCompany);
            this.Controls.Add(this.btn_newContract);
            this.Controls.Add(this.btn_newClient);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.Name = "frm_vocab";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Справочники";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_newClient;
        private System.Windows.Forms.Button btn_newContract;
        private System.Windows.Forms.Button btn_newCompany;
        private System.Windows.Forms.Button btn_newCar;
    }
}