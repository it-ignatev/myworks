﻿namespace Trans
{
    partial class frm_information
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle51 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle52 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle53 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle54 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle55 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle56 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle57 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle58 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle59 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle60 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle61 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle62 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle63 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle64 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle65 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle66 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle67 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle68 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle69 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle70 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle71 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle72 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle73 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle74 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle75 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle76 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tab_main = new System.Windows.Forms.TabControl();
            this.tab_clients = new System.Windows.Forms.TabPage();
            this.lb_clients = new System.Windows.Forms.ListBox();
            this.gb_client = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.te_clients_bank2 = new System.Windows.Forms.TextBox();
            this.te_clients_iban = new System.Windows.Forms.TextBox();
            this.te_clients_swift = new System.Windows.Forms.TextBox();
            this.btn_clients_act = new System.Windows.Forms.Button();
            this.te_clients_timeChange = new System.Windows.Forms.TextBox();
            this.te_clients_timeAdd = new System.Windows.Forms.TextBox();
            this.te_clients_ad = new System.Windows.Forms.RichTextBox();
            this.te_clients_pc = new System.Windows.Forms.TextBox();
            this.te_clients_bik = new System.Windows.Forms.TextBox();
            this.te_clients_bank = new System.Windows.Forms.TextBox();
            this.te_clients_p_c = new System.Windows.Forms.TextBox();
            this.te_clients_adr2 = new System.Windows.Forms.TextBox();
            this.te_clients_adr1 = new System.Windows.Forms.TextBox();
            this.te_clients_kpp = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.te_clients_inn = new System.Windows.Forms.TextBox();
            this.te_clients_face = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.te_clients_name = new System.Windows.Forms.TextBox();
            this.tab_companies = new System.Windows.Forms.TabPage();
            this.gb_company = new System.Windows.Forms.GroupBox();
            this.btn_companies_act = new System.Windows.Forms.Button();
            this.te_companies_timeChange = new System.Windows.Forms.TextBox();
            this.te_companies_timeAdd = new System.Windows.Forms.TextBox();
            this.te_companies_swift = new System.Windows.Forms.TextBox();
            this.te_companies_bank = new System.Windows.Forms.TextBox();
            this.te_companies_iban = new System.Windows.Forms.TextBox();
            this.te_companies_adr = new System.Windows.Forms.TextBox();
            this.te_companies_name = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lb_companies = new System.Windows.Forms.ListBox();
            this.tab_contracts = new System.Windows.Forms.TabPage();
            this.gb_contract = new System.Windows.Forms.GroupBox();
            this.btn_copy = new System.Windows.Forms.Button();
            this.gb_ad = new System.Windows.Forms.GroupBox();
            this.btn_contracts_act = new System.Windows.Forms.Button();
            this.tabc_contracts = new System.Windows.Forms.TabControl();
            this.tp_0 = new System.Windows.Forms.TabPage();
            this.dgv_0 = new System.Windows.Forms.DataGridView();
            this.time1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.time2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price_euro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price_rub = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.length_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tp_1 = new System.Windows.Forms.TabPage();
            this.dgv_1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tp_2 = new System.Windows.Forms.TabPage();
            this.dgv_2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tp_3 = new System.Windows.Forms.TabPage();
            this.dgv_3 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tp_4 = new System.Windows.Forms.TabPage();
            this.dgv_4 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tp_5 = new System.Windows.Forms.TabPage();
            this.dgv_5 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tp_6 = new System.Windows.Forms.TabPage();
            this.dgv_6 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.te_contracts_timeEdit = new System.Windows.Forms.TextBox();
            this.te_contracts_timeAdd = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.cb_contracts_face = new System.Windows.Forms.ComboBox();
            this.cb_contracts_route = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.te_contracts_name = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.te_contracts_number = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.lb_contracts = new System.Windows.Forms.ListBox();
            this.tab_cars = new System.Windows.Forms.TabPage();
            this.gb_car = new System.Windows.Forms.GroupBox();
            this.btn_cars_act = new System.Windows.Forms.Button();
            this.cb_cars_type = new System.Windows.Forms.ComboBox();
            this.cb_cars_client = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.te_cars_name = new System.Windows.Forms.TextBox();
            this.lb_cars = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.dgv_ad = new System.Windows.Forms.DataGridView();
            this.name = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ad_price1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ad_price2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.weight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tab_main.SuspendLayout();
            this.tab_clients.SuspendLayout();
            this.gb_client.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tab_companies.SuspendLayout();
            this.gb_company.SuspendLayout();
            this.tab_contracts.SuspendLayout();
            this.gb_contract.SuspendLayout();
            this.gb_ad.SuspendLayout();
            this.tabc_contracts.SuspendLayout();
            this.tp_0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_0)).BeginInit();
            this.tp_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_1)).BeginInit();
            this.tp_2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_2)).BeginInit();
            this.tp_3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_3)).BeginInit();
            this.tp_4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_4)).BeginInit();
            this.tp_5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_5)).BeginInit();
            this.tp_6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_6)).BeginInit();
            this.tab_cars.SuspendLayout();
            this.gb_car.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ad)).BeginInit();
            this.SuspendLayout();
            // 
            // tab_main
            // 
            this.tab_main.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.tab_main.Controls.Add(this.tab_clients);
            this.tab_main.Controls.Add(this.tab_companies);
            this.tab_main.Controls.Add(this.tab_contracts);
            this.tab_main.Controls.Add(this.tab_cars);
            this.tab_main.Location = new System.Drawing.Point(-4, -4);
            this.tab_main.Margin = new System.Windows.Forms.Padding(0);
            this.tab_main.Multiline = true;
            this.tab_main.Name = "tab_main";
            this.tab_main.Padding = new System.Drawing.Point(0, 0);
            this.tab_main.SelectedIndex = 0;
            this.tab_main.Size = new System.Drawing.Size(796, 650);
            this.tab_main.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tab_main.TabIndex = 0;
            // 
            // tab_clients
            // 
            this.tab_clients.Controls.Add(this.lb_clients);
            this.tab_clients.Controls.Add(this.gb_client);
            this.tab_clients.Location = new System.Drawing.Point(4, 4);
            this.tab_clients.Name = "tab_clients";
            this.tab_clients.Padding = new System.Windows.Forms.Padding(3);
            this.tab_clients.Size = new System.Drawing.Size(788, 620);
            this.tab_clients.TabIndex = 0;
            this.tab_clients.Text = "Клиенты";
            // 
            // lb_clients
            // 
            this.lb_clients.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lb_clients.FormattingEnabled = true;
            this.lb_clients.ItemHeight = 17;
            this.lb_clients.Location = new System.Drawing.Point(8, 18);
            this.lb_clients.Name = "lb_clients";
            this.lb_clients.Size = new System.Drawing.Size(161, 580);
            this.lb_clients.TabIndex = 1;
            this.lb_clients.SelectedIndexChanged += new System.EventHandler(this.lb_clients_SelectedIndexChanged);
            // 
            // gb_client
            // 
            this.gb_client.Controls.Add(this.groupBox1);
            this.gb_client.Controls.Add(this.btn_clients_act);
            this.gb_client.Controls.Add(this.te_clients_timeChange);
            this.gb_client.Controls.Add(this.te_clients_timeAdd);
            this.gb_client.Controls.Add(this.te_clients_ad);
            this.gb_client.Controls.Add(this.te_clients_pc);
            this.gb_client.Controls.Add(this.te_clients_bik);
            this.gb_client.Controls.Add(this.te_clients_bank);
            this.gb_client.Controls.Add(this.te_clients_p_c);
            this.gb_client.Controls.Add(this.te_clients_adr2);
            this.gb_client.Controls.Add(this.te_clients_adr1);
            this.gb_client.Controls.Add(this.te_clients_kpp);
            this.gb_client.Controls.Add(this.label14);
            this.gb_client.Controls.Add(this.label13);
            this.gb_client.Controls.Add(this.label12);
            this.gb_client.Controls.Add(this.label11);
            this.gb_client.Controls.Add(this.label10);
            this.gb_client.Controls.Add(this.label9);
            this.gb_client.Controls.Add(this.label8);
            this.gb_client.Controls.Add(this.label7);
            this.gb_client.Controls.Add(this.label6);
            this.gb_client.Controls.Add(this.label5);
            this.gb_client.Controls.Add(this.label4);
            this.gb_client.Controls.Add(this.te_clients_inn);
            this.gb_client.Controls.Add(this.te_clients_face);
            this.gb_client.Controls.Add(this.label3);
            this.gb_client.Controls.Add(this.label2);
            this.gb_client.Controls.Add(this.te_clients_name);
            this.gb_client.Location = new System.Drawing.Point(175, 8);
            this.gb_client.Name = "gb_client";
            this.gb_client.Size = new System.Drawing.Size(595, 591);
            this.gb_client.TabIndex = 0;
            this.gb_client.TabStop = false;
            this.gb_client.Text = "О клиенте/Действия";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label32);
            this.groupBox1.Controls.Add(this.label31);
            this.groupBox1.Controls.Add(this.label30);
            this.groupBox1.Controls.Add(this.te_clients_bank2);
            this.groupBox1.Controls.Add(this.te_clients_iban);
            this.groupBox1.Controls.Add(this.te_clients_swift);
            this.groupBox1.Location = new System.Drawing.Point(337, 24);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(252, 130);
            this.groupBox1.TabIndex = 27;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Иностранные реквизиты";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(6, 96);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(38, 17);
            this.label32.TabIndex = 5;
            this.label32.Text = "Банк:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(6, 65);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(39, 17);
            this.label31.TabIndex = 4;
            this.label31.Text = "IBAN:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(6, 34);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(46, 17);
            this.label30.TabIndex = 3;
            this.label30.Text = "SWIFT:";
            // 
            // te_clients_bank2
            // 
            this.te_clients_bank2.Enabled = false;
            this.te_clients_bank2.Location = new System.Drawing.Point(58, 93);
            this.te_clients_bank2.Name = "te_clients_bank2";
            this.te_clients_bank2.Size = new System.Drawing.Size(188, 25);
            this.te_clients_bank2.TabIndex = 2;
            this.te_clients_bank2.Text = "Выберите клиента";
            this.te_clients_bank2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // te_clients_iban
            // 
            this.te_clients_iban.Enabled = false;
            this.te_clients_iban.Location = new System.Drawing.Point(58, 62);
            this.te_clients_iban.Name = "te_clients_iban";
            this.te_clients_iban.Size = new System.Drawing.Size(188, 25);
            this.te_clients_iban.TabIndex = 1;
            this.te_clients_iban.Text = "Выберите клиента";
            this.te_clients_iban.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // te_clients_swift
            // 
            this.te_clients_swift.Enabled = false;
            this.te_clients_swift.Location = new System.Drawing.Point(58, 31);
            this.te_clients_swift.Name = "te_clients_swift";
            this.te_clients_swift.Size = new System.Drawing.Size(188, 25);
            this.te_clients_swift.TabIndex = 0;
            this.te_clients_swift.Text = "Выберите клиента";
            this.te_clients_swift.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btn_clients_act
            // 
            this.btn_clients_act.Enabled = false;
            this.btn_clients_act.Location = new System.Drawing.Point(377, 543);
            this.btn_clients_act.Name = "btn_clients_act";
            this.btn_clients_act.Size = new System.Drawing.Size(212, 27);
            this.btn_clients_act.TabIndex = 26;
            this.btn_clients_act.Text = "Изменить";
            this.btn_clients_act.UseVisualStyleBackColor = true;
            // 
            // te_clients_timeChange
            // 
            this.te_clients_timeChange.Enabled = false;
            this.te_clients_timeChange.Location = new System.Drawing.Point(92, 560);
            this.te_clients_timeChange.Name = "te_clients_timeChange";
            this.te_clients_timeChange.ReadOnly = true;
            this.te_clients_timeChange.Size = new System.Drawing.Size(263, 25);
            this.te_clients_timeChange.TabIndex = 25;
            this.te_clients_timeChange.Text = "Выберите клиента";
            this.te_clients_timeChange.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // te_clients_timeAdd
            // 
            this.te_clients_timeAdd.Enabled = false;
            this.te_clients_timeAdd.Location = new System.Drawing.Point(92, 527);
            this.te_clients_timeAdd.Name = "te_clients_timeAdd";
            this.te_clients_timeAdd.ReadOnly = true;
            this.te_clients_timeAdd.Size = new System.Drawing.Size(263, 25);
            this.te_clients_timeAdd.TabIndex = 24;
            this.te_clients_timeAdd.Text = "Выберите клиента";
            this.te_clients_timeAdd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // te_clients_ad
            // 
            this.te_clients_ad.Enabled = false;
            this.te_clients_ad.Location = new System.Drawing.Point(92, 401);
            this.te_clients_ad.Name = "te_clients_ad";
            this.te_clients_ad.Size = new System.Drawing.Size(497, 120);
            this.te_clients_ad.TabIndex = 23;
            this.te_clients_ad.Text = "Выберите клиента";
            // 
            // te_clients_pc
            // 
            this.te_clients_pc.Enabled = false;
            this.te_clients_pc.Location = new System.Drawing.Point(92, 370);
            this.te_clients_pc.Name = "te_clients_pc";
            this.te_clients_pc.Size = new System.Drawing.Size(497, 25);
            this.te_clients_pc.TabIndex = 22;
            this.te_clients_pc.Text = "Выберите клиента";
            this.te_clients_pc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // te_clients_bik
            // 
            this.te_clients_bik.Enabled = false;
            this.te_clients_bik.Location = new System.Drawing.Point(92, 339);
            this.te_clients_bik.Name = "te_clients_bik";
            this.te_clients_bik.Size = new System.Drawing.Size(497, 25);
            this.te_clients_bik.TabIndex = 21;
            this.te_clients_bik.Text = "Выберите клиента";
            this.te_clients_bik.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // te_clients_bank
            // 
            this.te_clients_bank.Enabled = false;
            this.te_clients_bank.Location = new System.Drawing.Point(92, 308);
            this.te_clients_bank.Name = "te_clients_bank";
            this.te_clients_bank.Size = new System.Drawing.Size(497, 25);
            this.te_clients_bank.TabIndex = 20;
            this.te_clients_bank.Text = "Выберите клиента";
            this.te_clients_bank.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // te_clients_p_c
            // 
            this.te_clients_p_c.Enabled = false;
            this.te_clients_p_c.Location = new System.Drawing.Point(92, 277);
            this.te_clients_p_c.Name = "te_clients_p_c";
            this.te_clients_p_c.Size = new System.Drawing.Size(497, 25);
            this.te_clients_p_c.TabIndex = 19;
            this.te_clients_p_c.Text = "Выберите клиента";
            this.te_clients_p_c.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // te_clients_adr2
            // 
            this.te_clients_adr2.Enabled = false;
            this.te_clients_adr2.Location = new System.Drawing.Point(92, 246);
            this.te_clients_adr2.Name = "te_clients_adr2";
            this.te_clients_adr2.Size = new System.Drawing.Size(497, 25);
            this.te_clients_adr2.TabIndex = 18;
            this.te_clients_adr2.Text = "Выберите клиента";
            this.te_clients_adr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // te_clients_adr1
            // 
            this.te_clients_adr1.Enabled = false;
            this.te_clients_adr1.Location = new System.Drawing.Point(92, 215);
            this.te_clients_adr1.Name = "te_clients_adr1";
            this.te_clients_adr1.Size = new System.Drawing.Size(497, 25);
            this.te_clients_adr1.TabIndex = 17;
            this.te_clients_adr1.Text = "Выберите клиента";
            this.te_clients_adr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // te_clients_kpp
            // 
            this.te_clients_kpp.Enabled = false;
            this.te_clients_kpp.Location = new System.Drawing.Point(51, 129);
            this.te_clients_kpp.Name = "te_clients_kpp";
            this.te_clients_kpp.Size = new System.Drawing.Size(280, 25);
            this.te_clients_kpp.TabIndex = 16;
            this.te_clients_kpp.Text = "Выберите клиента";
            this.te_clients_kpp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 563);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(78, 17);
            this.label14.TabIndex = 15;
            this.label14.Text = "Время изм.:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 530);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(79, 17);
            this.label13.TabIndex = 14;
            this.label13.Text = "Время доб.:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 455);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(64, 17);
            this.label12.TabIndex = 13;
            this.label12.Text = "Доп.Инф:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 373);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(32, 17);
            this.label11.TabIndex = 12;
            this.label11.Text = "К/С:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 342);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(36, 17);
            this.label10.TabIndex = 11;
            this.label10.Text = "БИК:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 311);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 17);
            this.label9.TabIndex = 10;
            this.label9.Text = "Банк:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 280);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 17);
            this.label8.TabIndex = 9;
            this.label8.Text = "Р/С:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 249);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 17);
            this.label7.TabIndex = 8;
            this.label7.Text = "Адрес ф.:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 218);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 17);
            this.label6.TabIndex = 7;
            this.label6.Text = "Адрес ю.:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 132);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 17);
            this.label5.TabIndex = 6;
            this.label5.Text = "КПП:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "ИНН:";
            // 
            // te_clients_inn
            // 
            this.te_clients_inn.Enabled = false;
            this.te_clients_inn.Location = new System.Drawing.Point(51, 98);
            this.te_clients_inn.Name = "te_clients_inn";
            this.te_clients_inn.Size = new System.Drawing.Size(280, 25);
            this.te_clients_inn.TabIndex = 4;
            this.te_clients_inn.Text = "Выберите клиента";
            this.te_clients_inn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // te_clients_face
            // 
            this.te_clients_face.Enabled = false;
            this.te_clients_face.Location = new System.Drawing.Point(51, 64);
            this.te_clients_face.Name = "te_clients_face";
            this.te_clients_face.Size = new System.Drawing.Size(280, 25);
            this.te_clients_face.TabIndex = 3;
            this.te_clients_face.Text = "Выберите клиента";
            this.te_clients_face.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Лицо:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Имя:";
            // 
            // te_clients_name
            // 
            this.te_clients_name.Enabled = false;
            this.te_clients_name.Location = new System.Drawing.Point(51, 33);
            this.te_clients_name.Name = "te_clients_name";
            this.te_clients_name.Size = new System.Drawing.Size(280, 25);
            this.te_clients_name.TabIndex = 0;
            this.te_clients_name.Text = "Выберите клиента";
            this.te_clients_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tab_companies
            // 
            this.tab_companies.Controls.Add(this.gb_company);
            this.tab_companies.Controls.Add(this.lb_companies);
            this.tab_companies.Location = new System.Drawing.Point(4, 4);
            this.tab_companies.Name = "tab_companies";
            this.tab_companies.Padding = new System.Windows.Forms.Padding(3);
            this.tab_companies.Size = new System.Drawing.Size(788, 620);
            this.tab_companies.TabIndex = 1;
            this.tab_companies.Text = "Компании";
            // 
            // gb_company
            // 
            this.gb_company.Controls.Add(this.btn_companies_act);
            this.gb_company.Controls.Add(this.te_companies_timeChange);
            this.gb_company.Controls.Add(this.te_companies_timeAdd);
            this.gb_company.Controls.Add(this.te_companies_swift);
            this.gb_company.Controls.Add(this.te_companies_bank);
            this.gb_company.Controls.Add(this.te_companies_iban);
            this.gb_company.Controls.Add(this.te_companies_adr);
            this.gb_company.Controls.Add(this.te_companies_name);
            this.gb_company.Controls.Add(this.label20);
            this.gb_company.Controls.Add(this.label19);
            this.gb_company.Controls.Add(this.label18);
            this.gb_company.Controls.Add(this.label17);
            this.gb_company.Controls.Add(this.label16);
            this.gb_company.Controls.Add(this.label15);
            this.gb_company.Controls.Add(this.label1);
            this.gb_company.Location = new System.Drawing.Point(175, 8);
            this.gb_company.Name = "gb_company";
            this.gb_company.Size = new System.Drawing.Size(589, 590);
            this.gb_company.TabIndex = 1;
            this.gb_company.TabStop = false;
            this.gb_company.Text = "О компании/Действия";
            // 
            // btn_companies_act
            // 
            this.btn_companies_act.Enabled = false;
            this.btn_companies_act.Location = new System.Drawing.Point(356, 263);
            this.btn_companies_act.Name = "btn_companies_act";
            this.btn_companies_act.Size = new System.Drawing.Size(227, 32);
            this.btn_companies_act.TabIndex = 14;
            this.btn_companies_act.Text = "Изменить";
            this.btn_companies_act.UseVisualStyleBackColor = true;
            // 
            // te_companies_timeChange
            // 
            this.te_companies_timeChange.Enabled = false;
            this.te_companies_timeChange.Location = new System.Drawing.Point(86, 275);
            this.te_companies_timeChange.Name = "te_companies_timeChange";
            this.te_companies_timeChange.ReadOnly = true;
            this.te_companies_timeChange.Size = new System.Drawing.Size(264, 25);
            this.te_companies_timeChange.TabIndex = 13;
            this.te_companies_timeChange.Text = "Выберите компанию";
            this.te_companies_timeChange.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // te_companies_timeAdd
            // 
            this.te_companies_timeAdd.Enabled = false;
            this.te_companies_timeAdd.Location = new System.Drawing.Point(86, 244);
            this.te_companies_timeAdd.Name = "te_companies_timeAdd";
            this.te_companies_timeAdd.ReadOnly = true;
            this.te_companies_timeAdd.Size = new System.Drawing.Size(264, 25);
            this.te_companies_timeAdd.TabIndex = 12;
            this.te_companies_timeAdd.Text = "Выберите компанию";
            this.te_companies_timeAdd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // te_companies_swift
            // 
            this.te_companies_swift.Enabled = false;
            this.te_companies_swift.Location = new System.Drawing.Point(58, 180);
            this.te_companies_swift.Name = "te_companies_swift";
            this.te_companies_swift.Size = new System.Drawing.Size(525, 25);
            this.te_companies_swift.TabIndex = 11;
            this.te_companies_swift.Text = "Выберите компанию";
            this.te_companies_swift.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // te_companies_bank
            // 
            this.te_companies_bank.Enabled = false;
            this.te_companies_bank.Location = new System.Drawing.Point(58, 149);
            this.te_companies_bank.Name = "te_companies_bank";
            this.te_companies_bank.Size = new System.Drawing.Size(525, 25);
            this.te_companies_bank.TabIndex = 10;
            this.te_companies_bank.Text = "Выберите компанию";
            this.te_companies_bank.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // te_companies_iban
            // 
            this.te_companies_iban.Enabled = false;
            this.te_companies_iban.Location = new System.Drawing.Point(58, 118);
            this.te_companies_iban.Name = "te_companies_iban";
            this.te_companies_iban.Size = new System.Drawing.Size(525, 25);
            this.te_companies_iban.TabIndex = 9;
            this.te_companies_iban.Text = "Выберите компанию";
            this.te_companies_iban.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // te_companies_adr
            // 
            this.te_companies_adr.Enabled = false;
            this.te_companies_adr.Location = new System.Drawing.Point(58, 87);
            this.te_companies_adr.Name = "te_companies_adr";
            this.te_companies_adr.Size = new System.Drawing.Size(525, 25);
            this.te_companies_adr.TabIndex = 8;
            this.te_companies_adr.Text = "Выберите компанию";
            this.te_companies_adr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // te_companies_name
            // 
            this.te_companies_name.Enabled = false;
            this.te_companies_name.Location = new System.Drawing.Point(58, 56);
            this.te_companies_name.Name = "te_companies_name";
            this.te_companies_name.Size = new System.Drawing.Size(525, 25);
            this.te_companies_name.TabIndex = 7;
            this.te_companies_name.Text = "Выберите компанию";
            this.te_companies_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(7, 278);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(78, 17);
            this.label20.TabIndex = 6;
            this.label20.Text = "Время изм.:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(7, 247);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(79, 17);
            this.label19.TabIndex = 5;
            this.label19.Text = "Время доб.:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 183);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(46, 17);
            this.label18.TabIndex = 4;
            this.label18.Text = "SWIFT:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 152);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(38, 17);
            this.label17.TabIndex = 3;
            this.label17.Text = "Банк:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 121);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(39, 17);
            this.label16.TabIndex = 2;
            this.label16.Text = "IBAN:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 90);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(47, 17);
            this.label15.TabIndex = 1;
            this.label15.Text = "Адрес:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Имя:";
            // 
            // lb_companies
            // 
            this.lb_companies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lb_companies.FormattingEnabled = true;
            this.lb_companies.ItemHeight = 17;
            this.lb_companies.Location = new System.Drawing.Point(8, 18);
            this.lb_companies.Name = "lb_companies";
            this.lb_companies.Size = new System.Drawing.Size(161, 580);
            this.lb_companies.TabIndex = 0;
            this.lb_companies.SelectedIndexChanged += new System.EventHandler(this.lb_companies_SelectedIndexChanged);
            // 
            // tab_contracts
            // 
            this.tab_contracts.Controls.Add(this.gb_contract);
            this.tab_contracts.Controls.Add(this.lb_contracts);
            this.tab_contracts.Location = new System.Drawing.Point(4, 4);
            this.tab_contracts.Name = "tab_contracts";
            this.tab_contracts.Size = new System.Drawing.Size(788, 620);
            this.tab_contracts.TabIndex = 3;
            this.tab_contracts.Text = "Договоры";
            // 
            // gb_contract
            // 
            this.gb_contract.Controls.Add(this.button2);
            this.gb_contract.Controls.Add(this.button1);
            this.gb_contract.Controls.Add(this.btn_copy);
            this.gb_contract.Controls.Add(this.gb_ad);
            this.gb_contract.Controls.Add(this.btn_contracts_act);
            this.gb_contract.Controls.Add(this.tabc_contracts);
            this.gb_contract.Controls.Add(this.te_contracts_timeEdit);
            this.gb_contract.Controls.Add(this.te_contracts_timeAdd);
            this.gb_contract.Controls.Add(this.label29);
            this.gb_contract.Controls.Add(this.label28);
            this.gb_contract.Controls.Add(this.cb_contracts_face);
            this.gb_contract.Controls.Add(this.cb_contracts_route);
            this.gb_contract.Controls.Add(this.label27);
            this.gb_contract.Controls.Add(this.label26);
            this.gb_contract.Controls.Add(this.te_contracts_name);
            this.gb_contract.Controls.Add(this.label25);
            this.gb_contract.Controls.Add(this.te_contracts_number);
            this.gb_contract.Controls.Add(this.label24);
            this.gb_contract.Location = new System.Drawing.Point(176, 12);
            this.gb_contract.Name = "gb_contract";
            this.gb_contract.Size = new System.Drawing.Size(596, 587);
            this.gb_contract.TabIndex = 1;
            this.gb_contract.TabStop = false;
            this.gb_contract.Text = "О договоре / Действия";
            // 
            // btn_copy
            // 
            this.btn_copy.Location = new System.Drawing.Point(253, 524);
            this.btn_copy.Name = "btn_copy";
            this.btn_copy.Size = new System.Drawing.Size(68, 56);
            this.btn_copy.TabIndex = 18;
            this.btn_copy.Text = "Скоп.";
            this.btn_copy.UseVisualStyleBackColor = true;
            this.btn_copy.Click += new System.EventHandler(this.btn_copy_Click);
            // 
            // gb_ad
            // 
            this.gb_ad.Controls.Add(this.dgv_ad);
            this.gb_ad.Location = new System.Drawing.Point(9, 310);
            this.gb_ad.Name = "gb_ad";
            this.gb_ad.Size = new System.Drawing.Size(574, 208);
            this.gb_ad.TabIndex = 17;
            this.gb_ad.TabStop = false;
            this.gb_ad.Text = "Доп. опции";
            // 
            // btn_contracts_act
            // 
            this.btn_contracts_act.Location = new System.Drawing.Point(327, 524);
            this.btn_contracts_act.Name = "btn_contracts_act";
            this.btn_contracts_act.Size = new System.Drawing.Size(256, 56);
            this.btn_contracts_act.TabIndex = 16;
            this.btn_contracts_act.Text = "Изменить";
            this.btn_contracts_act.UseVisualStyleBackColor = true;
            this.btn_contracts_act.Click += new System.EventHandler(this.btn_contracts_act_Click);
            // 
            // tabc_contracts
            // 
            this.tabc_contracts.Controls.Add(this.tp_0);
            this.tabc_contracts.Controls.Add(this.tp_1);
            this.tabc_contracts.Controls.Add(this.tp_2);
            this.tabc_contracts.Controls.Add(this.tp_3);
            this.tabc_contracts.Controls.Add(this.tp_4);
            this.tabc_contracts.Controls.Add(this.tp_5);
            this.tabc_contracts.Controls.Add(this.tp_6);
            this.tabc_contracts.Enabled = false;
            this.tabc_contracts.Location = new System.Drawing.Point(9, 86);
            this.tabc_contracts.Name = "tabc_contracts";
            this.tabc_contracts.SelectedIndex = 0;
            this.tabc_contracts.Size = new System.Drawing.Size(578, 218);
            this.tabc_contracts.TabIndex = 15;
            // 
            // tp_0
            // 
            this.tp_0.AutoScroll = true;
            this.tp_0.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tp_0.Controls.Add(this.dgv_0);
            this.tp_0.Location = new System.Drawing.Point(4, 26);
            this.tp_0.Name = "tp_0";
            this.tp_0.Padding = new System.Windows.Forms.Padding(3);
            this.tp_0.Size = new System.Drawing.Size(570, 188);
            this.tp_0.TabIndex = 0;
            this.tp_0.Text = "Monday";
            // 
            // dgv_0
            // 
            this.dgv_0.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_0.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_0.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.time1,
            this.time2,
            this.price_euro,
            this.price_rub,
            this.length_});
            this.dgv_0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_0.Location = new System.Drawing.Point(3, 3);
            this.dgv_0.Name = "dgv_0";
            this.dgv_0.Size = new System.Drawing.Size(564, 182);
            this.dgv_0.TabIndex = 0;
            // 
            // time1
            // 
            dataGridViewCellStyle42.Format = "00:00:00";
            dataGridViewCellStyle42.NullValue = "00:00:00";
            this.time1.DefaultCellStyle = dataGridViewCellStyle42;
            this.time1.HeaderText = "Time1";
            this.time1.Name = "time1";
            // 
            // time2
            // 
            dataGridViewCellStyle43.Format = "T";
            dataGridViewCellStyle43.NullValue = "00:00:00";
            this.time2.DefaultCellStyle = dataGridViewCellStyle43;
            this.time2.HeaderText = "Time2";
            this.time2.Name = "time2";
            // 
            // price_euro
            // 
            dataGridViewCellStyle44.Format = "C2";
            dataGridViewCellStyle44.NullValue = "0,00";
            this.price_euro.DefaultCellStyle = dataGridViewCellStyle44;
            this.price_euro.HeaderText = "Груженая";
            this.price_euro.Name = "price_euro";
            // 
            // price_rub
            // 
            dataGridViewCellStyle45.Format = "C2";
            dataGridViewCellStyle45.NullValue = "0,00";
            this.price_rub.DefaultCellStyle = dataGridViewCellStyle45;
            this.price_rub.HeaderText = "Негруженая";
            this.price_rub.Name = "price_rub";
            // 
            // length_
            // 
            dataGridViewCellStyle46.Format = "0,00";
            dataGridViewCellStyle46.NullValue = "0,00";
            this.length_.DefaultCellStyle = dataGridViewCellStyle46;
            this.length_.HeaderText = "Длинна";
            this.length_.Name = "length_";
            // 
            // tp_1
            // 
            this.tp_1.Controls.Add(this.dgv_1);
            this.tp_1.Location = new System.Drawing.Point(4, 26);
            this.tp_1.Name = "tp_1";
            this.tp_1.Padding = new System.Windows.Forms.Padding(3);
            this.tp_1.Size = new System.Drawing.Size(570, 188);
            this.tp_1.TabIndex = 1;
            this.tp_1.Text = "Tuesday";
            // 
            // dgv_1
            // 
            this.dgv_1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.dgv_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_1.Location = new System.Drawing.Point(3, 3);
            this.dgv_1.Name = "dgv_1";
            this.dgv_1.Size = new System.Drawing.Size(564, 182);
            this.dgv_1.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn1
            // 
            dataGridViewCellStyle47.Format = "00:00:00";
            dataGridViewCellStyle47.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle47;
            this.dataGridViewTextBoxColumn1.HeaderText = "Time1";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            dataGridViewCellStyle48.Format = "T";
            dataGridViewCellStyle48.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle48;
            this.dataGridViewTextBoxColumn2.HeaderText = "Time2";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle49.Format = "C2";
            dataGridViewCellStyle49.NullValue = "0,00";
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle49;
            this.dataGridViewTextBoxColumn3.HeaderText = "Груженая";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewCellStyle50.Format = "C2";
            dataGridViewCellStyle50.NullValue = "0,00";
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle50;
            this.dataGridViewTextBoxColumn4.HeaderText = "Негруженая";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            dataGridViewCellStyle51.Format = "N2";
            dataGridViewCellStyle51.NullValue = "0,00";
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle51;
            this.dataGridViewTextBoxColumn5.HeaderText = "Длинна";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // tp_2
            // 
            this.tp_2.Controls.Add(this.dgv_2);
            this.tp_2.Location = new System.Drawing.Point(4, 26);
            this.tp_2.Name = "tp_2";
            this.tp_2.Padding = new System.Windows.Forms.Padding(3);
            this.tp_2.Size = new System.Drawing.Size(570, 188);
            this.tp_2.TabIndex = 2;
            this.tp_2.Text = "Wednesday";
            // 
            // dgv_2
            // 
            this.dgv_2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10});
            this.dgv_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_2.Location = new System.Drawing.Point(3, 3);
            this.dgv_2.Name = "dgv_2";
            this.dgv_2.Size = new System.Drawing.Size(564, 182);
            this.dgv_2.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn6
            // 
            dataGridViewCellStyle52.Format = "00:00:00";
            dataGridViewCellStyle52.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle52;
            this.dataGridViewTextBoxColumn6.HeaderText = "Time1";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            dataGridViewCellStyle53.Format = "T";
            dataGridViewCellStyle53.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle53;
            this.dataGridViewTextBoxColumn7.HeaderText = "Time2";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            dataGridViewCellStyle54.Format = "C2";
            dataGridViewCellStyle54.NullValue = "0,00";
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle54;
            this.dataGridViewTextBoxColumn8.HeaderText = "Груженая";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn9
            // 
            dataGridViewCellStyle55.Format = "C2";
            dataGridViewCellStyle55.NullValue = "0,00";
            this.dataGridViewTextBoxColumn9.DefaultCellStyle = dataGridViewCellStyle55;
            this.dataGridViewTextBoxColumn9.HeaderText = "Негруженая";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // dataGridViewTextBoxColumn10
            // 
            dataGridViewCellStyle56.Format = "N2";
            dataGridViewCellStyle56.NullValue = "0,00";
            this.dataGridViewTextBoxColumn10.DefaultCellStyle = dataGridViewCellStyle56;
            this.dataGridViewTextBoxColumn10.HeaderText = "Длинна";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            // 
            // tp_3
            // 
            this.tp_3.Controls.Add(this.dgv_3);
            this.tp_3.Location = new System.Drawing.Point(4, 26);
            this.tp_3.Name = "tp_3";
            this.tp_3.Size = new System.Drawing.Size(570, 188);
            this.tp_3.TabIndex = 3;
            this.tp_3.Text = "Thursday";
            // 
            // dgv_3
            // 
            this.dgv_3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15});
            this.dgv_3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_3.Location = new System.Drawing.Point(0, 0);
            this.dgv_3.Name = "dgv_3";
            this.dgv_3.Size = new System.Drawing.Size(570, 188);
            this.dgv_3.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn11
            // 
            dataGridViewCellStyle57.Format = "00:00:00";
            dataGridViewCellStyle57.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn11.DefaultCellStyle = dataGridViewCellStyle57;
            this.dataGridViewTextBoxColumn11.HeaderText = "Time1";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            // 
            // dataGridViewTextBoxColumn12
            // 
            dataGridViewCellStyle58.Format = "T";
            dataGridViewCellStyle58.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn12.DefaultCellStyle = dataGridViewCellStyle58;
            this.dataGridViewTextBoxColumn12.HeaderText = "Time2";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            // 
            // dataGridViewTextBoxColumn13
            // 
            dataGridViewCellStyle59.Format = "C2";
            dataGridViewCellStyle59.NullValue = "0,00";
            this.dataGridViewTextBoxColumn13.DefaultCellStyle = dataGridViewCellStyle59;
            this.dataGridViewTextBoxColumn13.HeaderText = "Груженая";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            // 
            // dataGridViewTextBoxColumn14
            // 
            dataGridViewCellStyle60.Format = "C2";
            dataGridViewCellStyle60.NullValue = "0,00";
            this.dataGridViewTextBoxColumn14.DefaultCellStyle = dataGridViewCellStyle60;
            this.dataGridViewTextBoxColumn14.HeaderText = "Негруженая";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            // 
            // dataGridViewTextBoxColumn15
            // 
            dataGridViewCellStyle61.Format = "N2";
            dataGridViewCellStyle61.NullValue = "0,00";
            this.dataGridViewTextBoxColumn15.DefaultCellStyle = dataGridViewCellStyle61;
            this.dataGridViewTextBoxColumn15.HeaderText = "Длинна";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            // 
            // tp_4
            // 
            this.tp_4.Controls.Add(this.dgv_4);
            this.tp_4.Location = new System.Drawing.Point(4, 26);
            this.tp_4.Name = "tp_4";
            this.tp_4.Size = new System.Drawing.Size(570, 188);
            this.tp_4.TabIndex = 4;
            this.tp_4.Text = "Friday";
            // 
            // dgv_4
            // 
            this.dgv_4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20});
            this.dgv_4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_4.Location = new System.Drawing.Point(0, 0);
            this.dgv_4.Name = "dgv_4";
            this.dgv_4.Size = new System.Drawing.Size(570, 188);
            this.dgv_4.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn16
            // 
            dataGridViewCellStyle62.Format = "00:00:00";
            dataGridViewCellStyle62.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn16.DefaultCellStyle = dataGridViewCellStyle62;
            this.dataGridViewTextBoxColumn16.HeaderText = "Time1";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            // 
            // dataGridViewTextBoxColumn17
            // 
            dataGridViewCellStyle63.Format = "T";
            dataGridViewCellStyle63.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn17.DefaultCellStyle = dataGridViewCellStyle63;
            this.dataGridViewTextBoxColumn17.HeaderText = "Time2";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            // 
            // dataGridViewTextBoxColumn18
            // 
            dataGridViewCellStyle64.Format = "C2";
            dataGridViewCellStyle64.NullValue = "0,00";
            this.dataGridViewTextBoxColumn18.DefaultCellStyle = dataGridViewCellStyle64;
            this.dataGridViewTextBoxColumn18.HeaderText = "Груженая";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            // 
            // dataGridViewTextBoxColumn19
            // 
            dataGridViewCellStyle65.Format = "C2";
            dataGridViewCellStyle65.NullValue = "0,00";
            this.dataGridViewTextBoxColumn19.DefaultCellStyle = dataGridViewCellStyle65;
            this.dataGridViewTextBoxColumn19.HeaderText = "Негруженая";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            // 
            // dataGridViewTextBoxColumn20
            // 
            dataGridViewCellStyle66.Format = "N2";
            dataGridViewCellStyle66.NullValue = "0,00";
            this.dataGridViewTextBoxColumn20.DefaultCellStyle = dataGridViewCellStyle66;
            this.dataGridViewTextBoxColumn20.HeaderText = "Длинна";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            // 
            // tp_5
            // 
            this.tp_5.Controls.Add(this.dgv_5);
            this.tp_5.Location = new System.Drawing.Point(4, 26);
            this.tp_5.Name = "tp_5";
            this.tp_5.Size = new System.Drawing.Size(570, 188);
            this.tp_5.TabIndex = 5;
            this.tp_5.Text = "Saturday";
            // 
            // dgv_5
            // 
            this.dgv_5.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewTextBoxColumn25});
            this.dgv_5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_5.Location = new System.Drawing.Point(0, 0);
            this.dgv_5.Name = "dgv_5";
            this.dgv_5.Size = new System.Drawing.Size(570, 188);
            this.dgv_5.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn21
            // 
            dataGridViewCellStyle67.Format = "00:00:00";
            dataGridViewCellStyle67.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn21.DefaultCellStyle = dataGridViewCellStyle67;
            this.dataGridViewTextBoxColumn21.HeaderText = "Time1";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            // 
            // dataGridViewTextBoxColumn22
            // 
            dataGridViewCellStyle68.Format = "T";
            dataGridViewCellStyle68.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn22.DefaultCellStyle = dataGridViewCellStyle68;
            this.dataGridViewTextBoxColumn22.HeaderText = "Time2";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            // 
            // dataGridViewTextBoxColumn23
            // 
            dataGridViewCellStyle69.Format = "C2";
            dataGridViewCellStyle69.NullValue = "0,00";
            this.dataGridViewTextBoxColumn23.DefaultCellStyle = dataGridViewCellStyle69;
            this.dataGridViewTextBoxColumn23.HeaderText = "Груженая";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            // 
            // dataGridViewTextBoxColumn24
            // 
            dataGridViewCellStyle70.Format = "C2";
            dataGridViewCellStyle70.NullValue = "0,00";
            this.dataGridViewTextBoxColumn24.DefaultCellStyle = dataGridViewCellStyle70;
            this.dataGridViewTextBoxColumn24.HeaderText = "Негруженая";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            // 
            // dataGridViewTextBoxColumn25
            // 
            dataGridViewCellStyle71.Format = "N2";
            dataGridViewCellStyle71.NullValue = "0,00";
            this.dataGridViewTextBoxColumn25.DefaultCellStyle = dataGridViewCellStyle71;
            this.dataGridViewTextBoxColumn25.HeaderText = "Длинна";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            // 
            // tp_6
            // 
            this.tp_6.Controls.Add(this.dgv_6);
            this.tp_6.Location = new System.Drawing.Point(4, 26);
            this.tp_6.Name = "tp_6";
            this.tp_6.Size = new System.Drawing.Size(570, 188);
            this.tp_6.TabIndex = 6;
            this.tp_6.Text = "Sunday";
            // 
            // dgv_6
            // 
            this.dgv_6.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_6.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn26,
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewTextBoxColumn29,
            this.dataGridViewTextBoxColumn30});
            this.dgv_6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_6.Location = new System.Drawing.Point(0, 0);
            this.dgv_6.Name = "dgv_6";
            this.dgv_6.Size = new System.Drawing.Size(570, 188);
            this.dgv_6.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn26
            // 
            dataGridViewCellStyle72.Format = "00:00:00";
            dataGridViewCellStyle72.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn26.DefaultCellStyle = dataGridViewCellStyle72;
            this.dataGridViewTextBoxColumn26.HeaderText = "Time1";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            // 
            // dataGridViewTextBoxColumn27
            // 
            dataGridViewCellStyle73.Format = "T";
            dataGridViewCellStyle73.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn27.DefaultCellStyle = dataGridViewCellStyle73;
            this.dataGridViewTextBoxColumn27.HeaderText = "Time2";
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            // 
            // dataGridViewTextBoxColumn28
            // 
            dataGridViewCellStyle74.Format = "C2";
            dataGridViewCellStyle74.NullValue = "0,00";
            this.dataGridViewTextBoxColumn28.DefaultCellStyle = dataGridViewCellStyle74;
            this.dataGridViewTextBoxColumn28.HeaderText = "Груженая";
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            // 
            // dataGridViewTextBoxColumn29
            // 
            dataGridViewCellStyle75.Format = "C2";
            dataGridViewCellStyle75.NullValue = "0,00";
            this.dataGridViewTextBoxColumn29.DefaultCellStyle = dataGridViewCellStyle75;
            this.dataGridViewTextBoxColumn29.HeaderText = "Негруженая";
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            // 
            // dataGridViewTextBoxColumn30
            // 
            dataGridViewCellStyle76.Format = "N2";
            dataGridViewCellStyle76.NullValue = "0,00";
            this.dataGridViewTextBoxColumn30.DefaultCellStyle = dataGridViewCellStyle76;
            this.dataGridViewTextBoxColumn30.HeaderText = "Длинна";
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            // 
            // te_contracts_timeEdit
            // 
            this.te_contracts_timeEdit.Enabled = false;
            this.te_contracts_timeEdit.Location = new System.Drawing.Point(91, 555);
            this.te_contracts_timeEdit.Name = "te_contracts_timeEdit";
            this.te_contracts_timeEdit.ReadOnly = true;
            this.te_contracts_timeEdit.Size = new System.Drawing.Size(156, 25);
            this.te_contracts_timeEdit.TabIndex = 12;
            this.te_contracts_timeEdit.Text = "Выберите договор";
            this.te_contracts_timeEdit.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // te_contracts_timeAdd
            // 
            this.te_contracts_timeAdd.Enabled = false;
            this.te_contracts_timeAdd.Location = new System.Drawing.Point(91, 524);
            this.te_contracts_timeAdd.Name = "te_contracts_timeAdd";
            this.te_contracts_timeAdd.ReadOnly = true;
            this.te_contracts_timeAdd.Size = new System.Drawing.Size(156, 25);
            this.te_contracts_timeAdd.TabIndex = 11;
            this.te_contracts_timeAdd.Text = "Выберите договор";
            this.te_contracts_timeAdd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(6, 558);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(78, 17);
            this.label29.TabIndex = 10;
            this.label29.Text = "Время изм.:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 532);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(79, 17);
            this.label28.TabIndex = 9;
            this.label28.Text = "Время доб.:";
            // 
            // cb_contracts_face
            // 
            this.cb_contracts_face.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cb_contracts_face.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cb_contracts_face.Enabled = false;
            this.cb_contracts_face.FormattingEnabled = true;
            this.cb_contracts_face.Location = new System.Drawing.Point(353, 24);
            this.cb_contracts_face.Name = "cb_contracts_face";
            this.cb_contracts_face.Size = new System.Drawing.Size(234, 25);
            this.cb_contracts_face.TabIndex = 8;
            // 
            // cb_contracts_route
            // 
            this.cb_contracts_route.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_contracts_route.Enabled = false;
            this.cb_contracts_route.FormattingEnabled = true;
            this.cb_contracts_route.Location = new System.Drawing.Point(353, 55);
            this.cb_contracts_route.Name = "cb_contracts_route";
            this.cb_contracts_route.Size = new System.Drawing.Size(234, 25);
            this.cb_contracts_route.TabIndex = 7;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(279, 58);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(68, 17);
            this.label27.TabIndex = 6;
            this.label27.Text = "Маршрут:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(279, 27);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(42, 17);
            this.label26.TabIndex = 5;
            this.label26.Text = "Лицо:";
            // 
            // te_contracts_name
            // 
            this.te_contracts_name.Enabled = false;
            this.te_contracts_name.Location = new System.Drawing.Point(64, 55);
            this.te_contracts_name.Name = "te_contracts_name";
            this.te_contracts_name.Size = new System.Drawing.Size(209, 25);
            this.te_contracts_name.TabIndex = 3;
            this.te_contracts_name.Text = "Выберите договор";
            this.te_contracts_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 27);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(52, 17);
            this.label25.TabIndex = 2;
            this.label25.Text = "Номер:";
            // 
            // te_contracts_number
            // 
            this.te_contracts_number.Enabled = false;
            this.te_contracts_number.Location = new System.Drawing.Point(64, 24);
            this.te_contracts_number.Name = "te_contracts_number";
            this.te_contracts_number.Size = new System.Drawing.Size(209, 25);
            this.te_contracts_number.TabIndex = 1;
            this.te_contracts_number.Text = "Выберите договор";
            this.te_contracts_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 58);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(37, 17);
            this.label24.TabIndex = 0;
            this.label24.Text = "Имя:";
            // 
            // lb_contracts
            // 
            this.lb_contracts.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lb_contracts.FormattingEnabled = true;
            this.lb_contracts.ItemHeight = 17;
            this.lb_contracts.Location = new System.Drawing.Point(8, 18);
            this.lb_contracts.Name = "lb_contracts";
            this.lb_contracts.Size = new System.Drawing.Size(161, 580);
            this.lb_contracts.TabIndex = 0;
            this.lb_contracts.SelectedIndexChanged += new System.EventHandler(this.lb_contracts_SelectedIndexChanged);
            // 
            // tab_cars
            // 
            this.tab_cars.Controls.Add(this.gb_car);
            this.tab_cars.Controls.Add(this.lb_cars);
            this.tab_cars.Location = new System.Drawing.Point(4, 4);
            this.tab_cars.Name = "tab_cars";
            this.tab_cars.Size = new System.Drawing.Size(788, 620);
            this.tab_cars.TabIndex = 2;
            this.tab_cars.Text = "Машины";
            // 
            // gb_car
            // 
            this.gb_car.Controls.Add(this.btn_cars_act);
            this.gb_car.Controls.Add(this.cb_cars_type);
            this.gb_car.Controls.Add(this.cb_cars_client);
            this.gb_car.Controls.Add(this.label23);
            this.gb_car.Controls.Add(this.label22);
            this.gb_car.Controls.Add(this.label21);
            this.gb_car.Controls.Add(this.te_cars_name);
            this.gb_car.Location = new System.Drawing.Point(175, 12);
            this.gb_car.Name = "gb_car";
            this.gb_car.Size = new System.Drawing.Size(585, 569);
            this.gb_car.TabIndex = 1;
            this.gb_car.TabStop = false;
            this.gb_car.Text = "О машине/ Действия";
            // 
            // btn_cars_act
            // 
            this.btn_cars_act.Enabled = false;
            this.btn_cars_act.Location = new System.Drawing.Point(9, 161);
            this.btn_cars_act.Name = "btn_cars_act";
            this.btn_cars_act.Size = new System.Drawing.Size(570, 31);
            this.btn_cars_act.TabIndex = 6;
            this.btn_cars_act.Text = "Изменить";
            this.btn_cars_act.UseVisualStyleBackColor = true;
            // 
            // cb_cars_type
            // 
            this.cb_cars_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_cars_type.Enabled = false;
            this.cb_cars_type.FormattingEnabled = true;
            this.cb_cars_type.Location = new System.Drawing.Point(64, 88);
            this.cb_cars_type.Name = "cb_cars_type";
            this.cb_cars_type.Size = new System.Drawing.Size(515, 25);
            this.cb_cars_type.TabIndex = 5;
            // 
            // cb_cars_client
            // 
            this.cb_cars_client.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_cars_client.Enabled = false;
            this.cb_cars_client.FormattingEnabled = true;
            this.cb_cars_client.Location = new System.Drawing.Point(64, 57);
            this.cb_cars_client.Name = "cb_cars_client";
            this.cb_cars_client.Size = new System.Drawing.Size(515, 25);
            this.cb_cars_client.TabIndex = 4;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 91);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(32, 17);
            this.label23.TabIndex = 3;
            this.label23.Text = "Тип:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 60);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(52, 17);
            this.label22.TabIndex = 2;
            this.label22.Text = "Клиент:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 26);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(52, 17);
            this.label21.TabIndex = 1;
            this.label21.Text = "Номер:";
            // 
            // te_cars_name
            // 
            this.te_cars_name.Enabled = false;
            this.te_cars_name.Location = new System.Drawing.Point(64, 23);
            this.te_cars_name.Name = "te_cars_name";
            this.te_cars_name.Size = new System.Drawing.Size(515, 25);
            this.te_cars_name.TabIndex = 0;
            this.te_cars_name.Text = "Выберите машину";
            // 
            // lb_cars
            // 
            this.lb_cars.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lb_cars.FormattingEnabled = true;
            this.lb_cars.ItemHeight = 17;
            this.lb_cars.Location = new System.Drawing.Point(8, 18);
            this.lb_cars.Name = "lb_cars";
            this.lb_cars.Size = new System.Drawing.Size(161, 563);
            this.lb_cars.TabIndex = 0;
            this.lb_cars.SelectedIndexChanged += new System.EventHandler(this.lb_cars_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(469, 86);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(49, 23);
            this.button1.TabIndex = 19;
            this.button1.Text = "Скоп";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(524, 86);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(49, 23);
            this.button2.TabIndex = 20;
            this.button2.Text = "Встав";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // dgv_ad
            // 
            this.dgv_ad.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_ad.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_ad.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.name,
            this.ad_price1,
            this.ad_price2,
            this.weight});
            this.dgv_ad.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_ad.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgv_ad.Enabled = false;
            this.dgv_ad.Location = new System.Drawing.Point(3, 21);
            this.dgv_ad.Name = "dgv_ad";
            this.dgv_ad.Size = new System.Drawing.Size(568, 184);
            this.dgv_ad.TabIndex = 9;
            // 
            // name
            // 
            this.name.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.name.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.name.HeaderText = "Название";
            this.name.Name = "name";
            this.name.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // ad_price1
            // 
            dataGridViewCellStyle39.Format = "C2";
            dataGridViewCellStyle39.NullValue = "0,00";
            this.ad_price1.DefaultCellStyle = dataGridViewCellStyle39;
            this.ad_price1.HeaderText = "Груженая";
            this.ad_price1.Name = "ad_price1";
            // 
            // ad_price2
            // 
            dataGridViewCellStyle40.Format = "C2";
            dataGridViewCellStyle40.NullValue = "0,00";
            this.ad_price2.DefaultCellStyle = dataGridViewCellStyle40;
            this.ad_price2.HeaderText = "Негруженая";
            this.ad_price2.Name = "ad_price2";
            // 
            // weight
            // 
            dataGridViewCellStyle41.Format = "N2";
            dataGridViewCellStyle41.NullValue = "0,00";
            this.weight.DefaultCellStyle = dataGridViewCellStyle41;
            this.weight.HeaderText = "Вес";
            this.weight.Name = "weight";
            // 
            // frm_information
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(786, 643);
            this.Controls.Add(this.tab_main);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "frm_information";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Информация";
            this.tab_main.ResumeLayout(false);
            this.tab_clients.ResumeLayout(false);
            this.gb_client.ResumeLayout(false);
            this.gb_client.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tab_companies.ResumeLayout(false);
            this.gb_company.ResumeLayout(false);
            this.gb_company.PerformLayout();
            this.tab_contracts.ResumeLayout(false);
            this.gb_contract.ResumeLayout(false);
            this.gb_contract.PerformLayout();
            this.gb_ad.ResumeLayout(false);
            this.tabc_contracts.ResumeLayout(false);
            this.tp_0.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_0)).EndInit();
            this.tp_1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_1)).EndInit();
            this.tp_2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_2)).EndInit();
            this.tp_3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_3)).EndInit();
            this.tp_4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_4)).EndInit();
            this.tp_5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_5)).EndInit();
            this.tp_6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_6)).EndInit();
            this.tab_cars.ResumeLayout(false);
            this.gb_car.ResumeLayout(false);
            this.gb_car.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ad)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tab_main;
        private System.Windows.Forms.TabPage tab_clients;
        private System.Windows.Forms.TabPage tab_companies;
        private System.Windows.Forms.TabPage tab_contracts;
        private System.Windows.Forms.TabPage tab_cars;
        private System.Windows.Forms.ListBox lb_clients;
        private System.Windows.Forms.GroupBox gb_client;
        private System.Windows.Forms.GroupBox gb_company;
        private System.Windows.Forms.ListBox lb_companies;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox te_clients_inn;
        private System.Windows.Forms.TextBox te_clients_face;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox te_clients_name;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox te_clients_timeAdd;
        private System.Windows.Forms.RichTextBox te_clients_ad;
        private System.Windows.Forms.TextBox te_clients_pc;
        private System.Windows.Forms.TextBox te_clients_bik;
        private System.Windows.Forms.TextBox te_clients_bank;
        private System.Windows.Forms.TextBox te_clients_p_c;
        private System.Windows.Forms.TextBox te_clients_adr2;
        private System.Windows.Forms.TextBox te_clients_adr1;
        private System.Windows.Forms.TextBox te_clients_kpp;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox te_clients_timeChange;
        private System.Windows.Forms.Button btn_clients_act;
        private System.Windows.Forms.TextBox te_companies_timeChange;
        private System.Windows.Forms.TextBox te_companies_timeAdd;
        private System.Windows.Forms.TextBox te_companies_swift;
        private System.Windows.Forms.TextBox te_companies_bank;
        private System.Windows.Forms.TextBox te_companies_iban;
        private System.Windows.Forms.TextBox te_companies_adr;
        private System.Windows.Forms.TextBox te_companies_name;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btn_companies_act;
        private System.Windows.Forms.GroupBox gb_car;
        private System.Windows.Forms.ListBox lb_cars;
        private System.Windows.Forms.Button btn_cars_act;
        private System.Windows.Forms.ComboBox cb_cars_type;
        private System.Windows.Forms.ComboBox cb_cars_client;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox te_cars_name;
        private System.Windows.Forms.GroupBox gb_contract;
        private System.Windows.Forms.ListBox lb_contracts;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox te_contracts_number;
        private System.Windows.Forms.ComboBox cb_contracts_face;
        private System.Windows.Forms.ComboBox cb_contracts_route;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox te_contracts_name;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox te_contracts_timeEdit;
        private System.Windows.Forms.TextBox te_contracts_timeAdd;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button btn_contracts_act;
        private System.Windows.Forms.TabControl tabc_contracts;
        private System.Windows.Forms.TabPage tp_0;
        private System.Windows.Forms.TabPage tp_1;
        private System.Windows.Forms.TabPage tp_2;
        private System.Windows.Forms.TabPage tp_3;
        private System.Windows.Forms.TabPage tp_4;
        private System.Windows.Forms.TabPage tp_5;
        private System.Windows.Forms.TabPage tp_6;
        private System.Windows.Forms.GroupBox gb_ad;
        private System.Windows.Forms.Button btn_copy;
        private System.Windows.Forms.DataGridView dgv_0;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox te_clients_bank2;
        private System.Windows.Forms.TextBox te_clients_iban;
        private System.Windows.Forms.TextBox te_clients_swift;
        private System.Windows.Forms.DataGridView dgv_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridView dgv_2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridView dgv_3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridView dgv_4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridView dgv_5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridView dgv_6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewTextBoxColumn time1;
        private System.Windows.Forms.DataGridViewTextBoxColumn time2;
        private System.Windows.Forms.DataGridViewTextBoxColumn price_euro;
        private System.Windows.Forms.DataGridViewTextBoxColumn price_rub;
        private System.Windows.Forms.DataGridViewTextBoxColumn length_;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dgv_ad;
        private System.Windows.Forms.DataGridViewComboBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn ad_price1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ad_price2;
        private System.Windows.Forms.DataGridViewTextBoxColumn weight;
    }
}