﻿namespace Trans.Forms
{
    partial class frm_order
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_newOrder = new System.Windows.Forms.Button();
            this.btn_historyOrders = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_newOrder
            // 
            this.btn_newOrder.Location = new System.Drawing.Point(14, 16);
            this.btn_newOrder.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_newOrder.Name = "btn_newOrder";
            this.btn_newOrder.Size = new System.Drawing.Size(223, 55);
            this.btn_newOrder.TabIndex = 0;
            this.btn_newOrder.Text = "Новый заказ";
            this.btn_newOrder.UseVisualStyleBackColor = true;
            this.btn_newOrder.Click += new System.EventHandler(this.btn_newOrder_Click);
            // 
            // btn_historyOrders
            // 
            this.btn_historyOrders.Location = new System.Drawing.Point(14, 78);
            this.btn_historyOrders.Name = "btn_historyOrders";
            this.btn_historyOrders.Size = new System.Drawing.Size(223, 55);
            this.btn_historyOrders.TabIndex = 1;
            this.btn_historyOrders.Text = "История заказов";
            this.btn_historyOrders.UseVisualStyleBackColor = true;
            this.btn_historyOrders.Click += new System.EventHandler(this.btn_historyOrders_Click);
            // 
            // frm_order
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(251, 148);
            this.Controls.Add(this.btn_historyOrders);
            this.Controls.Add(this.btn_newOrder);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "frm_order";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Заказы";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_newOrder;
        private System.Windows.Forms.Button btn_historyOrders;
    }
}