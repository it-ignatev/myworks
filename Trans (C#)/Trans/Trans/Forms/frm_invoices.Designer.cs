﻿namespace Trans.Forms
{
    partial class frm_invoices
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_search_date = new System.Windows.Forms.Button();
            this.dtp_2 = new System.Windows.Forms.DateTimePicker();
            this.dtp_1 = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.btn_search_invoice = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.te_invoice = new System.Windows.Forms.TextBox();
            this.btn_search_all = new System.Windows.Forms.Button();
            this.btn_makeAct = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(202, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "#";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(264, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "СЧЕТ";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Location = new System.Drawing.Point(190, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(684, 285);
            this.panel1.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(356, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "СУММА(ЕВРО)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(452, 4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "СУММА(РУБЛИ)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(616, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "КЛИЕНТ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(765, 4);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 17);
            this.label6.TabIndex = 6;
            this.label6.Text = "ДАТА";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_makeAct);
            this.groupBox1.Controls.Add(this.btn_search_date);
            this.groupBox1.Controls.Add(this.dtp_2);
            this.groupBox1.Controls.Add(this.dtp_1);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.btn_search_invoice);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.te_invoice);
            this.groupBox1.Controls.Add(this.btn_search_all);
            this.groupBox1.Location = new System.Drawing.Point(12, 18);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(172, 291);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ПОИСК";
            // 
            // btn_search_date
            // 
            this.btn_search_date.Location = new System.Drawing.Point(6, 221);
            this.btn_search_date.Name = "btn_search_date";
            this.btn_search_date.Size = new System.Drawing.Size(160, 23);
            this.btn_search_date.TabIndex = 7;
            this.btn_search_date.Text = "ПОИСК";
            this.btn_search_date.UseVisualStyleBackColor = true;
            this.btn_search_date.Click += new System.EventHandler(this.btn_search_date_Click);
            // 
            // dtp_2
            // 
            this.dtp_2.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtp_2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_2.Location = new System.Drawing.Point(9, 190);
            this.dtp_2.Name = "dtp_2";
            this.dtp_2.Size = new System.Drawing.Size(157, 25);
            this.dtp_2.TabIndex = 6;
            // 
            // dtp_1
            // 
            this.dtp_1.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtp_1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_1.Location = new System.Drawing.Point(9, 159);
            this.dtp_1.Name = "dtp_1";
            this.dtp_1.Size = new System.Drawing.Size(157, 25);
            this.dtp_1.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 139);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 17);
            this.label8.TabIndex = 4;
            this.label8.Text = "ПО ДАТЕ:";
            // 
            // btn_search_invoice
            // 
            this.btn_search_invoice.Location = new System.Drawing.Point(6, 108);
            this.btn_search_invoice.Name = "btn_search_invoice";
            this.btn_search_invoice.Size = new System.Drawing.Size(160, 23);
            this.btn_search_invoice.TabIndex = 3;
            this.btn_search_invoice.Text = "ПОИСК";
            this.btn_search_invoice.UseVisualStyleBackColor = true;
            this.btn_search_invoice.Click += new System.EventHandler(this.btn_search_invoice_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 57);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 17);
            this.label7.TabIndex = 2;
            this.label7.Text = "ПО СЧЕТУ:";
            // 
            // te_invoice
            // 
            this.te_invoice.Location = new System.Drawing.Point(6, 77);
            this.te_invoice.Name = "te_invoice";
            this.te_invoice.Size = new System.Drawing.Size(160, 25);
            this.te_invoice.TabIndex = 1;
            // 
            // btn_search_all
            // 
            this.btn_search_all.Enabled = false;
            this.btn_search_all.Location = new System.Drawing.Point(6, 24);
            this.btn_search_all.Name = "btn_search_all";
            this.btn_search_all.Size = new System.Drawing.Size(160, 23);
            this.btn_search_all.TabIndex = 0;
            this.btn_search_all.Text = "ВСЕ";
            this.btn_search_all.UseVisualStyleBackColor = true;
            this.btn_search_all.Click += new System.EventHandler(this.btn_search_all_Click);
            // 
            // btn_makeAct
            // 
            this.btn_makeAct.Enabled = false;
            this.btn_makeAct.Location = new System.Drawing.Point(6, 262);
            this.btn_makeAct.Name = "btn_makeAct";
            this.btn_makeAct.Size = new System.Drawing.Size(160, 23);
            this.btn_makeAct.TabIndex = 8;
            this.btn_makeAct.Text = "СОЗДАТЬ";
            this.btn_makeAct.UseVisualStyleBackColor = true;
            this.btn_makeAct.Click += new System.EventHandler(this.btn_makeAct_Click);
            // 
            // frm_invoices
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(883, 321);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frm_invoices";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Счета";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_search_all;
        private System.Windows.Forms.Button btn_search_invoice;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox te_invoice;
        private System.Windows.Forms.DateTimePicker dtp_1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dtp_2;
        private System.Windows.Forms.Button btn_search_date;
        private System.Windows.Forms.Button btn_makeAct;
    }
}