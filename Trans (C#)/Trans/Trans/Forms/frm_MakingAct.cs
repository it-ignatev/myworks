﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;

using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;

using OfficeOpenXml;

namespace Trans.Forms
{
    public partial class frm_MakingAct : Form
    {
        Image bm = null, bm2 = null;
        string id1 = null;
        string sum1 = null, sum2 = null;
        decimal euro = 0;
        bool rubles = false;
        string[] prices = null;
        ExcelWorksheet from1, from2;
        string MName1, MName2;

        public frm_MakingAct(string name, string id, string sum)
        {
            InitializeComponent();

            id1 = id;
            sum1 = sum;

            rubles = true;

            prices = Sql.getInvoicePrices(id1).Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            
            Upd();   
        }

        public frm_MakingAct(string name, string id, string name2, string sum, string sum3)
        {
            InitializeComponent();

            id1 = id;
            sum1 = sum;

            sum2 = sum3;
            MName1 = name;
            MName2 = name2;
            
            //bm = Image.FromFile(Application.StartupPath + "\\TEMP\\" + name);
           // bm2 = Image.FromFile(Application.StartupPath + "\\TEMP\\" + name2);

            //pictureBox1.Image = new Bitmap(pictureBox1.Width, 350 + bm.Height);
            //pictureBox2.Image = new Bitmap(pictureBox2.Width, 350 + bm2.Height);

            //pictureBox1.Height = 350 + bm.Height;
            //pictureBox2.Height = 350 + bm2.Height;

            //pictureBox1_Paint(null, null);
            //pictureBox2_Paint(null, null);

            Upd();
        }

        private void Upd()
        {
            euro = GetCurVal(dateTimePicker1.Value);
            label1.Text = "Курс ЦБ(ЕВРО): " + euro.ToString();            
        }

        private decimal GetCurVal(DateTime dt)
        {

            var document = XDocument.Load("http://www.cbr.ru/scripts/XML_daily.asp?date_req=" + dt.ToString("dd/MM/yyyy"));
            var element = document.Root.Elements().First(x => x.Attribute("ID").Value == "R01239");
            Console.WriteLine("ЕВРО - {0}", element.Element("Value").Value);

            return Convert.ToDecimal(element.Element("Value").Value);
        }
        /*
        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = Graphics.FromImage(pictureBox1.Image);

            g.Clear(Color.White);

            g.DrawString("Акт №" + dateTimePicker1.Value.ToString("yyMMdd") + "-" + id1 + " от " + dateTimePicker1.Value.ToLongDateString(), new Font("Arial", 17, FontStyle.Bold), Brushes.Black, 4, 20);

            //g.DrawImage(bm, new Rectangle(0,80,597,bm.Height));

            if (rubles)
            {
                g.FillRectangle(Brushes.White, new Rectangle(402, 224, 74, 18));
                g.FillRectangle(Brushes.White, new Rectangle(484, 224, 82, 18));

                g.DrawString("Цена, евро", new Font("Segoe UI", 10, FontStyle.Bold), Brushes.Black, 402, 223);
                g.DrawString("Сумма, евро", new Font("Segoe UI", 10, FontStyle.Bold), Brushes.Black, 484, 223);

                decimal summary = 0;

                for (int i = 0; i < prices.Length; i++)
                {
                    g.FillRectangle(Brushes.White, new Rectangle(405, 260 + (i * 54), 60, 28));
                    g.FillRectangle(Brushes.White, new Rectangle(488, 260 + (i * 54), 60, 28));

                    g.DrawString(Math.Round(Convert.ToDecimal(prices[i]) * euro, 2).ToString(), new Font("Arial", 11, FontStyle.Regular), Brushes.Black, 410, 264 + (i * 54));
                    g.DrawString(Math.Round(Convert.ToDecimal(prices[i]) * euro, 2).ToString(), new Font("Arial", 11, FontStyle.Regular), Brushes.Black, 498, 264 + (i * 54));

                    summary = Math.Round(Convert.ToDecimal(prices[i]) + summary, 2);
                }

                g.FillRectangle(Brushes.White, new Rectangle(500, 80 + bm.Height - 36, 90, 20));
                g.DrawString(Math.Round(summary * euro, 2).ToString(), new Font("Segoe UI", 10, FontStyle.Bold), Brushes.Black, 520, 80 + bm.Height - 34);

                g.DrawString("Всего оказано услуг на сумму " + Math.Round(summary * euro, 2).ToString() + " рублей", new Font("Arial", 10, FontStyle.Regular), Brushes.Black, 14, bm.Height + 100);
                g.DrawString(Сумма.Пропись(Math.Round(summary * euro, 2), Валюта.Рубли, Заглавные.Первая), new Font("Arial", 10, FontStyle.Bold), Brushes.Black, 14, bm.Height + 120);
            }
            else
            {
                g.DrawString("Всего оказано услуг на сумму " + sum1 + " евро", new Font("Arial", 10, FontStyle.Regular), Brushes.Black, 14, bm.Height + 100);
                g.DrawString(Сумма.Пропись(Convert.ToDecimal(sum1), Валюта.Евро, Заглавные.Первая), new Font("Arial", 10, FontStyle.Bold), Brushes.Black, 14, bm.Height + 120);
            }

            g.DrawImage(Properties.Resources.Temple_3_1, new Rectangle(0, bm.Height + 140, 597, 140));
        }*/

        private void btn_saveAct_Click(object sender, EventArgs e)
        {           
            string name = "АКТ " + DateTime.Now.ToString("yyMMdd") + "-" + id1 + " от " + DateTime.Now.ToString("ddMM");
            string name2 = "АКТ " + DateTime.Now.ToString("yyMMdd") + "-" + id1 + "У от " + DateTime.Now.ToString("ddMM");

            if (!Directory.Exists(Application.StartupPath + "\\TEMP\\")) Directory.CreateDirectory(Application.StartupPath + "\\TEMP\\");
            if (!Directory.Exists(Application.StartupPath + "\\АКТЫ\\")) Directory.CreateDirectory(Application.StartupPath + "\\АКТЫ\\");

            //pictureBox1.Image.Save(Application.StartupPath + "\\TEMP\\" + name + ".jpg");

            if (sum2 != null) SavePic2(name2);

            try
            {
                string source = Application.StartupPath + "\\TEMP\\" + name + ".jpg";
                string destinaton = Application.StartupPath + "\\АКТЫ\\" + name;

                var package1 = new ExcelPackage(new FileInfo(Application.StartupPath + "\\СЧЕТА\\" + MName1));
                from1 = package1.Workbook.Worksheets[1];

                var package2 = new ExcelPackage(new FileInfo("sample2.xlsx"));
                FileStream fs;

                ExcelWorksheet workSheet = package2.Workbook.Worksheets[1];
                workSheet.Cells["H2"].Value = dateTimePicker1.Value.ToString("yyMMdd") + "-" + id1;
                workSheet.Cells["K2"].Value = dateTimePicker1.Value.ToLongDateString();

                List<Dictionary<string, string>> ans = Sql.getInvoiceCount(id1.ToString());
                string count = ans[0]["Count"];

                workSheet.InsertRow(5, 12 + Convert.ToInt32(count));
                
                from1.Cells["B10:K" + (21 + Convert.ToInt32(count)).ToString()].Copy(workSheet.Cells["B5:K" + (12 + Convert.ToInt32(count)).ToString()]);

                fs = new FileStream(destinaton + ".xlsx", FileMode.Create);
                package2.SaveAs(fs);
                package2.Dispose();

                //PdfDocument doc = new PdfDocument();
                //doc.Pages.Add(new PdfPage());
                //XGraphics xgr = XGraphics.FromPdfPage(doc.Pages[0]);
                //XImage img = XImage.FromFile(source);

                //xgr.DrawImage(img, 80, 0);
                //doc.Save(destinaton);
                //doc.Close();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                this.Dispose();
            }

        }

        private void SavePic2(string name)
        {
            if (!Directory.Exists(Application.StartupPath + "\\TEMP\\")) Directory.CreateDirectory(Application.StartupPath + "\\TEMP\\");
            if (!Directory.Exists(Application.StartupPath + "\\АКТЫ\\")) Directory.CreateDirectory(Application.StartupPath + "\\АКТЫ\\");

            try
            {
                string destinaton = Application.StartupPath + "\\АКТЫ\\" + name;

                //PdfDocument doc = new PdfDocument();
                //doc.Pages.Add(new PdfPage());
                //XGraphics xgr = XGraphics.FromPdfPage(doc.Pages[0]);
                //XImage img = XImage.FromFile(source);

                //xgr.DrawImage(img, 80, 0);
                //doc.Save(destinaton);
                //doc.Close();

                var package1 = new ExcelPackage(new FileInfo(Application.StartupPath + "\\СЧЕТА\\" + MName2));
                from1 = package1.Workbook.Worksheets[1];

                var package2 = new ExcelPackage(new FileInfo("sample2.xlsx"));
                FileStream fs;

                ExcelWorksheet workSheet = package2.Workbook.Worksheets[1];
                workSheet.Cells["H2"].Value = dateTimePicker1.Value.ToString("yyMMdd") + "-" + id1 + "-У";
                workSheet.Cells["K2"].Value = dateTimePicker1.Value.ToLongDateString();

                List<Dictionary<string, string>> ans = Sql.getInvoiceCount(id1.ToString());
                string count = ans[0]["Count"];

                workSheet.InsertRow(5, 12 + Convert.ToInt32(count));

                from1.Cells["B10:K" + (21 + Convert.ToInt32(count)).ToString()].Copy(workSheet.Cells["B5:K" + (12 + Convert.ToInt32(count)).ToString()]);

                fs = new FileStream(destinaton + ".xlsx", FileMode.Create);
                package2.SaveAs(fs);
                package2.Dispose();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void frm_MakingAct_Load(object sender, EventArgs e)
        {

        }
        /*
        private void pictureBox2_Paint(object sender, PaintEventArgs e)
        {
            if (sum2 == null) return;

            Graphics g = Graphics.FromImage(pictureBox2.Image);

            g.Clear(Color.White);

            g.DrawString("Акт №" + DateTime.Now.ToString("yyMMdd") + "-" + id1 + "У от " + DateTime.Now.ToLongDateString(), new Font("Arial", 17, FontStyle.Bold), Brushes.Black, 4, 20);

            //g.DrawImage(bm2, new Rectangle(0, 80, 597, bm2.Height));

            g.DrawString("Всего оказано услуг на сумму " + sum2 + " рублей", new Font("Arial", 10, FontStyle.Regular), Brushes.Black, 14, bm2.Height + 80);
            g.DrawString(Сумма.Пропись(Convert.ToDecimal(sum2), Валюта.Рубли, Заглавные.Первая), new Font("Arial", 10, FontStyle.Bold), Brushes.Black, 14, bm2.Height + 100);

            g.DrawImage(Properties.Resources.Temple_3_1, new Rectangle(0, bm2.Height + 140, 597, 140));
        
        }*/

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            Upd();

            //pictureBox1.Invalidate();
            //pictureBox1.Refresh();
        }
    }
}
