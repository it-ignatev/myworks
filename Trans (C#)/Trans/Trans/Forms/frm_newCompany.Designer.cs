﻿namespace Trans
{
    partial class frm_newCompany
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.te_name = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.te_adr = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.te_iban = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.te_bank = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.te_swift = new System.Windows.Forms.TextBox();
            this.btn_SaveAndAdd = new System.Windows.Forms.Button();
            this.btn_SaveAndExit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Название компании:";
            // 
            // te_name
            // 
            this.te_name.Location = new System.Drawing.Point(12, 29);
            this.te_name.Name = "te_name";
            this.te_name.Size = new System.Drawing.Size(221, 25);
            this.te_name.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Адрес:";
            // 
            // te_adr
            // 
            this.te_adr.Location = new System.Drawing.Point(12, 77);
            this.te_adr.Name = "te_adr";
            this.te_adr.Size = new System.Drawing.Size(221, 25);
            this.te_adr.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Счет(IBAN):";
            // 
            // te_iban
            // 
            this.te_iban.Location = new System.Drawing.Point(12, 125);
            this.te_iban.Name = "te_iban";
            this.te_iban.Size = new System.Drawing.Size(221, 25);
            this.te_iban.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 153);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Банк:";
            // 
            // te_bank
            // 
            this.te_bank.Location = new System.Drawing.Point(12, 173);
            this.te_bank.Name = "te_bank";
            this.te_bank.Size = new System.Drawing.Size(221, 25);
            this.te_bank.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 201);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "SWIFT:";
            // 
            // te_swift
            // 
            this.te_swift.Location = new System.Drawing.Point(12, 221);
            this.te_swift.Name = "te_swift";
            this.te_swift.Size = new System.Drawing.Size(221, 25);
            this.te_swift.TabIndex = 9;
            // 
            // btn_SaveAndAdd
            // 
            this.btn_SaveAndAdd.Enabled = false;
            this.btn_SaveAndAdd.Location = new System.Drawing.Point(12, 252);
            this.btn_SaveAndAdd.Name = "btn_SaveAndAdd";
            this.btn_SaveAndAdd.Size = new System.Drawing.Size(221, 33);
            this.btn_SaveAndAdd.TabIndex = 10;
            this.btn_SaveAndAdd.Text = "Записать и добавить договор";
            this.btn_SaveAndAdd.UseVisualStyleBackColor = true;
            // 
            // btn_SaveAndExit
            // 
            this.btn_SaveAndExit.Location = new System.Drawing.Point(12, 291);
            this.btn_SaveAndExit.Name = "btn_SaveAndExit";
            this.btn_SaveAndExit.Size = new System.Drawing.Size(221, 33);
            this.btn_SaveAndExit.TabIndex = 11;
            this.btn_SaveAndExit.Text = "Добавить и закрыть";
            this.btn_SaveAndExit.UseVisualStyleBackColor = true;
            this.btn_SaveAndExit.Click += new System.EventHandler(this.btn_SaveAndExit_Click);
            // 
            // frm_newCompany
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(245, 332);
            this.Controls.Add(this.btn_SaveAndExit);
            this.Controls.Add(this.btn_SaveAndAdd);
            this.Controls.Add(this.te_swift);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.te_bank);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.te_iban);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.te_adr);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.te_name);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frm_newCompany";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Новая компания";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox te_name;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox te_adr;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox te_iban;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox te_bank;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox te_swift;
        private System.Windows.Forms.Button btn_SaveAndAdd;
        private System.Windows.Forms.Button btn_SaveAndExit;
    }
}