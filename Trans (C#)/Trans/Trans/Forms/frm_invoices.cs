﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Trans.Forms
{
    public partial class frm_invoices : Form
    {
        private Dictionary<string, string> SqlQuery = new Dictionary<string, string>();
        private List<object> MClick = new List<object>();

        public frm_invoices()
        {
            InitializeComponent();

            LoadAll();
        }

        private void LoadAll()
        {
            SqlQuery.Clear();

            string[] res = Sql.LoadInvoices(SqlQuery);

            LoadInvoices(res);
        }

        private void LoadInvoices(string[] res)
        {
            if (res == null || res[0] == null) { panel1.Controls.Clear(); return; }

            string[] result1 = res[0].Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            string[] result2 = res[1].Split(":".ToCharArray());
            string[] result3 = res[2].Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            string[] result4 = res[3].Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            string[] result5 = res[4].Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            string[] result6 = res[5].Split("!".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            panel1.Controls.Clear();

            int add = 0;
            for (int i = 0; i < result1.Length; i++)
            {
                panel1.Controls.Add(CreatePanel(add, 0, 640, 31, "p_" + i.ToString(), null));
                ((Panel)panel1.Controls["p_" + i.ToString()]).Controls.Add(CreateTextBox(2, 8, 26, "te_id_" + i.ToString(), result1[i], false));
                ((Panel)panel1.Controls["p_" + i.ToString()]).Controls.Add(CreateTextBox(2, 34, 120, "te_invoice_" + i.ToString(), result3[i], false));
                ((Panel)panel1.Controls["p_" + i.ToString()]).Controls.Add(CreateTextBox(2, 154, 100, "te_price1_" + i.ToString(), result4[i], false));
                ((Panel)panel1.Controls["p_" + i.ToString()]).Controls.Add(CreateTextBox(2, 254, 100, "te_price2_" + i.ToString(), result5[i], false));
                ((Panel)panel1.Controls["p_" + i.ToString()]).Controls.Add(CreateTextBox(2, 354, 200, "te_client_" + i.ToString(), result2[i], false));
                ((Panel)panel1.Controls["p_" + i.ToString()]).Controls.Add(CreateTextBox(2, 554, 68, "te_date_" + i.ToString(), result6[i], false));

                add += 34;
            }
        }

        private Panel CreatePanel(int t, int l, int w, int h, string n, EventHandler e)
        {
            Panel p = new Panel();
            p.Top = t;
            p.Left = l;
            p.Width = w;
            p.Height = h;
            p.Name = n;
            p.BorderStyle = BorderStyle.FixedSingle;
            if (e != null) p.Click += e;
            p.MouseEnter += MouseHover;
            p.MouseLeave += MouseOver;
            p.MouseClick += MouseClick;

            return p;
        }

        private TextBox CreateTextBox(int t, int l, int w, string n, string txt, bool e)
        {
            TextBox te = new TextBox();
            te.Top = t;
            te.Left = l;
            te.Width = w;
            te.Name = n;
            te.Text = txt;
            te.Enabled = e;

            return te;
        }

        private void MouseHover(object s, EventArgs e)
        {
            ((Panel)s).BackColor = Color.YellowGreen;
        }

        private void MouseOver(object s, EventArgs e)
        {
            if (MClick.Contains(s)) return;

            ((Panel)s).BackColor = Color.FromArgb(0, 0, 0, 0);
        }

        private void MouseClick(object s, MouseEventArgs e)
        {
            if (MClick.Contains((Panel)s))
            {
                ((Panel)s).BackColor = Color.FromArgb(0, 0, 0, 0);
                MClick.Remove((Panel)s);
                btn_makeAct.Enabled = false;
                return;
            }

            if (MClick.Count > 0)
            {
                for (int i = 0; i < MClick.Count; i++)
                {
                    ((Panel)MClick[i]).BackColor = Color.FromArgb(0, 0, 0, 0);
                }

                MClick.Clear();
            }

            ((Panel)s).BackColor = Color.YellowGreen;

            MClick.Add(s);

            string[] r = ((Panel)MClick[0]).Name.Split("_".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            btn_makeAct.Enabled = true;
        }

        private void ReInstall(object s, EventArgs e)
        {
            btn_makeAct.Enabled = false;
            MClick.Clear();
        }

        private void btn_search_all_Click(object sender, EventArgs e)
        {
            btn_search_all.Enabled = false;

            LoadAll();
        }

        private void btn_search_invoice_Click(object sender, EventArgs e)
        {
            if (te_invoice.Text == string.Empty) return;

            if (SqlQuery.ContainsKey("SELECT_INVOICE")) SqlQuery.Remove("SELECT_INVOICE");

            SqlQuery.Add("SELECT_INVOICE", te_invoice.Text);
            string[] res = Sql.LoadInvoices(SqlQuery);
            LoadInvoices(res);

            btn_search_all.Enabled = true;
            MClick.Clear();
        }

        private void btn_search_date_Click(object sender, EventArgs e)
        {
            SqlQuery.Remove("SELECT_DTP_1");
            SqlQuery.Remove("SELECT_DTP_2");
            SqlQuery.Add("SELECT_DTP_1", dtp_1.Value.ToString("yyyy-MM-dd HH:mm:ss"));
            SqlQuery.Add("SELECT_DTP_2", dtp_2.Value.ToString("yyyy-MM-dd HH:mm:ss"));
            string[] res = Sql.LoadInvoices(SqlQuery);
            LoadInvoices(res);

            btn_search_all.Enabled = true;
            MClick.Clear();
        }

        private void btn_makeAct_Click(object sender, EventArgs e)
        {
            string[] r = ((Panel)MClick[0]).Name.Split("_".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            
            if (((TextBox)((Panel)MClick[0]).Controls["te_price2_" + r[1]]).Text != "0")
            {

                Form frm_make = new Forms.frm_MakingAct("INVOICE_" + ((TextBox)((Panel)MClick[0]).Controls["te_invoice_" + r[1]]).Text + "_" + Convert.ToDateTime(((TextBox)((Panel)MClick[0]).Controls["te_date_" + r[1]]).Text).ToString("dd_MM_yy_HH_mm") + ".xlsx", ((TextBox)((Panel)MClick[0]).Controls["te_id_" + r[1]]).Text, "INVOICE_" + ((TextBox)((Panel)MClick[0]).Controls["te_invoice_" + r[1]]).Text + "_" + Convert.ToDateTime(((TextBox)((Panel)MClick[0]).Controls["te_date_" + r[1]]).Text).ToString("dd_MM_yy_HH_mm") + "_Y.xlsx", ((TextBox)((Panel)MClick[0]).Controls["te_price1_" + r[1]]).Text, ((TextBox)((Panel)MClick[0]).Controls["te_price2_" + r[1]]).Text);

                frm_make.FormClosing += ReInstall;
                frm_make.ShowDialog();
            }
            else
            {
                Form frm_make = new Forms.frm_MakingAct("INVOICE_" + ((TextBox)((Panel)MClick[0]).Controls["te_invoice_" + r[1]]).Text + "_" + Convert.ToDateTime(((TextBox)((Panel)MClick[0]).Controls["te_date_" + r[1]]).Text).ToString("dd_MM_yy_HH_mm") + ".xlsx", ((TextBox)((Panel)MClick[0]).Controls["te_id_" + r[1]]).Text, ((TextBox)((Panel)MClick[0]).Controls["te_price1_" + r[1]]).Text);

                frm_make.FormClosing += ReInstall;
                frm_make.ShowDialog();
            }        
        }

        private bool CheckInvoice(string inv, string date, bool e)
        {
            string path = null;

            if (!Directory.Exists(Application.StartupPath + "\\TEMP\\")) Directory.CreateDirectory(Application.StartupPath + "\\TEMP\\");

            if (e) { path = Application.StartupPath + "\\TEMP\\TEMP_INVOICE_" + inv + "_" + Convert.ToDateTime(date).ToString("dd_MM_yy_HH_mm") + ".jpg"; } else { path = Application.StartupPath + "\\TEMP\\TEMP_INVOICE_Y_" + inv + "_" + Convert.ToDateTime(date).ToString("dd_MM_yy_HH_mm") + ".jpg"; }
            
            if (File.Exists(path))
            {
                return true;
            }

            return false;
        }
    }
}
