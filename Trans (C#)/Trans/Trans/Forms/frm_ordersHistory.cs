﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Trans.Forms
{
    public partial class frm_ordersHistory : Form
    {
        private Dictionary<string, string> SqlQuery = new Dictionary<string, string>();
        public static Form self { get; set; }

        public frm_ordersHistory()
        {
            InitializeComponent();

            LoadAll();
            LoadRoutes();
            LoadCars();

            self = this;

        }
        
        private void LoadRoutes()
        {
            string res = Sql.LoadRoutes();
            string[] result = res.Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            cb_routes.Items.AddRange(result);
        }

        private void LoadCars()
        {
            string res = Sql.LoadCars();
            string[] result = res.Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            
            cb_cars.Items.AddRange(result);
        }

        private void LoadOrders(List<Dictionary<string, string>> res)
        {
            mainPanel.Rows.Clear();

            if (res == null || res[0] == null) { return; }       

            int add = 0;
            for (int i = 0; i < res.Count; i++)
            {
                mainPanel.Rows.Add(
                    res[i]["id"],
                    res[i]["dateTime"],
                    res[i]["route_1"],
                    res[i]["car_1"],
                    res[i]["car_2"],
                    res[i]["loaded"],
                    res[i]["name"],
                    res[i]["price"],
                    res[i]["price"],
                    res[i]["name_2"],
                    (res[i]["invoice"] == string.Empty) ? "INVOICE НЕ ПОЛУЧЕН" : res[i]["invoice"],
                    (res[i]["invoice_client"] == string.Empty) ? "СЧЕТ НЕ ВЫСТАВЛЕН" : res[i]["invoice_client"]
                    );

                //((Panel)panel1.Controls["p_" + i.ToString()]).Controls.Add(CreateTextBox(2, 8, 26, "te_id_" + i.ToString(), res[i]["id"], false));

                //((Panel)panel1.Controls["p_" + i.ToString()]).Controls.Add(CreateTextBox(2, 34, 120, "te_date_" + i.ToString(), res[i]["dateTime"], false));
                //((Panel)panel1.Controls["p_" + i.ToString()]).Controls.Add(CreateTextBox(2, 154, 100, "te_route_" + i.ToString(), res[i]["route_1"], false));
                //((Panel)panel1.Controls["p_" + i.ToString()]).Controls.Add(CreateTextBox(2, 254, 68, "te_car1_" + i.ToString(), res[i]["car_1"], false));
                //((Panel)panel1.Controls["p_" + i.ToString()]).Controls.Add(CreateTextBox(2, 322, 68, "te_car2_" + i.ToString(), res[i]["car_2"], false));
                //((Panel)panel1.Controls["p_" + i.ToString()]).Controls.Add(CreateTextBox(2, 390, 30, "te_load_" + i.ToString(), (res[i]["loaded"] == "True") ? "Да" : "Нет", false));
                //((Panel)panel1.Controls["p_" + i.ToString()]).Controls.Add(CreateTextBox(2, 420, 140, "te_client_" + i.ToString(), res[i]["name"], false));
                //((Panel)panel1.Controls["p_" + i.ToString()]).Controls.Add(CreateTextBox(2, 560, 50, "te_price_" + i.ToString(), res[i]["price"], false));
                //((Panel)panel1.Controls["p_" + i.ToString()]).Controls.Add(CreateTextBox(2, 610, 140, "te_company_" + i.ToString(), res[i]["name_2"], false));
                //((Panel)panel1.Controls["p_" + i.ToString()]).Controls.Add(CreateTextBox(2, 750, 140, "te_invoice_" + i.ToString(), (res[i]["invoice"] == string.Empty) ? "INVOICE НЕ ПОЛУЧЕН" : res[i]["invoice"], false));
                //((Panel)panel1.Controls["p_" + i.ToString()]).Controls.Add(CreateTextBox(2, 890, 140, "te_invoice_client_" + i.ToString(), (res[i]["invoice_client"] == string.Empty) ? "СЧЕТ НЕ ВЫСТАВЛЕН" : res[i]["invoice_client"], false));

                add += 34;
            }
        }
        public void LoadAll()
        {
            SqlQuery.Clear();

             List<Dictionary<string, string>> res = Sql.LoadOrders(SqlQuery);

            LoadOrders(res);
        }
        private void btn_filter_invoice_Click(object sender, EventArgs e)
        {
            btn_filter_all.Enabled = true;
            btn_filter_invoice.Click -= btn_filter_invoice_Click;
            btn_filter_invoice.Click += btn_filter_invoice_NotGot;
            btn_filter_invoice.Text = "INVOICE НЕ ПОЛУЧЕН";

            SqlQuery.Remove("SELECT_INVOICE");
            if (!SqlQuery.ContainsKey("SELECT_INVOICE_GOT")) SqlQuery.Add("SELECT_INVOICE_GOT", "");

            LoadOrders(Sql.LoadOrders(SqlQuery));

            btn_installInvoice.Enabled = false;
            btn_setInvoice.Enabled = false;
            btn_editOrder.Enabled = false;
        }
        private void btn_filter_invoice_NotGot(object s, EventArgs e)
        {
            btn_filter_all.Enabled = true;
            btn_filter_invoice.Click -= btn_filter_invoice_NotGot;
            btn_filter_invoice.Click += btn_filter_invoice_Click;
            btn_filter_invoice.Text = "INVOICE ПОЛУЧЕН";

            SqlQuery.Remove("SELECT_INVOICE_GOT");
            if (!SqlQuery.ContainsKey("SELECT_INVOICE")) SqlQuery.Add("SELECT_INVOICE", "");
            LoadOrders(Sql.LoadOrders(SqlQuery));

            btn_installInvoice.Enabled = false;
            btn_setInvoice.Enabled = false;
            btn_editOrder.Enabled = false;
        }
        private void btn_filter_all_Click(object sender, EventArgs e)
        {
            ((Button)sender).Enabled = false;

            LoadAll();

            btn_installInvoice.Enabled = false;
            btn_setInvoice.Enabled = false;
            btn_editOrder.Enabled = false;
        }
        private void btn_filter_invoiceClient_Click(object sender, EventArgs e)
        {
            btn_filter_all.Enabled = true;
            btn_filter_invoiceClient.Click -= btn_filter_invoiceClient_Click;
            btn_filter_invoiceClient.Click += btn_filter_invoiceClient_NotSet;
            btn_filter_invoiceClient.Text = "СЧЕТ НЕ ВЫСТАВЛЕН";

            SqlQuery.Remove("SELECT_INVOICE_CLIENT");
            if (!SqlQuery.ContainsKey("SELECT_INVOICE_CLIENT_SET")) SqlQuery.Add("SELECT_INVOICE_CLIENT_SET", "");

            LoadOrders(Sql.LoadOrders(SqlQuery));

            btn_installInvoice.Enabled = false;
            btn_setInvoice.Enabled = false;
            btn_editOrder.Enabled = false;
        }
        private void btn_filter_invoiceClient_NotSet(object s, EventArgs e)
        {
            btn_filter_all.Enabled = true;
            btn_filter_invoiceClient.Click -= btn_filter_invoiceClient_NotSet;
            btn_filter_invoiceClient.Click += btn_filter_invoiceClient_Click;
            btn_filter_invoiceClient.Text = "СЧЕТ ВЫСТАВЛЕН";

            SqlQuery.Remove("SELECT_INVOICE_CLIENT_SET");
            if (!SqlQuery.ContainsKey("SELECT_INVOICE_CLIENT")) SqlQuery.Add("SELECT_INVOICE_CLIENT", "");
       
            LoadOrders(Sql.LoadOrders(SqlQuery));

            btn_installInvoice.Enabled = false;
            btn_setInvoice.Enabled = false;
            btn_editOrder.Enabled = false;
        }
        private void btn_search_route_Click(object sender, EventArgs e)
        {
            if (cb_routes.SelectedItem == null) { SqlQuery.Remove("SELECT_ROUTE"); return; }
            if (cb_routes.SelectedIndex == -1) { SqlQuery.Remove("SELECT_ROUTE"); return; }

            SqlQuery.Remove("SELECT_ROUTE");
            SqlQuery.Add("SELECT_ROUTE", cb_routes.Text);
            LoadOrders(Sql.LoadOrders(SqlQuery));

            btn_filter_all.Enabled = true;
            btn_installInvoice.Enabled = false;
            btn_setInvoice.Enabled = false;
            btn_editOrder.Enabled = false;
        }
        private void btn_installInvoice_Click(object sender, EventArgs e)
        {
            if (mainPanel.SelectedRows.Count != 1) return;

            string id = null, route = null, time = null, pr = null;

            id = mainPanel.SelectedRows[0].Cells["id"].Value.ToString();
            route = mainPanel.SelectedRows[0].Cells["route"].Value.ToString();
            time = mainPanel.SelectedRows[0].Cells["time"].Value.ToString();
            pr = mainPanel.SelectedRows[0].Cells["priceCompany"].Value.ToString();

            Form frm_setinvoice = new Forms.Orders.frm_setInvoice(id,route,time, pr);
            frm_setinvoice.FormClosing += Upd;
            frm_setinvoice.ShowDialog();           
        }
        private void Upd(object s, EventArgs e)
        {
            LoadAll();

            btn_installInvoice.Enabled = false;
            btn_setInvoice.Enabled = false;
            btn_editOrder.Enabled = false;

            mainPanel.ClearSelection();
        }
        private void btn_setInvoice_Click(object sender, EventArgs e)
        {
            if (mainPanel.SelectedRows.Count != 1) return;

            string id = null, route = null, time = null, client = null, car1 = null, car2 = null, price = null;

            id = mainPanel.SelectedRows[0].Cells["id"].Value.ToString();
            route = mainPanel.SelectedRows[0].Cells["route"].Value.ToString();
            time = mainPanel.SelectedRows[0].Cells["time"].Value.ToString();
            client = mainPanel.SelectedRows[0].Cells["client"].Value.ToString();
            car1 = mainPanel.SelectedRows[0].Cells["car1"].Value.ToString();
            car2 = mainPanel.SelectedRows[0].Cells["car2"].Value.ToString();
            price = mainPanel.SelectedRows[0].Cells["priceCompany"].Value.ToString();

            Form frm_setinvoice = new Forms.Orders.frm_setClient_Invoice(id, route, time, client, car1, car2, price);
            frm_setinvoice.FormClosing += Upd;
            frm_setinvoice.ShowDialog();          
        }
        private void btn_search_car_Click(object sender, EventArgs e)
        {
            if (cb_cars.SelectedItem == null) { SqlQuery.Remove("SELECT_CAR"); return; }
            if (cb_cars.SelectedIndex == -1) { SqlQuery.Remove("SELECT_CAR"); return; }

            SqlQuery.Remove("SELECT_CAR");
            SqlQuery.Add("SELECT_CAR", cb_cars.Text);
            LoadOrders(Sql.LoadOrders(SqlQuery));

            btn_filter_all.Enabled = true;
            btn_installInvoice.Enabled = false;
            btn_setInvoice.Enabled = false;
            btn_editOrder.Enabled = false;
        }
        private void btn_search_period_Click(object sender, EventArgs e)
        {
            SqlQuery.Remove("SELECT_DTP_1");
            SqlQuery.Remove("SELECT_DTP_2");
            SqlQuery.Add("SELECT_DTP_1", dtp_1.Value.ToString("yyyy-MM-dd HH:mm:ss"));
            SqlQuery.Add("SELECT_DTP_2", dtp_2.Value.ToString("yyyy-MM-dd HH:mm:ss"));

            LoadOrders(Sql.LoadOrders(SqlQuery));

            btn_filter_all.Enabled = true;
            btn_installInvoice.Enabled = false;
            btn_setInvoice.Enabled = false;
            btn_editOrder.Enabled = false;
        }
        private void btn_setMultiInvoice_Click(object sender, EventArgs e)
        {
            string[] ids = new string[mainPanel.SelectedRows.Count];
            string[] routes = new string[mainPanel.SelectedRows.Count];
            string[] times = new string[mainPanel.SelectedRows.Count];
            string[] car1 = new string[mainPanel.SelectedRows.Count];
            string[] car2 = new string[mainPanel.SelectedRows.Count];
            string[] price = new string[mainPanel.SelectedRows.Count];
            string client = null;

            for (int i = 0; i < mainPanel.SelectedRows.Count; i++)
            {
                ids[i] = mainPanel.SelectedRows[i].Cells["id"].Value.ToString();
                routes[i] = mainPanel.SelectedRows[i].Cells["route"].Value.ToString();
                times[i] = mainPanel.SelectedRows[i].Cells["time"].Value.ToString();
                car1[i] = mainPanel.SelectedRows[i].Cells["car1"].Value.ToString();
                car2[i] = mainPanel.SelectedRows[i].Cells["car2"].Value.ToString();
                price[i] = mainPanel.SelectedRows[i].Cells["priceCompany"].Value.ToString();

                if (i == mainPanel.SelectedRows.Count - 1) client = mainPanel.SelectedRows[i].Cells["client"].Value.ToString();
            }

            Form frm_sci = new Forms.Orders.frm_setClient_Invoice(ids, routes, times, client, car1, car2, price);
            frm_sci.FormClosing += Upd;
            frm_sci.ShowDialog();
        }
        private void btn_editOrder_Click(object sender, EventArgs e)
        {
            if (mainPanel.SelectedRows.Count != 1) return;

            Form frm_editOrder = new Forms.Orders.frm_editOrder(mainPanel.SelectedRows[0].Cells["id"].Value.ToString());
            frm_editOrder.FormClosing += Upd;
            frm_editOrder.ShowDialog();
        }
        private void mainPanel_SelectionChanged(object sender, EventArgs e)
        {
            if(mainPanel.SelectedRows.Count <= 0)
            {
                return;
            }

            btn_installInvoice.Enabled = false;
            btn_setInvoice.Enabled = false;
            btn_editOrder.Enabled = false;
            btn_setMultiInvoice.Enabled = false;

            if(mainPanel.SelectedRows.Count > 1)
            {
                btn_setMultiInvoice.Enabled = true;
                return;
            }

            if (mainPanel.SelectedRows[0].Cells["invoice"].Value.ToString() == "INVOICE НЕ ПОЛУЧЕН")
            {
                btn_installInvoice.Enabled = true;
            }
            else
            {
                if (mainPanel.SelectedRows[0].Cells["invoice_client"].Value.ToString() == "СЧЕТ НЕ ВЫСТАВЛЕН") btn_setInvoice.Enabled = true;
            }

            btn_editOrder.Enabled = true;
        }
    }
}
