﻿namespace Trans.Forms
{
    partial class frm_ordersHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_search_period = new System.Windows.Forms.Button();
            this.dtp_2 = new System.Windows.Forms.DateTimePicker();
            this.dtp_1 = new System.Windows.Forms.DateTimePicker();
            this.btn_setMultiInvoice = new System.Windows.Forms.Button();
            this.btn_search_car = new System.Windows.Forms.Button();
            this.cb_cars = new System.Windows.Forms.ComboBox();
            this.btn_search_route = new System.Windows.Forms.Button();
            this.cb_routes = new System.Windows.Forms.ComboBox();
            this.btn_filter_invoiceClient = new System.Windows.Forms.Button();
            this.btn_filter_all = new System.Windows.Forms.Button();
            this.btn_editOrder = new System.Windows.Forms.Button();
            this.btn_setInvoice = new System.Windows.Forms.Button();
            this.btn_installInvoice = new System.Windows.Forms.Button();
            this.btn_filter_invoice = new System.Windows.Forms.Button();
            this.mainPanel = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.route = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.car1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.car2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.loaded = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.client = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceCompany = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceClient = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.company = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.invoice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.invoice_client = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainPanel)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_search_period);
            this.groupBox1.Controls.Add(this.dtp_2);
            this.groupBox1.Controls.Add(this.dtp_1);
            this.groupBox1.Controls.Add(this.btn_setMultiInvoice);
            this.groupBox1.Controls.Add(this.btn_search_car);
            this.groupBox1.Controls.Add(this.cb_cars);
            this.groupBox1.Controls.Add(this.btn_search_route);
            this.groupBox1.Controls.Add(this.cb_routes);
            this.groupBox1.Controls.Add(this.btn_filter_invoiceClient);
            this.groupBox1.Controls.Add(this.btn_filter_all);
            this.groupBox1.Controls.Add(this.btn_editOrder);
            this.groupBox1.Controls.Add(this.btn_setInvoice);
            this.groupBox1.Controls.Add(this.btn_installInvoice);
            this.groupBox1.Controls.Add(this.btn_filter_invoice);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(264, 365);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Управление";
            // 
            // btn_search_period
            // 
            this.btn_search_period.Location = new System.Drawing.Point(182, 196);
            this.btn_search_period.Name = "btn_search_period";
            this.btn_search_period.Size = new System.Drawing.Size(76, 23);
            this.btn_search_period.TabIndex = 13;
            this.btn_search_period.Text = "Поиск";
            this.btn_search_period.UseVisualStyleBackColor = true;
            this.btn_search_period.Click += new System.EventHandler(this.btn_search_period_Click);
            // 
            // dtp_2
            // 
            this.dtp_2.CustomFormat = "HH:mm:ss dd:MM:yyyy";
            this.dtp_2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_2.Location = new System.Drawing.Point(6, 210);
            this.dtp_2.Name = "dtp_2";
            this.dtp_2.Size = new System.Drawing.Size(170, 25);
            this.dtp_2.TabIndex = 12;
            // 
            // dtp_1
            // 
            this.dtp_1.CustomFormat = "HH:mm:ss dd:MM:yyyy";
            this.dtp_1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_1.Location = new System.Drawing.Point(6, 179);
            this.dtp_1.Name = "dtp_1";
            this.dtp_1.Size = new System.Drawing.Size(170, 25);
            this.dtp_1.TabIndex = 11;
            // 
            // btn_setMultiInvoice
            // 
            this.btn_setMultiInvoice.Enabled = false;
            this.btn_setMultiInvoice.Location = new System.Drawing.Point(6, 330);
            this.btn_setMultiInvoice.Name = "btn_setMultiInvoice";
            this.btn_setMultiInvoice.Size = new System.Drawing.Size(252, 29);
            this.btn_setMultiInvoice.TabIndex = 10;
            this.btn_setMultiInvoice.Text = "Мульти-Выписка";
            this.btn_setMultiInvoice.UseVisualStyleBackColor = true;
            this.btn_setMultiInvoice.Click += new System.EventHandler(this.btn_setMultiInvoice_Click);
            // 
            // btn_search_car
            // 
            this.btn_search_car.Location = new System.Drawing.Point(182, 148);
            this.btn_search_car.Name = "btn_search_car";
            this.btn_search_car.Size = new System.Drawing.Size(76, 25);
            this.btn_search_car.TabIndex = 9;
            this.btn_search_car.Text = "Поиск";
            this.btn_search_car.UseVisualStyleBackColor = true;
            this.btn_search_car.Click += new System.EventHandler(this.btn_search_car_Click);
            // 
            // cb_cars
            // 
            this.cb_cars.FormattingEnabled = true;
            this.cb_cars.Location = new System.Drawing.Point(6, 148);
            this.cb_cars.Name = "cb_cars";
            this.cb_cars.Size = new System.Drawing.Size(170, 25);
            this.cb_cars.TabIndex = 8;
            // 
            // btn_search_route
            // 
            this.btn_search_route.Location = new System.Drawing.Point(182, 117);
            this.btn_search_route.Name = "btn_search_route";
            this.btn_search_route.Size = new System.Drawing.Size(76, 25);
            this.btn_search_route.TabIndex = 7;
            this.btn_search_route.Text = "Поиск";
            this.btn_search_route.UseVisualStyleBackColor = true;
            this.btn_search_route.Click += new System.EventHandler(this.btn_search_route_Click);
            // 
            // cb_routes
            // 
            this.cb_routes.FormattingEnabled = true;
            this.cb_routes.Location = new System.Drawing.Point(6, 117);
            this.cb_routes.Name = "cb_routes";
            this.cb_routes.Size = new System.Drawing.Size(170, 25);
            this.cb_routes.TabIndex = 6;
            // 
            // btn_filter_invoiceClient
            // 
            this.btn_filter_invoiceClient.Location = new System.Drawing.Point(6, 86);
            this.btn_filter_invoiceClient.Name = "btn_filter_invoiceClient";
            this.btn_filter_invoiceClient.Size = new System.Drawing.Size(252, 25);
            this.btn_filter_invoiceClient.TabIndex = 5;
            this.btn_filter_invoiceClient.Text = "Счет выставлен\\не выставлен";
            this.btn_filter_invoiceClient.UseVisualStyleBackColor = true;
            this.btn_filter_invoiceClient.Click += new System.EventHandler(this.btn_filter_invoiceClient_Click);
            // 
            // btn_filter_all
            // 
            this.btn_filter_all.Enabled = false;
            this.btn_filter_all.Location = new System.Drawing.Point(6, 24);
            this.btn_filter_all.Name = "btn_filter_all";
            this.btn_filter_all.Size = new System.Drawing.Size(252, 25);
            this.btn_filter_all.TabIndex = 4;
            this.btn_filter_all.Text = "Все";
            this.btn_filter_all.UseVisualStyleBackColor = true;
            this.btn_filter_all.Click += new System.EventHandler(this.btn_filter_all_Click);
            // 
            // btn_editOrder
            // 
            this.btn_editOrder.Enabled = false;
            this.btn_editOrder.Location = new System.Drawing.Point(6, 259);
            this.btn_editOrder.Name = "btn_editOrder";
            this.btn_editOrder.Size = new System.Drawing.Size(252, 30);
            this.btn_editOrder.TabIndex = 3;
            this.btn_editOrder.Text = "Редактировать заказ";
            this.btn_editOrder.UseVisualStyleBackColor = true;
            this.btn_editOrder.Click += new System.EventHandler(this.btn_editOrder_Click);
            // 
            // btn_setInvoice
            // 
            this.btn_setInvoice.Enabled = false;
            this.btn_setInvoice.Location = new System.Drawing.Point(131, 295);
            this.btn_setInvoice.Name = "btn_setInvoice";
            this.btn_setInvoice.Size = new System.Drawing.Size(127, 29);
            this.btn_setInvoice.TabIndex = 2;
            this.btn_setInvoice.Text = "Выписать счет";
            this.btn_setInvoice.UseVisualStyleBackColor = true;
            this.btn_setInvoice.Click += new System.EventHandler(this.btn_setInvoice_Click);
            // 
            // btn_installInvoice
            // 
            this.btn_installInvoice.Enabled = false;
            this.btn_installInvoice.Location = new System.Drawing.Point(6, 295);
            this.btn_installInvoice.Name = "btn_installInvoice";
            this.btn_installInvoice.Size = new System.Drawing.Size(119, 29);
            this.btn_installInvoice.TabIndex = 1;
            this.btn_installInvoice.Text = "Установить счет";
            this.btn_installInvoice.UseVisualStyleBackColor = true;
            this.btn_installInvoice.Click += new System.EventHandler(this.btn_installInvoice_Click);
            // 
            // btn_filter_invoice
            // 
            this.btn_filter_invoice.Location = new System.Drawing.Point(6, 55);
            this.btn_filter_invoice.Name = "btn_filter_invoice";
            this.btn_filter_invoice.Size = new System.Drawing.Size(252, 25);
            this.btn_filter_invoice.TabIndex = 0;
            this.btn_filter_invoice.Text = "Invoice Получен\\Не получен";
            this.btn_filter_invoice.UseVisualStyleBackColor = true;
            this.btn_filter_invoice.Click += new System.EventHandler(this.btn_filter_invoice_Click);
            // 
            // mainPanel
            // 
            this.mainPanel.AllowUserToAddRows = false;
            this.mainPanel.AllowUserToDeleteRows = false;
            this.mainPanel.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.mainPanel.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.mainPanel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mainPanel.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.time,
            this.route,
            this.car1,
            this.car2,
            this.loaded,
            this.client,
            this.priceCompany,
            this.priceClient,
            this.company,
            this.invoice,
            this.invoice_client});
            this.mainPanel.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.mainPanel.Location = new System.Drawing.Point(282, 12);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.ReadOnly = true;
            this.mainPanel.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.mainPanel.Size = new System.Drawing.Size(1143, 365);
            this.mainPanel.TabIndex = 13;
            this.mainPanel.SelectionChanged += new System.EventHandler(this.mainPanel_SelectionChanged);
            // 
            // id
            // 
            this.id.HeaderText = "#ID";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.id.Width = 40;
            // 
            // time
            // 
            this.time.HeaderText = "ВРЕМЯ";
            this.time.Name = "time";
            this.time.ReadOnly = true;
            // 
            // route
            // 
            this.route.HeaderText = "МАРШРУТ";
            this.route.Name = "route";
            this.route.ReadOnly = true;
            // 
            // car1
            // 
            this.car1.HeaderText = "МАШИНА#1";
            this.car1.Name = "car1";
            this.car1.ReadOnly = true;
            // 
            // car2
            // 
            this.car2.HeaderText = "МАШИНА#2";
            this.car2.Name = "car2";
            this.car2.ReadOnly = true;
            // 
            // loaded
            // 
            this.loaded.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.loaded.HeaderText = "ЗАГРУЖ?";
            this.loaded.Name = "loaded";
            this.loaded.ReadOnly = true;
            this.loaded.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.loaded.Width = 60;
            // 
            // client
            // 
            this.client.HeaderText = "КЛИЕНТ";
            this.client.Name = "client";
            this.client.ReadOnly = true;
            this.client.Width = 80;
            // 
            // priceCompany
            // 
            this.priceCompany.HeaderText = "ЦЕНА(КОМПАНИЯ)";
            this.priceCompany.Name = "priceCompany";
            this.priceCompany.ReadOnly = true;
            this.priceCompany.Width = 110;
            // 
            // priceClient
            // 
            this.priceClient.HeaderText = "ЦЕНА(КЛИЕНТ)";
            this.priceClient.Name = "priceClient";
            this.priceClient.ReadOnly = true;
            this.priceClient.Width = 110;
            // 
            // company
            // 
            this.company.HeaderText = "КОМПАНИЯ";
            this.company.Name = "company";
            this.company.ReadOnly = true;
            // 
            // invoice
            // 
            this.invoice.HeaderText = "INVOICE";
            this.invoice.Name = "invoice";
            this.invoice.ReadOnly = true;
            // 
            // invoice_client
            // 
            this.invoice_client.HeaderText = "СЧЕТ КЛИЕНТ";
            this.invoice_client.Name = "invoice_client";
            this.invoice_client.ReadOnly = true;
            // 
            // frm_ordersHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1432, 384);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "frm_ordersHistory";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "История заказов";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainPanel)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_filter_invoice;
        private System.Windows.Forms.Button btn_setInvoice;
        private System.Windows.Forms.Button btn_installInvoice;
        private System.Windows.Forms.Button btn_editOrder;
        private System.Windows.Forms.Button btn_filter_all;
        private System.Windows.Forms.Button btn_filter_invoiceClient;
        private System.Windows.Forms.Button btn_search_route;
        private System.Windows.Forms.ComboBox cb_routes;
        private System.Windows.Forms.Button btn_search_car;
        private System.Windows.Forms.ComboBox cb_cars;
        private System.Windows.Forms.Button btn_setMultiInvoice;
        private System.Windows.Forms.DateTimePicker dtp_2;
        private System.Windows.Forms.DateTimePicker dtp_1;
        private System.Windows.Forms.Button btn_search_period;
        private System.Windows.Forms.DataGridView mainPanel;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn time;
        private System.Windows.Forms.DataGridViewTextBoxColumn route;
        private System.Windows.Forms.DataGridViewTextBoxColumn car1;
        private System.Windows.Forms.DataGridViewTextBoxColumn car2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn loaded;
        private System.Windows.Forms.DataGridViewTextBoxColumn client;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceCompany;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceClient;
        private System.Windows.Forms.DataGridViewTextBoxColumn company;
        private System.Windows.Forms.DataGridViewTextBoxColumn invoice;
        private System.Windows.Forms.DataGridViewTextBoxColumn invoice_client;
    }
}