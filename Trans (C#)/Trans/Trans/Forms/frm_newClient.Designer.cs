﻿namespace Trans
{
    partial class frm_newClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl1 = new System.Windows.Forms.Label();
            this.cb_face = new System.Windows.Forms.ComboBox();
            this.panel_main = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.Location = new System.Drawing.Point(12, 18);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(102, 17);
            this.lbl1.TabIndex = 0;
            this.lbl1.Text = "Выберите лицо:";
            // 
            // cb_face
            // 
            this.cb_face.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_face.FormattingEnabled = true;
            this.cb_face.Items.AddRange(new object[] {
            "Физическое",
            "Юридическое"});
            this.cb_face.Location = new System.Drawing.Point(120, 15);
            this.cb_face.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cb_face.Name = "cb_face";
            this.cb_face.Size = new System.Drawing.Size(121, 25);
            this.cb_face.TabIndex = 1;
            this.cb_face.SelectedIndexChanged += new System.EventHandler(this.cb_face_SelectedIndexChanged);
            // 
            // panel_main
            // 
            this.panel_main.Location = new System.Drawing.Point(15, 47);
            this.panel_main.Name = "panel_main";
            this.panel_main.Size = new System.Drawing.Size(226, 31);
            this.panel_main.TabIndex = 2;
            // 
            // frm_newClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(253, 54);
            this.Controls.Add(this.panel_main);
            this.Controls.Add(this.cb_face);
            this.Controls.Add(this.lbl1);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "frm_newClient";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Новый клиент";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.ComboBox cb_face;
        private System.Windows.Forms.Panel panel_main;
    }
}