﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Trans
{
    public partial class frm_vocab : Form
    {
        public frm_vocab()
        {
            InitializeComponent();
        }

        private void btn_newClient_Click(object sender, EventArgs e)
        {
            Form frm_newClient = new frm_newClient();
            frm_newClient.ShowDialog();
        }

        private void btn_newCompany_Click(object sender, EventArgs e)
        {
            Form frm_newCompany = new frm_newCompany();
            frm_newCompany.ShowDialog();
        }

        private void btn_newCar_Click(object sender, EventArgs e)
        {
            Form frm_newCar = new frm_newCar();
            frm_newCar.ShowDialog();
        }

        private void btn_newContract_Click(object sender, EventArgs e)
        {
            Form frm_newContract = new frm_newContract();
            frm_newContract.ShowDialog();
        }
    }
}
