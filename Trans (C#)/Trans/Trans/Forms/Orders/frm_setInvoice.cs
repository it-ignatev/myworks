﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace Trans.Forms.Orders
{
    public partial class frm_setInvoice : Form
    {
        private string id = null, loaded = null;
        private decimal price = 0;
        private dynamic ad = null;
        private decimal pr = 0;
        
        public frm_setInvoice(string i, string route, string time, string pri)
        {
            InitializeComponent();

            label1.Text = "Заказ номер: " + i;
            label2.Text = "Маршрут: " + route;
            label3.Text = "Время/Дата: " + time;

            id = i;

            string[] res = Sql.LoadAdOrder(id);
            ad = JsonConvert.DeserializeObject(res[0]);
            if (ad == null) return;

            nud_weight.Value = Convert.ToDecimal(res[1]);
            nud_price.Value = 0;
            int n = 0;
            loaded = res[2];

            foreach (dynamic r in ad.AD)
            {
                if (ad["AD"]["V" + n.ToString()]["name"] != "Вес")
                {
                    n++;
                    continue;
                }

                if (loaded == "True")
                {
                    nud_price.Value += Convert.ToDecimal(Convert.ToString(ad["AD"]["V" + n.ToString()]["price1"])) * nud_weight.Value;
                }
                else
                {
                    nud_price.Value += Convert.ToDecimal(Convert.ToString(ad["AD"]["V" + n.ToString()]["price2"])) * nud_weight.Value;
                }

                break;
            }

            price = nud_price.Value;

            pr = Convert.ToDecimal(pri);

            nud_price.Value = (pr - price) + nud_price.Value;
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            if (te_invoice.Text == string.Empty) return;

            string p = Math.Round(nud_price.Value, 2).ToString();

            string mes = Sql.UpdateOrderInvoice(id, te_invoice.Text, nud_weight.Value.ToString(), p);

            frm_Main.Message(mes);
            this.Dispose();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            nud_price.Value = 0;

            int n = 0;

            foreach (dynamic r in ad.AD)
            {
                if (ad["AD"]["V" + n.ToString()]["name"] != "Вес")
                {
                    n++;
                    continue;
                }

                if (loaded == "True")
                {
                    nud_price.Value += Convert.ToDecimal(Convert.ToString(ad["AD"]["V" + n.ToString()]["price1"])) * nud_weight.Value;
                }
                else
                {
                    nud_price.Value += Convert.ToDecimal(Convert.ToString(ad["AD"]["V" + n.ToString()]["price2"])) * nud_weight.Value;
                }

                break;
            }

            nud_price.Value = (pr - price) + nud_price.Value;
        }
    }
}
