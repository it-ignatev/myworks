﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace Trans.Forms.Orders
{
    public partial class frm_editOrder : Form
    {
        private int OrderID, Company, Client;
        private List<Dictionary<string, string>> Order;
        private List<Dictionary<string, string>> Companies;
        private List<Dictionary<string, string>> Clients;
        private List<int> LoadedComp = new List<int>();
        private List<int> LoadedClients = new List<int>();
        private string ClientContract;
        public frm_editOrder(string id)
        {
            Logger.info("LOADIND ORDER EDITOR");

            InitializeComponent();

            OrderID = Convert.ToInt32(id);
            Order = Sql.getOrderByID(id);

            LoadCompanies();
            LoadClients();
            te_route_1.Text = Order[0]["route_1"];
            te_route_2.Text = Order[0]["route_2"];
            dateTimePicker1.Value = Convert.ToDateTime(Order[0]["dateTime"]);
            dynamic json = JsonConvert.DeserializeObject(Order[0]["period"]);
            te_p.Text = json["Days"]["D0"]["P0"]["Time1"] + "-" + json["Days"]["D0"]["P0"]["Time2"];
            te_p_price_1.Text = json["Days"]["D0"]["P0"]["Price1"];
            te_p_price_2.Text = json["Days"]["D0"]["P0"]["Price2"];
            te_length.Text = json["Days"]["D0"]["P0"]["Lenght"];
            cb_empty.Checked = Convert.ToBoolean(Order[0]["loaded"]);
            nud_length.Value = Convert.ToDecimal(Order[0]["length"]);
            nud_price.Value = Convert.ToDecimal(Order[0]["price"]);

            json = JsonConvert.DeserializeObject(Order[0]["ad"]);
            int i = 0;
            while(json["AD"]["V" + i.ToString()] != null)
            {
                dataGridView1.Rows.Add(json["AD"]["V" + i.ToString()]["name"], json["AD"]["V" + i.ToString()]["price1"], json["AD"]["V" + i.ToString()]["price2"], json["AD"]["V" + i.ToString()]["weight"]);
                i++;
            }

            Logger.info("SUCCESSFULLY LOADED");
        }
        private void LoadCompanies()
        {
            Companies = Sql.LoadCompaniesFromContracts();

            if (Companies == null) return;

            cb_company.Items.Clear();
            LoadedComp.Clear();
            string lCompany = Sql.getCompanyFromId(Order[0]["company"])[0]["name"];

            foreach (Dictionary<string, string> r in Companies)
            {
                if (!LoadedComp.Contains(Convert.ToInt32(r["id"])))
                {
                    cb_company.Items.Add(r["name"]);
                    LoadedComp.Add(Convert.ToInt32(r["id"]));
                    Company = Convert.ToInt32(r["id_8"]);
                    if(r["name_7"] == lCompany) cb_company.Text = r["name"];
                }
            }
        }
        private void LoadClients()
        {
            Clients = Sql.LoadClientsFromContracts();

            if (Clients == null) return;

            cb_client.Items.Clear();
            LoadedClients.Clear();

            string lClient = Sql.getClientFromId(Order[0]["client"])[0]["name"];

            foreach (Dictionary<string, string> r in Clients)
            {
                if (!LoadedClients.Contains(Convert.ToInt32(r["id"])))
                {
                    cb_client.Items.Add(r["name_3"]);
                    LoadedClients.Add(Convert.ToInt32(r["id"]));
                    cb_cars_1.Items.Add(r["number"]);
                    cb_cars_2.Items.Add(r["number"]);
                    Client = Convert.ToInt32(r["id_5"]);
                    if (r["name_3"] == lClient) cb_client.Text = lClient;
                    ClientContract = r["id"];
                }
            }

            cb_cars_1.Text = Order[0]["car_1"];
            cb_cars_2.Text = Order[0]["car_2"];
        }
        private void ChangePrice()
        {
            if (te_length.Text == string.Empty || te_p_price_1.Text == string.Empty || te_p_price_2.Text == string.Empty) return;
            if (te_p.Text == "Не найден") return;

            nud_price.Value = 0;

            if (cb_empty.Checked)
            {
                nud_price.Value = nud_price.Value + ((nud_length.Value / Convert.ToDecimal(te_length.Text)) * Convert.ToDecimal(te_p_price_1.Text));
            }
            else
            {
                nud_price.Value = nud_price.Value + ((nud_length.Value / Convert.ToDecimal(te_length.Text)) * Convert.ToDecimal(te_p_price_2.Text));
            }

            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                if (dataGridView1.Rows[i].Cells[3].Value == null) continue;
                if (dataGridView1.Rows[i].Cells[3].Value.ToString() == string.Empty) continue;

                if (cb_empty.Checked)
                {
                    decimal multiply = Math.Round(Convert.ToDecimal(dataGridView1.Rows[i].Cells[1].Value.ToString()), 2);
                    nud_price.Value = nud_price.Value + multiply * Convert.ToDecimal(dataGridView1.Rows[i].Cells[3].Value);
                }
                else
                {
                    nud_price.Value = nud_price.Value + (Convert.ToDecimal(dataGridView1.Rows[i].Cells[2].Value) * Convert.ToDecimal(dataGridView1.Rows[i].Cells[3].Value));
                }
            }

            price_client.Value = GetClientPrice();
        }

        private void nud_length_ValueChanged(object sender, EventArgs e)
        {
            ChangePrice();
        }

        private void cb_empty_CheckedChanged(object sender, EventArgs e)
        {
            ChangePrice();
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            ChangePrice();
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            string period = null;

            if (cb_empty.Checked)
            {
                TimePeriods tp = new TimePeriods();
                Dictionary<string, Dictionary<string, TimePeriodsValue>> days = new Dictionary<string, Dictionary<string, TimePeriodsValue>>();
                TimePeriodsValue tpva = new TimePeriodsValue();
                tpva.Time1 = te_p.Text.Split("-".ToCharArray())[0];
                tpva.Time2 = te_p.Text.Split("-".ToCharArray())[1];
                tpva.Price1 = te_p_price_1.Text;
                tpva.Price2 = te_p_price_2.Text;
                tpva.Lenght = te_length.Text;
                days.Add("D0", new Dictionary<string, TimePeriodsValue>() { { "P0", tpva } });
                tp.Days = days;
                period = JsonConvert.SerializeObject(tp, Formatting.Indented);
            }
            else
            {
                TimePeriods tp = new TimePeriods();
                Dictionary<string, Dictionary<string, TimePeriodsValue>> days = new Dictionary<string, Dictionary<string, TimePeriodsValue>>();
                TimePeriodsValue tpva = new TimePeriodsValue();
                tpva.Time1 = te_p.Text.Split("-".ToCharArray())[0];
                tpva.Time2 = te_p.Text.Split("-".ToCharArray())[1];
                tpva.Price1 = te_p_price_1.Text;
                tpva.Price2 = te_p_price_2.Text;
                tpva.Lenght = te_length.Text;
                days.Add("D0", new Dictionary<string, TimePeriodsValue>() { { "P0", tpva } });
                tp.Days = days;
                period = JsonConvert.SerializeObject(tp, Formatting.Indented);
            }


            Additional ad = new Additional();
            Dictionary<string, AdditionalValue> adop = new Dictionary<string, AdditionalValue>();
            string weight = "0";

            for (int i = 0; i < dataGridView1.RowCount - 1; i++)
            {
                AdditionalValue adv = new AdditionalValue();

                adv.name = dataGridView1.Rows[i].Cells[0].Value.ToString();
                adv.price1 = dataGridView1.Rows[i].Cells[1].Value.ToString();
                adv.price2 = dataGridView1.Rows[i].Cells[2].Value.ToString();
                adv.weight = dataGridView1.Rows[i].Cells[3].Value.ToString();

                if(adv.name == "Вес")
                {
                    weight = adv.weight;
                }

                adop.Add("V" + i.ToString(), adv);
            }
            ad.AD = adop;
            string json = JsonConvert.SerializeObject(ad, Formatting.Indented);

            bool ans = Sql.UpdateOrder(new Dictionary<string, string>() { 
                { "id", OrderID.ToString() },
                { "period", period },
                { "ad", json },
                { "company", Company.ToString() },
                { "client", Client.ToString() },
                { "load", cb_empty.Checked.ToString() },
                { "car1", cb_cars_1.Text },
                { "car2", cb_cars_2.Text },
                { "route1", te_route_1.Text },
                { "route2", te_route_2.Text },
                { "len", nud_length.Value.ToString() },
                { "weight", weight },
                { "price", nud_price.Value.ToString() },
                { "date", dateTimePicker1.Value.ToString("yyyy-MM-dd HH:mm:ss") },
                { "clientprice", GetClientPrice().ToString() }
            });

            MessageBox.Show(ans ? "Обновлено !" : "Ошибка !");

            this.Close();
        }

        private void cb_company_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (Dictionary<string, string> r in Companies)
            {
                if (cb_company.Text == r["name"])
                {
                    Company = Convert.ToInt32(r["id_8"]);
                }
            }
        }

        private void cb_client_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (Dictionary<string, string> r in Clients)
            {
                if (cb_client.Text == r["name"])
                {
                    Client = Convert.ToInt32(r["id_5"]);
                    ClientContract = r["id"];
                }
            }
        }
        private decimal GetClientPrice()
        {
            List<Dictionary<string, string>> t = Sql.getTimeTableAndAdFromClient(ClientContract);

            /* Кароч, тут мы получаем таблицу и доп опции с договора клиента. Нужно их пересчитать по заданной дате, длине и весу и тд
             * Ну вроде все так то. Далее цена доб в базу остальное не трогаем, расчет ведем по клиенту
             */
            decimal Price = 0;
            int i = 0;
            dynamic result = JsonConvert.DeserializeObject(t[0]["period"]);
            string len = null, pr1 = null, pr2 = null;

            if (result != null)
            {

                int day = (((int)dateTimePicker1.Value.DayOfWeek == 0) ? 7 : (int)dateTimePicker1.Value.DayOfWeek) - 1;

                while (result.Days["D" + day.ToString()]["P" + i.ToString()] != null)
                {
                    Console.WriteLine(result["Days"]["D" + day.ToString()]["P" + i.ToString()]);
                    string t1 = result["Days"]["D" + day.ToString()]["P" + i.ToString()]["Time1"];
                    string t2 = result["Days"]["D" + day.ToString()]["P" + i.ToString()]["Time2"];

                    if (t1 == null) t1 = "00:00:00";
                    if (t2 == null) t2 = "00:00:00";

                    if (Convert.ToDateTime(t1).TimeOfDay <= dateTimePicker1.Value.TimeOfDay && dateTimePicker1.Value.TimeOfDay <= Convert.ToDateTime(t2).TimeOfDay)
                    {
                        pr1 = result["Days"]["D" + day.ToString()]["P" + i.ToString()]["Price1"];
                        pr2 = result["Days"]["D" + day.ToString()]["P" + i.ToString()]["Price2"];
                        len = result["Days"]["D" + day.ToString()]["P" + i.ToString()]["Lenght"];
                    }

                    i++;
                }
            }

            if (cb_empty.Checked)
            {
                Price += ((nud_length.Value / Convert.ToDecimal(len)) * Convert.ToDecimal(pr1));
            }
            else
            {
                Price += ((nud_length.Value / Convert.ToDecimal(len)) * Convert.ToDecimal(pr2));
            }

            result = JsonConvert.DeserializeObject(t[0]["name"]);

            i = 0;
            DataGridView dgv = new DataGridView();
            dgv.Columns.Add("Name", "NameOFA");
            dgv.Columns.Add("Price", "PriceOFA");
            dgv.Columns.Add("Price2", "Price2OFA");
            dgv.Columns.Add("Weight", "WeightOFA");

            while (result.AD["V" + i.ToString()] != null)
            {
                if (result.AD["V" + i.ToString()]["name"] != null && result.AD["V" + i.ToString()]["name"] != string.Empty)
                {
                    string price1 = result.AD["V" + i.ToString()]["price1"];
                    string price2 = result.AD["V" + i.ToString()]["price2"];
                    string weight = result.AD["V" + i.ToString()]["weight"];

                    dgv.Rows.Add(
                        result.AD["V" + i.ToString()]["name"],
                        (price1 == string.Empty || price1 == null ? "0" : price1),
                        (price2 == string.Empty || price2 == null ? "0" : price2),
                        (weight == string.Empty || weight == null ? "1" : weight)
                        );
                }

                i++;
            }

            for (i = 0; i < dgv.Rows.Count; i++)
            {
                if (dgv.Rows[i].Cells[3].Value == null) continue;
                if (dgv.Rows[i].Cells[3].Value.ToString() == string.Empty) continue;

                if (cb_empty.Checked)
                {
                    Price += Convert.ToDecimal(dgv.Rows[i].Cells[1].Value) * Convert.ToDecimal(dgv.Rows[i].Cells[3].Value);
                }
                else
                {
                    Price += Convert.ToDecimal(dgv.Rows[i].Cells[2].Value) * Convert.ToDecimal(dgv.Rows[i].Cells[3].Value);
                }
            }

            return Math.Round(Price, 2);
        }
    }
}
