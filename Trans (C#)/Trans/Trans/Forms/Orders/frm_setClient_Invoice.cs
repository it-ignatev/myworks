﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;

using System.IO;

using OfficeOpenXml;

namespace Trans.Forms.Orders
{
    public partial class frm_setClient_Invoice : Form
    {
        private string[] id = null;
        private int count = 0;

        private string t = null;
        private string invoice = null;
        private string inn = null;
        private string kpp = null;
        private string bik = null;
        private string pInv = null;
        private string kInv = null;
        private string Mname = null;
        private string MAdr = null;
        private string percent = "2,5 %";
        private string MainPrice = "0";
        private string MainPriceE = "0";

        private string ClientName = null;

        private decimal Euro = 0;

        private string[] Client;

        private string[,] TableText;

        public frm_setClient_Invoice(string i, string route, string time, string client, string car1, string car2, string price)
        {
            InitializeComponent();

            label1.Text = "Заказ номер: " + i;
            label2.Text = "Маршрут: " + route;
            label3.Text = "Время/Дата: " + time;

            id = new string[1];

            id[0] = i;
            t = time;
            dtp_date.Value = Convert.ToDateTime(time);
            count = 1;
            invoice = i;

            LoadMAIN();

            SetClient(client);

            TableText = new string[1, 6];

            TableText[0, 0] = car1;
            TableText[0, 1] = car2;
            TableText[0, 2] = time;
            TableText[0, 3] = route;
            TableText[0, 4] = price;


            MainPrice = Math.Round(Convert.ToDecimal(price), 2, MidpointRounding.AwayFromZero).ToString();
           // MainPriceE = Math.Round(Convert.ToDecimal(price) * Euro, 2, MidpointRounding.AwayFromZero).ToString();

            MainPriceConst = MainPrice;

            nud_per.Maximum = Convert.ToDecimal(MainPrice);

            pictureBox1.Image = new Bitmap(pictureBox1.Width, 469 + count * 40 + 296);
            pictureBox2.Image = new Bitmap(pictureBox2.Width, 469 + count * 40 + 296);

            ClientName = client;
        }

        public frm_setClient_Invoice(string[] i, string[] route, string[] time, string client, string[] car1, string[] car2, string[] price)
        {
            InitializeComponent();

            count = i.Length;
            id = i;
            label1.Text = "Заказ номер: -";
            label2.Text = "Маршрут: -";
            label3.Text = "Время/Дата: -";
            invoice = "#########";

            LoadMAIN();
            SetClient(client);

            TableText = new string[i.Length, 6];

            for (int j = 0; j < i.Length; j++)
            {
                TableText[j, 0] = car1[j];
                TableText[j, 1] = car2[j];
                TableText[j, 2] = time[j];
                TableText[j, 3] = route[j];
                TableText[j, 4] = price[j];

                MainPrice = Math.Round(Convert.ToDecimal(MainPrice) + Convert.ToDecimal(price[j]), 2, MidpointRounding.AwayFromZero).ToString();
                //MainPriceE = Math.Round(Convert.ToDecimal(MainPriceE) + Convert.ToDecimal(price[j]) * Euro, 2, MidpointRounding.AwayFromZero).ToString();
            }

            MainPriceConst = MainPrice;
            nud_per.Maximum = Convert.ToDecimal(MainPrice);

            pictureBox1.Image = new Bitmap(pictureBox1.Width, 469 + count * 40 + 296);
            pictureBox2.Image = new Bitmap(pictureBox2.Width, 469 + count * 40 + 296);

            ClientName = client;
        }

        private void SetClient(string cl)
        {
            string[] result = Sql.getClient(cl);

            Client = new string[9];
            Client[0] = cl;
            Client[1] = result[1];
            Client[2] = result[2];
            Client[3] = result[3];
            Client[4] = result[5];
            Client[5] = result[6];
            Client[6] = result[7];
            Client[7] = result[8];

            Euro = Convert.ToDecimal(GetCurVal(DateTime.Now));
            label5.Text = "Курс ЦБ: " + Euro.ToString();
        }

        private string GetCurVal(DateTime dt)
        {

            var document = XDocument.Load("http://www.cbr.ru/scripts/XML_daily.asp?date_req=" + dt.ToString("dd/MM/yyyy"));
            var element = document.Root.Elements().First(x => x.Attribute("ID").Value == "R01239");
            Console.WriteLine("ЕВРО - {0}", element.Element("Value").Value);

            return element.Element("Value").Value;
        }

        private void ImageToPDF(string destination)
        {
            try
            {
                /*
                PdfDocument doc = new PdfDocument();
                doc.Pages.Add(new PdfPage());
                
                XGraphics xgr = XGraphics.FromPdfPage(doc.Pages[0]);
                XImage img = XImage.FromFile(source);
                
                xgr.DrawImage(img, 80, 0);
                doc.Save(destinaton);
                doc.Close();
                */

                var package = new ExcelPackage(new FileInfo("sample.xlsx"));
                FileStream fs;

                ExcelWorksheet workSheet = package.Workbook.Worksheets[1];

                workSheet.Cells["K7"].Value = Convert.ToDateTime(t).ToLongDateString();
                workSheet.Cells["H7"].Value = invoice;
                workSheet.Cells["F10"].Value = Mname + ", ИНН " + inn + ", КПП " + kpp + ", \r\n" + MAdr;
                workSheet.Cells["F11"].Value = Client[0] + ", ИНН " + Client[1] + ", \r\n КПП " + Client[2] + ", " + Client[3];
                workSheet.Cells["K17"].Value = MainPrice;
                workSheet.Cells["K19"].Value = MainPrice;

                workSheet.Cells["B21"].Value = "Всего наименований " + count + ", на сумму " + MainPrice + " евро";
                workSheet.Cells["B22"].Value = Сумма.Пропись(Convert.ToDecimal(MainPrice), Валюта.Евро, Заглавные.Первая);

                if (comboBox1.SelectedIndex == 1)
                {
                    workSheet.Cells["I4"].Value = "40702978228261003607";

                    workSheet.InsertRow(15, count - 1);

                    for (int i = 15, n = 0; i < count + 15; i++, n++)
                    {
                        workSheet.Cells["B" + i].Value = n + 1;
                        workSheet.Cells["D" + i].Value = "Перевозка а/м " + TableText[n, 0] + "/" + TableText[n, 1] + "\n" + Convert.ToDateTime(TableText[0, 2]).ToString("dd.MM.yyyy HH:mm") + "  на грузовом пароме по маршруту: \r\n" + TableText[n, 3];
                        workSheet.Cells["G" + i].Value = "1";
                        workSheet.Cells["H" + i].Value = "шт";
                        workSheet.Cells["I" + i].Value = Math.Round(Convert.ToDecimal(TableText[n, 4]) - nud_per.Value, 2).ToString();
                        workSheet.Cells["K" + i].Value = Math.Round(Convert.ToDecimal(TableText[n, 4]) - nud_per.Value, 2).ToString();
                    }

                    fs = new FileStream(destination + ".xlsx", FileMode.Create);
                    package.SaveAs(fs);

                    //------------------

                    package = new ExcelPackage(new FileInfo("sample.xlsx"));
                    workSheet = package.Workbook.Worksheets[1];

                    workSheet.Cells["K7"].Value = Convert.ToDateTime(t).ToLongDateString();
                    workSheet.Cells["H7"].Value = invoice;
                    workSheet.Cells["F10"].Value = Mname + ", ИНН " + inn + ", КПП " + kpp + ", \r\n" + MAdr;
                    workSheet.Cells["F11"].Value = Client[0] + ", ИНН " + Client[1] + ", \r\n КПП " + Client[2] + ", " + Client[3];

                    workSheet.Cells["I4"].Value = kInv;

                    workSheet.Cells["K17"].Value = MainPriceE;
                    workSheet.Cells["K19"].Value = MainPriceE;

                    workSheet.Cells["B21"].Value = "Всего наименований " + count + ", на сумму " + MainPriceE + " руб";
                    workSheet.Cells["B22"].Value = Сумма.Пропись(Convert.ToDecimal(MainPriceE), Валюта.Рубли, Заглавные.Первая);
                    workSheet.Cells["I14"].Value = "Цена, руб";
                    workSheet.Cells["K14"].Value = "Сумма, руб";

                    for (int i = 15, n = 0; i < count + 15; i++, n++)
                    {
                        workSheet.Cells["B" + i].Value = n + 1;
                        workSheet.Cells["D" + i].Value = "Услуга по бронированию места (комиссия агента)\nдля а/м " + TableText[n, 0] + "/" + TableText[n, 1] + "  " + Convert.ToDateTime(TableText[0, 2]).ToString("dd.MM.yyyy HH:mm") + "  на\nгрузовом пароме по маршруту: \n" + TableText[n, 3];
                        workSheet.Cells["G" + i].Value = "1";
                        workSheet.Cells["H" + i].Value = "шт";
                        workSheet.Cells["I" + i].Value = Math.Round(nud_per.Value * Euro, 2).ToString();
                        workSheet.Cells["K" + i].Value = Math.Round(nud_per.Value * Euro, 2).ToString();
                    }

                    fs = new FileStream(destination + "_Y.xlsx", FileMode.Create);
                    package.SaveAs(fs);
                }
                else
                {                
                    workSheet.Cells["B25"].Value = "Оплата производится в рублях по курсу ЦБ РФ на дату платежа " + percent + ".";
                    workSheet.InsertRow(15, count - 1);

                    for (int i = 15, n = 0; i < count + 15; i++, n++)
                    {
                        workSheet.Cells["B" + i].Value = n + 1;
                        workSheet.Cells["D" + i].Value = "Услуги по бронированию места на пароме для а/м \r\n" + TableText[n, 0] + "/" + TableText[n, 1] + " " + TableText[n, 2] + " маршрут: \r\n" + TableText[n, 3];
                        workSheet.Cells["G" + i].Value = "1";
                        workSheet.Cells["H" + i].Value = "шт";
                        workSheet.Cells["I" + i].Value = TableText[n, 4];
                        workSheet.Cells["K" + i].Value = TableText[n, 4];
                    }

                    fs = new FileStream(destination + ".xlsx", FileMode.Create);
                    package.SaveAs(fs);
                }

                package.Dispose();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        private void LoadMAIN()
        {

            string res = Sql.LoadMAIN();
            string[] result = res.Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            inn = result[1];
            kpp = result[2];
            bik = result[7];
            pInv = result[5];
            kInv = result[8];
            Mname = result[0];
            MAdr = result[3];

        }

        private void Setup()
        {
            pictureBox1.Refresh();
            pictureBox1.Invalidate();

            pictureBox2.Refresh();
            pictureBox2.Invalidate();
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            if (te_invoice.Text == string.Empty) return;

            string mes = Sql.UpdateOrderClientInvoice(id, te_invoice.Text);
            string prices = null;

            for(int i =0;i<count;i++)
            {
                prices += TableText[i, 4] + "|";
            }

            if (comboBox1.SelectedIndex == 0)
            {
                Sql.AddInvoice(id, te_invoice.Text, MainPrice, MainPriceE, ClientName, prices, count);
            }
            else
            {
                Sql.AddInvoice(id, te_invoice.Text, MainPrice, MainPriceE, ClientName, count);
            }

            if (!Directory.Exists(Application.StartupPath + "\\TEMP\\")) Directory.CreateDirectory(Application.StartupPath + "\\TEMP\\");
            if (!Directory.Exists(Application.StartupPath + "\\СЧЕТА\\")) Directory.CreateDirectory(Application.StartupPath + "\\СЧЕТА\\");

            string name = "INVOICE_" + te_invoice.Text + "_" + DateTime.Now.ToString("dd_MM_yy_HH_mm");
            pictureBox1.Image.Save(Application.StartupPath + "\\TEMP\\" + name + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

            //Image img = pictureBox1.Image;
            //Bitmap bmp = new Bitmap(597, 260 + count * 40);
            //Graphics g = Graphics.FromImage(bmp);
            //g.Clear(System.Drawing.Color.White);
            //g.DrawImage(img, new Rectangle(0, 0, 597, 260 + count * 40), new Rectangle(0, 180, 597, 240 + count * 40), GraphicsUnit.Pixel);
            //bmp.Save(Application.StartupPath + "\\TEMP\\TEMP_" + name + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

            ImageToPDF(Application.StartupPath + "\\СЧЕТА\\" + name);     
            pictureBox2.Image.Save(Application.StartupPath + "\\TEMP\\" + name + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);


            //frm_Main.Message(mes);
            this.Dispose();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {       
            Graphics g = Graphics.FromImage(pictureBox1.Image);
            g.Clear(System.Drawing.Color.White);

            Pen pen = new Pen(Brushes.Black, 2);
            int rows = 1 + count;
            System.Drawing.Font f = new System.Drawing.Font("Segoe Ui", 10, FontStyle.Bold);

            pictureBox1.Height = 468 + rows * 40 + 296;
            g.DrawImage(Properties.Resources.Temple1, new Rectangle(0, 0, 587, 328));
            if (comboBox1.SelectedIndex == 0) { g.DrawImage(Properties.Resources.Temple2, new Rectangle(0, 328 + rows * 40, 587, 296)); } else { g.DrawImage(Properties.Resources.Temple2_2, new Rectangle(0, 328 + rows * 40, 587, 296)); }

            DrawString(g, inn, 38, 53, new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black);
            DrawString(g, kpp, 220, 53, new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black);
            DrawString(g, bik, 392, 6, new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black);
            DrawString(g, pInv, 392, 26, new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black);

            if (comboBox1.SelectedIndex == 1)
            {
                DrawString(g, "40702978228261003607", 392, 56, new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black);

            }
            else
            {
                DrawString(g, kInv, 392, 56, new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black);                 
            }

            DrawString(g, invoice + "  от  " + Convert.ToDateTime(t).ToLongDateString(), 210, 130, new System.Drawing.Font("Segoe Ui", 17, FontStyle.Bold), Brushes.Black);

            DrawString(g, Mname + ", ИНН " + inn + ", КПП " + kpp + ", \r\n" + MAdr, 90, 185, new System.Drawing.Font("Arial", 10, FontStyle.Bold), Brushes.Black);

            DrawString(g, Client[0] + ", ИНН " + Client[1] + ", \r\n КПП " + Client[2] + ", " + ((Client[3].Length > 50) ? (Client[3].Substring(0, 50) + new Regex(" ").Replace(Client[3].Substring(50, Client[3].Length - 50), Environment.NewLine, 1)) : Client[3]), 90, 232, new System.Drawing.Font("Arial", 10, FontStyle.Bold), Brushes.Black);

            g.DrawLines(pen, DrawTable(rows));

            DrawString(g, "№", 5, 315, f, Brushes.Black);
            DrawString(g, "Товары (работы, услуги)", 80, 315, f, Brushes.Black);
            DrawString(g, "Кол-во", 304, 315, f, Brushes.Black);
            DrawString(g, "Ед.", 370, 315, f, Brushes.Black);
            DrawString(g, "Цена, евро", 402, 315, f, Brushes.Black);
            DrawString(g, "Сумма, евро", 482, 315, f, Brushes.Black);

            DrawText(g, count, true);

            DrawString(g, MainPrice, 540, rows * 40 + 348, f, Brushes.Black);
            DrawString(g, MainPrice, 540, rows * 40 + 382, f, Brushes.Black);

            DrawString(g, count.ToString(), 150, rows * 40 + 401, f, Brushes.Black);
            DrawString(g, MainPrice, 250, rows * 40 + 401, f, Brushes.Black);

            DrawString(g, Сумма.Пропись(Convert.ToDecimal(MainPrice), Валюта.Евро, Заглавные.Первая), 4, rows * 40 + 420, f, Brushes.Black);

            DrawString(g, percent, 524, rows * 40 + 496, new System.Drawing.Font("Arial", 12, FontStyle.Italic | FontStyle.Bold), Brushes.Black);
        }

        private Point[] DrawTable(int rows)
        {
            Point[] p = new Point[rows * 20];

            p[0] = new Point(4, 340);
            p[1] = new Point(4, 310);

            p[2] = new Point(26, 310);
            p[3] = new Point(26, 340);
            p[4] = new Point(26, 310);

            p[5] = new Point(300, 310);
            p[6] = new Point(300, 340);
            p[7] = new Point(300, 310);

            p[8] = new Point(360, 310);
            p[9] = new Point(360, 340);
            p[10] = new Point(360, 310);

            p[11] = new Point(400, 310);
            p[12] = new Point(400, 340);
            p[13] = new Point(400, 310);

            p[14] = new Point(480, 310);
            p[15] = new Point(480, 340);
            p[16] = new Point(480, 310);

            p[17] = new Point(570, 310);
            p[18] = new Point(570, 340);

            p[19] = new Point(4, 340);

            if (rows == 1) return p;

            int add = 20;

            for (int i = 0; i < rows-1; i++)
            {
                //p[add] = new Point(4, 340 + (i-1 * 30));

                p[add] = new Point(570, 340 + (i * 50));
                p[add + 1] = new Point(570, 390 + (i * 50));

                p[add + 2] = new Point(480, 390 + (i * 50));
                p[add + 3] = new Point(480, 340 + (i * 50));
                p[add + 4] = new Point(480, 390 + (i * 50));

                p[add + 5] = new Point(400, 390 + (i * 50));
                p[add + 6] = new Point(400, 340 + (i * 50));
                p[add + 7] = new Point(400, 390 + (i * 50));

                p[add + 8] = new Point(360, 390 + (i * 50));
                p[add + 9] = new Point(360, 340 + (i * 50));
                p[add + 10] = new Point(360, 390 + (i * 50));

                p[add + 11] = new Point(300, 390 + (i * 50));
                p[add + 12] = new Point(300, 340 + (i * 50));
                p[add + 13] = new Point(300, 390 + (i * 50));

                p[add + 14] = new Point(26, 390 + (i * 50));
                p[add + 15] = new Point(26, 340 + (i * 50));
                p[add + 16] = new Point(26, 390 + (i * 50));

                p[add + 17] = new Point(4, 390 + (i * 50));
                p[add + 18] = new Point(4, 340 + (i * 50));
                p[add + 19] = new Point(4, 390 + (i * 50));

                add+=20;
            }

            return p;
        }

        private void DrawString(Graphics e, string str, int x, int y, System.Drawing.Font f, Brush b)
        {
            e.DrawString(str, f, b, x, y);
        }

        private void DrawText(Graphics e, int c, bool t)
        {
            if (comboBox1.SelectedIndex == 0)
            {
                if (c == 1)
                {
                    e.DrawString("1", new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black, 7, 355);
                    e.DrawString("Услуги по бронированию места на пароме для а/м \r\n" + TableText[0, 0] + "/" + TableText[0, 1] + " " + TableText[0, 2] + " маршрут: \r\n" + TableText[0, 3], new System.Drawing.Font("Arial", 8, FontStyle.Regular), Brushes.Black, 28, 342);
                    e.DrawString("1", new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black, 322, 355);
                    e.DrawString("шт", new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black, 366, 355);
                    e.DrawString(TableText[0, 4], new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black, 410, 355);
                    e.DrawString(TableText[0, 4], new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black, 490, 355);
                }
                else
                {
                    for (int i = 0; i < c; i++)
                    {
                        e.DrawString((i + 1).ToString(), new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black, 7, 355 + (i * 50));
                        e.DrawString("Услуги по бронированию места на пароме для а/м \r\n" + TableText[i, 0] + "/" + TableText[i, 1] + " " + TableText[i, 2] + " маршрут: \r\n" + TableText[i, 3], new System.Drawing.Font("Arial", 8, FontStyle.Regular), Brushes.Black, 28, 342 + (i * 50));
                        e.DrawString("1", new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black, 322, 355 + (i * 50));
                        e.DrawString("шт", new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black, 366, 355 + (i * 50));
                        e.DrawString(TableText[i, 4], new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black, 410, 355 + (i * 50));
                        e.DrawString(TableText[i, 4], new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black, 490, 355 + (i * 50));
                    }
                }
            }

            if (comboBox1.SelectedIndex == 1)
            {
                if (c == 1)
                {
                    e.DrawString("1", new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black, 7, 355);
                    if (t)
                    {
                        e.DrawString("Перевозка а/м " + TableText[0, 0] + "/" + TableText[0, 1] + "\n" + Convert.ToDateTime(TableText[0, 2]).ToString("dd.MM.yyyy HH:mm") + "  на грузовом пароме по маршруту: \r\n" + TableText[0, 3], new System.Drawing.Font("Arial", 8, FontStyle.Regular), Brushes.Black, 26, 342);
                        e.DrawString(Math.Round(Convert.ToDecimal(TableText[0, 4]) - nud_per.Value, 2).ToString(), new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black, 410, 355);
                        e.DrawString(Math.Round(Convert.ToDecimal(TableText[0, 4]) - nud_per.Value, 2).ToString(), new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black, 490, 355);
                    }
                    else
                    {
                        e.DrawString("Услуга по бронированию места (комиссия агента)\nдля а/м " + TableText[0, 0] + "/" + TableText[0, 1] + "  " + Convert.ToDateTime(TableText[0, 2]).ToString("dd.MM.yyyy HH:mm") + "  на\nгрузовом пароме по маршруту: \n" + TableText[0, 3], new System.Drawing.Font("Arial", 8, FontStyle.Regular), Brushes.Black, 26, 342);
                        e.DrawString(Math.Round(nud_per.Value * Euro, 2).ToString(), new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black, 410, 355);
                        e.DrawString(Math.Round(nud_per.Value * Euro, 2).ToString(), new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black, 490, 355);
                    }

                    e.DrawString("1", new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black, 322, 355);
                    e.DrawString("шт", new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black, 366, 355);
                    }
                else
                {
                    for (int i = 0; i < c; i++)
                    {
                        e.DrawString((i + 1).ToString(), new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black, 7, 355 + (i * 50));
                        if (t)
                        {
                            e.DrawString("Перевозка а/м " + TableText[i, 0] + "/" + TableText[i, 1] + "\n" + Convert.ToDateTime(TableText[0, 2]).ToString("dd.MM.yyyy HH:mm") + "  на грузовом пароме по маршруту: \r\n" + TableText[i, 3], new System.Drawing.Font("Arial", 8, FontStyle.Regular), Brushes.Black, 26, 342 + (i * 50));
                            e.DrawString(Math.Round(Convert.ToDecimal(TableText[i, 4]) - nud_per.Value, 2).ToString(), new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black, 410, 355 + (i * 50));
                            e.DrawString(Math.Round(Convert.ToDecimal(TableText[i, 4]) - nud_per.Value, 2).ToString(), new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black, 490, 355 + (i * 50));
                        }
                        else
                        {
                            e.DrawString("Услуга по бронированию места (комиссия агента)\nдля а/м " + TableText[i, 0] + "/" + TableText[i, 1] + "  " + Convert.ToDateTime(TableText[0, 2]).ToString("dd.MM.yyyy HH:mm") + "  на\nгрузовом пароме по маршруту: \n" + TableText[i, 3], new System.Drawing.Font("Arial", 8, FontStyle.Regular), Brushes.Black, 26, 342 + (i * 50));
                            e.DrawString(Math.Round(nud_per.Value * Euro, 2).ToString(), new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black, 410, 355 + (i * 50));
                            e.DrawString(Math.Round(nud_per.Value * Euro, 2).ToString(), new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black, 490, 355 + (i * 50));
                        }
                        e.DrawString("1", new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black, 322, 355 + (i * 50));
                        e.DrawString("шт", new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black, 366, 355 + (i * 50));
                    }
                }
            }
        }

        private void frm_setClient_Invoice_Load(object sender, EventArgs e)
        {
            Setup();
        }

        private void dtp_date_ValueChanged(object sender, EventArgs e)
        {
            Euro = Convert.ToDecimal(GetCurVal(dtp_date.Value));
            label5.Text = "Курс ЦБ: " + Euro.ToString();

            t = dtp_date.Value.ToLongDateString();
            Setup();
        }

        private void te_invoice_TextChanged(object sender, EventArgs e)
        {
            invoice = te_invoice.Text;
            Setup();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == -1) return;

            Setup();
        }

        private void pictureBox2_Paint(object sender, PaintEventArgs e)
        {
            if (comboBox1.SelectedIndex != 1) return;

            Graphics g = Graphics.FromImage(pictureBox2.Image);
            g.Clear(System.Drawing.Color.White);

            Pen pen = new Pen(Brushes.Black, 2);
            int rows = 1 + count;
            System.Drawing.Font f = new System.Drawing.Font("Segoe Ui", 10, FontStyle.Bold);

            pictureBox2.Height = 468 + rows * 50 + 296;
            g.DrawImage(Properties.Resources.Temple1, new Rectangle(0, 0, 587, 328));
            g.DrawImage(Properties.Resources.Temple2_3, new Rectangle(0, 328 + rows * 50, 587, 296));

            DrawString(g, inn, 38, 53, new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black);
            DrawString(g, kpp, 220, 53, new System.Drawing.Font("Arial", 11, System.Drawing.FontStyle.Regular), Brushes.Black);
            DrawString(g, bik, 392, 6, new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black);
            DrawString(g, pInv, 392, 26, new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black);
            DrawString(g, kInv, 392, 56, new System.Drawing.Font("Arial", 11, FontStyle.Regular), Brushes.Black);

            DrawString(g, invoice + "  от  " + Convert.ToDateTime(t).ToLongDateString(), 210, 130, new System.Drawing.Font("Segoe Ui", 17, FontStyle.Bold), Brushes.Black);

            DrawString(g, Mname + ", ИНН " + inn + ", КПП " + kpp + ", \r\n" + MAdr, 90, 185, new System.Drawing.Font("Arial", 10, FontStyle.Bold), Brushes.Black);

            DrawString(g, Client[0] + ", ИНН " + Client[1] + ", \r\n КПП " + Client[2] + ", " + ((Client[3].Length > 50) ? (Client[3].Substring(0, 50) + new Regex(" ").Replace(Client[3].Substring(50, Client[3].Length - 50), Environment.NewLine, 1)) : Client[3]), 90, 232, new System.Drawing.Font("Arial", 10, FontStyle.Bold), Brushes.Black);

            g.DrawLines(pen, DrawTable(rows));

            DrawString(g, "№", 5, 315, f, Brushes.Black);
            DrawString(g, "Товары (работы, услуги)", 80, 315, f, Brushes.Black);
            DrawString(g, "Кол-во", 304, 315, f, Brushes.Black);
            DrawString(g, "Ед.", 370, 315, f, Brushes.Black);
            DrawString(g, "Цена, руб", 402, 315, f, Brushes.Black);
            DrawString(g, "Сумма, руб", 482, 315, f, Brushes.Black);

            DrawText(g, count, false);

            DrawString(g, MainPriceE, 520, rows * 50 + 328, f, Brushes.Black);
            DrawString(g, MainPriceE, 520, rows * 50 + 382, f, Brushes.Black);

            DrawString(g, count.ToString(), 146, rows * 50 + 401, f, Brushes.Black);
            DrawString(g, MainPriceE, 250, rows * 50 + 401, f, Brushes.Black);

            DrawString(g, Сумма.Пропись(Convert.ToDecimal(MainPriceE), Валюта.Рубли, Заглавные.Первая), 8, rows * 50 + 420, f, Brushes.Black);
        }

        private string MainPriceConst = null;

        private void nud_per_ValueChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex != 1) return;
       
            MainPrice = Math.Round(Convert.ToDecimal(MainPriceConst) - nud_per.Value * count, 2).ToString();
            MainPriceE = Math.Round(nud_per.Value * count * Euro, 2, MidpointRounding.AwayFromZero).ToString("0.00");

            Setup();
        }

        private void te_percent_TextChanged(object sender, EventArgs e)
        {
            percent = te_percent.Text;
            Setup();
        }
    }


}
