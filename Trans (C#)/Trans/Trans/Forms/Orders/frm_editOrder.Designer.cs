﻿namespace Trans.Forms.Orders
{
    partial class frm_editOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btn_save = new System.Windows.Forms.Button();
            this.nud_price = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lenght = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.nud_length = new System.Windows.Forms.NumericUpDown();
            this.cb_cars_2 = new System.Windows.Forms.ComboBox();
            this.cb_cars_1 = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cb_empty = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.gb_1 = new System.Windows.Forms.GroupBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.te_p_price_1 = new System.Windows.Forms.TextBox();
            this.te_p_price_2 = new System.Windows.Forms.TextBox();
            this.te_length = new System.Windows.Forms.TextBox();
            this.te_p = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cb_company = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.te_route_1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cb_client = new System.Windows.Forms.ComboBox();
            this.te_route_2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.price_client = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.nud_price)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_length)).BeginInit();
            this.gb_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.price_client)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(323, 428);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(308, 32);
            this.btn_save.TabIndex = 29;
            this.btn_save.Text = "Сохранить";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // nud_price
            // 
            this.nud_price.DecimalPlaces = 2;
            this.nud_price.Enabled = false;
            this.nud_price.Location = new System.Drawing.Point(323, 366);
            this.nud_price.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.nud_price.Name = "nud_price";
            this.nud_price.ReadOnly = true;
            this.nud_price.Size = new System.Drawing.Size(308, 25);
            this.nud_price.TabIndex = 28;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(243, 368);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 17);
            this.label9.TabIndex = 27;
            this.label9.Text = "Стоимость:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridView1);
            this.groupBox2.Location = new System.Drawing.Point(323, 9);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(308, 351);
            this.groupBox2.TabIndex = 26;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Доп. опции";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.name,
            this.price1,
            this.price2,
            this.lenght});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 21);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(302, 327);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellValueChanged);
            // 
            // name
            // 
            this.name.HeaderText = "Название";
            this.name.Name = "name";
            // 
            // price1
            // 
            dataGridViewCellStyle7.Format = "C2";
            dataGridViewCellStyle7.NullValue = "0,00";
            this.price1.DefaultCellStyle = dataGridViewCellStyle7;
            this.price1.HeaderText = "Груженая";
            this.price1.Name = "price1";
            // 
            // price2
            // 
            dataGridViewCellStyle8.Format = "C2";
            dataGridViewCellStyle8.NullValue = "0,00";
            this.price2.DefaultCellStyle = dataGridViewCellStyle8;
            this.price2.HeaderText = "Негруженая";
            this.price2.Name = "price2";
            // 
            // lenght
            // 
            dataGridViewCellStyle9.Format = "N2";
            dataGridViewCellStyle9.NullValue = "0,00";
            this.lenght.DefaultCellStyle = dataGridViewCellStyle9;
            this.lenght.HeaderText = "Длина";
            this.lenght.Name = "lenght";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.nud_length);
            this.groupBox1.Controls.Add(this.cb_cars_2);
            this.groupBox1.Controls.Add(this.cb_cars_1);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cb_empty);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(9, 225);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(308, 117);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Настройка машины";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 88);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 17);
            this.label12.TabIndex = 19;
            this.label12.Text = "Длина:";
            // 
            // nud_length
            // 
            this.nud_length.DecimalPlaces = 2;
            this.nud_length.Location = new System.Drawing.Point(94, 86);
            this.nud_length.Name = "nud_length";
            this.nud_length.Size = new System.Drawing.Size(197, 25);
            this.nud_length.TabIndex = 18;
            this.nud_length.ValueChanged += new System.EventHandler(this.nud_length_ValueChanged);
            // 
            // cb_cars_2
            // 
            this.cb_cars_2.FormattingEnabled = true;
            this.cb_cars_2.Location = new System.Drawing.Point(195, 18);
            this.cb_cars_2.Name = "cb_cars_2";
            this.cb_cars_2.Size = new System.Drawing.Size(96, 25);
            this.cb_cars_2.TabIndex = 17;
            // 
            // cb_cars_1
            // 
            this.cb_cars_1.FormattingEnabled = true;
            this.cb_cars_1.Location = new System.Drawing.Point(94, 18);
            this.cb_cars_1.Name = "cb_cars_1";
            this.cb_cars_1.Size = new System.Drawing.Size(95, 25);
            this.cb_cars_1.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 17);
            this.label6.TabIndex = 10;
            this.label6.Text = "Машина:";
            // 
            // cb_empty
            // 
            this.cb_empty.AutoSize = true;
            this.cb_empty.Location = new System.Drawing.Point(94, 49);
            this.cb_empty.Name = "cb_empty";
            this.cb_empty.Size = new System.Drawing.Size(15, 14);
            this.cb_empty.TabIndex = 12;
            this.cb_empty.UseVisualStyleBackColor = true;
            this.cb_empty.CheckedChanged += new System.EventHandler(this.cb_empty_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 17);
            this.label7.TabIndex = 13;
            this.label7.Text = "Груженая ?";
            // 
            // gb_1
            // 
            this.gb_1.Controls.Add(this.dateTimePicker1);
            this.gb_1.Controls.Add(this.te_p_price_1);
            this.gb_1.Controls.Add(this.te_p_price_2);
            this.gb_1.Controls.Add(this.te_length);
            this.gb_1.Controls.Add(this.te_p);
            this.gb_1.Controls.Add(this.label11);
            this.gb_1.Controls.Add(this.label1);
            this.gb_1.Controls.Add(this.cb_company);
            this.gb_1.Controls.Add(this.label2);
            this.gb_1.Controls.Add(this.label3);
            this.gb_1.Controls.Add(this.te_route_1);
            this.gb_1.Controls.Add(this.label4);
            this.gb_1.Controls.Add(this.cb_client);
            this.gb_1.Controls.Add(this.te_route_2);
            this.gb_1.Controls.Add(this.label5);
            this.gb_1.Location = new System.Drawing.Point(9, 9);
            this.gb_1.Name = "gb_1";
            this.gb_1.Size = new System.Drawing.Size(308, 210);
            this.gb_1.TabIndex = 24;
            this.gb_1.TabStop = false;
            this.gb_1.Text = "Настройка маршрута";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "HH:mm:ss dd:MM:yyyy";
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(94, 142);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(197, 25);
            this.dateTimePicker1.TabIndex = 15;
            // 
            // te_p_price_1
            // 
            this.te_p_price_1.Enabled = false;
            this.te_p_price_1.Location = new System.Drawing.Point(179, 173);
            this.te_p_price_1.Name = "te_p_price_1";
            this.te_p_price_1.ReadOnly = true;
            this.te_p_price_1.Size = new System.Drawing.Size(36, 25);
            this.te_p_price_1.TabIndex = 14;
            this.te_p_price_1.Text = "-";
            // 
            // te_p_price_2
            // 
            this.te_p_price_2.Enabled = false;
            this.te_p_price_2.Location = new System.Drawing.Point(221, 173);
            this.te_p_price_2.Name = "te_p_price_2";
            this.te_p_price_2.ReadOnly = true;
            this.te_p_price_2.Size = new System.Drawing.Size(39, 25);
            this.te_p_price_2.TabIndex = 13;
            this.te_p_price_2.Text = "-";
            // 
            // te_length
            // 
            this.te_length.Enabled = false;
            this.te_length.Location = new System.Drawing.Point(266, 173);
            this.te_length.Name = "te_length";
            this.te_length.ReadOnly = true;
            this.te_length.Size = new System.Drawing.Size(25, 25);
            this.te_length.TabIndex = 12;
            this.te_length.Text = "-";
            // 
            // te_p
            // 
            this.te_p.Enabled = false;
            this.te_p.Location = new System.Drawing.Point(94, 173);
            this.te_p.Name = "te_p";
            this.te_p.ReadOnly = true;
            this.te_p.Size = new System.Drawing.Size(79, 25);
            this.te_p.TabIndex = 11;
            this.te_p.Text = "Не найден";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 176);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 17);
            this.label11.TabIndex = 10;
            this.label11.Text = "Период:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Компания:";
            // 
            // cb_company
            // 
            this.cb_company.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_company.FormattingEnabled = true;
            this.cb_company.Location = new System.Drawing.Point(94, 18);
            this.cb_company.Name = "cb_company";
            this.cb_company.Size = new System.Drawing.Size(197, 25);
            this.cb_company.TabIndex = 1;
            this.cb_company.SelectedIndexChanged += new System.EventHandler(this.cb_company_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 148);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Дата/Время:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Маршрут 1:";
            // 
            // te_route_1
            // 
            this.te_route_1.Enabled = false;
            this.te_route_1.Location = new System.Drawing.Point(94, 49);
            this.te_route_1.Name = "te_route_1";
            this.te_route_1.ReadOnly = true;
            this.te_route_1.Size = new System.Drawing.Size(197, 25);
            this.te_route_1.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Маршрут 2:";
            // 
            // cb_client
            // 
            this.cb_client.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_client.FormattingEnabled = true;
            this.cb_client.Location = new System.Drawing.Point(94, 80);
            this.cb_client.Name = "cb_client";
            this.cb_client.Size = new System.Drawing.Size(197, 25);
            this.cb_client.TabIndex = 9;
            this.cb_client.SelectedIndexChanged += new System.EventHandler(this.cb_client_SelectedIndexChanged);
            // 
            // te_route_2
            // 
            this.te_route_2.Enabled = false;
            this.te_route_2.Location = new System.Drawing.Point(94, 111);
            this.te_route_2.Name = "te_route_2";
            this.te_route_2.ReadOnly = true;
            this.te_route_2.Size = new System.Drawing.Size(197, 25);
            this.te_route_2.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 83);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "Клиент:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(201, 401);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(121, 17);
            this.label8.TabIndex = 31;
            this.label8.Text = "Стоимость(клиент):";
            // 
            // price_client
            // 
            this.price_client.DecimalPlaces = 2;
            this.price_client.Enabled = false;
            this.price_client.Location = new System.Drawing.Point(323, 397);
            this.price_client.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.price_client.Name = "price_client";
            this.price_client.ReadOnly = true;
            this.price_client.Size = new System.Drawing.Size(308, 25);
            this.price_client.TabIndex = 30;
            // 
            // frm_editOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(640, 468);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.price_client);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.nud_price);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gb_1);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frm_editOrder";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Редактирование заказа";
            ((System.ComponentModel.ISupportInitialize)(this.nud_price)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_length)).EndInit();
            this.gb_1.ResumeLayout(false);
            this.gb_1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.price_client)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.NumericUpDown nud_price;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn price1;
        private System.Windows.Forms.DataGridViewTextBoxColumn price2;
        private System.Windows.Forms.DataGridViewTextBoxColumn lenght;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown nud_length;
        private System.Windows.Forms.ComboBox cb_cars_2;
        private System.Windows.Forms.ComboBox cb_cars_1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox cb_empty;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox gb_1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox te_p_price_1;
        private System.Windows.Forms.TextBox te_p_price_2;
        private System.Windows.Forms.TextBox te_length;
        private System.Windows.Forms.TextBox te_p;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cb_company;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox te_route_1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cb_client;
        private System.Windows.Forms.TextBox te_route_2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown price_client;

    }
}