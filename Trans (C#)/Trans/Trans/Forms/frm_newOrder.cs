﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using Newtonsoft.Json;

namespace Trans.Forms
{
    public partial class frm_newOrder : Form
    {
        private List<Dictionary<string, string>> Companies = null;
        private List<Dictionary<string, string>> Clients = null;
        private List<int> LoadedComp = new List<int>(), LoadedClients = new List<int>();
        private string TimeTable = null, Company = null, Client = null, ClientContract, TimeTableClient;

        public frm_newOrder()
        {
            InitializeComponent();

            LoadClients();
            LoadCompanies();       
        }

        private void LoadCompanies()
        {
            Companies = Sql.LoadCompaniesFromContracts();

            if (Companies == null) return;

            cb_company.Items.Clear();
            LoadedComp.Clear();

            foreach(Dictionary<string, string> r in Companies)
            {
                if (!LoadedComp.Contains(Convert.ToInt32(r["id"])))
                {
                    cb_company.Items.Add(r["name"]);
                    LoadedComp.Add(Convert.ToInt32(r["id"]));
                }
            } 
        }
        private void LoadClients()
        {
            Clients = Sql.LoadClientsFromContracts();

            if (Clients == null) return;

            cb_client.Items.Clear();
            LoadedClients.Clear();

            foreach(Dictionary<string, string> r in Clients)
            {
                if (!LoadedClients.Contains(Convert.ToInt32(r["id"])))
                {
                    cb_client.Items.Add(r["name_3"]);
                    LoadedClients.Add(Convert.ToInt32(r["id"]));
                    ClientContract=r["id"];
                }
            }            
        }
        private void cb_company_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cb_company.SelectedIndex != -1)
            {
                string Additional = null;

                foreach(Dictionary<string, string> r in Companies)
                {
                    if (LoadedComp[cb_company.SelectedIndex] == Convert.ToInt32(r["id"]))
                    {
                        TimeTable = r["period"];
                        te_route_1.Text = r["name_2"];
                        Additional = r["name_6"];
                        Company = r["name_7"];
                    }
                }

                dynamic result = JsonConvert.DeserializeObject(Additional);

                if(result != null)
                {
                    int i = 0;

                    while(result.AD["V" + i .ToString()] != null)
                    {
                        if (result.AD["V" + i.ToString()]["name"] != null && result.AD["V" + i.ToString()]["name"] != string.Empty)
                        {
                            string price1 = result.AD["V" + i.ToString()]["price1"];
                            string price2 = result.AD["V" + i.ToString()]["price2"];
                            string weight = result.AD["V" + i.ToString()]["weight"];

                            dataGridView1.Rows.Add(
                                result.AD["V" + i.ToString()]["name"],
                                (price1 == string.Empty || price1 == null ? "0" : price1),
                                (price2 == string.Empty || price2 == null ? "0" : price2),
                                (weight == string.Empty || weight == null ? "1" : weight)
                                );
                        }

                        i++;
                    }
                }
                te_p.Text = "Не найден";
                te_p_price_1.Text = "-";
                te_p_price_2.Text = "-";
                te_length.Text = "-";

                dateTimePicker1_ValueChanged(null, null);
            }
        }

        private void cb_client_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cb_client.SelectedIndex != -1)
            {
                cb_cars_1.Items.Clear(); 
                cb_cars_2.Items.Clear();
                
                foreach(Dictionary<string, string> r in Clients)
                {
                    if (LoadedClients[cb_client.SelectedIndex] == Convert.ToInt32(r["id"]))
                    {
                        cb_cars_1.Items.Add(r["number"]);
                        cb_cars_2.Items.Add(r["number"]);
                        te_route_2.Text = r["name_1"];
                        Client = r["name_3"];
                    }
                }                
            }
        }

        private void te_p_price_1_TextChanged(object sender, EventArgs e)
        {
            ChangePrice();
        }

        private void te_p_price_2_TextChanged(object sender, EventArgs e)
        {
            ChangePrice();
        }

        private void te_length_TextChanged(object sender, EventArgs e)
        {
            ChangePrice();
        }

        private void ChangePrice()
        {
            if (te_length.Text == string.Empty || te_p_price_1.Text == string.Empty || te_p_price_2.Text == string.Empty) return;
            if (te_length.Text == "-") return;
            if (te_p.Text == "Не найден") return;
            if (te_p_price_1.Text == "-") return;

            nud_price.Value = 0;

            if (cb_empty.Checked)
            {
                nud_price.Value = nud_price.Value + ((nud_length.Value / Convert.ToDecimal(te_length.Text)) * Convert.ToDecimal(te_p_price_1.Text));
            }
            else
            {
                nud_price.Value = nud_price.Value + ((nud_length.Value / Convert.ToDecimal(te_length.Text)) * Convert.ToDecimal(te_p_price_2.Text));
            }

            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                if (dataGridView1.Rows[i].Cells[3].Value == null) continue;
                if (dataGridView1.Rows[i].Cells[3].Value.ToString() == string.Empty) continue;

                if (cb_empty.Checked)
                {
                    nud_price.Value = nud_price.Value + Convert.ToDecimal(dataGridView1.Rows[i].Cells[1].Value) * Convert.ToDecimal(dataGridView1.Rows[i].Cells[3].Value);
                }
                else
                {
                    nud_price.Value = nud_price.Value + Convert.ToDecimal(dataGridView1.Rows[i].Cells[2].Value) * Convert.ToDecimal(dataGridView1.Rows[i].Cells[3].Value);
                }
            }

            price_client.Value = Convert.ToDecimal(GetClientPrice());
        }

        private void nud_length_ValueChanged(object sender, EventArgs e)
        {
            ChangePrice();
        }

        private void CheckNUD(object s, EventArgs e)
        {
            ChangePrice();
        }

        private ComboBox CreateComboBox(int t, int l, int w, string n, string[] value, EventHandler e)
        {
            ComboBox cb = new ComboBox();
            cb.Top = t;
            cb.Left = l;
            cb.Width = w;
            cb.Name = n;
            if (value != null) cb.Items.AddRange(value);
            if (e != null) cb.TextChanged += e;

            return cb;
        }

        private NumericUpDown CreateNUD(int t, int l, int w, string n, EventHandler e)
        {
            NumericUpDown nud = new NumericUpDown();

            nud.Top = t;
            nud.Left = l;
            nud.Width = w;
            nud.Name = n;
            nud.DecimalPlaces = 2;
            if (e != null) nud.ValueChanged += e;

            return nud;
        }

        private void cb_empty_CheckedChanged(object sender, EventArgs e)
        {
            ChangePrice();
        }

        private void nud_weight_ValueChanged(object sender, EventArgs e)
        {
            ChangePrice();
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            string period = null;

            if (cb_empty.Checked)
            {
                TimePeriods tp = new TimePeriods();
                Dictionary<string, Dictionary<string, TimePeriodsValue>> days = new Dictionary<string, Dictionary<string, TimePeriodsValue>>();
                TimePeriodsValue tpva = new TimePeriodsValue();
                tpva.Time1 = te_p.Text.Split("-".ToCharArray())[0];
                tpva.Time2 = te_p.Text.Split("-".ToCharArray())[1];
                tpva.Price1 = te_p_price_1.Text;
                tpva.Price2 = te_p_price_2.Text;
                tpva.Lenght = te_length.Text;
                days.Add("D0", new Dictionary<string, TimePeriodsValue>() { {"P0", tpva}});
                tp.Days = days;
                period = JsonConvert.SerializeObject(tp, Formatting.Indented);
            }
            else
            {
                TimePeriods tp = new TimePeriods();
                Dictionary<string, Dictionary<string, TimePeriodsValue>> days = new Dictionary<string, Dictionary<string, TimePeriodsValue>>();
                TimePeriodsValue tpva = new TimePeriodsValue();
                tpva.Time1 = te_p.Text.Split("-".ToCharArray())[0];
                tpva.Time2 = te_p.Text.Split("-".ToCharArray())[1];
                tpva.Price1 = te_p_price_1.Text;
                tpva.Price2 = te_p_price_2.Text;
                tpva.Lenght = te_length.Text;
                days.Add("D0", new Dictionary<string, TimePeriodsValue>() { { "P0", tpva } });
                tp.Days = days;
                period = JsonConvert.SerializeObject(tp, Formatting.Indented);
            }


            Additional ad = new Additional();
            Dictionary<string, AdditionalValue> adop = new Dictionary<string, AdditionalValue>();
            string weight = "0";

            for (int i = 0; i < dataGridView1.RowCount - 1; i++)
            {
                AdditionalValue adv = new AdditionalValue();

                adv.name = dataGridView1.Rows[i].Cells[0].Value.ToString();
                adv.price1 = dataGridView1.Rows[i].Cells[1].Value.ToString();
                adv.price2 = dataGridView1.Rows[i].Cells[2].Value.ToString();
                adv.weight = dataGridView1.Rows[i].Cells[3].Value.ToString();

                if(adv.name == "Вес")
                {
                    weight = adv.weight;
                }

                adop.Add("V" + i.ToString(), adv);
            }
            ad.AD = adop;

            string[] res = Sql.getCar(cb_cars_1.Text);
            if (res[0] == null || res[0] == string.Empty) Sql.AddCar(cb_cars_1.Text, Sql.getClientIDByName(Client), 1);
            res = Sql.getCar(cb_cars_2.Text);
            if (res[0] == null || res[0] == string.Empty) Sql.AddCar(cb_cars_2.Text, Sql.getClientIDByName(Client), 1);

            string json = JsonConvert.SerializeObject(ad, Formatting.Indented);

            Sql.AddOrder(Company, Client, cb_empty.Checked.ToString(), cb_cars_1.Text, cb_cars_2.Text, nud_length.Value.ToString(), weight, period, te_route_1.Text, te_route_2.Text, json, Math.Round(nud_price.Value, 2).ToString(), dateTimePicker1.Value.ToString(), ClientContract, GetClientPrice());

            frm_Main.Message("Добавлено");

            this.Dispose();
        }

        private string GetClientPrice()
        {
            List<Dictionary<string, string>> t = Sql.getTimeTableAndAdFromClient(ClientContract);

            /* Кароч, тут мы получаем таблицу и доп опции с договора клиента. Нужно их пересчитать по заданной дате, длине и весу и тд
             * Ну вроде все так то. Далее цена доб в базу остальное не трогаем, расчет ведем по клиенту
             */
            decimal Price=0;
            int i = 0;
            dynamic result = JsonConvert.DeserializeObject(t[0]["period"]);
            string len=null, pr1=null, pr2=null;

            if (result != null)
            {

                int day = (((int)dateTimePicker1.Value.DayOfWeek == 0) ? 7 : (int)dateTimePicker1.Value.DayOfWeek) - 1;

                while (result.Days["D" + day.ToString()]["P" + i.ToString()] != null)
                {
                    Console.WriteLine(result["Days"]["D" + day.ToString()]["P" + i.ToString()]);
                    string t1 = result["Days"]["D" + day.ToString()]["P" + i.ToString()]["Time1"];
                    string t2 = result["Days"]["D" + day.ToString()]["P" + i.ToString()]["Time2"];

                    if (t1 == null) t1 = "00:00:00";
                    if (t2 == null) t2 = "00:00:00";

                    if (Convert.ToDateTime(t1).TimeOfDay <= dateTimePicker1.Value.TimeOfDay && dateTimePicker1.Value.TimeOfDay <= Convert.ToDateTime(t2).TimeOfDay)
                    {
                        pr1 = result["Days"]["D" + day.ToString()]["P" + i.ToString()]["Price1"];
                        pr2 = result["Days"]["D" + day.ToString()]["P" + i.ToString()]["Price2"];
                        len = result["Days"]["D" + day.ToString()]["P" + i.ToString()]["Lenght"];              
                    }

                    i++;
                }
            }

            if (cb_empty.Checked)
            {
                Price += ((nud_length.Value / Convert.ToDecimal(len)) * Convert.ToDecimal(pr1));
            }
            else
            {
                Price += ((nud_length.Value / Convert.ToDecimal(len)) * Convert.ToDecimal(pr2));
            }

            result = JsonConvert.DeserializeObject(t[0]["name"]);

            i = 0;
            DataGridView dgv = new DataGridView();
            dgv.Columns.Add("Name", "NameOFA");
            dgv.Columns.Add("Price", "PriceOFA");
            dgv.Columns.Add("Price2", "Price2OFA");
            dgv.Columns.Add("Weight", "WeightOFA");

            while (result.AD["V" + i.ToString()] != null)
            {
                if (result.AD["V" + i.ToString()]["name"] != null && result.AD["V" + i.ToString()]["name"] != string.Empty)
                {
                    string price1 = result.AD["V" + i.ToString()]["price1"];
                    string price2 = result.AD["V" + i.ToString()]["price2"];
                    string weight = result.AD["V" + i.ToString()]["weight"];

                    dgv.Rows.Add(
                        result.AD["V" + i.ToString()]["name"],
                        (price1 == string.Empty || price1 == null ? "0" : price1),
                        (price2 == string.Empty || price2 == null ? "0" : price2),
                        (weight == string.Empty || weight == null ? "1" : weight)
                        );
                }

                i++;
            }

            for (i = 0; i < dgv.Rows.Count; i++)
            {
                if (dgv.Rows[i].Cells[3].Value == null) continue;
                if (dgv.Rows[i].Cells[3].Value.ToString() == string.Empty) continue;

                if (cb_empty.Checked)
                {
                    Price += Convert.ToDecimal(dgv.Rows[i].Cells[1].Value) * Convert.ToDecimal(dgv.Rows[i].Cells[3].Value);
                }
                else
                {
                    Price += Convert.ToDecimal(dgv.Rows[i].Cells[2].Value) * Convert.ToDecimal(dgv.Rows[i].Cells[3].Value);
                }
            }

            return Math.Round(Price, 2).ToString();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                Task.Run(new Action(() =>
                    {
                        Console.WriteLine("Parse begin: ");
                        //if (TimeTable == null || TimeTable == string.Empty) return;
                        int day = (((int)dateTimePicker1.Value.DayOfWeek == 0) ? 7 : (int)dateTimePicker1.Value.DayOfWeek) - 1;
                        dynamic result = JsonConvert.DeserializeObject(TimeTable);
                        //if (result == null) return;
                        Console.WriteLine(result);

                        int i = 0;

                        while (result.Days["D" + day.ToString()]["P" + i.ToString()] != null)
                        {
                            Console.WriteLine(result["Days"]["D" + day.ToString()]["P" + i.ToString()]);
                            string t1 = result["Days"]["D" + day.ToString()]["P" + i.ToString()]["Time1"];
                            string t2 = result["Days"]["D" + day.ToString()]["P" + i.ToString()]["Time2"];

                            if (t1 == null) t1 = "00:00:00";
                            if (t2 == null) t2 = "00:00:00";

                            if (Convert.ToDateTime(t1).TimeOfDay <= dateTimePicker1.Value.TimeOfDay && dateTimePicker1.Value.TimeOfDay <= Convert.ToDateTime(t2).TimeOfDay)
                            {
                                te_p.Invoke(new Action(() =>
                                {
                                    te_p.Text = t1 + "-" + t2;
                                    te_p_price_1.Text = result["Days"]["D" + day.ToString()]["P" + i.ToString()]["Price1"];
                                    te_p_price_2.Text = result["Days"]["D" + day.ToString()]["P" + i.ToString()]["Price2"];
                                    te_length.Text = result["Days"]["D" + day.ToString()]["P" + i.ToString()]["Lenght"];
                                }));
                                return;
                            }

                            i++;
                        }

                        te_p.Text = "Не найден";
                        te_p_price_1.Text = "-";
                        te_p_price_2.Text = "-";
                        te_length.Text = "-";
                    }));
            }catch(Exception exept)
            {
                Console.WriteLine(exept.ToString());
            }
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            ChangePrice();
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }
    }
}
