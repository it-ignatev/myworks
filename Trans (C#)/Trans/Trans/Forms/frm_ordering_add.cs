﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Trans.Forms
{
    public partial class frm_ordering_add : Form
    {
        private string Cl = null;
        private decimal deb = 0;
        private DateTime dat;

        public frm_ordering_add(string client, decimal debit, DateTime dt)
        {
            InitializeComponent();

            Cl = client;
            deb = debit;
            dat = dt;
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            if (te_name.Text == string.Empty || Cl == string.Empty) return;

            Sql.AddOrdering(te_name.Text, dateTimePicker1.Value.ToString("yyyy-MM-dd HH:mm:ss"), te_credit.Text, te_deb.Text, Cl);

            this.Dispose();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBox1.Checked)
            {
                te_deb.Text = deb.ToString();
                te_credit.Clear();
                te_credit.Enabled = false;
                dateTimePicker1.Value = dat;
                te_name.Text = "Сальдо на " + dateTimePicker1.Value.ToString("dd.MM.yyyy");
            }
            else
            {
                te_deb.Text = "";
                te_credit.Enabled = true;
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked) te_name.Text = "Сальдо на " + dateTimePicker1.Value.ToString("dd.MM.yyyy");
        }
    }
}
