﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Trans
{
    public partial class frm_newCompany : Form
    {
        public frm_newCompany()
        {
            InitializeComponent();
        }

        private void btn_SaveAndExit_Click(object sender, EventArgs e)
        {
            string mes = Sql.AddCompany(te_name.Text, te_adr.Text, te_iban.Text, te_bank.Text, te_swift.Text);
            frm_Main.Message(mes);

            this.Dispose();
        }
    }
}
