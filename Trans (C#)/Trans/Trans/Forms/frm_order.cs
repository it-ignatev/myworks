﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Trans.Forms
{
    public partial class frm_order : Form
    {
        public frm_order()
        {
            InitializeComponent();
        }

        private void btn_newOrder_Click(object sender, EventArgs e)
        {
            Form frm_newOrder = new frm_newOrder();
            frm_newOrder.ShowDialog();
        }

        private void btn_historyOrders_Click(object sender, EventArgs e)
        {
            Form frm_ho = new frm_ordersHistory();
            frm_ho.ShowDialog();
        }
    }
}
