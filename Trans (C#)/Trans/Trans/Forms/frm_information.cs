﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using Newtonsoft.Json;

namespace Trans
{
    public partial class frm_information : Form
    {
        public frm_information()
        {
            InitializeComponent();

            LoadClientsAndCompanies();
        }

        private string[,] CountsTime = new string[7,1];
        private string ClientOrcompany;
        private bool face = true;
        private List<string> Clients = new List<string>();
        
        private void LoadClientsAndCompanies()
        {
            lb_clients.Items.Clear();
            lb_companies.Items.Clear();
            lb_cars.Items.Clear();
            lb_contracts.Items.Clear();

            List<Dictionary<string, string>> resultC = Sql.LoadClients();

            foreach(Dictionary<string, string> r in resultC)
                 lb_clients.Items.Add(r["name"]);

            string res = null;
            string[] result = null;

            res = Sql.LoadCompanies();
            if (res != null && res != string.Empty) {result = res.Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries); lb_companies.Items.AddRange(result);}

            res = null;
            res = Sql.LoadCars();
            if (res != null && res != string.Empty) { result = res.Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries); lb_cars.Items.AddRange(result); }

            res = null;
            res = Sql.LoadContracts();
            if (res != null && res != string.Empty) { result = res.Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries); lb_contracts.Items.AddRange(result); }
        }

        private void LoadCars()
        {
            cb_cars_type.Items.Clear();
            cb_cars_client.Items.Clear();

            List<Dictionary<string, string>> resultC = Sql.LoadClients();

            foreach(Dictionary<string, string> r in resultC)
            {
                cb_cars_client.Items.Add(r["name"]);
            }

            string res = Sql.LoadCarTypes();
            string[] result = res.Split(":".ToCharArray());

            for (int i = 0; i < result.Length; i+=2)
            {
                cb_cars_type.Items.Add(result[i]);
            }
        }

        private void lb_companies_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lb_companies.SelectedItem == null) return;

            te_companies_name.Text = lb_companies.SelectedItem.ToString();

            string[] res;
            res = Sql.getCompany(lb_companies.SelectedItem.ToString());
            if (res == null) this.Dispose();
            if (res[0] == null || res[0] == string.Empty) return;

            te_companies_adr.Text = res[1];
            te_companies_iban.Text = res[2];
            te_companies_bank.Text = res[3];
            te_companies_swift.Text = res[4];
            te_companies_timeAdd.Text = res[5] + " " + Convert.ToDateTime(res[6]).ToShortDateString();
            te_companies_timeChange.Text = res[7];

            btn_companies_act.Enabled = true;
            btn_companies_act.Click -= btn_companies_act_change;
            btn_companies_act.Click += btn_companies_act_change;
        }

        private void lb_clients_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lb_clients.SelectedItem == null) return;

            te_clients_name.Text = lb_clients.SelectedItem.ToString();

            string[] res;
            res = Sql.getClient(lb_clients.SelectedItem.ToString());

            if (res == null) this.Dispose();
            if (res[0] == null || res[0] == string.Empty) return;

            te_clients_face.Text = res[0];
            te_clients_inn.Text = res[1];
            te_clients_kpp.Text = res[2];
            te_clients_adr1.Text = res[3];
            te_clients_adr2.Text = res[4];
            te_clients_p_c.Text = res[5];
            te_clients_bank.Text = res[6];
            te_clients_bik.Text = res[7];
            te_clients_pc.Text = res[8];
            te_clients_ad.Text = res[9];
            te_clients_timeAdd.Text = res[10] + " " + Convert.ToDateTime(res[11]).ToShortDateString();
            te_clients_timeChange.Text = res[12];

            dynamic foreign = null;

            if(res[13] != null)
                foreign = JsonConvert.DeserializeObject(res[13]);

            if (foreign != null)
            {
                te_clients_swift.Text = foreign.foreign.swift;
                te_clients_iban.Text = foreign.foreign.iban;
                te_clients_bank2.Text = foreign.foreign.bank2;
            }
            else
            {
                te_clients_bank2.Text = "";
                te_clients_iban.Text = "";
                te_clients_swift.Text = "";
            }

            btn_clients_act.Enabled = true;
            btn_clients_act.Click -= btn_clients_act_change;
            btn_clients_act.Click += btn_clients_act_change;
        }

        private void btn_companies_act_change(object sender, EventArgs e)
        {
            btn_companies_act.Click -= btn_companies_act_change;
            btn_companies_act.Click += btn_companies_act_changeAndSave;
            btn_companies_act.Text = "Сохранить";

            te_companies_name.Enabled = true;
            te_companies_adr.Enabled = true;
            te_companies_iban.Enabled = true;
            te_companies_bank.Enabled = true;
            te_companies_swift.Enabled = true;

        }

        private void btn_companies_act_changeAndSave(object sender, EventArgs e)
        {         
            btn_companies_act.Click -= btn_companies_act_changeAndSave;
            btn_companies_act.Click += btn_companies_act_change;
            btn_companies_act.Text = "Изменить";
            btn_companies_act.Enabled = false;

            te_companies_name.Enabled = false;
            te_companies_adr.Enabled = false;
            te_companies_iban.Enabled = false;
            te_companies_bank.Enabled = false;
            te_companies_swift.Enabled = false;

            string mes = Sql.UpdateCompany(lb_companies.Text, te_companies_name.Text, te_companies_adr.Text, te_companies_iban.Text, te_companies_bank.Text, te_companies_swift.Text);

            frm_Main.Message(mes);

            LoadClientsAndCompanies();
        }

        private void btn_clients_act_change(object sender, EventArgs e)
        {
            btn_clients_act.Click -= btn_clients_act_change;
            btn_clients_act.Click += btn_clients_act_saveAndChange;
            btn_clients_act.Text = "Сохранить";

            te_clients_name.Enabled = true;
            te_clients_face.Enabled = true;
            te_clients_inn.Enabled = true;
            te_clients_kpp.Enabled = true;
            te_clients_adr1.Enabled = true;
            te_clients_adr2.Enabled = true;
            te_clients_p_c.Enabled = true;
            te_clients_bank.Enabled = true;
            te_clients_bik.Enabled = true;
            te_clients_pc.Enabled = true;
            te_clients_ad.Enabled = true;

            te_clients_bank2.Enabled = true;
            te_clients_iban.Enabled = true;
            te_clients_swift.Enabled = true;
        }

        private void btn_clients_act_saveAndChange(object sender, EventArgs e)
        {
            btn_clients_act.Click -= btn_clients_act_saveAndChange;
            btn_clients_act.Click += btn_clients_act_change;
            btn_clients_act.Text = "Изменить";
            btn_clients_act.Enabled = false;

            te_clients_name.Enabled = false;
            te_clients_face.Enabled = false;
            te_clients_inn.Enabled = false;
            te_clients_kpp.Enabled = false;
            te_clients_adr1.Enabled = false;
            te_clients_adr2.Enabled = false;
            te_clients_p_c.Enabled = false;
            te_clients_bank.Enabled = false;
            te_clients_bik.Enabled = false;
            te_clients_pc.Enabled = false;
            te_clients_ad.Enabled = false;

            te_clients_bank2.Enabled = false;
            te_clients_iban.Enabled = false;
            te_clients_swift.Enabled = false;

            Foreign fn = new Foreign();

            fn.foreign = new Dictionary<string, string>() { {"swift", te_clients_swift.Text}, {"iban", te_clients_iban.Text}, {"bank2", te_clients_bank2.Text}};

            string json = JsonConvert.SerializeObject(fn);

            string mes = Sql.UpdateClient(lb_clients.SelectedItem.ToString(), te_clients_name.Text, te_clients_face.Text, te_clients_inn.Text, te_clients_kpp.Text, te_clients_adr1.Text, te_clients_adr2.Text, te_clients_p_c.Text, te_clients_bank.Text, te_clients_bik.Text, te_clients_pc.Text, te_clients_ad.Text, json);

            frm_Main.Message(mes);

            LoadClientsAndCompanies();
        } 

        private void lb_cars_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lb_cars.SelectedItem == null) return;

            LoadCars();

            te_cars_name.Text = lb_cars.SelectedItem.ToString();

            string[] res = Sql.getCar(lb_cars.SelectedItem.ToString());

            cb_cars_type.SelectedIndex = cb_cars_type.Items.IndexOf(res[0]);
            cb_cars_client.SelectedIndex = cb_cars_client.Items.IndexOf(res[1]);

            btn_cars_act.Enabled = true;
            btn_cars_act.Click -= btn_cars_act_change;
            btn_cars_act.Click += btn_cars_act_change;
        }

        private void btn_cars_act_change(object sender, EventArgs e)
        {
            btn_cars_act.Click -= btn_cars_act_change;
            btn_cars_act.Click += btn_cars_act_changeAndSave;
            btn_cars_act.Text = "Сохранить";

            te_cars_name.Enabled = true;
            cb_cars_client.Enabled = true;
            cb_cars_type.Enabled = true;
        }

        private void btn_cars_act_changeAndSave(object sender, EventArgs e)
        {
            if (cb_cars_client.SelectedItem == null || cb_cars_type.SelectedItem == null) return;

            btn_cars_act.Click -= btn_cars_act_changeAndSave;
            btn_cars_act.Click += btn_cars_act_change;
            btn_cars_act.Text = "Изменить";
            btn_cars_act.Enabled = false;

            te_cars_name.Enabled = false;
            cb_cars_type.Enabled = false;
            cb_cars_client.Enabled = false;

            string mes = Sql.UpdateCar(lb_cars.SelectedItem.ToString(), te_cars_name.Text, cb_cars_client.Text, cb_cars_type.Text);

            frm_Main.Message(mes);

            LoadCars();
            LoadClientsAndCompanies();
        }

        private void lb_contracts_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lb_contracts.SelectedItem == null) return;
            LoadRoutesClient();

            btn_contracts_act.Enabled = true;
            te_contracts_name.Text = lb_contracts.SelectedItem.ToString();

            string[] res;
            res = Sql.getContract(lb_contracts.Text);
            if (res == null || res[0] == string.Empty) return;

            ClientOrcompany = res[1];
            te_contracts_number.Text = res[0];

            if (res[1] == "Компания") face = false;

            if (res[9] == null || res[9] == string.Empty) LoadCC(res[10]); else LoadCC(res[9]);

            te_contracts_timeAdd.Text = res[6] + " " + Convert.ToDateTime(res[7]).ToShortDateString();
            te_contracts_timeEdit.Text = res[8];
            cb_contracts_route.Text = res[11];

            dynamic periods = JsonConvert.DeserializeObject(res[13]);

            if (periods == null) return;

            for(int i = 0; i < 7; i++)
            {
                int n = 0;

                ((DataGridView)tabc_contracts.TabPages["tp_" + i.ToString()].Controls["dgv_" + i.ToString()]).Rows.Clear();

                while(periods.Days["D" + i.ToString()]["P" + n.ToString()] != null)
                {
                    if (periods.Days["D" + i.ToString()]["P" + n.ToString()].Time1 != null ||
                        periods.Days["D" + i.ToString()]["P" + n.ToString()].Time2 != null ||
                        periods.Days["D" + i.ToString()]["P" + n.ToString()].Price1 != null ||
                        periods.Days["D" + i.ToString()]["P" + n.ToString()].Price2 != null ||
                        periods.Days["D" + i.ToString()]["P" + n.ToString()].Lenght != null)
                    {
                        ((DataGridView)tabc_contracts.TabPages["tp_" + i.ToString()].Controls["dgv_" + i.ToString()]).Rows.Add(
                            periods.Days["D" + i.ToString()]["P" + n.ToString()].Time1 == null ? "00:00:00" : periods.Days["D" + i.ToString()]["P" + n.ToString()].Time1,
                            periods.Days["D" + i.ToString()]["P" + n.ToString()].Time2 == null ? "00:00:00" : periods.Days["D" + i.ToString()]["P" + n.ToString()].Time2,
                            periods.Days["D" + i.ToString()]["P" + n.ToString()].Price1 == null ? "0,00" : periods.Days["D" + i.ToString()]["P" + n.ToString()].Price1,
                            periods.Days["D" + i.ToString()]["P" + n.ToString()].Price2 == null ? "0,00" : periods.Days["D" + i.ToString()]["P" + n.ToString()].Price2,
                            periods.Days["D" + i.ToString()]["P" + n.ToString()].Lenght == null ? "0,00" : periods.Days["D" + i.ToString()]["P" + n.ToString()].Lenght
                        );
                    }

                    n++;
                }
            }

            dynamic ads = JsonConvert.DeserializeObject(res[15]);

            if (ads == null) return;

            int b = 0;

            dgv_ad.Rows.Clear();

            while(ads.AD["V" + b.ToString()] != null)
            {
                dgv_ad.Rows.Add(null, ads.AD["V" + b.ToString()].price1, ads.AD["V" + b.ToString()].price2, ads.AD["V" + b.ToString()].weight);
                string name = ads.AD["V" + b.ToString()].name;
                if (((DataGridViewComboBoxCell)dgv_ad.Rows[b].Cells[0]).Items.Contains(name.Trim()))
                    ((DataGridViewComboBoxCell)dgv_ad.Rows[b].Cells[0]).Value = name.Trim();
                b++;
            }
        }

        private void BlockOnTime(Control obj)
        {
            try
            {
                obj.Invoke((MethodInvoker)(() => obj.Enabled = false));
                Thread.Sleep(2000);
                obj.Invoke((MethodInvoker)(() => obj.Enabled = true));
            }
            catch
            {
            
            }
        }

        private void LoadCC(string name)
        {
            cb_contracts_face.Items.Clear();

            List<Dictionary<string, string>> resultC = Sql.LoadClients();

            foreach(Dictionary<string, string> r in resultC)
            {
                cb_contracts_face.Items.Add(r["name"]);
                Clients.Add(r["name"]);
            }

            string res = Sql.LoadCompanies();
            string[] result = res.Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Select(str => str.Trim()).Where(str => str != string.Empty).ToArray();

            cb_contracts_face.Items.AddRange(result);


            List<Dictionary<string, string>> sql = Sql.getClientFromId(name);
            string getname = (sql != null ? sql[0]["name"] : null);
            if (getname != null)
            if (cb_contracts_face.Items.Contains(getname))
            {
                cb_contracts_face.SelectedItem = getname;
                cb_contracts_face.Text = getname;
            }

            sql = Sql.getCompanyFromId(name);
            getname = (sql != null ? sql[0]["name"] : null);
            if(getname != null)
            if (cb_contracts_face.Items.Contains(getname))
            {
                cb_contracts_face.SelectedItem = getname;
                cb_contracts_face.Text = getname;
            }

            res = Sql.LoadAd();
            if (res == null) return;
            result = res.Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            ((DataGridViewComboBoxColumn)dgv_ad.Columns["name"]).Items.AddRange(result);         
        }

        private void LoadRoutesClient()
        {
            string res = Sql.LoadRoutes();
            string[] result = res.Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            cb_contracts_route.Items.AddRange(result);
        }

        private void btn_contracts_act_Click(object sender, EventArgs e)
        {
            btn_contracts_act.Click -= btn_contracts_act_Click;
            btn_contracts_act.Click += btn_contracts_act_change;
            btn_contracts_act.Text = "Сохранить";

            te_contracts_name.Enabled = true;
            cb_contracts_face.Enabled = true;
            cb_contracts_route.Enabled = true;
            tabc_contracts.Enabled = true;
            dgv_ad.Enabled = true;
        }

        private void btn_contracts_act_change(object sender, EventArgs e)
        {
            if (te_contracts_name.Text == string.Empty) return;
   
            btn_contracts_act.Click -= btn_contracts_act_change;
            btn_contracts_act.Click += btn_contracts_act_Click;
            btn_contracts_act.Text = "Изменить";

            te_contracts_name.Enabled = false;
            cb_contracts_face.Enabled = false;
            cb_contracts_route.Enabled = false;
            tabc_contracts.Enabled = false;
            dgv_ad.Enabled = false;

            TimePeriods tp = new TimePeriods();
            Dictionary<string, Dictionary<string, TimePeriodsValue>> days = new Dictionary<string, Dictionary<string, TimePeriodsValue>>();

            for (int i = 0; i < 7; i++)
            {
                Dictionary<string, TimePeriodsValue> dict = new Dictionary<string, TimePeriodsValue>();

                for (int j = 0; j < ((DataGridView)tabc_contracts.TabPages["tp_" + i.ToString()].Controls["dgv_" + i.ToString()]).RowCount - 1; j++)
                {
                    TimePeriodsValue tpva = new TimePeriodsValue();

                    DataGridView dgv = ((DataGridView)tabc_contracts.TabPages["tp_" + i.ToString()].Controls["dgv_" + i.ToString()]);

                    tpva.Time1 = dgv.Rows[j].Cells[0].Value.ToString();
                    tpva.Time2 = dgv.Rows[j].Cells[1].Value.ToString();
                    tpva.Price1 = dgv.Rows[j].Cells[2].Value.ToString();
                    tpva.Price2 = dgv.Rows[j].Cells[3].Value.ToString();
                    tpva.Lenght = dgv.Rows[j].Cells[4].Value.ToString();

                    dict.Add("P" + j.ToString(), tpva);
                }

                days.Add("D" + i.ToString(), dict);
            }
            tp.Days = days;

            string json = JsonConvert.SerializeObject(tp, Formatting.Indented);


            Additional ad = new Additional();
            Dictionary<string, AdditionalValue> adop = new Dictionary<string, AdditionalValue>();
                
            for (int i = 0; i < dgv_ad.RowCount - 1; i++)
            {
                AdditionalValue adv = new AdditionalValue();

                adv.name = dgv_ad.Rows[i].Cells[0].Value.ToString();
                adv.price1 = dgv_ad.Rows[i].Cells[1].Value == null ? "0,00" : dgv_ad.Rows[i].Cells[1].Value.ToString();
                adv.price2 = dgv_ad.Rows[i].Cells[2].Value == null ? "0,00" : dgv_ad.Rows[i].Cells[2].Value.ToString();
                adv.weight = dgv_ad.Rows[i].Cells[3].Value == null ? "0,00" : dgv_ad.Rows[i].Cells[3].Value.ToString();

                adop.Add("V" + i.ToString(), adv);
            }

            ad.AD = adop;

            string ads = JsonConvert.SerializeObject(ad, Formatting.Indented);

            string res = null;

            res = Sql.UpdateContract(te_contracts_number.Text, te_contracts_name.Text, cb_contracts_route.Text, json, (Clients.Contains(cb_contracts_face.Text) ? "Клиент" : "Компания"), cb_contracts_face.Text, ads);

            MessageBox.Show(res);

            LoadClientsAndCompanies();
        }


        private void btn_copy_Click(object sender, EventArgs e)
        {
            if (lb_contracts.SelectedItem != null && te_contracts_number.Text != null && te_contracts_number.Text != string.Empty)
            {
                string mes = Sql.CopyContract(te_contracts_number.Text);
                frm_Main.Message(mes);
                LoadClientsAndCompanies();
            }
        }

        private DataGridView Copied;

        private void button1_Click(object sender, EventArgs e)
        {
            Copied = ((DataGridView)tabc_contracts.SelectedTab.Controls["dgv_" + tabc_contracts.SelectedIndex.ToString()]);
        }

        private void CopyDataGridView(DataGridView dgv_org)
        {
            dgv_org.Rows.Clear();
            dgv_org.Columns.Clear();

            try
            {
                if (dgv_org.Columns.Count == 0)
                {
                    foreach (DataGridViewColumn dgvc in Copied.Columns)
                    {
                        dgv_org.Columns.Add(dgvc.Clone() as DataGridViewColumn);
                    }
                }

                DataGridViewRow row = new DataGridViewRow();

                for (int i = 0; i < Copied.Rows.Count - 1; i++)
                {
                    row = (DataGridViewRow)Copied.Rows[i].Clone();
                    int intColIndex = 0;
                    foreach (DataGridViewCell cell in Copied.Rows[i].Cells)
                    {
                        row.Cells[intColIndex].Value = cell.Value;
                        intColIndex++;
                    }
                    dgv_org.Rows.Add(row);
                }

                dgv_org.Refresh();

            }
            catch (Exception ex)
            {

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(Copied != null)
                CopyDataGridView(((DataGridView)tabc_contracts.SelectedTab.Controls["dgv_" + tabc_contracts.SelectedIndex.ToString()]));
        }

        private void dgv_ad_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == name.DisplayIndex)
            {
                if (!this.name.Items.Contains(e.FormattedValue))
                {
                    this.name.Items.Add(e.FormattedValue);
                }
            }
        }

        private void dgv_ad_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (dgv_ad.CurrentCell.ColumnIndex == 0)
            {
                ComboBox combo = e.Control as ComboBox;

                if (combo == null)
                    return;

                combo.DropDownStyle = ComboBoxStyle.DropDown;
            }
        }
    }
}
