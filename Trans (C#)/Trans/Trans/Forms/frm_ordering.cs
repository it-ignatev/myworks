﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;

namespace Trans.Forms
{
    public partial class frm_ordering : Form
    {
        private string client = "##########";
        private Dictionary<string, string> SqlQuery = new Dictionary<string, string>();
        private int invs = 0;
        private string[] dates = null;
        private string[] dates2 = null;

        private string[] inv = null;
        private string[] price1 = null, price2 = null;

        private string[] name = null;

        private int substract = 1;
        private string[] credit = null, debit = null, saldo = null;

        private decimal AllDebit = 0, AllCredit = 0, LastSaldo = 0;

        private int AddValue = 140;

        private string Currency = "евро";


        public frm_ordering()
        {
            InitializeComponent();

            Load();
        }

        private void Load()
        {
            dateTimePicker1.Value = Convert.ToDateTime("01." + DateTime.Now.ToString("MM.yyyy") + " 00:00:00");
            dateTimePicker2.Value = Convert.ToDateTime(DateTime.Now.ToString("dd.MM.yyyy") + " 00:00:00");

            List<Dictionary<string, string>> res = Sql.LoadClients();

            foreach(Dictionary<string, string> r in res)
            {
                comboBox1.Items.Add(r["name"]);
            }

            pictureBox1.Image = new Bitmap(706, 650 + AddValue);
            pictureBox1.Height = pictureBox1.Image.Height;
            pictureBox1.Width = pictureBox1.Image.Width;

            pictureBox1_Paint(null, null);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            client = comboBox1.Text;
            btn_addOr.Enabled = true;

            SqlQuery.Remove("SELECT_NAME");
            SqlQuery.Add("SELECT_NAME", client);

            LoadInv();
            Upd();
        }

        private void LoadInv()
        {            
            string[] res = Sql.LoadInvoices(SqlQuery);
            string[] id = null;
            dates = null;
            inv = null;
            price1 = null;
            dates2 = null;
            name = null;
            credit = null;
            debit = null;
            saldo = null;
            invs = 0;

            if (res != null)
            {
                if (res[0] != null)
                {

                    id = res[0].Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);


                    dates = res[5].Split("!".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    inv = res[2].Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    price1 = res[3].Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    price2 = res[4].Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                }
            }
            else { invs = 0; }

            SqlQuery.Clear();
            SqlQuery.Add("SELECT_CLIENT", client);
            SqlQuery.Add("SELECT_DTP_1", dateTimePicker1.Value.ToString("yyyy-MM-dd HH:mm:ss"));
            SqlQuery.Add("SELECT_DTP_2", dateTimePicker2.Value.ToString("yyyy-MM-dd HH:mm:ss"));

            res = Sql.LoadOrdering(SqlQuery);

            if (res[0] != null)
            {
                dates2 = res[4].Split("!".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                name = res[1].Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                credit = res[2].Split(":".ToCharArray());
                debit = res[3].Split(":".ToCharArray());
                saldo = res[5].Split(":".ToCharArray());

                if (id != null) { invs = id.Length + dates2.Length; }
                else
                {
                    invs = dates2.Length;
                }
            }
            else
            {
                if (id != null) invs = id.Length; else invs = 0;
            }

            if(pictureBox1.Image != null) pictureBox1.Image.Dispose();
            pictureBox1.Image = new Bitmap(706, 250 + (invs * 70) + AddValue);
            pictureBox1.Height = pictureBox1.Image.Height;
            pictureBox1.Width = pictureBox1.Image.Width;
        }

        private void DrawInv(Graphics g, int count)
        {
            if (count < 1) return;
            AllDebit = 0;
            AllCredit = 0;
            LastSaldo = 0;
            int line = 0;
            List<int> dat = new List<int>();
            int salds = 0;
         
            if (saldo != null)
            {
                if (saldo.Length <= 1)
                {
                    for (int i = 0; i < saldo.Length; i++)
                    {
                        if (saldo[i] != null)
                        {
                            if (saldo[i] == "1")
                            {
                                g.DrawString(name[i], new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 26, 92 + (line * 20) + AddValue);
                                g.DrawString(debit[i], new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 212, 94 + (line * 20) + AddValue);
                                //AllDebit += Convert.ToDecimal(string.IsNullOrEmpty(debit[i]) ? "0,00" : debit[i]);
                                line += 1;
                                salds += 1;
                                LastSaldo = Convert.ToDecimal(debit[i]);
                                dat.Add(i);
                            }

                        }
                    }
                }
                else
                {
                    g.DrawString(name[0], new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 26, 92 + (line * 20) + AddValue);
                    g.DrawString(debit[0], new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 212, 94 + (line * 20) + AddValue);
                    //AllDebit += Convert.ToDecimal(string.IsNullOrEmpty(debit[0]) ? "0,00" : debit[0]);
                    LastSaldo = Convert.ToDecimal((string.IsNullOrEmpty(debit[0])) ? "0,00" : debit[0]);
                    dat.Add(0);
                    line += 1;
                    salds += 1;
                }

                for (int i = 0; i < dates2.Length; i++)
                {
                    if (saldo[i] != "1" || dat.Contains(i)) continue;
                    count -= 1;
                    dat.Add(i);
                }
            }

            if (salds == 0)
            {
                g.DrawString("Сальдо на " + dateTimePicker1.Value.ToString("dd/MM/yyyy HH:mm"), new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 26, 92 + (line * 20) + AddValue);
                g.DrawString("0, 00", new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 212, 94 + (line * 20) + AddValue);
                line += 1;
                count += 1;
            }

            if (dates != null)
            {
                for (int i = 0; i < dates.Length; i++)
                {
                    if (dates2 != null)
                    {
                        for (int j = 0; j < dates2.Length; j++)
                        {
                            if (dat.Contains(j)) { substract += 1; continue; }
                            

                            if (DateTime.Compare(Convert.ToDateTime(dates2[j]), Convert.ToDateTime(dates[i])) < 0)
                            {
                                g.DrawString(name[j], new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 26, 92 + (line * 20) + AddValue);
                                g.DrawString(debit[j], new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 212, 94 + (line * 20) + AddValue);
                                g.DrawString(credit[j], new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 282, 94 + (line * 20) + AddValue);

                                dat.Add(j);
                                line += 1;
                                AllDebit += Convert.ToDecimal(string.IsNullOrEmpty(debit[j]) ? "0,00" : debit[j]);
                                AllCredit += Convert.ToDecimal(string.IsNullOrEmpty(credit[j]) ? "0,00" : credit[j]);
                            }
                        }
                    }

                    if (Convert.ToDecimal(price2[i]) > 0 || Currency == "евро")
                    {
                        g.DrawString("счет от " + Convert.ToDateTime(dates[i]).ToString("dd/MM/yyyy HH:mm") + " №" + inv[i], new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 26, 92 + (line * 20) + AddValue);
                        g.DrawString(((Currency == "евро") ? price1[i] : price2[i]), new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 212, 94 + (line * 20) + AddValue);
                        AllDebit += Convert.ToDecimal(((Currency == "евро") ? price1[i] : price2[i]));
                        line += 1;
                    }
                    else
                    {
                        count -= 1;
                    }
                }
            }

            if (dates2 != null)
            {
                if (dat.Count < dates2.Length)
                { 
                    for (int i = 0; i < dates2.Length; i++)
                    {
                        if (!dat.Contains(i))
                        {
                            substract = 1 + salds;

                            for (int j = 0; j < dates2.Length; j++)
                            {
                                if (dat.Contains(j)) { continue; }

                                if (DateTime.Compare(Convert.ToDateTime(dates2[j]), Convert.ToDateTime(dates2[i])) < 0)
                                {
                                    g.DrawString(name[j], new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 26, 92 + (line * 20) + AddValue);
                                    g.DrawString(debit[j], new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 212, 94 + (line * 20) + AddValue);
                                    g.DrawString(credit[j], new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 282, 94 + (line * 20) + AddValue);

                                    line += 1;
                                    dat.Add(j);
                                    substract += 1;
                                    AllDebit += Convert.ToDecimal(string.IsNullOrEmpty(debit[j]) ? "0,00" : debit[j]);
                                    AllCredit += Convert.ToDecimal(string.IsNullOrEmpty(credit[j]) ? "0,00" : credit[j]);
                                }
                            }
                        }
                    }

                    int max = 0;

                    for(int i=0;i<dates2.Length;i++)
                    {
                        for(int k=0;k<dates2.Length;k++)
                        {
                            if (DateTime.Compare(Convert.ToDateTime(dates2[k]), Convert.ToDateTime(dates2[i])) > 0)
                            {
                                max = k;
                            }
                        }
                    }

                    g.DrawString(name[max], new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 26, 92 + (line * 20) + AddValue);
                    g.DrawString(debit[max], new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 212, 94 + (line * 20) + AddValue);
                    g.DrawString(credit[max], new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 282, 94 + (line * 20) + AddValue);

                    line += 1;
                    dat.Add(max);
                    substract += 1;
                    AllDebit += Convert.ToDecimal(string.IsNullOrEmpty(debit[max]) ? "0,00" : debit[max]);
                    AllCredit += Convert.ToDecimal(string.IsNullOrEmpty(credit[max]) ? "0,00" : credit[max]);
                }
            }

            g.DrawString("Обороты за период", new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 26, 92 + (line * 20) + AddValue);
            g.DrawString(Math.Round(AllDebit, 2).ToString(), new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 212, 94 + (line * 20) + AddValue);
            g.DrawString(Math.Round(AllCredit, 2).ToString(), new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 282, 94 + (line * 20) + AddValue);
            line += 1;
            count += 1;

            g.DrawString("Сальдо на " + dateTimePicker2.Value.ToString("dd.MM.yyyy"), new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 26, 92 + (line * 20) + AddValue);
            g.DrawString(Math.Round(AllDebit + LastSaldo - AllCredit, 2).ToString(), new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 212, 94 + (line * 20) + AddValue);
            line += 1;
            count += 1;

            for (int i = 0; i < count; i++)
            {
                Point[] p = { new Point(4, 90 + (i * 20) + AddValue), new Point(4, 110 + (i * 20) + AddValue), 
                                new Point(25, 110 + (i * 20) + AddValue), new Point(25, 90 + (i * 20) + AddValue), new Point(25, 110 + (i * 20) + AddValue),
                                new Point(210, 110 + (i * 20) + AddValue), new Point(210, 90 + (i * 20) + AddValue), new Point(210, 110 + (i * 20) + AddValue),
                                new Point(280, 110 + (i * 20) + AddValue), new Point(280, 90 + (i * 20) + AddValue), new Point(280, 110 + (i * 20) + AddValue),
                                new Point(354, 110 + (i * 20) + AddValue), new Point(354, 90 + (i * 20) + AddValue), new Point(354, 110 + (i * 20) + AddValue),
                                new Point(375, 110 + (i * 20) + AddValue), new Point(375, 90 + (i * 20) + AddValue), new Point(375, 110 + (i * 20) + AddValue),
                                new Point(564, 110 + (i * 20) + AddValue), new Point(564, 90 + (i * 20) + AddValue), new Point(564, 110 + (i * 20) + AddValue),
                                new Point(634, 110 + (i * 20) + AddValue), new Point(634, 90 + (i * 20) + AddValue), new Point(634, 110 + (i * 20) + AddValue),
                                new Point(704, 110 + (i * 20) + AddValue), new Point(704, 90 + (i * 20) + AddValue), new Point(704, 110 + (i * 20) + AddValue)
                            };

                g.DrawLines(new Pen(Brushes.Black, 1), p);

                g.DrawString((i + 1).ToString(), new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 8, 94 + (i * 20) + AddValue);
            }
        }

        private void Upd()
        {
            pictureBox1.Refresh();
            pictureBox1.Invalidate();
        }

        private void DrawTable(Graphics g)
        {
            g.DrawString("По данным ООО \"Лидер\", " + Currency, new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 5, 45 + AddValue);
            g.DrawString("По данным " + client + ", " + Currency, new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 355, 45 + AddValue);

            g.DrawString(" № \r\nп/п", new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 5, 61 + AddValue);
            g.DrawString("Наименование операции,\r\n           документы", new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 48, 61 + AddValue);
            g.DrawString("Дебет", new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 224, 68 + AddValue);
            g.DrawString("Кредит", new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 300, 68 + AddValue);

            g.DrawString(" № \r\nп/п", new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 355, 61 + AddValue);
            g.DrawString("Наименование операции,\r\n           документы", new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 400, 61 + AddValue);
            g.DrawString("Дебет", new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 580, 68 + AddValue);
            g.DrawString("Кредит", new Font("Arial", 8, FontStyle.Regular), Brushes.Black, 650, 68 + AddValue);

            Point[] p = { new Point(4, 60 + AddValue), new Point(4, 44 + AddValue), 
                            new Point(354, 44 + AddValue), new Point(354, 60 + AddValue), new Point(354, 44 + AddValue),
                            new Point(704, 44 + AddValue),new Point(704, 60 + AddValue), new Point(4, 60 + AddValue),

                            new Point(4,90 + AddValue), new Point(25,90 + AddValue), new Point(25,60 + AddValue), new Point(25,90 + AddValue),
                            new Point(210,90 + AddValue), new Point(210,60 + AddValue), new Point(210,90 + AddValue),
                            new Point(280,90 + AddValue), new Point(280,60 + AddValue),new Point(280,90 + AddValue),
                            new Point(354,90 + AddValue), new Point(354,60 + AddValue),new Point(354,90 + AddValue),

                            new Point(375,90 + AddValue), new Point(375,60 + AddValue),new Point(375,90 + AddValue),
                            new Point(564,90 + AddValue), new Point(564,60 + AddValue),new Point(564,90 + AddValue),
                            new Point(634,90 + AddValue), new Point(634,60 + AddValue),new Point(634,90 + AddValue),
                            new Point(704,90 + AddValue), new Point(704,60 + AddValue),new Point(704,90 + AddValue)
                        };

            g.DrawLines(new Pen(Brushes.Black, 1), p);
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = Graphics.FromImage(pictureBox1.Image);
            g.Clear(Color.White);

            g.DrawString("АКТ СВЕРКИ", new Font("Arial", 11, FontStyle.Bold), Brushes.Black, 310, 5);
            g.DrawString("взаимных расчетов по состоянию на " + dateTimePicker2.Value.ToString("dd.MM.yyyy") + "\r\n между " + client + "\r\n                        и ООО \"Лидер\"", new Font("Arial", 10, FontStyle.Regular), Brushes.Black, 240, 25);
            g.DrawString("Мы, нижеподписавшиеся, __________________________________________, с одной стороны, и\r\n_________________ ООО \"Лидер\" _____________. составили настоящий акт сверки о том, что состояние\r\nвзаимных расчетов по данным бухгатерского учета следущее:", new Font("Arial", 10, FontStyle.Regular), Brushes.Black, 4, 100);

            DrawTable(g);
            DrawInv(g, invs);


            g.DrawString("По данным ООО \"Лидер\"", new Font("Arial", 10, FontStyle.Regular), Brushes.Black, 4, 160 + (invs * 21) + AddValue);
            g.DrawString("на " + dateTimePicker2.Value.ToString("dd.MM.yyyy") + " задолженность в пользу " + ((AllDebit + LastSaldo - AllCredit < 0) ? client + " " : "ООО \"Лидер\" ") + ((AllDebit + LastSaldo - AllCredit != 0) ? (Math.Abs(AllDebit + LastSaldo - AllCredit)).ToString() + " " + Currency : "отсутствует"), new Font("Arial", 10, FontStyle.Bold), Brushes.Black, 4, 190 + (invs * 21) + AddValue);
            g.DrawString("ООО \"Лидер\"", new Font("Arial", 10, FontStyle.Regular), Brushes.Black, 4, 220 + (invs * 21) + AddValue);
            g.DrawString("_______________________  (____________)", new Font("Arial", 10, FontStyle.Regular), Brushes.Black, 4, 240 + (invs * 21) + AddValue);

            g.DrawString(client, new Font("Arial", 10, FontStyle.Regular), Brushes.Black, 357, 220 + (invs * 21) + AddValue);
            g.DrawString("_______________________  (____________)", new Font("Arial", 10, FontStyle.Regular), Brushes.Black, 357, 240 + (invs * 21) + AddValue);
        }

        private void btn_addOr_Click(object sender, EventArgs e)
        {
            Form frm_add = new Forms.frm_ordering_add(comboBox1.Text, AllDebit + LastSaldo - AllCredit, dateTimePicker2.Value);
            frm_add.FormClosing += Frm_Add_closing;
            frm_add.ShowDialog();
        }

        private void Frm_Add_closing(object s, EventArgs e)
        {
            SqlQuery.Add("SELECT_NAME", client);

            LoadInv();
            Upd();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            if(SqlQuery.ContainsKey("SELECT_DTP_1")) SqlQuery.Remove("SELECT_DTP_1");

            SqlQuery.Add("SELECT_DTP_1", dateTimePicker1.Value.ToString("yyyy-MM-dd HH:mm:ss"));
            if (!SqlQuery.ContainsKey("SELECT_NAME")) SqlQuery.Add("SELECT_NAME", client);

            LoadInv();
            Upd();
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            if(SqlQuery.ContainsKey("SELECT_DTP_2")) SqlQuery.Remove("SELECT_DTP_2");

            SqlQuery.Add("SELECT_DTP_2", dateTimePicker2.Value.ToString("yyyy-MM-dd HH:mm:ss"));
            if (!SqlQuery.ContainsKey("SELECT_NAME")) SqlQuery.Add("SELECT_NAME", client);

            LoadInv();
            Upd();
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            string name = "CBEPKA (" + DateTime.Now.ToString("yy/MM/dd") + ") " + client.Replace("\"", "");

            if (!Directory.Exists(Application.StartupPath + "\\TEMP\\")) Directory.CreateDirectory(Application.StartupPath + "\\TEMP\\");
            if (!Directory.Exists(Application.StartupPath + "\\СВЕРКИ\\")) Directory.CreateDirectory(Application.StartupPath + "\\СВЕРКИ\\");

            pictureBox1.Image.Save(Application.StartupPath + "\\TEMP\\" + name + ".jpg");

            try
            {
                string source = Application.StartupPath + "\\TEMP\\" + name + ".jpg";
                string destinaton = Application.StartupPath + "\\СВЕРКИ\\" + name + ".pdf";

                //PdfDocument doc = new PdfDocument();
                //doc.Pages.Add(new PdfPage());
                //XGraphics xgr = XGraphics.FromPdfPage(doc.Pages[0]);
                //XImage img = XImage.FromFile(source);

                //xgr.DrawImage(img, 30, 30);
                //doc.Save(destinaton);
                //doc.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                this.Dispose();
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            Currency = comboBox2.Text;
            pictureBox1_Paint(null, null);
            Upd();
        }
    }
}
