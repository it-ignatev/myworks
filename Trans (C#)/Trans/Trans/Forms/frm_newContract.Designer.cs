﻿namespace Trans
{
    partial class frm_newContract
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle54 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle51 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle52 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle53 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle60 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle55 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle56 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle57 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle58 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle59 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle66 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle61 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle62 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle63 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle64 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle65 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle72 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle67 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle68 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle69 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle70 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle71 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle78 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle73 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle74 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle75 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle76 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle77 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle84 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle79 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle80 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle81 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle82 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle83 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle90 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle85 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle86 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle87 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle88 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle89 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.contract_physic = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            this.dgv_ad = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tab_periods = new System.Windows.Forms.TabControl();
            this.monday = new System.Windows.Forms.TabPage();
            this.dgv_0 = new System.Windows.Forms.DataGridView();
            this.time1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.time2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lenght = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tuesday = new System.Windows.Forms.TabPage();
            this.dgv_1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wednesday = new System.Windows.Forms.TabPage();
            this.dgv_2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.thursday = new System.Windows.Forms.TabPage();
            this.dgv_3 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.friday = new System.Windows.Forms.TabPage();
            this.dgv_4 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.saturday = new System.Windows.Forms.TabPage();
            this.dgv_5 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sunday = new System.Windows.Forms.TabPage();
            this.dgv_6 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cb_route1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cb_clients = new System.Windows.Forms.ComboBox();
            this.contract_uridic = new System.Windows.Forms.TabPage();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btn_comp_save = new System.Windows.Forms.Button();
            this.dgv_ad_companies = new System.Windows.Forms.DataGridView();
            this.ad_c_name = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn66 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn67 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn68 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label8 = new System.Windows.Forms.Label();
            this.tab_perdiods_companies = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dgv_comp_0 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dgv_comp_1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dgv_comp_2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn41 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn43 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn44 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn45 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dgv_comp_3 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn46 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn47 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn48 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn49 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn50 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.dgv_comp_4 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn51 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn52 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn53 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn54 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn55 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.dgv_comp_5 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn56 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn57 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn58 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn59 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn60 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.dgv_comp_6 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn61 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn62 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn63 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn64 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn65 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cb_route2 = new System.Windows.Forms.ComboBox();
            this.cb_companies = new System.Windows.Forms.ComboBox();
            this.nameOfAdDGV = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ad_price1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ad_price2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.weight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1.SuspendLayout();
            this.contract_physic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ad)).BeginInit();
            this.tab_periods.SuspendLayout();
            this.monday.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_0)).BeginInit();
            this.tuesday.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_1)).BeginInit();
            this.wednesday.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_2)).BeginInit();
            this.thursday.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_3)).BeginInit();
            this.friday.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_4)).BeginInit();
            this.saturday.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_5)).BeginInit();
            this.sunday.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_6)).BeginInit();
            this.contract_uridic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ad_companies)).BeginInit();
            this.tab_perdiods_companies.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_comp_0)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_comp_1)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_comp_2)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_comp_3)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_comp_4)).BeginInit();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_comp_5)).BeginInit();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_comp_6)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.contract_physic);
            this.tabControl1.Controls.Add(this.contract_uridic);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(567, 480);
            this.tabControl1.TabIndex = 2;
            // 
            // contract_physic
            // 
            this.contract_physic.Controls.Add(this.button2);
            this.contract_physic.Controls.Add(this.button1);
            this.contract_physic.Controls.Add(this.btn_save);
            this.contract_physic.Controls.Add(this.dgv_ad);
            this.contract_physic.Controls.Add(this.label4);
            this.contract_physic.Controls.Add(this.label3);
            this.contract_physic.Controls.Add(this.tab_periods);
            this.contract_physic.Controls.Add(this.cb_route1);
            this.contract_physic.Controls.Add(this.label2);
            this.contract_physic.Controls.Add(this.label1);
            this.contract_physic.Controls.Add(this.cb_clients);
            this.contract_physic.Location = new System.Drawing.Point(4, 26);
            this.contract_physic.Name = "contract_physic";
            this.contract_physic.Size = new System.Drawing.Size(559, 450);
            this.contract_physic.TabIndex = 0;
            this.contract_physic.Text = "Физическое лицо";
            this.contract_physic.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(497, 60);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(54, 23);
            this.button2.TabIndex = 11;
            this.button2.Text = "Встав";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(497, 35);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(54, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Скоп";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(422, 418);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(129, 29);
            this.btn_save.TabIndex = 9;
            this.btn_save.Text = "Добавить";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // dgv_ad
            // 
            this.dgv_ad.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_ad.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_ad.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameOfAdDGV,
            this.ad_price1,
            this.ad_price2,
            this.weight});
            this.dgv_ad.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgv_ad.Location = new System.Drawing.Point(11, 239);
            this.dgv_ad.Name = "dgv_ad";
            this.dgv_ad.Size = new System.Drawing.Size(540, 174);
            this.dgv_ad.TabIndex = 8;
            this.dgv_ad.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgv_ad_CellValidating);
            this.dgv_ad.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgv_ad_EditingControlShowing);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 219);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "Доп.Опции:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Периоды:";
            // 
            // tab_periods
            // 
            this.tab_periods.Controls.Add(this.monday);
            this.tab_periods.Controls.Add(this.tuesday);
            this.tab_periods.Controls.Add(this.wednesday);
            this.tab_periods.Controls.Add(this.thursday);
            this.tab_periods.Controls.Add(this.friday);
            this.tab_periods.Controls.Add(this.saturday);
            this.tab_periods.Controls.Add(this.sunday);
            this.tab_periods.Location = new System.Drawing.Point(11, 61);
            this.tab_periods.Name = "tab_periods";
            this.tab_periods.SelectedIndex = 0;
            this.tab_periods.Size = new System.Drawing.Size(540, 155);
            this.tab_periods.TabIndex = 5;
            // 
            // monday
            // 
            this.monday.Controls.Add(this.dgv_0);
            this.monday.Location = new System.Drawing.Point(4, 26);
            this.monday.Name = "monday";
            this.monday.Size = new System.Drawing.Size(532, 125);
            this.monday.TabIndex = 0;
            this.monday.Text = "Monday";
            this.monday.UseVisualStyleBackColor = true;
            // 
            // dgv_0
            // 
            this.dgv_0.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_0.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_0.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.time1,
            this.time2,
            this.price1,
            this.price2,
            this.lenght});
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.Format = "N2";
            dataGridViewCellStyle9.NullValue = "0";
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_0.DefaultCellStyle = dataGridViewCellStyle9;
            this.dgv_0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_0.Location = new System.Drawing.Point(0, 0);
            this.dgv_0.Name = "dgv_0";
            this.dgv_0.Size = new System.Drawing.Size(532, 125);
            this.dgv_0.TabIndex = 0;
            this.dgv_0.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dataGridView1_CellValidating);
            // 
            // time1
            // 
            dataGridViewCellStyle4.Format = "H:i:s";
            dataGridViewCellStyle4.NullValue = "00:00:00";
            this.time1.DefaultCellStyle = dataGridViewCellStyle4;
            this.time1.HeaderText = "Время1";
            this.time1.Name = "time1";
            // 
            // time2
            // 
            dataGridViewCellStyle5.Format = "T";
            dataGridViewCellStyle5.NullValue = "00:00:00";
            this.time2.DefaultCellStyle = dataGridViewCellStyle5;
            this.time2.HeaderText = "Время2";
            this.time2.Name = "time2";
            // 
            // price1
            // 
            dataGridViewCellStyle6.Format = "C2";
            dataGridViewCellStyle6.NullValue = "0,00";
            this.price1.DefaultCellStyle = dataGridViewCellStyle6;
            this.price1.HeaderText = "Груженая";
            this.price1.Name = "price1";
            // 
            // price2
            // 
            dataGridViewCellStyle7.Format = "C2";
            dataGridViewCellStyle7.NullValue = "0,00";
            this.price2.DefaultCellStyle = dataGridViewCellStyle7;
            this.price2.HeaderText = "Негруженая";
            this.price2.Name = "price2";
            // 
            // lenght
            // 
            dataGridViewCellStyle8.Format = "N2";
            dataGridViewCellStyle8.NullValue = "0,00";
            this.lenght.DefaultCellStyle = dataGridViewCellStyle8;
            this.lenght.HeaderText = "Длина";
            this.lenght.Name = "lenght";
            // 
            // tuesday
            // 
            this.tuesday.Controls.Add(this.dgv_1);
            this.tuesday.Location = new System.Drawing.Point(4, 26);
            this.tuesday.Name = "tuesday";
            this.tuesday.Size = new System.Drawing.Size(532, 125);
            this.tuesday.TabIndex = 1;
            this.tuesday.Text = "Tuesday";
            this.tuesday.UseVisualStyleBackColor = true;
            // 
            // dgv_1
            // 
            this.dgv_1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.Format = "N2";
            dataGridViewCellStyle15.NullValue = "0";
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_1.DefaultCellStyle = dataGridViewCellStyle15;
            this.dgv_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_1.Location = new System.Drawing.Point(0, 0);
            this.dgv_1.Name = "dgv_1";
            this.dgv_1.Size = new System.Drawing.Size(532, 125);
            this.dgv_1.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn1
            // 
            dataGridViewCellStyle10.Format = "H:i:s";
            dataGridViewCellStyle10.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewTextBoxColumn1.HeaderText = "Время1";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            dataGridViewCellStyle11.Format = "T";
            dataGridViewCellStyle11.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewTextBoxColumn2.HeaderText = "Время2";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle12.Format = "C2";
            dataGridViewCellStyle12.NullValue = "0,00";
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewTextBoxColumn3.HeaderText = "Груженая";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewCellStyle13.Format = "C2";
            dataGridViewCellStyle13.NullValue = "0,00";
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewTextBoxColumn4.HeaderText = "Негруженая";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            dataGridViewCellStyle14.Format = "N2";
            dataGridViewCellStyle14.NullValue = "0,00";
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewTextBoxColumn5.HeaderText = "Длина";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // wednesday
            // 
            this.wednesday.Controls.Add(this.dgv_2);
            this.wednesday.Location = new System.Drawing.Point(4, 26);
            this.wednesday.Name = "wednesday";
            this.wednesday.Size = new System.Drawing.Size(532, 125);
            this.wednesday.TabIndex = 2;
            this.wednesday.Text = "Wednesday";
            this.wednesday.UseVisualStyleBackColor = true;
            // 
            // dgv_2
            // 
            this.dgv_2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10});
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle21.Format = "N2";
            dataGridViewCellStyle21.NullValue = "0";
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_2.DefaultCellStyle = dataGridViewCellStyle21;
            this.dgv_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_2.Location = new System.Drawing.Point(0, 0);
            this.dgv_2.Name = "dgv_2";
            this.dgv_2.Size = new System.Drawing.Size(532, 125);
            this.dgv_2.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn6
            // 
            dataGridViewCellStyle16.Format = "H:i:s";
            dataGridViewCellStyle16.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridViewTextBoxColumn6.HeaderText = "Время1";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            dataGridViewCellStyle17.Format = "T";
            dataGridViewCellStyle17.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridViewTextBoxColumn7.HeaderText = "Время2";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            dataGridViewCellStyle18.Format = "C2";
            dataGridViewCellStyle18.NullValue = "0,00";
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle18;
            this.dataGridViewTextBoxColumn8.HeaderText = "Груженая";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn9
            // 
            dataGridViewCellStyle19.Format = "C2";
            dataGridViewCellStyle19.NullValue = "0,00";
            this.dataGridViewTextBoxColumn9.DefaultCellStyle = dataGridViewCellStyle19;
            this.dataGridViewTextBoxColumn9.HeaderText = "Негруженая";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // dataGridViewTextBoxColumn10
            // 
            dataGridViewCellStyle20.Format = "N2";
            dataGridViewCellStyle20.NullValue = "0,00";
            this.dataGridViewTextBoxColumn10.DefaultCellStyle = dataGridViewCellStyle20;
            this.dataGridViewTextBoxColumn10.HeaderText = "Длина";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            // 
            // thursday
            // 
            this.thursday.Controls.Add(this.dgv_3);
            this.thursday.Location = new System.Drawing.Point(4, 26);
            this.thursday.Name = "thursday";
            this.thursday.Size = new System.Drawing.Size(532, 125);
            this.thursday.TabIndex = 3;
            this.thursday.Text = "Thursday";
            this.thursday.UseVisualStyleBackColor = true;
            // 
            // dgv_3
            // 
            this.dgv_3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15});
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle27.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle27.Format = "N2";
            dataGridViewCellStyle27.NullValue = "0";
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_3.DefaultCellStyle = dataGridViewCellStyle27;
            this.dgv_3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_3.Location = new System.Drawing.Point(0, 0);
            this.dgv_3.Name = "dgv_3";
            this.dgv_3.Size = new System.Drawing.Size(532, 125);
            this.dgv_3.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn11
            // 
            dataGridViewCellStyle22.Format = "H:i:s";
            dataGridViewCellStyle22.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn11.DefaultCellStyle = dataGridViewCellStyle22;
            this.dataGridViewTextBoxColumn11.HeaderText = "Время1";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            // 
            // dataGridViewTextBoxColumn12
            // 
            dataGridViewCellStyle23.Format = "T";
            dataGridViewCellStyle23.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn12.DefaultCellStyle = dataGridViewCellStyle23;
            this.dataGridViewTextBoxColumn12.HeaderText = "Время2";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            // 
            // dataGridViewTextBoxColumn13
            // 
            dataGridViewCellStyle24.Format = "C2";
            dataGridViewCellStyle24.NullValue = "0,00";
            this.dataGridViewTextBoxColumn13.DefaultCellStyle = dataGridViewCellStyle24;
            this.dataGridViewTextBoxColumn13.HeaderText = "Груженая";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            // 
            // dataGridViewTextBoxColumn14
            // 
            dataGridViewCellStyle25.Format = "C2";
            dataGridViewCellStyle25.NullValue = "0,00";
            this.dataGridViewTextBoxColumn14.DefaultCellStyle = dataGridViewCellStyle25;
            this.dataGridViewTextBoxColumn14.HeaderText = "Негруженая";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            // 
            // dataGridViewTextBoxColumn15
            // 
            dataGridViewCellStyle26.Format = "N2";
            dataGridViewCellStyle26.NullValue = "0,00";
            this.dataGridViewTextBoxColumn15.DefaultCellStyle = dataGridViewCellStyle26;
            this.dataGridViewTextBoxColumn15.HeaderText = "Длина";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            // 
            // friday
            // 
            this.friday.Controls.Add(this.dgv_4);
            this.friday.Location = new System.Drawing.Point(4, 26);
            this.friday.Name = "friday";
            this.friday.Size = new System.Drawing.Size(532, 125);
            this.friday.TabIndex = 4;
            this.friday.Text = "Friday";
            this.friday.UseVisualStyleBackColor = true;
            // 
            // dgv_4
            // 
            this.dgv_4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20});
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle33.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle33.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle33.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle33.Format = "N2";
            dataGridViewCellStyle33.NullValue = "0";
            dataGridViewCellStyle33.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle33.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle33.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_4.DefaultCellStyle = dataGridViewCellStyle33;
            this.dgv_4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_4.Location = new System.Drawing.Point(0, 0);
            this.dgv_4.Name = "dgv_4";
            this.dgv_4.Size = new System.Drawing.Size(532, 125);
            this.dgv_4.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn16
            // 
            dataGridViewCellStyle28.Format = "H:i:s";
            dataGridViewCellStyle28.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn16.DefaultCellStyle = dataGridViewCellStyle28;
            this.dataGridViewTextBoxColumn16.HeaderText = "Время1";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            // 
            // dataGridViewTextBoxColumn17
            // 
            dataGridViewCellStyle29.Format = "T";
            dataGridViewCellStyle29.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn17.DefaultCellStyle = dataGridViewCellStyle29;
            this.dataGridViewTextBoxColumn17.HeaderText = "Время2";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            // 
            // dataGridViewTextBoxColumn18
            // 
            dataGridViewCellStyle30.Format = "C2";
            dataGridViewCellStyle30.NullValue = "0,00";
            this.dataGridViewTextBoxColumn18.DefaultCellStyle = dataGridViewCellStyle30;
            this.dataGridViewTextBoxColumn18.HeaderText = "Груженая";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            // 
            // dataGridViewTextBoxColumn19
            // 
            dataGridViewCellStyle31.Format = "C2";
            dataGridViewCellStyle31.NullValue = "0,00";
            this.dataGridViewTextBoxColumn19.DefaultCellStyle = dataGridViewCellStyle31;
            this.dataGridViewTextBoxColumn19.HeaderText = "Негруженая";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            // 
            // dataGridViewTextBoxColumn20
            // 
            dataGridViewCellStyle32.Format = "N2";
            dataGridViewCellStyle32.NullValue = "0,00";
            this.dataGridViewTextBoxColumn20.DefaultCellStyle = dataGridViewCellStyle32;
            this.dataGridViewTextBoxColumn20.HeaderText = "Длина";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            // 
            // saturday
            // 
            this.saturday.Controls.Add(this.dgv_5);
            this.saturday.Location = new System.Drawing.Point(4, 26);
            this.saturday.Name = "saturday";
            this.saturday.Size = new System.Drawing.Size(532, 125);
            this.saturday.TabIndex = 5;
            this.saturday.Text = "Saturday";
            this.saturday.UseVisualStyleBackColor = true;
            // 
            // dgv_5
            // 
            this.dgv_5.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewTextBoxColumn25});
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle39.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle39.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle39.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle39.Format = "N2";
            dataGridViewCellStyle39.NullValue = "0";
            dataGridViewCellStyle39.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle39.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle39.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_5.DefaultCellStyle = dataGridViewCellStyle39;
            this.dgv_5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_5.Location = new System.Drawing.Point(0, 0);
            this.dgv_5.Name = "dgv_5";
            this.dgv_5.Size = new System.Drawing.Size(532, 125);
            this.dgv_5.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn21
            // 
            dataGridViewCellStyle34.Format = "H:i:s";
            dataGridViewCellStyle34.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn21.DefaultCellStyle = dataGridViewCellStyle34;
            this.dataGridViewTextBoxColumn21.HeaderText = "Время1";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            // 
            // dataGridViewTextBoxColumn22
            // 
            dataGridViewCellStyle35.Format = "T";
            dataGridViewCellStyle35.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn22.DefaultCellStyle = dataGridViewCellStyle35;
            this.dataGridViewTextBoxColumn22.HeaderText = "Время2";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            // 
            // dataGridViewTextBoxColumn23
            // 
            dataGridViewCellStyle36.Format = "C2";
            dataGridViewCellStyle36.NullValue = "0,00";
            this.dataGridViewTextBoxColumn23.DefaultCellStyle = dataGridViewCellStyle36;
            this.dataGridViewTextBoxColumn23.HeaderText = "Груженая";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            // 
            // dataGridViewTextBoxColumn24
            // 
            dataGridViewCellStyle37.Format = "C2";
            dataGridViewCellStyle37.NullValue = "0,00";
            this.dataGridViewTextBoxColumn24.DefaultCellStyle = dataGridViewCellStyle37;
            this.dataGridViewTextBoxColumn24.HeaderText = "Негруженая";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            // 
            // dataGridViewTextBoxColumn25
            // 
            dataGridViewCellStyle38.Format = "N2";
            dataGridViewCellStyle38.NullValue = "0,00";
            this.dataGridViewTextBoxColumn25.DefaultCellStyle = dataGridViewCellStyle38;
            this.dataGridViewTextBoxColumn25.HeaderText = "Длина";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            // 
            // sunday
            // 
            this.sunday.Controls.Add(this.dgv_6);
            this.sunday.Location = new System.Drawing.Point(4, 26);
            this.sunday.Name = "sunday";
            this.sunday.Size = new System.Drawing.Size(532, 125);
            this.sunday.TabIndex = 6;
            this.sunday.Text = "Sunday";
            this.sunday.UseVisualStyleBackColor = true;
            // 
            // dgv_6
            // 
            this.dgv_6.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_6.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn26,
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewTextBoxColumn29,
            this.dataGridViewTextBoxColumn30});
            dataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle45.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle45.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle45.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle45.Format = "N2";
            dataGridViewCellStyle45.NullValue = "0";
            dataGridViewCellStyle45.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle45.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle45.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_6.DefaultCellStyle = dataGridViewCellStyle45;
            this.dgv_6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_6.Location = new System.Drawing.Point(0, 0);
            this.dgv_6.Name = "dgv_6";
            this.dgv_6.Size = new System.Drawing.Size(532, 125);
            this.dgv_6.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn26
            // 
            dataGridViewCellStyle40.Format = "H:i:s";
            dataGridViewCellStyle40.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn26.DefaultCellStyle = dataGridViewCellStyle40;
            this.dataGridViewTextBoxColumn26.HeaderText = "Время1";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            // 
            // dataGridViewTextBoxColumn27
            // 
            dataGridViewCellStyle41.Format = "T";
            dataGridViewCellStyle41.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn27.DefaultCellStyle = dataGridViewCellStyle41;
            this.dataGridViewTextBoxColumn27.HeaderText = "Время2";
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            // 
            // dataGridViewTextBoxColumn28
            // 
            dataGridViewCellStyle42.Format = "C2";
            dataGridViewCellStyle42.NullValue = "0,00";
            this.dataGridViewTextBoxColumn28.DefaultCellStyle = dataGridViewCellStyle42;
            this.dataGridViewTextBoxColumn28.HeaderText = "Груженая";
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            // 
            // dataGridViewTextBoxColumn29
            // 
            dataGridViewCellStyle43.Format = "C2";
            dataGridViewCellStyle43.NullValue = "0,00";
            this.dataGridViewTextBoxColumn29.DefaultCellStyle = dataGridViewCellStyle43;
            this.dataGridViewTextBoxColumn29.HeaderText = "Негруженая";
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            // 
            // dataGridViewTextBoxColumn30
            // 
            dataGridViewCellStyle44.Format = "N2";
            dataGridViewCellStyle44.NullValue = "0,00";
            this.dataGridViewTextBoxColumn30.DefaultCellStyle = dataGridViewCellStyle44;
            this.dataGridViewTextBoxColumn30.HeaderText = "Длина";
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            // 
            // cb_route1
            // 
            this.cb_route1.FormattingEnabled = true;
            this.cb_route1.Location = new System.Drawing.Point(321, 3);
            this.cb_route1.Name = "cb_route1";
            this.cb_route1.Size = new System.Drawing.Size(230, 25);
            this.cb_route1.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(247, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Маршрут:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Клиент:";
            // 
            // cb_clients
            // 
            this.cb_clients.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_clients.FormattingEnabled = true;
            this.cb_clients.Location = new System.Drawing.Point(66, 3);
            this.cb_clients.Name = "cb_clients";
            this.cb_clients.Size = new System.Drawing.Size(175, 25);
            this.cb_clients.TabIndex = 0;
            // 
            // contract_uridic
            // 
            this.contract_uridic.Controls.Add(this.button4);
            this.contract_uridic.Controls.Add(this.button3);
            this.contract_uridic.Controls.Add(this.btn_comp_save);
            this.contract_uridic.Controls.Add(this.dgv_ad_companies);
            this.contract_uridic.Controls.Add(this.label8);
            this.contract_uridic.Controls.Add(this.tab_perdiods_companies);
            this.contract_uridic.Controls.Add(this.label7);
            this.contract_uridic.Controls.Add(this.label6);
            this.contract_uridic.Controls.Add(this.label5);
            this.contract_uridic.Controls.Add(this.cb_route2);
            this.contract_uridic.Controls.Add(this.cb_companies);
            this.contract_uridic.Location = new System.Drawing.Point(4, 26);
            this.contract_uridic.Name = "contract_uridic";
            this.contract_uridic.Size = new System.Drawing.Size(559, 450);
            this.contract_uridic.TabIndex = 1;
            this.contract_uridic.Text = "Юридическое лицо";
            this.contract_uridic.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(503, 61);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(48, 23);
            this.button4.TabIndex = 13;
            this.button4.Text = "Встав";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(503, 38);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(48, 23);
            this.button3.TabIndex = 12;
            this.button3.Text = "Скоп";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btn_comp_save
            // 
            this.btn_comp_save.Location = new System.Drawing.Point(422, 418);
            this.btn_comp_save.Name = "btn_comp_save";
            this.btn_comp_save.Size = new System.Drawing.Size(129, 29);
            this.btn_comp_save.TabIndex = 11;
            this.btn_comp_save.Text = "Добавить";
            this.btn_comp_save.UseVisualStyleBackColor = true;
            this.btn_comp_save.Click += new System.EventHandler(this.btn_comp_save_Click);
            // 
            // dgv_ad_companies
            // 
            this.dgv_ad_companies.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_ad_companies.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_ad_companies.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ad_c_name,
            this.dataGridViewTextBoxColumn66,
            this.dataGridViewTextBoxColumn67,
            this.dataGridViewTextBoxColumn68});
            this.dgv_ad_companies.Location = new System.Drawing.Point(11, 238);
            this.dgv_ad_companies.Name = "dgv_ad_companies";
            this.dgv_ad_companies.Size = new System.Drawing.Size(540, 174);
            this.dgv_ad_companies.TabIndex = 10;
            this.dgv_ad_companies.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgv_ad_CellValidating);
            this.dgv_ad_companies.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgv_ad_EditingControlShowing);
            // 
            // ad_c_name
            // 
            this.ad_c_name.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.ad_c_name.HeaderText = "Название";
            this.ad_c_name.Name = "ad_c_name";
            this.ad_c_name.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ad_c_name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewTextBoxColumn66
            // 
            dataGridViewCellStyle46.Format = "C2";
            dataGridViewCellStyle46.NullValue = "0,00";
            this.dataGridViewTextBoxColumn66.DefaultCellStyle = dataGridViewCellStyle46;
            this.dataGridViewTextBoxColumn66.HeaderText = "Груженая";
            this.dataGridViewTextBoxColumn66.Name = "dataGridViewTextBoxColumn66";
            // 
            // dataGridViewTextBoxColumn67
            // 
            dataGridViewCellStyle47.Format = "C2";
            dataGridViewCellStyle47.NullValue = "0,00";
            this.dataGridViewTextBoxColumn67.DefaultCellStyle = dataGridViewCellStyle47;
            this.dataGridViewTextBoxColumn67.HeaderText = "Негруженая";
            this.dataGridViewTextBoxColumn67.Name = "dataGridViewTextBoxColumn67";
            // 
            // dataGridViewTextBoxColumn68
            // 
            dataGridViewCellStyle48.Format = "N2";
            dataGridViewCellStyle48.NullValue = "0,00";
            this.dataGridViewTextBoxColumn68.DefaultCellStyle = dataGridViewCellStyle48;
            this.dataGridViewTextBoxColumn68.HeaderText = "Вес";
            this.dataGridViewTextBoxColumn68.Name = "dataGridViewTextBoxColumn68";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 219);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 17);
            this.label8.TabIndex = 9;
            this.label8.Text = "Доп.Опции:";
            // 
            // tab_perdiods_companies
            // 
            this.tab_perdiods_companies.Controls.Add(this.tabPage1);
            this.tab_perdiods_companies.Controls.Add(this.tabPage2);
            this.tab_perdiods_companies.Controls.Add(this.tabPage3);
            this.tab_perdiods_companies.Controls.Add(this.tabPage4);
            this.tab_perdiods_companies.Controls.Add(this.tabPage5);
            this.tab_perdiods_companies.Controls.Add(this.tabPage6);
            this.tab_perdiods_companies.Controls.Add(this.tabPage7);
            this.tab_perdiods_companies.Location = new System.Drawing.Point(11, 61);
            this.tab_perdiods_companies.Name = "tab_perdiods_companies";
            this.tab_perdiods_companies.SelectedIndex = 0;
            this.tab_perdiods_companies.Size = new System.Drawing.Size(540, 155);
            this.tab_perdiods_companies.TabIndex = 8;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dgv_comp_0);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(532, 125);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Monday";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dgv_comp_0
            // 
            this.dgv_comp_0.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_comp_0.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_comp_0.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn31,
            this.dataGridViewTextBoxColumn32,
            this.dataGridViewTextBoxColumn33,
            this.dataGridViewTextBoxColumn34,
            this.dataGridViewTextBoxColumn35});
            dataGridViewCellStyle54.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle54.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle54.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle54.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle54.Format = "N2";
            dataGridViewCellStyle54.NullValue = "0";
            dataGridViewCellStyle54.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle54.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle54.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_comp_0.DefaultCellStyle = dataGridViewCellStyle54;
            this.dgv_comp_0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_comp_0.Location = new System.Drawing.Point(0, 0);
            this.dgv_comp_0.Name = "dgv_comp_0";
            this.dgv_comp_0.Size = new System.Drawing.Size(532, 125);
            this.dgv_comp_0.TabIndex = 0;
            this.dgv_comp_0.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dataGridView1_CellValidating);
            // 
            // dataGridViewTextBoxColumn31
            // 
            dataGridViewCellStyle49.Format = "H:i:s";
            dataGridViewCellStyle49.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn31.DefaultCellStyle = dataGridViewCellStyle49;
            this.dataGridViewTextBoxColumn31.HeaderText = "Время1";
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            // 
            // dataGridViewTextBoxColumn32
            // 
            dataGridViewCellStyle50.Format = "T";
            dataGridViewCellStyle50.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn32.DefaultCellStyle = dataGridViewCellStyle50;
            this.dataGridViewTextBoxColumn32.HeaderText = "Время2";
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            // 
            // dataGridViewTextBoxColumn33
            // 
            dataGridViewCellStyle51.Format = "C2";
            dataGridViewCellStyle51.NullValue = "0,00";
            this.dataGridViewTextBoxColumn33.DefaultCellStyle = dataGridViewCellStyle51;
            this.dataGridViewTextBoxColumn33.HeaderText = "Груженая";
            this.dataGridViewTextBoxColumn33.Name = "dataGridViewTextBoxColumn33";
            // 
            // dataGridViewTextBoxColumn34
            // 
            dataGridViewCellStyle52.Format = "C2";
            dataGridViewCellStyle52.NullValue = "0,00";
            this.dataGridViewTextBoxColumn34.DefaultCellStyle = dataGridViewCellStyle52;
            this.dataGridViewTextBoxColumn34.HeaderText = "Негруженая";
            this.dataGridViewTextBoxColumn34.Name = "dataGridViewTextBoxColumn34";
            // 
            // dataGridViewTextBoxColumn35
            // 
            dataGridViewCellStyle53.Format = "N2";
            dataGridViewCellStyle53.NullValue = "0,00";
            this.dataGridViewTextBoxColumn35.DefaultCellStyle = dataGridViewCellStyle53;
            this.dataGridViewTextBoxColumn35.HeaderText = "Длина";
            this.dataGridViewTextBoxColumn35.Name = "dataGridViewTextBoxColumn35";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dgv_comp_1);
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(532, 125);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Tuesday";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dgv_comp_1
            // 
            this.dgv_comp_1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_comp_1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_comp_1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn36,
            this.dataGridViewTextBoxColumn37,
            this.dataGridViewTextBoxColumn38,
            this.dataGridViewTextBoxColumn39,
            this.dataGridViewTextBoxColumn40});
            dataGridViewCellStyle60.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle60.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle60.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle60.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle60.Format = "N2";
            dataGridViewCellStyle60.NullValue = "0";
            dataGridViewCellStyle60.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle60.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle60.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_comp_1.DefaultCellStyle = dataGridViewCellStyle60;
            this.dgv_comp_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_comp_1.Location = new System.Drawing.Point(0, 0);
            this.dgv_comp_1.Name = "dgv_comp_1";
            this.dgv_comp_1.Size = new System.Drawing.Size(532, 125);
            this.dgv_comp_1.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn36
            // 
            dataGridViewCellStyle55.Format = "H:i:s";
            dataGridViewCellStyle55.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn36.DefaultCellStyle = dataGridViewCellStyle55;
            this.dataGridViewTextBoxColumn36.HeaderText = "Время1";
            this.dataGridViewTextBoxColumn36.Name = "dataGridViewTextBoxColumn36";
            // 
            // dataGridViewTextBoxColumn37
            // 
            dataGridViewCellStyle56.Format = "T";
            dataGridViewCellStyle56.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn37.DefaultCellStyle = dataGridViewCellStyle56;
            this.dataGridViewTextBoxColumn37.HeaderText = "Время2";
            this.dataGridViewTextBoxColumn37.Name = "dataGridViewTextBoxColumn37";
            // 
            // dataGridViewTextBoxColumn38
            // 
            dataGridViewCellStyle57.Format = "C2";
            dataGridViewCellStyle57.NullValue = "0,00";
            this.dataGridViewTextBoxColumn38.DefaultCellStyle = dataGridViewCellStyle57;
            this.dataGridViewTextBoxColumn38.HeaderText = "Груженая";
            this.dataGridViewTextBoxColumn38.Name = "dataGridViewTextBoxColumn38";
            // 
            // dataGridViewTextBoxColumn39
            // 
            dataGridViewCellStyle58.Format = "C2";
            dataGridViewCellStyle58.NullValue = "0,00";
            this.dataGridViewTextBoxColumn39.DefaultCellStyle = dataGridViewCellStyle58;
            this.dataGridViewTextBoxColumn39.HeaderText = "Негруженая";
            this.dataGridViewTextBoxColumn39.Name = "dataGridViewTextBoxColumn39";
            // 
            // dataGridViewTextBoxColumn40
            // 
            dataGridViewCellStyle59.Format = "N2";
            dataGridViewCellStyle59.NullValue = "0,00";
            this.dataGridViewTextBoxColumn40.DefaultCellStyle = dataGridViewCellStyle59;
            this.dataGridViewTextBoxColumn40.HeaderText = "Длина";
            this.dataGridViewTextBoxColumn40.Name = "dataGridViewTextBoxColumn40";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dgv_comp_2);
            this.tabPage3.Location = new System.Drawing.Point(4, 26);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(532, 125);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Wednesday";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dgv_comp_2
            // 
            this.dgv_comp_2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_comp_2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_comp_2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn41,
            this.dataGridViewTextBoxColumn42,
            this.dataGridViewTextBoxColumn43,
            this.dataGridViewTextBoxColumn44,
            this.dataGridViewTextBoxColumn45});
            dataGridViewCellStyle66.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle66.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle66.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle66.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle66.Format = "N2";
            dataGridViewCellStyle66.NullValue = "0";
            dataGridViewCellStyle66.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle66.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle66.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_comp_2.DefaultCellStyle = dataGridViewCellStyle66;
            this.dgv_comp_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_comp_2.Location = new System.Drawing.Point(0, 0);
            this.dgv_comp_2.Name = "dgv_comp_2";
            this.dgv_comp_2.Size = new System.Drawing.Size(532, 125);
            this.dgv_comp_2.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn41
            // 
            dataGridViewCellStyle61.Format = "H:i:s";
            dataGridViewCellStyle61.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn41.DefaultCellStyle = dataGridViewCellStyle61;
            this.dataGridViewTextBoxColumn41.HeaderText = "Время1";
            this.dataGridViewTextBoxColumn41.Name = "dataGridViewTextBoxColumn41";
            // 
            // dataGridViewTextBoxColumn42
            // 
            dataGridViewCellStyle62.Format = "T";
            dataGridViewCellStyle62.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn42.DefaultCellStyle = dataGridViewCellStyle62;
            this.dataGridViewTextBoxColumn42.HeaderText = "Время2";
            this.dataGridViewTextBoxColumn42.Name = "dataGridViewTextBoxColumn42";
            // 
            // dataGridViewTextBoxColumn43
            // 
            dataGridViewCellStyle63.Format = "C2";
            dataGridViewCellStyle63.NullValue = "0,00";
            this.dataGridViewTextBoxColumn43.DefaultCellStyle = dataGridViewCellStyle63;
            this.dataGridViewTextBoxColumn43.HeaderText = "Груженая";
            this.dataGridViewTextBoxColumn43.Name = "dataGridViewTextBoxColumn43";
            // 
            // dataGridViewTextBoxColumn44
            // 
            dataGridViewCellStyle64.Format = "C2";
            dataGridViewCellStyle64.NullValue = "0,00";
            this.dataGridViewTextBoxColumn44.DefaultCellStyle = dataGridViewCellStyle64;
            this.dataGridViewTextBoxColumn44.HeaderText = "Негруженая";
            this.dataGridViewTextBoxColumn44.Name = "dataGridViewTextBoxColumn44";
            // 
            // dataGridViewTextBoxColumn45
            // 
            dataGridViewCellStyle65.Format = "N2";
            dataGridViewCellStyle65.NullValue = "0,00";
            this.dataGridViewTextBoxColumn45.DefaultCellStyle = dataGridViewCellStyle65;
            this.dataGridViewTextBoxColumn45.HeaderText = "Длина";
            this.dataGridViewTextBoxColumn45.Name = "dataGridViewTextBoxColumn45";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dgv_comp_3);
            this.tabPage4.Location = new System.Drawing.Point(4, 26);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(532, 125);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Thursday";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dgv_comp_3
            // 
            this.dgv_comp_3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_comp_3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_comp_3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn46,
            this.dataGridViewTextBoxColumn47,
            this.dataGridViewTextBoxColumn48,
            this.dataGridViewTextBoxColumn49,
            this.dataGridViewTextBoxColumn50});
            dataGridViewCellStyle72.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle72.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle72.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle72.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle72.Format = "N2";
            dataGridViewCellStyle72.NullValue = "0";
            dataGridViewCellStyle72.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle72.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle72.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_comp_3.DefaultCellStyle = dataGridViewCellStyle72;
            this.dgv_comp_3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_comp_3.Location = new System.Drawing.Point(0, 0);
            this.dgv_comp_3.Name = "dgv_comp_3";
            this.dgv_comp_3.Size = new System.Drawing.Size(532, 125);
            this.dgv_comp_3.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn46
            // 
            dataGridViewCellStyle67.Format = "H:i:s";
            dataGridViewCellStyle67.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn46.DefaultCellStyle = dataGridViewCellStyle67;
            this.dataGridViewTextBoxColumn46.HeaderText = "Время1";
            this.dataGridViewTextBoxColumn46.Name = "dataGridViewTextBoxColumn46";
            // 
            // dataGridViewTextBoxColumn47
            // 
            dataGridViewCellStyle68.Format = "T";
            dataGridViewCellStyle68.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn47.DefaultCellStyle = dataGridViewCellStyle68;
            this.dataGridViewTextBoxColumn47.HeaderText = "Время2";
            this.dataGridViewTextBoxColumn47.Name = "dataGridViewTextBoxColumn47";
            // 
            // dataGridViewTextBoxColumn48
            // 
            dataGridViewCellStyle69.Format = "C2";
            dataGridViewCellStyle69.NullValue = "0,00";
            this.dataGridViewTextBoxColumn48.DefaultCellStyle = dataGridViewCellStyle69;
            this.dataGridViewTextBoxColumn48.HeaderText = "Груженая";
            this.dataGridViewTextBoxColumn48.Name = "dataGridViewTextBoxColumn48";
            // 
            // dataGridViewTextBoxColumn49
            // 
            dataGridViewCellStyle70.Format = "C2";
            dataGridViewCellStyle70.NullValue = "0,00";
            this.dataGridViewTextBoxColumn49.DefaultCellStyle = dataGridViewCellStyle70;
            this.dataGridViewTextBoxColumn49.HeaderText = "Негруженая";
            this.dataGridViewTextBoxColumn49.Name = "dataGridViewTextBoxColumn49";
            // 
            // dataGridViewTextBoxColumn50
            // 
            dataGridViewCellStyle71.Format = "N2";
            dataGridViewCellStyle71.NullValue = "0,00";
            this.dataGridViewTextBoxColumn50.DefaultCellStyle = dataGridViewCellStyle71;
            this.dataGridViewTextBoxColumn50.HeaderText = "Длина";
            this.dataGridViewTextBoxColumn50.Name = "dataGridViewTextBoxColumn50";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.dgv_comp_4);
            this.tabPage5.Location = new System.Drawing.Point(4, 26);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(532, 125);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Friday";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // dgv_comp_4
            // 
            this.dgv_comp_4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_comp_4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_comp_4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn51,
            this.dataGridViewTextBoxColumn52,
            this.dataGridViewTextBoxColumn53,
            this.dataGridViewTextBoxColumn54,
            this.dataGridViewTextBoxColumn55});
            dataGridViewCellStyle78.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle78.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle78.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle78.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle78.Format = "N2";
            dataGridViewCellStyle78.NullValue = "0";
            dataGridViewCellStyle78.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle78.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle78.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_comp_4.DefaultCellStyle = dataGridViewCellStyle78;
            this.dgv_comp_4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_comp_4.Location = new System.Drawing.Point(0, 0);
            this.dgv_comp_4.Name = "dgv_comp_4";
            this.dgv_comp_4.Size = new System.Drawing.Size(532, 125);
            this.dgv_comp_4.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn51
            // 
            dataGridViewCellStyle73.Format = "H:i:s";
            dataGridViewCellStyle73.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn51.DefaultCellStyle = dataGridViewCellStyle73;
            this.dataGridViewTextBoxColumn51.HeaderText = "Время1";
            this.dataGridViewTextBoxColumn51.Name = "dataGridViewTextBoxColumn51";
            // 
            // dataGridViewTextBoxColumn52
            // 
            dataGridViewCellStyle74.Format = "T";
            dataGridViewCellStyle74.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn52.DefaultCellStyle = dataGridViewCellStyle74;
            this.dataGridViewTextBoxColumn52.HeaderText = "Время2";
            this.dataGridViewTextBoxColumn52.Name = "dataGridViewTextBoxColumn52";
            // 
            // dataGridViewTextBoxColumn53
            // 
            dataGridViewCellStyle75.Format = "C2";
            dataGridViewCellStyle75.NullValue = "0,00";
            this.dataGridViewTextBoxColumn53.DefaultCellStyle = dataGridViewCellStyle75;
            this.dataGridViewTextBoxColumn53.HeaderText = "Груженая";
            this.dataGridViewTextBoxColumn53.Name = "dataGridViewTextBoxColumn53";
            // 
            // dataGridViewTextBoxColumn54
            // 
            dataGridViewCellStyle76.Format = "C2";
            dataGridViewCellStyle76.NullValue = "0,00";
            this.dataGridViewTextBoxColumn54.DefaultCellStyle = dataGridViewCellStyle76;
            this.dataGridViewTextBoxColumn54.HeaderText = "Негруженая";
            this.dataGridViewTextBoxColumn54.Name = "dataGridViewTextBoxColumn54";
            // 
            // dataGridViewTextBoxColumn55
            // 
            dataGridViewCellStyle77.Format = "N2";
            dataGridViewCellStyle77.NullValue = "0,00";
            this.dataGridViewTextBoxColumn55.DefaultCellStyle = dataGridViewCellStyle77;
            this.dataGridViewTextBoxColumn55.HeaderText = "Длина";
            this.dataGridViewTextBoxColumn55.Name = "dataGridViewTextBoxColumn55";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.dgv_comp_5);
            this.tabPage6.Location = new System.Drawing.Point(4, 26);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(532, 125);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Saturday";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // dgv_comp_5
            // 
            this.dgv_comp_5.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_comp_5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_comp_5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn56,
            this.dataGridViewTextBoxColumn57,
            this.dataGridViewTextBoxColumn58,
            this.dataGridViewTextBoxColumn59,
            this.dataGridViewTextBoxColumn60});
            dataGridViewCellStyle84.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle84.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle84.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle84.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle84.Format = "N2";
            dataGridViewCellStyle84.NullValue = "0";
            dataGridViewCellStyle84.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle84.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle84.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_comp_5.DefaultCellStyle = dataGridViewCellStyle84;
            this.dgv_comp_5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_comp_5.Location = new System.Drawing.Point(0, 0);
            this.dgv_comp_5.Name = "dgv_comp_5";
            this.dgv_comp_5.Size = new System.Drawing.Size(532, 125);
            this.dgv_comp_5.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn56
            // 
            dataGridViewCellStyle79.Format = "H:i:s";
            dataGridViewCellStyle79.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn56.DefaultCellStyle = dataGridViewCellStyle79;
            this.dataGridViewTextBoxColumn56.HeaderText = "Время1";
            this.dataGridViewTextBoxColumn56.Name = "dataGridViewTextBoxColumn56";
            // 
            // dataGridViewTextBoxColumn57
            // 
            dataGridViewCellStyle80.Format = "T";
            dataGridViewCellStyle80.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn57.DefaultCellStyle = dataGridViewCellStyle80;
            this.dataGridViewTextBoxColumn57.HeaderText = "Время2";
            this.dataGridViewTextBoxColumn57.Name = "dataGridViewTextBoxColumn57";
            // 
            // dataGridViewTextBoxColumn58
            // 
            dataGridViewCellStyle81.Format = "C2";
            dataGridViewCellStyle81.NullValue = "0,00";
            this.dataGridViewTextBoxColumn58.DefaultCellStyle = dataGridViewCellStyle81;
            this.dataGridViewTextBoxColumn58.HeaderText = "Груженая";
            this.dataGridViewTextBoxColumn58.Name = "dataGridViewTextBoxColumn58";
            // 
            // dataGridViewTextBoxColumn59
            // 
            dataGridViewCellStyle82.Format = "C2";
            dataGridViewCellStyle82.NullValue = "0,00";
            this.dataGridViewTextBoxColumn59.DefaultCellStyle = dataGridViewCellStyle82;
            this.dataGridViewTextBoxColumn59.HeaderText = "Негруженая";
            this.dataGridViewTextBoxColumn59.Name = "dataGridViewTextBoxColumn59";
            // 
            // dataGridViewTextBoxColumn60
            // 
            dataGridViewCellStyle83.Format = "N2";
            dataGridViewCellStyle83.NullValue = "0,00";
            this.dataGridViewTextBoxColumn60.DefaultCellStyle = dataGridViewCellStyle83;
            this.dataGridViewTextBoxColumn60.HeaderText = "Длина";
            this.dataGridViewTextBoxColumn60.Name = "dataGridViewTextBoxColumn60";
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.dgv_comp_6);
            this.tabPage7.Location = new System.Drawing.Point(4, 26);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(532, 125);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Sunday";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // dgv_comp_6
            // 
            this.dgv_comp_6.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_comp_6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_comp_6.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn61,
            this.dataGridViewTextBoxColumn62,
            this.dataGridViewTextBoxColumn63,
            this.dataGridViewTextBoxColumn64,
            this.dataGridViewTextBoxColumn65});
            dataGridViewCellStyle90.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle90.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle90.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle90.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle90.Format = "N2";
            dataGridViewCellStyle90.NullValue = "0";
            dataGridViewCellStyle90.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle90.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle90.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_comp_6.DefaultCellStyle = dataGridViewCellStyle90;
            this.dgv_comp_6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_comp_6.Location = new System.Drawing.Point(0, 0);
            this.dgv_comp_6.Name = "dgv_comp_6";
            this.dgv_comp_6.Size = new System.Drawing.Size(532, 125);
            this.dgv_comp_6.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn61
            // 
            dataGridViewCellStyle85.Format = "H:i:s";
            dataGridViewCellStyle85.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn61.DefaultCellStyle = dataGridViewCellStyle85;
            this.dataGridViewTextBoxColumn61.HeaderText = "Время1";
            this.dataGridViewTextBoxColumn61.Name = "dataGridViewTextBoxColumn61";
            // 
            // dataGridViewTextBoxColumn62
            // 
            dataGridViewCellStyle86.Format = "T";
            dataGridViewCellStyle86.NullValue = "00:00:00";
            this.dataGridViewTextBoxColumn62.DefaultCellStyle = dataGridViewCellStyle86;
            this.dataGridViewTextBoxColumn62.HeaderText = "Время2";
            this.dataGridViewTextBoxColumn62.Name = "dataGridViewTextBoxColumn62";
            // 
            // dataGridViewTextBoxColumn63
            // 
            dataGridViewCellStyle87.Format = "C2";
            dataGridViewCellStyle87.NullValue = "0,00";
            this.dataGridViewTextBoxColumn63.DefaultCellStyle = dataGridViewCellStyle87;
            this.dataGridViewTextBoxColumn63.HeaderText = "Груженая";
            this.dataGridViewTextBoxColumn63.Name = "dataGridViewTextBoxColumn63";
            // 
            // dataGridViewTextBoxColumn64
            // 
            dataGridViewCellStyle88.Format = "C2";
            dataGridViewCellStyle88.NullValue = "0,00";
            this.dataGridViewTextBoxColumn64.DefaultCellStyle = dataGridViewCellStyle88;
            this.dataGridViewTextBoxColumn64.HeaderText = "Негруженая";
            this.dataGridViewTextBoxColumn64.Name = "dataGridViewTextBoxColumn64";
            // 
            // dataGridViewTextBoxColumn65
            // 
            dataGridViewCellStyle89.Format = "N2";
            dataGridViewCellStyle89.NullValue = "0,00";
            this.dataGridViewTextBoxColumn65.DefaultCellStyle = dataGridViewCellStyle89;
            this.dataGridViewTextBoxColumn65.HeaderText = "Длина";
            this.dataGridViewTextBoxColumn65.Name = "dataGridViewTextBoxColumn65";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 41);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 17);
            this.label7.TabIndex = 7;
            this.label7.Text = "Периоды:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(289, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 17);
            this.label6.TabIndex = 3;
            this.label6.Text = "Маршрут:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 17);
            this.label5.TabIndex = 2;
            this.label5.Text = "Компания:";
            // 
            // cb_route2
            // 
            this.cb_route2.FormattingEnabled = true;
            this.cb_route2.Location = new System.Drawing.Point(363, 3);
            this.cb_route2.Name = "cb_route2";
            this.cb_route2.Size = new System.Drawing.Size(188, 25);
            this.cb_route2.TabIndex = 1;
            // 
            // cb_companies
            // 
            this.cb_companies.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_companies.FormattingEnabled = true;
            this.cb_companies.Location = new System.Drawing.Point(85, 3);
            this.cb_companies.Name = "cb_companies";
            this.cb_companies.Size = new System.Drawing.Size(198, 25);
            this.cb_companies.TabIndex = 0;
            // 
            // nameOfAdDGV
            // 
            this.nameOfAdDGV.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.nameOfAdDGV.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.nameOfAdDGV.HeaderText = "Название";
            this.nameOfAdDGV.Name = "nameOfAdDGV";
            this.nameOfAdDGV.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.nameOfAdDGV.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // ad_price1
            // 
            dataGridViewCellStyle1.Format = "C2";
            dataGridViewCellStyle1.NullValue = "0,00";
            this.ad_price1.DefaultCellStyle = dataGridViewCellStyle1;
            this.ad_price1.HeaderText = "Груженая";
            this.ad_price1.Name = "ad_price1";
            // 
            // ad_price2
            // 
            dataGridViewCellStyle2.Format = "C2";
            dataGridViewCellStyle2.NullValue = "0,00";
            this.ad_price2.DefaultCellStyle = dataGridViewCellStyle2;
            this.ad_price2.HeaderText = "Негруженая";
            this.ad_price2.Name = "ad_price2";
            // 
            // weight
            // 
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = "0,00";
            this.weight.DefaultCellStyle = dataGridViewCellStyle3;
            this.weight.HeaderText = "Вес";
            this.weight.Name = "weight";
            // 
            // frm_newContract
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(567, 480);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "frm_newContract";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Новый договор";
            this.tabControl1.ResumeLayout(false);
            this.contract_physic.ResumeLayout(false);
            this.contract_physic.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ad)).EndInit();
            this.tab_periods.ResumeLayout(false);
            this.monday.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_0)).EndInit();
            this.tuesday.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_1)).EndInit();
            this.wednesday.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_2)).EndInit();
            this.thursday.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_3)).EndInit();
            this.friday.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_4)).EndInit();
            this.saturday.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_5)).EndInit();
            this.sunday.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_6)).EndInit();
            this.contract_uridic.ResumeLayout(false);
            this.contract_uridic.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ad_companies)).EndInit();
            this.tab_perdiods_companies.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_comp_0)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_comp_1)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_comp_2)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_comp_3)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_comp_4)).EndInit();
            this.tabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_comp_5)).EndInit();
            this.tabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_comp_6)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage contract_physic;
        private System.Windows.Forms.TabPage contract_uridic;
        private System.Windows.Forms.ComboBox cb_clients;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cb_route1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabControl tab_periods;
        private System.Windows.Forms.TabPage monday;
        private System.Windows.Forms.TabPage tuesday;
        private System.Windows.Forms.TabPage wednesday;
        private System.Windows.Forms.TabPage thursday;
        private System.Windows.Forms.TabPage friday;
        private System.Windows.Forms.TabPage saturday;
        private System.Windows.Forms.TabPage sunday;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cb_route2;
        private System.Windows.Forms.ComboBox cb_companies;
        private System.Windows.Forms.DataGridView dgv_ad;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.DataGridView dgv_1;
        private System.Windows.Forms.DataGridView dgv_2;
        private System.Windows.Forms.DataGridView dgv_3;
        private System.Windows.Forms.DataGridView dgv_4;
        private System.Windows.Forms.DataGridView dgv_5;
        private System.Windows.Forms.DataGridView dgv_6;
        public System.Windows.Forms.DataGridView dgv_0;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_comp_save;
        private System.Windows.Forms.DataGridView dgv_ad_companies;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TabControl tab_perdiods_companies;
        private System.Windows.Forms.TabPage tabPage1;
        public System.Windows.Forms.DataGridView dgv_comp_0;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dgv_comp_1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView dgv_comp_2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView dgv_comp_3;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataGridView dgv_comp_4;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.DataGridView dgv_comp_5;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.DataGridView dgv_comp_6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewTextBoxColumn time1;
        private System.Windows.Forms.DataGridViewTextBoxColumn time2;
        private System.Windows.Forms.DataGridViewTextBoxColumn price1;
        private System.Windows.Forms.DataGridViewTextBoxColumn price2;
        private System.Windows.Forms.DataGridViewTextBoxColumn lenght;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewComboBoxColumn ad_c_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn66;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn67;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn68;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn33;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn34;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn35;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn36;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn37;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn38;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn39;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn40;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn41;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn42;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn43;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn44;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn45;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn46;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn47;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn48;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn49;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn50;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn51;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn52;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn53;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn54;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn55;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn56;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn57;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn58;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn59;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn60;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn61;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn62;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn63;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn64;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn65;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.DataGridViewComboBoxColumn nameOfAdDGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn ad_price1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ad_price2;
        private System.Windows.Forms.DataGridViewTextBoxColumn weight;

    }
}