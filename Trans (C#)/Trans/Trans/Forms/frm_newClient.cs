﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Trans
{
    public partial class frm_newClient : Form
    {
        public frm_newClient()
        {
            InitializeComponent();
        }

        private void cb_face_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cb_face.SelectedIndex.Equals(0))
            {
                this.Height = 400;
                this.panel_main.Height = 320;
                this.panel_main.Controls.Clear();

                CreateLable(10, 0, 40, "ФИО: ");
                CreateTextBox(6, 40, 186, "te_name");
                CreateLable(40, 0, 80, "Доп. Инф: ");
                CreateRichTextBox(70, 0, 226, 160, "te_ad");
                //CreateButton(240, 0, 226, 30, "Добавить и добавить договор", btn_SaveAndAdd);
                CreateButton(276, 0, 226, 30, "Добавить и закрыть", btn_SaveAndExit);
            }

            if (cb_face.SelectedIndex.Equals(1))
            {
                this.Height = 740;
                this.panel_main.Height = 640;
                this.panel_main.Controls.Clear();

                CreateLable(10, 0, 130, "Название компании: ");
                CreateTextBox(36, 0, 226, "te_name");

                CreateLable(70, 0, 40, "ИНН: ");
                CreateTextBox(66, 40, 186, "te_inn");

                CreateLable(100, 0, 40, "КПП: ");
                CreateTextBox(96, 40, 186, "te_kpp");

                CreateLable(130, 0, 180, "Адрес юридический: ");
                CreateTextBox(156, 0, 226, "te_adr1");
                CreateButton(186, 156, 70, 26, "Совпад", btn_copyAdr);
                CreateLable(190, 0, 180, "Адрес фактический: ");
                CreateTextBox(216, 0, 226, "te_adr2");

                CreateLable(250, 0, 40, "Р/С: ");
                CreateTextBox(246, 40, 186, "te_p_c");

                CreateLable(280, 0, 40, "Банк: ");
                CreateTextBox(276, 40, 186, "te_bank");

                CreateLable(310, 0, 40, "БИК: ");
                CreateTextBox(306, 40, 186, "te_bik");

                CreateLable(340, 0, 40, "К/С: ");
                CreateTextBox(336, 40, 186, "te_pc");

                CreateLable(370, 0, 80, "Доп. Инф: ");
                CreateRichTextBox(400, 0, 226, 160, "te_ad");
                //CreateButton(570, 0, 226, 30, "Добавить и добавить договор", btn_SaveAndAdd);
                CreateButton(610, 0, 226, 30, "Добавить и закрыть", btn_SaveAndExit);
            }
        }

        private void btn_copyAdr(object sender, EventArgs e)
        {
            ((TextBox)panel_main.Controls["te_adr2"]).Text = ((TextBox)panel_main.Controls["te_adr1"]).Text;
        }

        private void btn_SaveAndExit(object sender, EventArgs e)
        {
            string name = null, inn = null, kpp = null, adr1 = null, adr2 = null, p_c = null, bank = null, bik = null, pc = null;

            name = ((TextBox)panel_main.Controls["te_name"]).Text;

            if (panel_main.Controls.Find("te_inn", true) != null && panel_main.Controls.Find("te_inn", true).Length > 0)
            {
                inn = ((TextBox)panel_main.Controls.Find("te_inn", true).First()).Text;
                kpp = ((TextBox)panel_main.Controls.Find("te_kpp", true).First()).Text;
                adr1 = ((TextBox)panel_main.Controls.Find("te_adr1", true).First()).Text;
                adr2 = ((TextBox)panel_main.Controls.Find("te_adr2", true).First()).Text;
                p_c = ((TextBox)panel_main.Controls.Find("te_p_c", true).First()).Text;
                bank = ((TextBox)panel_main.Controls.Find("te_bank", true).First()).Text;
                bik = ((TextBox)panel_main.Controls.Find("te_bik", true).First()).Text;
                pc = ((TextBox)panel_main.Controls.Find("te_pc", true).First()).Text;
            }

            RichTextBox rtd = (RichTextBox)panel_main.Controls["te_ad"];

            string mes = Sql.AddClient(cb_face.SelectedItem.ToString(), name, inn, kpp, adr1, adr2, p_c, bank, bik, pc, rtd.Text);
            frm_Main.Message(mes);

            this.Dispose();
        }

        private void btn_SaveAndAdd(object sender, EventArgs e)
        {

        }

        private Button CreateButton(int t, int l, int w, int h, string text, EventHandler act)
        {
            Button btn = new Button();
            this.panel_main.Controls.Add(btn);
            btn.Top = t;
            btn.Left = l;
            btn.Width = w;
            btn.Height = h;
            btn.Text = text;
            btn.Click += act;

            return btn;
        }

        private TextBox CreateTextBox(int t, int l, int w, string n)
        {
            TextBox txt = new TextBox();
            this.panel_main.Controls.Add(txt);
            txt.Top = t;
            txt.Left = l;
            txt.Width = w;
            txt.Name = n;

            return txt;
        }

        private RichTextBox CreateRichTextBox(int t, int l, int w, int h, string n)
        {
            RichTextBox rtb = new RichTextBox();
            this.panel_main.Controls.Add(rtb);
            rtb.Top = t;
            rtb.Left = l;
            rtb.Width = w;
            rtb.Height = h;
            rtb.Name = n;

            return rtb;
        }

        private Label CreateLable(int t, int l, int w, string text)
        {
            Label lbl = new Label();
            this.panel_main.Controls.Add(lbl);
            lbl.Top = t;
            lbl.Left = l;
            lbl.Text = text;
            lbl.Width = w;

            return lbl;
        }
    }
}
