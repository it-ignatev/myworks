﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.IO;
using System.Threading;
using System.Diagnostics;
using System.Reflection;

namespace PimLauncher
{
    /// <summary>
    /// Interaction logic for Message.xaml
    /// </summary>
    public partial class Message : Window
    {
        public Message(Window w, string title, string text, bool ok=true, bool no=false, bool cancel=false, bool p=false)
        {
            if(w!=null)
                this.Owner = w;
            InitializeComponent();

            this.Title = title;
            TextBl.Text = text;
            btn_ok.IsEnabled = ok;
            btn_no.IsEnabled = no;
            btn_cancel.IsEnabled = cancel;
            this.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner;

            CPath.Text = PIM.GetMPath();
            CPath.Visibility = p ? Visibility.Visible : Visibility.Hidden;
            but_path.Visibility = p ? Visibility.Visible : Visibility.Hidden;
        }

        private void btn_ok_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void btn_no_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btn_cancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void but_path_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog dlg = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult dlgr = dlg.ShowDialog();

            if (dlgr == System.Windows.Forms.DialogResult.OK)
            {
                if (dlg.SelectedPath != null && dlg.SelectedPath != string.Empty)
                    if (Directory.Exists(dlg.SelectedPath))
                    {
                        CPath.Text = dlg.SelectedPath + @"\";
                        PIM.WriteRegistry("MainPath", dlg.SelectedPath + @"\", RegistryValueKind.String);
                    }
                    else
                        MessageBox.Show("Папка не существует!");
            }
        }
    }
}
