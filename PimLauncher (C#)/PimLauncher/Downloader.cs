﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Net;
using System.ComponentModel;
using System.Windows.Controls;

namespace PimLauncher
{
    class Downloader
    {
        private Dictionary<string, string> _downloadUrls = new Dictionary<string, string>();
        private bool ssl;
        private ProgressBar pb;

        public void Download(string url, string path, ProgressBar pb, bool ssl = false)
        {
            this.ssl = ssl;
            this.pb = pb;
            _downloadUrls.Add(url, path);
            DownloadFile();
        }

        public void Download(Dictionary<string, string> f, ProgressBar pb, bool ssl = false)
        {
            this.ssl = ssl;
            this.pb = pb;
            _downloadUrls.Union(f).ToDictionary(k => k.Key, v => v.Value);
            DownloadFile();
        }

        private void DownloadFile()
        {
            if(_downloadUrls.Any())
            {
                var url = _downloadUrls.FirstOrDefault();
                _downloadUrls.Remove(url.Key);

                LOGGER.LOG("DOWNLOADER, BEGIN: " + url.Key);

                WebClient wc = new WebClient();

                if (ssl)
                {
                    wc.Proxy = null;
                    wc.Credentials = CredentialCache.DefaultCredentials;

                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                    ServicePointManager.ServerCertificateValidationCallback =
                        (sender, certificate, chain, sslPolicyErrors) =>
                        {
                            return true;
                        };
                }

                wc.DownloadProgressChanged += client_DownloadProgressChanged;
                wc.DownloadFileCompleted += client_DownloadFileCompleted;          
                wc.DownloadFileAsync(new Uri(url.Key), url.Value);
                return;
            }

            // End of the download
            LOGGER.LOG("DOWNLOADER, COMPLETE");           
        }

        private void client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                // handle error scenario
                throw e.Error;
            }
            if (e.Cancelled)
            {
                // handle cancelled scenario
            }

            LOGGER.LOG("DOWNLOADER, END");

            DownloadFile();
        }

        void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            double bytesIn = double.Parse(e.BytesReceived.ToString());
            double totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
            double percentage = bytesIn / totalBytes * 100;
            pb.Value = int.Parse(Math.Truncate(percentage).ToString());
        }
    }
}
