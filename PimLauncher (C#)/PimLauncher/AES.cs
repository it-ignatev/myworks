﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Security.Cryptography;

namespace PimLauncher
{
    class AES
    {
        public static string EncryptText(string plainText, byte[] keyArray)
        {       
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(plainText);
            RijndaelManaged rijndaelManaged = new RijndaelManaged();
            rijndaelManaged.Key = keyArray;
            rijndaelManaged.Mode = CipherMode.ECB;
            rijndaelManaged.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = rijndaelManaged.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            return Convert.ToBase64String(resultArray);
        }

        public static string DecryptText(string encryptedText, byte[] keyArray)
        {          
            byte[] toEncryptArray = Convert.FromBase64String(encryptedText);
            RijndaelManaged rijndaelManaged = new RijndaelManaged();
            rijndaelManaged.Key = keyArray;
            rijndaelManaged.Mode = CipherMode.ECB;
            rijndaelManaged.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = rijndaelManaged.CreateDecryptor();

            try
            {
                byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
                return Encoding.UTF8.GetString(resultArray);
            }
            catch(CryptographicException e) { 
                LOGGER.LOG("CRYPTER" + e.ToString());
            }

            return null;
        }
    }
}
