﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Resources;
using System.Reflection;
using System.Collections;
using Microsoft.Win32;
using System.Threading;
using System.ComponentModel;
using System.Net.NetworkInformation;
using System.Windows;
using System.Globalization;
using System.IO.Compression;
using System.IO.Packaging;
using System.Web;
using System.Net.Security;
using System.Windows.Media;

namespace PimLauncher
{
    class PIM : Settings
    {
        public static string GetMPath()
        {
            string p = (string)ReadRegistry("MainPath");
            if (p != null) return p;
            return AppDomain.CurrentDomain.BaseDirectory;
        }
        public static string GetLauncherLastVersion()
        {
            return UrlRequest.Request("http://pimcraft.com/launcher/getv.php", false);
        }
        public static List<string> CheckLibs()
        {
            string PathTL = PIM.GetMPath() + @"libs\";

            try
            {
                List<string> temp = new List<string>();

                foreach (KeyValuePair<string, int> k in Settings.LauncherLibs)
                {
                    if (string.IsNullOrEmpty((string)ReadRegistry(k.Key))||(int)ReadRegistry(k.Key) != k.Value)
                    {
                        temp.Add(k.Key);
                        continue;
                    }

                    if (!File.Exists(Path.Combine(PathTL, k.Key))) temp.Add(k.Key);
                }

                return temp;

            }catch(Exception e)
            {
                LOGGER.LOG("FAILED TO CHECK LIBS ! ERROR: " + e.Message);
                MessageBox.Show("Ошибка #5, подробнее в лог файле. Приложение будет закрыто.");
                Application.Current.Shutdown();
            }

            return null;
        }

        public static void WriteRegistry(string val, object value, RegistryValueKind rvk)
        {
            try
            {
                RegistryKey hklm = null;

                if (Registry.CurrentUser.OpenSubKey(Settings.RegistryPath) == null) Registry.CurrentUser.CreateSubKey(Settings.RegistryPath);
                hklm = Registry.CurrentUser.OpenSubKey(Settings.RegistryPath, true);
                hklm.SetValue(val, value, rvk);
                hklm.Close();
            }catch(Exception e)
            {
                LOGGER.LOG("FAILED TO WRITEREGISTRY ! ERROR: " + e.Message);
                MessageBox.Show("Ошибка #1, подробнее в лог файле. Приложение будет закрыто.");
                Application.Current.Shutdown();
            }
        }

        public static object ReadRegistry(string val)
        {
            try
            {
                
                RegistryKey hklm = null;
                if (Registry.CurrentUser.OpenSubKey(Settings.RegistryPath) == null)
                    Registry.CurrentUser.CreateSubKey(Settings.RegistryPath);
                hklm = Registry.CurrentUser.OpenSubKey(Settings.RegistryPath, true);
                object obj = hklm.GetValue(val);
                hklm.Close();
                return obj;
            }catch(Exception e)
            {
                LOGGER.LOG("FAILED TO READREGISTRY ! ERROR: " + e.Message);
                MessageBox.Show("Ошибка #2, подробнее в лог файле. Приложение будет закрыто.");
                Application.Current.Shutdown();
                return null;
            }
        }

        public static bool IsInstalled()
        {
            bool isI = false;
            List<string> libs = CheckLibs();
            
            isI = (ReadRegistry("Installed") != null ? Convert.ToBoolean(ReadRegistry("Installed")) : false);

            if (libs.Count > 0)
            {
                LOGGER.LOG("INSTALLING LIBRARIES...");
                //Доделать загрузку библиотек и их установку
            }

            LOGGER.LOG("Is launcher istalled ?" + isI.ToString());
            return isI;
        }
        public static void Installation(string path)
        {
            LOGGER.LOG("Installing...");
            //Registry
            WriteRegistry("Installed", true, RegistryValueKind.String);
            WriteRegistry("MainPath", path, RegistryValueKind.String);
            WriteRegistry("AutoUPD", 0, RegistryValueKind.DWord);
            WriteRegistry("AutoL", 0, RegistryValueKind.DWord);
            WriteRegistry("Memory", 1300, RegistryValueKind.DWord);

            //Files
            try
            {
                if (!Directory.Exists(path)) Directory.CreateDirectory(path);
                File.Move(Assembly.GetExecutingAssembly().Location, Path.Combine(path, Path.GetFileName(Assembly.GetExecutingAssembly().Location)));
            }catch(Exception e)
            {
                LOGGER.LOG("EXCEPTION ON INSTALLING FILES. CODE: " + e.Message);
            }
            LOGGER.LOG("Installed!");
        }

        public static bool Auth(string login, string password, Window w=null)
        {
            if(string.IsNullOrEmpty(login) || string.IsNullOrEmpty(password))
            {
                new Message(w, "Ошибка", "Одно из полей не заполнено!").ShowDialog();
                return false;
            }

            LOGGER.LOG("Auth Login:  " + HttpUtility.UrlEncode(AES.EncryptText(login, Settings.KeyV1)));
            LOGGER.LOG("Auth Password:  " + HttpUtility.UrlEncode(AES.EncryptText(password, Settings.KeyV1)));

            string ans = UrlRequest.Request(Settings.urlAuth, UrlRequest.GenUrlParams(new Dictionary<string, string> { 
               { "login", HttpUtility.UrlEncode(AES.EncryptText(login, Settings.KeyV1))}, 
               { "password", HttpUtility.UrlEncode(AES.EncryptText(password, Settings.KeyV1))}, 
               { "mac", HttpUtility.UrlEncode(AES.EncryptText(GetMACAddress(), Settings.KeyV1))}
            }), false);

            if (ans == null) 
            {
                LOGGER.LOG("AUTH, AUTHENTICATION WAS FAILED ! EMPTY ANSWER");
                new Message(w, "Ошибка", "Ответ от сервера не получен!").ShowDialog();
                return false; 
            }

            if (ans.Split(":".ToCharArray()).Length > 0)
                if (ans.Split(":".ToCharArray())[0] == "OK")
                {
                    WriteRegistry("login", login, RegistryValueKind.String);

                    if (Convert.ToBoolean(ReadRegistry("AutoL")))
                    {
                        WriteRegistry("password", AES.EncryptText(password, Settings.KeyV1), RegistryValueKind.String);
                    }

                    Settings.CurrentSession = AES.DecryptText(HttpUtility.UrlDecode(ans.Split(":".ToCharArray())[1]), Settings.KeyV2);
                    Settings.UUID = AES.DecryptText(HttpUtility.UrlDecode(ans.Split(":".ToCharArray())[2]), Settings.KeyV2);

                    LOGGER.LOG("Auth, Session:  " + Settings.CurrentSession);
                    LOGGER.LOG("Auth, UUID:  " + Settings.UUID);

                    return true;
                }

            LOGGER.LOG("AUTH, ERRROR: " + ans);

            return false;
        }
        public static string GetMACAddress()
        {
            NetworkInterface[] macAdress = NetworkInterface.GetAllNetworkInterfaces();
            string macAd = null;

            foreach (NetworkInterface mac in macAdress)
            {
                macAd = mac.GetPhysicalAddress().ToString();
                LOGGER.LOG("SEARCH MAC ADDR: " + macAd);
                if (macAd != null && macAd != string.Empty) break;
            }

            return macAd;
        }

        public static void SetVisibility(UIElement parent)
        {
            var childNumber = VisualTreeHelper.GetChildrenCount(parent);

            for (var i = 0; i <= childNumber - 1; i++)
            {
                var uiElement = VisualTreeHelper.GetChild(parent, i) as UIElement;
                uiElement.Visibility = Visibility.Collapsed;               

                if (uiElement != null && VisualTreeHelper.GetChildrenCount(uiElement) > 0)
                {
                    SetVisibility(uiElement);
                }
            }
        }
    }
}
