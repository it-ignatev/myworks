﻿using System.Collections.Generic;

namespace PimLauncher
{
    class Settings
    {
        public static string Version = "1.0";
        public static string Date = "02.01.2016";

        public static string RegistryPath = @"SOFTWARE\PimLauncher\";
        public static Dictionary<string, int> LauncherLibs = new Dictionary<string, int> {
            {"Newtonsoft.Json.dll", 1},
            {"SharpCompress.dll", 1}
        };

        public static byte[] KeyV1 = new byte[] { 68, 48, 53, 48, 57, 57, 51, 55, 68, 48, 69, 49, 68, 48, 53, 48 };
        public static byte[] KeyV2 = new byte[] { 113, 104, 68, 52, 68, 69, 100, 53, 111, 86, 90, 100, 88, 72, 50, 85 };

        public static string CurrentSession = null;
        public static string UUID = null;

        //URLS
        protected static string urlAuth = "http://pimcraft.com/client/launcher/Auth.php";
        protected static string urlLauncherVersion = "http://pimcraft.com/client/launcher/GetVersion.php";
        public static string urlUpdateLauncher = "http://pimcraft.com/client/launcher/files/";
        public static string urlNews = "http://pimcraft.com/client/launcher/GetNews.php";
        public static string urlInf = "http://pimcraft.com/client/launcher/GetInformation.php";
        public static string urlClientVer = "http://pimcraft.com/client/launcher/GetClientVersion.php";
        public static string urlClients = "http://pimcraft.com/client/";
    }
}
