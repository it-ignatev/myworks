﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.IO;
using Microsoft.Win32;
using System.Globalization;
using System.Windows;

namespace PimLauncher
{
    class LOGGER
    {
        static string path = null;

        [DllImport("kernel32.dll")]
        static extern bool AllocConsole();
        [DllImport("kernel32.dll")]
        static extern bool AttachConsole(int pw);
        private static bool _hasConsole, _keepConsoleOpen;
        static private void EnsureConsole()
        {
            _hasConsole = _hasConsole || AttachConsole(-1);
            _keepConsoleOpen = !_hasConsole || _keepConsoleOpen;
            _hasConsole = _hasConsole || AllocConsole();
        }

        public LOGGER(bool console, string p)
        {
            if (console) EnsureConsole();
            path = p;

            LOG("INITIALIZING...");
            LOG("PATH: " + path);
            LOG("VERSION: " + Settings.Version + "/" + Settings.Date);
            LOG("##################");
            LOG("TRYNG TO GET VERSIONS OF .NET...");

            foreach (string v in GetNetframeworkVersions())
            {
                LOG(v);
            }

            LOG("LOGGER LAUNCHED !");
        }
        public static void LOG(string text)
        {
            Console.WriteLine("[{0}] {1}",  DateTime.Now.ToString("HH:mm:ss"), text);
            File.AppendAllText(path, string.Format("[{0}] {1}",  DateTime.Now.ToString("HH:mm:ss"), text) + Environment.NewLine);
        }
        private string[] GetNetframeworkVersions()
        {
            try
            {
                RegistryKey installed_versions = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\NET Framework Setup\NDP");
                string[] version_names = installed_versions.GetSubKeyNames();
                //version names start with 'v', eg, 'v3.5' which needs to be trimmed off before conversion
                return version_names;
            }catch(Exception e)
            {
                LOGGER.LOG("FAILED TO GET .NET VERSIONS ! ERROR: " + e.Message);
                MessageBox.Show("Ошибка #3, подробнее в лог файле. Приложение будет закрыто.");
                Application.Current.Shutdown();
                return null;
            }
            //double Framework = Convert.ToDouble(version_names[version_names.Length - 1].Remove(0, 1), CultureInfo.InvariantCulture);
            //int SP = Convert.ToInt32(installed_versions.OpenSubKey(version_names[version_names.Length - 1]).GetValue("SP", 0));
        }
    }
}
