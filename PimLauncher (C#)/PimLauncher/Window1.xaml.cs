﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections;
using System.Diagnostics;
using System.Reflection;

namespace PimLauncher
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public Window1()
        {
            InitializeComponent();
            new LOGGER(true, PIM.GetMPath() + "log.log");
            if (!PIM.IsInstalled())
            {
                new Message(this, "Установка", "Мы обнаружили, что ранее лаунчер не был установлен. Укажите путь для установки:", true, false, false, true).ShowDialog();

                PIM.Installation(PIM.GetMPath());
            }

            loginBox.Text = (string)PIM.ReadRegistry("login");
            passwordBox.Password = (string)PIM.ReadRegistry("password");

            //Downloader dl = new Downloader();
            //dl.Download("http://pimcraft.com/client/libraries.zip", "libs.zip", Progressbar);
        }

        private void Label_MouseLeave(object sender, MouseEventArgs e)
        {
            ((Control)sender).Background = Brushes.Transparent;
        }

        private void Label_MouseEnter(object sender, MouseEventArgs e)
        {
            ((Control)sender).Background = new SolidColorBrush(Color.FromArgb(10, 0, 0, 0));
        }

        private void Label_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            LOGGER.LOG("###############\rCLOSING APPLICATION");
            Application.Current.Shutdown();
        }

        private void Label_MouseLeftButtonUp_1(object sender, MouseButtonEventArgs e)
        {
            this.WindowState = System.Windows.WindowState.Minimized;
        }

        private void Label_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            LOGGER.LOG("TRYING TO AUTH USER...");
            bool ans = PIM.Auth(loginBox.Text, passwordBox.Password, this);
            LOGGER.LOG("AUTHING WAS ENDED");

            if (ans)
            {
                new Message(this, "Аунтификация", "Авторизация успешно выполнена!").ShowDialog();
                PIM.SetVisibility(MainGrid);
            }
            else
                new Message(this, "Аунтификация", "Логин или пароль введены неверно!").ShowDialog();
        }

        private void REG_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("http://pimcraft.com/?page=begin");
        }

        private void FORGOT_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("http://pimcraft.com/?page=recover");
        }
    }
}
