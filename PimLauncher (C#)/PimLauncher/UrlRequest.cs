﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;

namespace PimLauncher
{
    class UrlRequest
    {
        public static string Request(string url, bool ssl)
        {
            try
            {
                LOGGER.LOG("REQUESTER, Url: " + url);
                var wr = (HttpWebRequest)WebRequest.Create(url);
                LOGGER.LOG("REQUESTER, MAKING REQUEST...");

                if (ssl)
                {
                    wr.Proxy = null;
                    wr.Credentials = CredentialCache.DefaultCredentials;

                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                    ServicePointManager.ServerCertificateValidationCallback =
                        (sender, certificate, chain, sslPolicyErrors) =>
                        {
                            return true;
                        };
                }

                HttpWebResponse response = (HttpWebResponse)wr.GetResponse();
                StreamReader sr = new StreamReader(response.GetResponseStream());

                string line = sr.ReadToEnd();
                if (line != null) LOGGER.LOG("REQUESTER, Response:  " + line.Trim());
                if (line == null) return "";

                return line.Trim();

            }
            catch (Exception e)
            {
                LOGGER.LOG("REQUESTER" + e.ToString());
                new Message(null, "Ошибка", "Ошибка #4. Произошла ошибка при обращении к серверу.");
                return null;
            }
        }

        public static string Request(string url, byte[] data, bool ssl)
        {
            try
            {
                LOGGER.LOG("REQUESTER, Url: " + url);
                var wr = (HttpWebRequest)WebRequest.Create(url);
                LOGGER.LOG("REQUESTER, MAKING REQUEST...");

                if (ssl)
                {
                    wr.Proxy = null;
                    wr.Credentials = CredentialCache.DefaultCredentials;

                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                    ServicePointManager.ServerCertificateValidationCallback =
                        (sender, certificate, chain, sslPolicyErrors) =>
                        {
                            return true;
                        };
                }

                wr.Method = "POST";
                wr.ContentType = "application/x-www-form-urlencoded";
                wr.ContentLength = data.Length;

                Stream str = wr.GetRequestStream();
                str.Write(data, 0, data.Length);
                str.Close();

                HttpWebResponse response = (HttpWebResponse)wr.GetResponse();
                StreamReader sr = new StreamReader(response.GetResponseStream());

                string line = sr.ReadToEnd();
                if (line != null) LOGGER.LOG("REQUESTER, Response:  " + line.Trim());
                if (line == null) return "";

                return line.Trim();

            }
            catch (Exception e)
            {
                LOGGER.LOG("REQUESTER" + e.ToString());
                new Message(null, "Ошибка", "Ошибка #4. Произошла ошибка при обращении к серверу.");
                return null;
            }
        }

        public static byte[] GenUrlParams(Dictionary<string, string> dic = null)
        {
            if (dic == null) return null;
            string res = "";

            foreach (string s in dic.Keys)
            {
                res += s + "=" + dic[s] + "&";
            }

            res = res.Substring(0, res.Length - 1);
            LOGGER.LOG("GENERATED PARAMS: " + res);

            return Encoding.UTF8.GetBytes(res);
        }
    }
}
