﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LeaderLogistics.UserControls;
using LeaderLogistics.Data;

namespace LeaderLogistics
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private double _Height { get; set; }
        public MainWindow()
        {
            InitializeComponent();

            this.Closing += MainWindow_Closing;

            _Height = this.Height;
        }

        void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            new MySQL().MySQLClose();
            MessageBox.Show("Соединение разорвано!");
        }
        private void DataBase_Click(object sender, RoutedEventArgs e)
        {
            DeleteContent();

            UserControl uc = new DataBase();
            ContentMain.Content = uc;
            this.Height += uc.Height;
        }
        private void DeleteContent()
        {
            this.Height = _Height;
            ContentMain.Content = null;
        }
        private void Info_Click(object sender, RoutedEventArgs e)
        {
            DeleteContent();

            UserControl uc = new Info();
            ContentMain.Content = uc;
            this.Height += uc.Height;
        }
    }
}
