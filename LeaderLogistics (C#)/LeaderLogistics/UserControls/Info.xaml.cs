﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LeaderLogistics.Data;

namespace LeaderLogistics.UserControls
{
    /// <summary>
    /// Interaction logic for Info.xaml
    /// </summary>
    public partial class Info : UserControl
    {
        public Info()
        {
            InitializeComponent();
            Loaded += Info_Loaded;
        }
        private void Info_Loaded(object sender, RoutedEventArgs e)
        {
            lbl_version.Content = string.Format("Версия программы: {0}", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version);
        }
    }
}
