﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using LeaderLogistics.Data;

namespace LeaderLogistics.UserControls
{
    /// <summary>
    /// Interaction logic for DataBase.xaml
    /// </summary>
    public partial class DataBase : UserControl
    {
        public DataBase()
        {
            InitializeComponent();

            but_conn.Click += MakeConnection;
            but_state.Click += CheckState;
            but_close.Click += CloseConnection;
        }
        private void MakeConnection(object s, RoutedEventArgs e)
        {
            MySQLC mdata = new MySQLC();
            mdata.Base = tb_base.Text;
            mdata.Port = tb_port.Text;
            mdata.Server = tb_host.Text;
            mdata.User = tb_user.Text;
            mdata.Pass = tb_pass.Password;

            MySQL mysql = new MySQL();
            mysql.MySQLOpen(mdata);

            MessageBox.Show(mysql.MySQLState().ToString());

            if (mysql.MySQLState() == System.Data.ConnectionState.Open)
                but_close.IsEnabled = true;
        }
        private void CheckState(object s, RoutedEventArgs e)
        {
            MessageBox.Show(new MySQL().MySQLState().ToString());
        }
        private void CloseConnection(object s, RoutedEventArgs e)
        {
            MySQL mysql = new MySQL();
            mysql.MySQLClose();

            MessageBox.Show(mysql.MySQLState().ToString());
            ((Button)s).IsEnabled = false;
        }
    }
}
