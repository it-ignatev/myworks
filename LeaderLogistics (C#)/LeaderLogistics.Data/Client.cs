﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LeaderLogistics.Data.PersonPreferences;

namespace LeaderLogistics.Data
{
    public class Client
    {
        public string name { get; set; } // Имя клиента
        public long inn { get; set; } // ИНН клиента
        public long kpp { get; set; } // Код причины постановки
        public ForeignDetails foreign { get; set; } // Иностранные реквезиты
        public Adress adress { get; set; } // Адреса клиента
        public Bank bank { get; set; } // Банк клиента
        public string info { get; set; } // Дополнительная инофрмация о клиенте
        public DateTime addTime { get; set; } // Время добавления клиента
        public DateTime lastEditTime { get; set; } // Последнее время изменения клиента
    }
}
