﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaderLogistics.Data.OrderPreferences
{
    public class TimeTable
    {
        public Dictionary<int, TimeTableContent> table { get; set; }
    }
}
