﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaderLogistics.Data.OrderPreferences
{
    public class AdditionalOptions
    {
        public List<string> name { get; set; }
        public List<decimal> priceCargo { get; set; }
        public List<decimal> priceWOCargo { get; set; }
        public List<decimal> weight { get; set; }
    }
}
