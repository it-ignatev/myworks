﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaderLogistics.Data.OrderPreferences
{
    public class TimeTableContent
    {
        public List<DateTime> time1 { get; set; }
        public List<DateTime> time2 { get; set; }
        public List<decimal> priceCargo { get; set; }
        public List<decimal> priceWOCargo { get; set; }
        public List<decimal> length { get; set; }
    }
}
