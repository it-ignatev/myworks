﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LeaderLogistics.Data.OrderPreferences;

namespace LeaderLogistics.Data
{
    public class ClientOrder
    {
        public long number { get; set; }
        public string name { get; set; }
        public Client person { get; set; }
        public string route { get; set; }
        public TimeTable table { get; set; }
        public AdditionalOptions aoptions { get; set; }
        public DateTime addTime { get; set; }
        public DateTime lastEditTime { get; set; }
    }
}
