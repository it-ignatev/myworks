﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaderLogistics.Data
{
    public class Company
    {
        public string name { get; set; }
        public Adress adress { get; set; }
        public ForeignDetails bank { get; set; }
        public DateTime addTime { get; set; }
        public DateTime lastEditTime { get; set; }
    }
}
