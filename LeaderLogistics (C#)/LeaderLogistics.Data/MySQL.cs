﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data.MySqlClient;

namespace LeaderLogistics.Data
{
    public class MySQL
    {
        public static MySqlConnection _mysql = null;
        public void MySQLOpen(MySQLC data)
        {
            string conn = string.Format("server={0};uid={1};pwd={2};database={3};", data.Server, data.User, data.Pass, data.Base);

            try
            {
                _mysql = new MySqlConnection(conn);
                _mysql.Open();
            }
            catch(MySqlException e)
            {
                throw e;
            }

        }
        public void MySQLClose()
        {
            if(_mysql != null)
                _mysql.Close();
        }
        public ConnectionState MySQLState()
        {
            if (_mysql == null) return ConnectionState.Closed;
            return _mysql.State;
        }
    }
}
