﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaderLogistics.Data
{
    public class Adress
    {
        public string juridic { get; set; } // Юридический
        public string actual { get; set; } // Фактический
    }
}
