﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaderLogistics.Data
{
    public class ForeignDetails
    {
        public string bank { get; set; } // Имя иностранного банка
        public long swift { get; set; } // swift
        public long iban { get; set; } // IBAN
    }
}
