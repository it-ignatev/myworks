﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaderLogistics.Data.PersonPreferences
{
    public class Bank
    {
        public string name { get; set; } // Имя банка
        public long CalCheck { get; set; } // Расчетный счет
        public long bik { get; set; } // БИК
        public long CorCheck { get; set; } // Корреспондентский счет
    }
}
