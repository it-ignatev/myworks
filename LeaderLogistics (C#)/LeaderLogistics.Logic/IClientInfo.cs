﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LeaderLogistics.Logic.IPerson;

namespace LeaderLogistics.Logic
{
    public interface IClientInfo
    {
        string getName();
        long getINN();
        long getKPP();
        IForeign foreign();
        IAdress adress();
        IBank bank();
        string getInfo();
        DateTime getAddTime();
        DateTime getLastEditTime();
    }
}
