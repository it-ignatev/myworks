﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaderLogistics.Logic.IPerson
{
    public interface IAdress
    {
        string getActual();
        string getJuridic(); 
    }
}
